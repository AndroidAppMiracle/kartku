package kartku.com.product_detail;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.rate_review.RateAndReview;
import kartku.com.adapter.SlidingImage;
import kartku.com.cart.listener.AddToCartListener;
import kartku.com.cart.manager.AddToCartManager;
import kartku.com.product_detail.listener.ProductDetailListener;
import kartku.com.product_detail.manager.ProductDetailManager;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.listener.AddToWishlistListener;
import kartku.com.wishlist.listener.RemoveFromWishlistListener;
import kartku.com.wishlist.manager.AddToWishlistManager;
import kartku.com.wishlist.manager.RemoveFromWishlistManager;


public class ProductDetail extends AppCompatActivity implements View.OnClickListener, ProductDetailListener, AddToCartListener, AddToWishlistListener, RemoveFromWishlistListener {
    TextView text_material, text_color, text_pattern, text_dimesions, text_qty, text_description, text_share, text_rateReview;
    private ViewPager mPager;
    EditText qty_text;
    AQuery aQuery;
    int count = 1;
    TextView add_qty, sub_qty;
    Button add_cart, btn_AddToWishlist;
    SharedPreferences pref;
    //private int countBadge = 1;

    //   private static final Integer[] IMAGES= {R.mipmap.banner,R.mipmap.banner,R.mipmap.banner,R.mipmap.banner};
    //public static final int DELAY = 100;
    CirclePageIndicator indicator;
    ArrayList<String> arrayList = new ArrayList<>();
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private String product_id = "";
    String user_id;
    //private MenuItem itemCart;
    //private LayerDrawable icon;
    //AlertDialog alertDialog;
    TextView mCounter;
    boolean isInWishlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        setupWindowAnimations();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        mPager = (ViewPager) findViewById(R.id.pager);
        aQuery = new AQuery(this);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        text_material = (TextView) findViewById(R.id.text_material);
        text_color = (TextView) findViewById(R.id.text_color);
        text_pattern = (TextView) findViewById(R.id.text_pattern);
        add_cart = (Button) findViewById(R.id.btn_shipping);
        btn_AddToWishlist = (Button) findViewById(R.id.btn_AddToWishlist);
        text_dimesions = (TextView) findViewById(R.id.text_dimesions);
        text_share = (TextView) findViewById(R.id.text_share);
        text_rateReview = (TextView) findViewById(R.id.text_rateReview);
        text_description = (TextView) findViewById(R.id.text_description);
        add_qty = (TextView) findViewById(R.id.add_qty);
        sub_qty = (TextView) findViewById(R.id.sub_qty);
        qty_text = (EditText) findViewById(R.id.qty_text);
        qty_text.setText("" + 1);
        setSupportActionBar(toolbar);
        setListener();
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            Intent intent = getIntent();
            product_id = intent.getStringExtra(Constant.SELECTED_CATEGORY_INDEX);
            user_id = Session.get_userId(pref);
            isInWishlist = intent.getBooleanExtra(Constant.WISHLIST, false);
            //productName = intent.getStringExtra(Constant.PRODUCT_NAME);
            //productPrice = intent.getStringExtra(Constant.PRODUCT_PRICE);




            if (isInWishlist) {
                btn_AddToWishlist.setText(R.string.remove_from_wishlist);
            } else {
                btn_AddToWishlist.setText(R.string.add_to_wishlist_caps);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /***
         * get product categories on basis of category id
         */
        try {
            Log.d("", "product_id ******   " + product_id);
            new ProductDetailManager(ProductDetail.this, this).sendRequest("/".concat(product_id));
        } catch (Exception e) {
            e.printStackTrace();
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    public void setListener() {

        text_share.setOnClickListener(this);
        text_rateReview.setOnClickListener(this);

        add_qty.setOnClickListener(this);
        sub_qty.setOnClickListener(this);
        add_cart.setOnClickListener(this);
        btn_AddToWishlist.setOnClickListener(this);
    }

    @Override
    public void onSuccess(String response) {
        try {
            JSONObject product_info = new JSONObject(response);

            text_material.setText(product_info.get("material").toString());
            text_color.setText(product_info.get("color").toString());
            text_description.setText(product_info.get("description").toString());

            Log.i("Images ", response);
            for (int i = 0; i < 4; i++) {
                arrayList.add(product_info.get("image").toString());
            }
            Log.i("arraylistSize", "" + arrayList.size());
            init(arrayList);

//            JSONArray jsonArray=product_info.getJSONArray("related_products");
//            for(int i=0;i<jsonArray.length();i++){
//
//                JSONObject jsonObject=jsonArray.getJSONObject(i);
//                Product_DetailModel product_detailModel=new Product_DetailModel();
//                product_detailModel.setId(jsonObject.get("related_product_id").toString());
//                product_detailModel.setImage(jsonObject.get("related_product_title").toString());
//                product_detailModel.setTittle(jsonObject.get("related_products_image_url").toString());
//                arrayList.add(product_detailModel);
//
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String error) {
        ToastMsg.showShortToast(ProductDetail.this, error);
    }

    private void init(ArrayList<String> arrayList) {
//        for(int i=0;i<IMAGES.length;i++)
//         ImagesArray.add(IMAGES[i]);

        Log.e("Images link", "Images link" + arrayList.size() + "image" + arrayList.get(0));
        mPager.setAdapter(new SlidingImage(this, arrayList));


        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = arrayList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };


        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    private void shareIt() {
        /*Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>Download Lotus Decor APP to purchase products..</p>"));
        startActivity(Intent.createChooser(sharingIntent, "Share using"));*/

        String shareBody = "Download Lotus Decor APP to purchase products..";
       /* Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Lotus Decor");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));*/


        // Create and fire off our Intent in one fell swoop
        ShareCompat.IntentBuilder
                .from(ProductDetail.this) // getActivity() or activity field if within Fragment
                .setText(shareBody)
                .setType("text/plain") // most general text sharing MIME type
                .setChooserTitle(getResources().getString(R.string.share_using))
                .startChooser();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.text_share) {
            shareIt();

        } else if (id == R.id.text_rateReview) {
            /*Intent in = new Intent(ProductDetail.this, ProductReview.class);
            startActivity(in);*/
            //addReviewCustomDialog();
            Intent intent = new Intent(ProductDetail.this, RateAndReview.class);
            intent.putExtra(Constant.USER_ID, user_id);
            intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, product_id);
            startActivity(intent);

        } else if (id == R.id.sub_qty) {
            count++;
            qty_text.setText("" + count);
        } else if (id == R.id.add_qty) {
            if (count == 1) {
                qty_text.setText("" + count);
            } else {
                count--;
                qty_text.setText("" + count);
            }

        } else if (id == R.id.btn_shipping) {
            try {
                addCart();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (id == R.id.btn_AddToWishlist) {
            try {
                if (isInWishlist) {
                    removeWishlist(user_id, product_id);
                } else {
                    addWishlist(user_id, product_id);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void addCart() {

        try {
            if (!qty_text.getText().toString().trim().equals("0")) {
                String qty = "" + qty_text.getText().toString();
                String cart_id = Session.getcart_id(pref);
                new AddToCartManager(getApplicationContext(), this).sendRequest(user_id, "", product_id, qty, cart_id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onAddCartError(String error) {
        ToastMsg.showShortToast(ProductDetail.this, error);
    }

    @Override
    public void onAddCartSuccess(String messgae) {
        Log.d("", "onAddCartSuccess *******  " + messgae);
        //Utility.setBadgeCount(ProductDetail.this, icon, qty_text.getText().toString().trim());
        ToastMsg.showShortToast(ProductDetail.this, messgae);
        //Session.setCartItemsQuantity(pref, (Integer.toString(countBadge) + Session.getCartItemsQuantity(pref).trim()));
        //mCounter.setText("+" + Session.getCartItemsQuantity(pref));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(R.id.badge).getActionView();
        mCounter = (TextView) badgeLayout.findViewById(R.id.tv_counter);
        //mCounter.setText(Session.getCartItemsQuantity(pref));
        return super.onCreateOptionsMenu(menu);
    }

  /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        itemCart = menu.findItem(R.id.action_cart);
        icon = (LayerDrawable) itemCart.getIcon();
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        /*switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                    *//*Intent intent = new Intent(ProductDetail.this, ProductList.class);
                    startActivity(intent);
                    finish();*//*
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }*/
        return (super.onOptionsItemSelected(menuItem));
    }


    private void addWishlist(String user_id, String selected_productId) {
        Log.d("", "addWishlist product_id *****  " + selected_productId);
        System.out.println("add to wishlist product id ****  " + selected_productId);


        new AddToWishlistManager(ProductDetail.this, this).sendRequest(user_id, selected_productId);
    }

    private void removeWishlist(String user_id, String selected_productId) {
        Log.d("", "addWishlist product_id *****  " + selected_productId);
        System.out.println("add to wishlist product id ****  " + selected_productId);

        new RemoveFromWishlistManager(ProductDetail.this, this).sendRequest(user_id, selected_productId);
    }


    @Override
    public void onAddwishlistSuccess(String response) {
        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(ProductDetail.this, response);
        btn_AddToWishlist.setText(R.string.remove_from_wishlist);

    }

    @Override
    public void onAddwishlistError(String error) {
        ToastMsg.showShortToast(ProductDetail.this, error);
    }

    @Override
    public void removewishlistOnSuccess(String response) {
        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(ProductDetail.this, response);
        btn_AddToWishlist.setText(R.string.add_to_wishlist_caps);
    }

    @Override
    public void removewishlistOnError(String error) {
        ToastMsg.showShortToast(ProductDetail.this, error);
    }


    private void setupWindowAnimations() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setDuration(1000);
            getWindow().setEnterTransition(fade);

            Slide slide = new Slide();
            slide.setDuration(1000);
            getWindow().setReturnTransition(slide);
        }

    }

   /* private void addReviewCustomDialog() {
        try {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductDetail.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.content_rate_and_review, null);
            dialogBuilder.setView(dialogView);

            final EditText review_message = (EditText) dialogView.findViewById(R.id.review_message);
            final RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.ratingBar);
            ratingBar.setNumStars(5);
            final Button bt_submit_review = (Button) dialogView.findViewById(R.id.bt_submit_review);


            alertDialog = dialogBuilder.create();
            alertDialog.show();


            bt_submit_review.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("RatingBAR", "" + ratingBar.getRating());
                    Utility.hideSoftKeyboard(ProductDetail.this);
                    if (ratingBar.getRating() == 0) {
                        ToastMsg.showShortToast(ProductDetail.this, "Please provide a rating.");
                    } else {
                        String reviewMessage = review_message.getText().toString().trim();
                        new RatingManager(ProductDetail.this, ratingListener).sendRequest(user_id, ratingBar.getRating(), product_id, reviewMessage);
                    }

                   *//* if (review_message.getText().toString().trim().length() == 0) {

                    } else {
                        String reviewMessage = review_message.getText().toString().trim();
                    }*//*
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

  /*  RatingListener ratingListener = new RatingListener() {
        @Override
        public void onRatingSuccess(String message) {
            ToastMsg.showShortToast(ProductDetail.this, "Ratings and review saved");
            if (alertDialog.isShowing()) {
                alertDialog.dismiss();
            }

        }

        @Override
        public void onRatingError(String error) {
            ToastMsg.showShortToast(ProductDetail.this, error);
        }
    };*/

}
