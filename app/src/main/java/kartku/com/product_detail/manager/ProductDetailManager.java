package kartku.com.product_detail.manager;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kartku.com.dashboard_module.api.HomeBannersApi;
import kartku.com.dashboard_module.listener.HomeBannersListener;
import kartku.com.product_detail.api.ProductDetailApi;
import kartku.com.product_detail.listener.ProductDetailListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class ProductDetailManager {

    private Context context;
    private ProductDetailListener listener;

    public ProductDetailManager(Context context, ProductDetailListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void sendRequest(String params) {
        try {
            new ProductDetailApi(context, ProductDetailManager.this).sendRequest(API.PRODUCT_DETAIL_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                try {
                    JSONObject product_info = new JSONObject(res_object.getString(Constant.PRODUCT_INFO));
                    listener.onSuccess("" + product_info);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    String message = res_object.getString(Constant.PRODUCT_INFO);
                    listener.onError(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onError(String message) {

        listener.onError(message);
    }

}
