package kartku.com.product_detail;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.bumptech.glide.Glide;
import com.viewpagerindicator.CirclePageIndicator;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.activity.AboutUsActivity;
import kartku.com.activity.ContactActivity;
import kartku.com.adapter.CustomSpinnerAdapter;
import kartku.com.activity.MyOrdersActivity;
import kartku.com.activity.MyProductReviewsActivity;
import kartku.com.activity.PrivacyPolicyActivity;
import kartku.com.activity.ProfileActivity;
import kartku.com.activity.SearchProductsBrandActivity;
import kartku.com.activity.WishlistActivity;
import kartku.com.adapter.RelatedProductsAdapter;
import kartku.com.adapter.SlidingImage;
import kartku.com.cart.CartDetailsNew;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.faq_module.FAQ;
import kartku.com.login_module.Login;
import kartku.com.rate_review.RateAndReview;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.AddToCartNewModal;
import kartku.com.retrofit.modal.AddToWishListModal;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.EventBusCartUpdate;
import kartku.com.retrofit.modal.EventBusCategoryProductsUpdateWishlist;
import kartku.com.retrofit.modal.EventBusLogout;
import kartku.com.retrofit.modal.EventBusProductDetailUpdateModal;
import kartku.com.retrofit.modal.GetProductImagesWithColourModal;
import kartku.com.retrofit.modal.ProductColourOptionsModal;
import kartku.com.retrofit.modal.ProductDetailModal;
import kartku.com.retrofit.modal.RemoveFromWishlistModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 12/29/2016.
 */

public class ProductDetailNew extends Utility implements ApiServerResponse {

    TextView text_material, text_color, text_pattern, text_dimesions, text_qty, text_description;
    private ViewPager mPager;
    EditText qty_text;
    AQuery aQuery;
    int count = 1;
    //TextView add_qty, sub_qty;
    Button add_cart;
    ImageView img_AddToWishlist;
    SharedPreferences pref;
    //private int countBadge = 1;

    //   private static final Integer[] IMAGES= {R.mipmap.banner,R.mipmap.banner,R.mipmap.banner,R.mipmap.banner};
    //public static final int DELAY = 100;
    CirclePageIndicator indicator;
    ArrayList<String> arrayList = new ArrayList<>();
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private String product_id = "";
    String user_id;
    //private MenuItem itemCart;
    //private LayerDrawable icon;
    //AlertDialog alertDialog;
    TextView mCounter;
    int isInWishlist;
    //String productName, productPrice;
    TextView textView_NotRealPrice, textView_RealPrice, tv_ProductName, tv_left_in_stock;
    ImageView iv_rateReview, iv_share;
    int itemAmount;

    Resources res;
    String itemStock, shareBody;
    boolean loginStatus;
    //ArrayAdapter<String> quantity;
    ArrayList<String> quantityList;
    TextView tv_count, tv_product_brand, tv_product_warranty, tv_product_faq, text_brand_name;
    String brandName, brandImage, brandDesc, faqDesc, warrantyDesc;
    RecyclerView rv_related_products;
    Spinner sp_colour_Spinner;
    String colourItemID;
    CardView cv_similar_items;
    TextView text_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondary_product_details_new);
        //setupWindowAnimations();
        /*if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }*/

        quantityList = new ArrayList<String>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        mPager = (ViewPager) findViewById(R.id.pager);
        aQuery = new AQuery(this);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        text_material = (TextView) findViewById(R.id.text_material);
        text_color = (TextView) findViewById(R.id.text_color);
        text_pattern = (TextView) findViewById(R.id.text_pattern);
        add_cart = (Button) findViewById(R.id.btn_shipping);
        img_AddToWishlist = (ImageView) findViewById(R.id.img_AddToWishlist);
        text_dimesions = (TextView) findViewById(R.id.text_dimesions);
        iv_share = (ImageView) findViewById(R.id.iv_share);
        iv_rateReview = (ImageView) findViewById(R.id.iv_rateReview);
        text_description = (TextView) findViewById(R.id.text_description);
        tv_left_in_stock = (TextView) findViewById(R.id.tv_left_in_stock);
        tv_product_brand = (TextView) findViewById(R.id.tv_product_brand);
        tv_product_warranty = (TextView) findViewById(R.id.tv_product_warranty);
        tv_product_faq = (TextView) findViewById(R.id.tv_product_faq);
        text_brand_name = (TextView) findViewById(R.id.text_brand_name);
        sp_colour_Spinner = (Spinner) findViewById(R.id.sp_colour_Spinner);
        rv_related_products = (RecyclerView) findViewById(R.id.rv_related_products);
        rv_related_products.setLayoutManager(new LinearLayoutManager(ProductDetailNew.this, LinearLayoutManager.HORIZONTAL, false));
        loginStatus = Session.get_login_statuc(pref);
        cv_similar_items = (CardView) findViewById(R.id.cv_similar_items);
        text_price = (TextView) findViewById(R.id.text_price);

        //quantity = new ArrayAdapter<String>(ProductDetailNew.this, android.R.layout.simple_list_item_1);

        /*add_qty = (TextView) findViewById(R.id.add_qty);
        sub_qty = (TextView) findViewById(R.id.sub_qty);
*/

        //Changes
        tv_ProductName = (TextView) findViewById(R.id.tv_ProductName);
        textView_NotRealPrice = (TextView) findViewById(R.id.textView_NotRealPrice);
        textView_RealPrice = (TextView) findViewById(R.id.textView_RealPrice);

        qty_text = (EditText) findViewById(R.id.qty_text);
        qty_text.setText("" + 1);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        /*setListener();*/
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.title_activity_product_detail);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            Intent intent = getIntent();
            product_id = intent.getStringExtra(Constant.SELECTED_CATEGORY_INDEX);
            user_id = Session.get_userId(pref);
            //isInWishlist = intent.getIntExtra(Constant.WISHLIST, 0);
            //String productName = intent.getStringExtra(Constant.PRODUCT_NAME);
            //String productPrice = intent.getStringExtra(Constant.PRODUCT_PRICE);


            res = getResources();

            tv_product_brand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        brandDetailsDialog(brandName, brandDesc, brandImage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            tv_product_faq.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //faqDetailsDialog(faqDesc);

                        if (faqDesc.equalsIgnoreCase("")) {
                            showAlertDialogFAQWarranty("FAQ", getString(R.string.no_faq));
                        } else {
                            showAlertDialogFAQWarranty("FAQ", faqDesc);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            tv_product_warranty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //warrantyDetailsDialog(warrantyDesc);
                        if (warrantyDesc.equalsIgnoreCase("")) {
                            showAlertDialogFAQWarranty(getString(R.string.warranty), getString(R.string.no_description));
                        } else {
                            showAlertDialogFAQWarranty(getString(R.string.warranty), warrantyDesc);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            sp_colour_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String item = parent.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            qty_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        //showDialog(quantityList);


                        showAlertDialog(quantityList);
                /*        AlertDialog.Builder builderSingle = new AlertDialog.Builder(ProductDetailNew.this);
                        //builderSingle.setIcon(R.drawable.ic_launcher);
                        //builderSingle.setTitle("Select One Name:-");


                        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        builderSingle.setAdapter(quantity, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String strName = quantity.getItem(which);
                                qty_text.setText(strName);
                                //showLoading();
                                //EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));
                                //addCart();

                   *//* AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                    builderInner.setMessage(strName);
                    builderInner.setTitle("Your Selected Item is");
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();*//*
                            }
                        });
                        builderSingle.show();*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            add_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (loginStatus) {
                            if (add_cart.getText().toString().equalsIgnoreCase(getResources().getString(R.string.open_cart))) {
                                Intent i = new Intent(ProductDetailNew.this, CartDetailsNew.class);
                                startActivity(i);
                            } else {
                                if (sp_colour_Spinner.getAdapter().getCount() > 1 && sp_colour_Spinner.getSelectedItemPosition() == 0) {
                                    ToastMsg.showShortToast(ProductDetailNew.this, getString(R.string.select_colour));
                                } else if (sp_colour_Spinner.getAdapter().getCount() <= 1) {
                                    colourItemID = "0";

                                    addCart(product_id, qty_text.getText().toString(), Session.get_userId(pref), Session.getcart_id(pref), colourItemID);
                                } else if (sp_colour_Spinner.getAdapter().getCount() > 1) {
                                    addCart(product_id, qty_text.getText().toString(), Session.get_userId(pref), Session.getcart_id(pref), colourItemID);
                                }


                            }
                        } else {
                            ToastMsg.showShortToast(ProductDetailNew.this, getString(R.string.login_to_continuew));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            img_AddToWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        if (loginStatus) {
                            if (isInWishlist == 1) {
                                removeWishlist(user_id, product_id);

                            } else {
                                addWishlist(user_id, product_id);

                            }
                        } else {
                            ToastMsg.showShortToast(ProductDetailNew.this, "Please login to proceed further.");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            iv_rateReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(ProductDetailNew.this, RateAndReview.class);
                        intent.putExtra(Constant.USER_ID, user_id);
                        intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, product_id);
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            iv_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        shareIt();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            //String textPrice = String.format(res.getString(R.string.not_real_price), productPrice);
            /*String text = String.format(res.getString(R.string.not_real_price), productPrice);*/

            //textView_NotRealPrice.setText(textPrice);
            /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textView_NotRealPrice.setBackground(getResources().getDrawable(R.drawable.line, null));
            } else {
                textView_NotRealPrice.setBackground(getResources().getDrawable(R.drawable.line));
            }*/

            // textView_NotRealPrice.setPaintFlags(textView_NotRealPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            //textView_RealPrice.setText(textPrice);
            //tv_ProductName.setText(productName);


           /* if (isInWishlist == 1) {
                Glide.with(ProductDetailNew.this)
                        .load(R.drawable.ic_wishlist_not).override(45, 45)
                        .into(img_AddToWishlist);

            } else {
                Glide.with(ProductDetailNew.this)
                        .load(R.drawable.ic_wishlist).override(45, 45)
                        .into(img_AddToWishlist);
            }*/

           /* if (isInWishlist) {
                img_AddToWishlist.setImageResource(R.drawable.ic_wishlist);
                img_AddToWishlist.setTag(R.drawable.ic_wishlist);

            } else {

                img_AddToWishlist.setImageResource(R.drawable.ic_wishlist_not);
                img_AddToWishlist.setTag(R.drawable.ic_wishlist_not);
            }
*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        /***
         * get product categories on basis of category id
         */
        try {
            // Log.d("", "product_id ******   " + product_id);
            //new ProductDetailManager(ProductDetailNew.this, this).sendRequest("/".concat(product_id));
            showLoading();
            ServerAPI.getInstance().getProductDetail(ApiServerResponse.PRODUCT_DETAIL, product_id, Session.get_userId(pref), this);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

   /* public void setListener() {

        iv_share.setOnClickListener(this);
        iv_rateReview.setOnClickListener(this);

        //add_qty.setOnClickListener(this);
        //sub_qty.setOnClickListener(this);
        add_cart.setOnClickListener(this);
        img_AddToWishlist.setOnClickListener(this);
    }*/

/*    @Override
    public void onSuccess(String response) {
        try {
            JSONObject product_info = new JSONObject(response);

            Resources res = getResources();

            text_material.setText(product_info.get("material").toString());
            text_color.setText(product_info.get("color").toString());
            text_description.setText(product_info.get("description").toString());

            String text = String.format(res.getString(R.string.two_parameters_x), product_info.get("height").toString(), product_info.get("width").toString());

            text_dimesions.setText(text);
            if (product_info.get("discounted_price").equals("")) {
                textView_NotRealPrice.setVisibility(View.GONE);
                textView_RealPrice.setText(product_info.get("price").toString());
            } else {
                textView_NotRealPrice.setVisibility(View.VISIBLE);
                textView_NotRealPrice.setText(product_info.get("price").toString());
                textView_RealPrice.setText(product_info.get("discounted_price").toString());
            }


            Log.i("Images ", response);
            for (int i = 0; i < 4; i++) {
                arrayList.add(product_info.get("image").toString());
            }
            Log.i("arraylistSize", "" + arrayList.size());
            init(arrayList);

//            JSONArray jsonArray=product_info.getJSONArray("related_products");
//            for(int i=0;i<jsonArray.length();i++){
//
//                JSONObject jsonObject=jsonArray.getJSONObject(i);
//                Product_DetailModel product_detailModel=new Product_DetailModel();
//                product_detailModel.setId(jsonObject.get("related_product_id").toString());
//                product_detailModel.setImage(jsonObject.get("related_product_title").toString());
//                product_detailModel.setTittle(jsonObject.get("related_products_image_url").toString());
//                arrayList.add(product_detailModel);
//
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

  /*  @Override
    public void onError(String error) {
        ToastMsg.showShortToast(ProductDetailNew.this, error);
    }*/

    private void init(ArrayList<String> arrayList) {
//        for(int i=0;i<IMAGES.length;i++)
//         ImagesArray.add(IMAGES[i]);

        //Log.e("Images link", "Images link" + arrayList.size() + "image" + arrayList.get(0));
        mPager.setAdapter(new SlidingImage(this, arrayList));


        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = arrayList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };


        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    private void shareIt() {
        /*Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>Download Lotus Decor APP to purchase products..</p>"));
        startActivity(Intent.createChooser(sharingIntent, "Share using"));*/

        String shareMessage = "Check out this product\n" + shareBody + "\nDownload Lotus Decor APP to purchase more products.";
        //shareBody = "Download Lotus Decor APP to purchase products..";
       /* Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Lotus Decor");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));*/


        // Create and fire off our Intent in one fell swoop
        ShareCompat.IntentBuilder
                .from(ProductDetailNew.this) // getActivity() or activity field if within Fragment
                .setText(shareMessage)
                .setType("text/plain") // most general text sharing MIME type
                .setChooserTitle(getResources().getString(R.string.share_using))
                .startChooser();
    }


/*    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.iv_share) {
            shareIt();

        } else if (id == R.id.iv_rateReview) {
            *//*Intent in = new Intent(ProductDetailNew.this, ProductReview.class);
            startActivity(in);*//*
            //addReviewCustomDialog();
            Intent intent = new Intent(ProductDetailNew.this, RateAndReview.class);
            intent.putExtra(Constant.USER_ID, user_id);
            intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, product_id);
            startActivity(intent);

        }

            count++;add
            qty_text.setText("" + count);
        } else if (id == R.id.add_qty) {
            if (count == 1) {
                qty_text.setText("" + count);
            } else {
                count--;
                qty_text.setText("" + count);
            }

        }*//*

        else if (id == R.id.btn_shipping) {
            try {
                if (loginStatus) {
                    if (add_cart.getText().toString().equalsIgnoreCase(getResources().getString(R.string.open_cart))) {
                        Intent i = new Intent(ProductDetailNew.this, CartDetailsNew.class);
                        startActivity(i);
                    } else {
                        addCart();
                    }
                } else {
                    ToastMsg.showShortToast(ProductDetailNew.this, "Please login to proceed further.");
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (id == R.id.img_AddToWishlist) {
            try {

                if (loginStatus) {
                    if (isInWishlist == 1) {
                        removeWishlist(user_id, product_id);

                    } else {
                        addWishlist(user_id, product_id);

                    }
                } else {
                    ToastMsg.showShortToast(ProductDetailNew.this, "Please login to proceed further.");
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/

    private void addCart(String productID, String qty, String userID, String cartID, String colourID) {

        try {

            if (checkInternetConnection(ProductDetailNew.this)) {
                int totalAmount = Integer.parseInt(qty_text.getText().toString()) * itemAmount;
                //Log.i("ADDCART ", qty + " " + cart_id + " " + itemAmount);
                String cart_id = Session.getcart_id(pref);

                //if (!cart_id.equalsIgnoreCase("")) {
                showLoading();
                //also do in wishlist
                ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, String.valueOf(totalAmount), productID, qty, userID, cartID, colourID, this);
            } else {
                showAlertDialog(ProductDetailNew.this);
            }

            //} else {
            //   showLoading();
            //     ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, String.valueOf(totalAmount), product_id, qty_text.getText().toString(), user_id, "0", this);
            // }


   /*         showLoading();
            EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));
            ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, String.valueOf(totalAmount), product_id, qty_text.getText().toString(), user_id, cart_id, this);

            if (qty_text.getText().toString().equalsIgnoreCase("0")) {

                ToastMsg.showShortToast(ProductDetailNew.this, "Please enter a valid product quantity.");
                qty_text.setText("1");
            } else if (!qty_text.getText().toString().equalsIgnoreCase("0") && checkStockOfItem()) {
                String qty = qty_text.getText().toString();
                *//*String qty = "" + qty_text.getText().toString();*//*
                String cart_id = Session.getcart_id(pref);

                if (!cart_id.equalsIgnoreCase("")) {
                    int totalAmount = Integer.parseInt(qty_text.getText().toString()) * itemAmount;
                    Log.i("ADDCART ", qty + " " + cart_id + " " + itemAmount);
                    // new AddToCartManager(getApplicationContext(), this).sendRequest(user_id, "", product_id, qty, cart_id);
                    showLoading();
                    EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));
                    ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, String.valueOf(totalAmount), product_id, qty, user_id, cart_id, this);
                } else {
                    int totalAmount = Integer.parseInt(qty_text.getText().toString()) * itemAmount;
                    Log.i("ADDCART ", qty + " " + cart_id + " " + itemAmount);
                    // new AddToCartManager(getApplicationContext(), this).sendRequest(user_id, "", product_id, qty, cart_id);
                    showLoading();
                    ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, String.valueOf(totalAmount), product_id, qty, user_id, "", this);
                }

            } else {
                ToastMsg.showShortToast(ProductDetailNew.this, "Please enter a valid product quantity.");
                qty_text.setText("1");
            }


            if (!qty_text.getText().toString().trim().equals("0") && Integer.parseInt(qty_text.getText().toString().trim()) < 100) {

            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

/*
    private boolean checkStockOfItem() {
        boolean inStock = false;
        try {


            int quantity = Integer.valueOf(qty_text.getText().toString().trim());

            if (quantity > Integer.parseInt(itemStock)) {

                inStock = false;
            } else if (quantity < Integer.parseInt(itemStock)) {
                inStock = true;
            } else if (quantity == Integer.parseInt(itemStock)) {
                inStock = true;
            }


           */
/* int compare = qty_text.getText().toString().trim().compareToIgnoreCase(itemStock.trim());
            ToastMsg.showShortToast(ProductDetailNew.this, "" + compare);
            if (qty_text.getText().toString().trim().compareToIgnoreCase(itemStock.trim()) < 0) {
                String qty = qty_text.getText().toString();

                inStock = true;
                //return inStock;
            } else if (qty_text.getText().toString().trim().compareToIgnoreCase(itemStock.trim()) > -1) {
                String qty = qty_text.getText().toString();

                inStock = false;
                //return inStock;
            } else if (qty_text.getText().toString().trim().compareToIgnoreCase(itemStock.trim()) == 0) {
                inStock = true;
                //return inStock;
            }*//*


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inStock;

    }
*/

    /* @Override
     public void onAddCartError(String error) {
         ToastMsg.showShortToast(ProductDetailNew.this, error);
     }

     @Override
     public void onAddCartSuccess(String messgae) {
         Log.d("", "onAddCartSuccess *******  " + messgae);
         //Utility.setBadgeCount(ProductDetailNew.this, icon, qty_text.getText().toString().trim());
         ToastMsg.showShortToast(ProductDetailNew.this, messgae);
         //Session.setCartItemsQuantity(pref, (Integer.toString(countBadge) + Session.getCartItemsQuantity(pref).trim()));
         //mCounter.setText("+" + Session.getCartItemsQuantity(pref));
     }
 */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.clear();
        menu.clear();
        if (Session.get_login_statuc(pref)) {
            getMenuInflater().inflate(R.menu.menu_universal_extra_options_login, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_universal_extra_options_logout, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ProductDetailNew.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case R.id.menu_home:

                Intent intentHome = new Intent(ProductDetailNew.this, DemoHomeDashBoard.class);
                intentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentHome);
                break;
            case R.id.search:

                Intent search = new Intent(ProductDetailNew.this, SearchProductsBrandActivity.class);
                startActivity(search);

                break;
            case R.id.menu_count:
                Intent in = new Intent(ProductDetailNew.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
                /*Intent intent1 = new Intent(DemoHomeDashBoard.this, SellerHomeDashBoard.class);
                startActivity(intent1);*/
                break;
            case R.id.menu_my_orders:


                Intent intent1 = new Intent(ProductDetailNew.this, MyOrdersActivity.class);
                startActivity(intent1);
                //MyOrdersFragment ordersFragment = new MyOrdersFragment();
                            /*OrdersFragment ordersFragment = new OrdersFragment();*/
                //tag = "My Orders";
                /*ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);*/
                //swtichFragement(R.id.content_frame, ordersFragment, tag);

                break;
            case R.id.menu_my_product_review:

                Intent intentReviews = new Intent(ProductDetailNew.this, MyProductReviewsActivity.class);
                startActivity(intentReviews);

                break;
            case R.id.menu_my_wishlist:
                Intent intentWishlist = new Intent(ProductDetailNew.this, WishlistActivity.class);
                startActivity(intentWishlist);
                break;
            case R.id.menu_my_profile:
                Intent intentProfile = new Intent(ProductDetailNew.this, ProfileActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.menu_contact_us:

                Intent intentContact = new Intent(ProductDetailNew.this, ContactActivity.class);
                startActivity(intentContact);

                break;
            case R.id.menu_privacy_policy:


                //if (checkInternetConnection(DemoHomeDashBoard.this)) {

                Intent intentPrivacyPolicy = new Intent(ProductDetailNew.this, PrivacyPolicyActivity.class);
                startActivity(intentPrivacyPolicy);

                // swtichFragement(R.id.content_frame, privacyPolicyFragment, tag);


                break;
            case R.id.menu_about_us:

                Intent intentAboutUs = new Intent(ProductDetailNew.this, AboutUsActivity.class);
                startActivity(intentAboutUs);
                break;
            case R.id.menu_logout:

                //Session.getLogin_type(pref);

                EventBus.getDefault().post(new EventBusLogout(Session.getLogin_type(pref)));
                /*switch (loginType) {
                    case "google":
                        if (mGoogleApiClient.isConnected()) {
                            signOut();
                            drawerLayout.closeDrawers();
                            //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                            mGoogleApiClient.disconnect();
                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            LoginFragment fragmentLoginLogout = new LoginFragment();
                            tag = "Login";
                            swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);

                        }
                        break;
                    case "facebook": {
                        LoginManager.getInstance().logOut();
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        break;
                    }
                    default: {
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        //prepareListData();
                        Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                        startActivity(i);
                        finish();
                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        break;
                    }
                }*/

                break;

            case R.id.menu_login:
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                Intent intent = new Intent(ProductDetailNew.this, Login.class);
                startActivity(intent);
                break;
            case R.id.menu_faq:

                Intent intentFAQ = new Intent(ProductDetailNew.this, FAQ.class);
                startActivity(intentFAQ);

                break;
        }
        return super.onOptionsItemSelected(item);
    }


  /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        itemCart = menu.findItem(R.id.action_cart);
        icon = (LayerDrawable) itemCart.getIcon();
        return true;
    }*/


    private void addWishlist(String user_id, String selected_productId) {
        if (checkInternetConnection(ProductDetailNew.this)) {
            Log.d("", "addWishlist product_id *****  " + selected_productId);
            System.out.println("add to wishlist product id ****  " + selected_productId);
            showLoading();
            ServerAPI.getInstance().addToWishlist(ApiServerResponse.ADD_TO_WISHLIST, user_id, selected_productId, this);
        } else {
            showAlertDialog(ProductDetailNew.this);
        }

        // new AddToWishlistManager(ProductDetailNew.this, this).sendRequest(user_id, selected_productId);
    }

    private void removeWishlist(String user_id, String selected_productId) {
        if (checkInternetConnection(ProductDetailNew.this)) {
            Log.d("", "addWishlist product_id *****  " + selected_productId);
            System.out.println("add to wishlist product id ****  " + selected_productId);
            showLoading();
            ServerAPI.getInstance().removeFromWishlist(ApiServerResponse.REMOVE_FROM_WISHLIST, user_id, selected_productId, this);
        } else {
            showAlertDialog(ProductDetailNew.this);
        }

        // new RemoveFromWishlistManager(ProductDetailNew.this, this).sendRequest(user_id, selected_productId);
    }


   /* @Override
    public void onAddwishlistSuccess(String response) {
        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(ProductDetailNew.this, response);
        img_AddToWishlist.setImageResource(R.drawable.ic_wishlist);
        img_AddToWishlist.setTag(R.drawable.ic_wishlist);

    }

    @Override
    public void onAddwishlistError(String error) {
        ToastMsg.showShortToast(ProductDetailNew.this, error);
    }

    @Override
    public void removewishlistOnSuccess(String response) {
        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(ProductDetailNew.this, response);
        img_AddToWishlist.setImageResource(R.drawable.ic_wishlist_not);
        img_AddToWishlist.setTag(R.drawable.ic_wishlist_not);
    }

    @Override
    public void removewishlistOnError(String error) {
        ToastMsg.showShortToast(ProductDetailNew.this, error);
    }*/


    private void setupWindowAnimations() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setDuration(1000);
            getWindow().setEnterTransition(fade);

            Slide slide = new Slide();
            slide.setDuration(1000);
            getWindow().setReturnTransition(slide);
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {
                String cart_id = Session.getcart_id(pref);
                ProductDetailModal productDetailModal;
                AddToCartNewModal addToCartNewModal;
                AddToWishListModal addToWishListModal;
                RemoveFromWishlistModal removeFromWishlistModal;
                CartItemCountModal cartItemCountModal;
                String textStock;
                ProductColourOptionsModal productColourOptionsModal;
                GetProductImagesWithColourModal getProductImagesWithColourModal;

                switch (tag) {

                    case ApiServerResponse.PRODUCT_DETAIL:
                        productDetailModal = (ProductDetailModal) response.body();

                        if (productDetailModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            if (!arrayList.isEmpty()) {
                                arrayList.clear();
                                arrayList.add(productDetailModal.getProduct_info().getImage());
                                //arrayList.add(productDetailModal.getProduct_info().getImage2());
                                init(arrayList);


                            } else {
                                arrayList.add(productDetailModal.getProduct_info().getImage());
                                //arrayList.add(productDetailModal.getProduct_info().getImage2());
                                init(arrayList);
                            }


                            tv_ProductName.setText(productDetailModal.getProduct_info().getName());
                            text_material.setText(productDetailModal.getProduct_info().getMaterial());
                            ///text_color.setText(productDetailModal.getProduct_info().getColor());
                            text_brand_name.setText(productDetailModal.getProduct_info().getBrand());

                            tv_product_brand.setText(String.format(res.getString(R.string.two_parameters_options), getString(R.string.about_title), productDetailModal.getProduct_info().getBrand()));

                            if (!productDetailModal.getProduct_info().getDiscounted_price().equalsIgnoreCase("")) {


                                textView_NotRealPrice.setPaintFlags(textView_NotRealPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    textView_NotRealPrice.setTextColor(getResources().getColor(R.color.colorLogoRed, null));
                                } else {
                                    textView_NotRealPrice.setTextColor(getResources().getColor(R.color.colorLogoRed));
                                }
                                textView_NotRealPrice.setText(String.format(res.getString(R.string.original_price_in_RP), productDetailModal.getProduct_info().getPrice()));
                                textView_RealPrice.setVisibility(View.VISIBLE);
                                text_price.setText(String.format(res.getString(R.string.discounted_price_RP), productDetailModal.getProduct_info().getDiscounted_price()));
                                textView_RealPrice.setText(String.format(res.getString(R.string.discounted_price_RP), productDetailModal.getProduct_info().getDiscounted_price()));


                                itemAmount = Integer.parseInt(productDetailModal.getProduct_info().getDiscounted_price());
                                // itemMax = productDetailModal.getProduct_info().get

                            } else if (!productDetailModal.getProduct_info().getPrice().equalsIgnoreCase("") && productDetailModal.getProduct_info().getDiscounted_price().equalsIgnoreCase("")) {
                                textView_NotRealPrice.setText(String.format(res.getString(R.string.original_price_in_RP), productDetailModal.getProduct_info().getPrice()));
                                text_price.setText(String.format(res.getString(R.string.original_price_in_RP), productDetailModal.getProduct_info().getPrice()));
                                textView_RealPrice.setVisibility(View.GONE);

                                itemAmount = Integer.parseInt(productDetailModal.getProduct_info().getPrice());
                            } else {
                                textView_NotRealPrice.setText(String.format(res.getString(R.string.original_price_in_RP), productDetailModal.getProduct_info().getPrice()));
                                text_price.setText(String.format(res.getString(R.string.original_price_in_RP), productDetailModal.getProduct_info().getPrice()));
                                textView_RealPrice.setVisibility(View.GONE);
                                itemAmount = Integer.parseInt(productDetailModal.getProduct_info().getPrice());
                            }


                            if (Session.get_login_statuc(pref)) {
                                if (productDetailModal.getProduct_info().getWishlist().equalsIgnoreCase("1")) {
                                    isInWishlist = Integer.parseInt(productDetailModal.getProduct_info().getWishlist().trim());
                                    Glide.with(ProductDetailNew.this)
                                            .load(R.drawable.ic_wishlist).override(45, 45)
                                            .into(img_AddToWishlist);

                                } else {
                                    isInWishlist = Integer.parseInt(productDetailModal.getProduct_info().getWishlist().trim());
                                    Glide.with(ProductDetailNew.this)
                                            .load(R.drawable.ic_wishlist_not).override(45, 45)
                                            .into(img_AddToWishlist);
                                }

                            }


                            //String text = String.format(res.getString(R.string.three_parameters_x), productDetailModal.getProduct_info().getHeight(), productDetailModal.getProduct_info().getWeight(), productDetailModal.getProduct_info().getDepth());


                            if (!productDetailModal.getProduct_info().getHeight().equalsIgnoreCase("") && !productDetailModal.getProduct_info().getWeight().equalsIgnoreCase("") && !productDetailModal.getProduct_info().getDepth().equalsIgnoreCase("")) {
                                String text = String.format(res.getString(R.string.three_parameters_x), productDetailModal.getProduct_info().getHeight(), productDetailModal.getProduct_info().getWidth(), productDetailModal.getProduct_info().getDepth());
                                text_dimesions.setText(text);
                            } else {
                                text_dimesions.setText("");
                            }


                            text_description.setText(productDetailModal.getProduct_info().getDescription());


                            if (!productDetailModal.getProduct_info().getMax_quantity().equalsIgnoreCase("") && !productDetailModal.getProduct_info().getMax_quantity().equalsIgnoreCase("0")) {
                                //itemStock = productDetailModal.getProduct_info().getMax_quantity();
                                if (!quantityList.isEmpty()) {
                                    quantityList.clear();
                                }
                                for (int i = 1; i <= Integer.valueOf(productDetailModal.getProduct_info().getMax_quantity()); i++) {
                                    //quantity.add(String.valueOf(i));

                                    quantityList.add(String.valueOf(i));
                                }


                                qty_text.setText("1");
                                qty_text.setEnabled(true);
                                textStock = String.format(res.getString(R.string.left_in_stock_with_parameter), productDetailModal.getProduct_info().getMax_quantity());
                                tv_left_in_stock.setText(textStock);
                                add_cart.setEnabled(true);
                                add_cart.setText(getResources().getString(R.string.add_to_cart));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    add_cart.setBackground(getResources().getDrawable(R.drawable.roundedbutton, null));
                                } else {
                                    add_cart.setBackground(getResources().getDrawable(R.drawable.roundedbutton));
                                }
                            } else {
                                //itemStock = "1";
                                //quantityList.clear();
                                //quantityList.add("0");
                                qty_text.setText("0");
                                qty_text.setEnabled(false);
                                textStock = getString(R.string.out_of_stock_title);
                                tv_left_in_stock.setText(textStock);
                                add_cart.setEnabled(false);
                                add_cart.setText(getResources().getString(R.string.add_to_cart));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    add_cart.setBackground(getResources().getDrawable(R.drawable.roundedbutton_disabled, null));
                                } else {
                                    add_cart.setBackground(getResources().getDrawable(R.drawable.roundedbutton_disabled));
                                }

                            }
                            if (productDetailModal.getProduct_info().getIs_cart().equalsIgnoreCase("1")) {
                                add_cart.setText(getResources().getString(R.string.open_cart));
                                qty_text.setEnabled(false);
                            } else {
                                add_cart.setText(getResources().getString(R.string.add_to_cart));
                                qty_text.setEnabled(true);
                            }

                           /* if (!productDetailModal.getProduct_info().getMax_quantity().equalsIgnoreCase("") && !productDetailModal.getProduct_info().getMax_quantity().equalsIgnoreCase("0")) {
                                textStock = String.format(res.getString(R.string.left_in_stock_with_parameter), productDetailModal.getProduct_info().getMax_quantity());
                                tv_left_in_stock.setText(textStock);
                                qty_text.setEnabled(true);
                                add_cart.setEnabled(true);
                            } else {
                                textStock = "Out of stock";
                                tv_left_in_stock.setText(textStock);
                                qty_text.setEnabled(false);
                                add_cart.setEnabled(false);
                                add_cart.setText(textStock);
                            }*/

                            if (!productDetailModal.getProduct_info().getShare_url().equalsIgnoreCase("")) {
                                shareBody = productDetailModal.getProduct_info().getShare_url();

                            }

                            brandName = productDetailModal.getProduct_info().getBrand();
                            brandDesc = productDetailModal.getProduct_info().getBrand_description();
                            brandImage = productDetailModal.getProduct_info().getBrand_image();
                            faqDesc = Html.fromHtml(productDetailModal.getProduct_info().getFaq()).toString();
                            warrantyDesc = Html.fromHtml(productDetailModal.getProduct_info().getWarranty()).toString();


                            if (productDetailModal.getRelated_products().size() != 0) {
                                List<ProductDetailModal.RelatedProductsBean> relatedProductsBeanList = new ArrayList<>();
                                relatedProductsBeanList = productDetailModal.getRelated_products();
                                RelatedProductsAdapter relatedProductsAdapter = new RelatedProductsAdapter(ProductDetailNew.this, relatedProductsBeanList);
                                rv_related_products.setAdapter(relatedProductsAdapter);

                            } else {
                                cv_similar_items.setVisibility(View.GONE);
                            }


                            if (productDetailModal.getProduct_info().getColor().size() != 0) {

                                ProductDetailModal.ProductInfoBean.ColorBean pr = new ProductDetailModal.ProductInfoBean.ColorBean();
                                pr.setName(getString(R.string.select_colour_title));
                                pr.setId(0);

                                List<ProductDetailModal.ProductInfoBean.ColorBean> colorList = new ArrayList<>();
                                //colorList.add(pr);

                                colorList = productDetailModal.getProduct_info().getColor();
                                //List<ProductColourOptionsModal.ListBean> colorList = new ArrayList<>();
                                colorList.add(0, pr);
                                initialiseCustomSpinnerAdapter(colorList);
                            } else {
                                ArrayList<String> languages = new ArrayList<String>();
                                languages.add(getString(R.string.no_colour_options));
                                initialiseSpinnerAdapter(languages);
                            }


                        }


                        if (!cart_id.equalsIgnoreCase("0")) {
                            ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), cart_id, this);
                        }

                        //ServerAPI.getInstance().getProductColour(ApiServerResponse.PRODUCT_COLOURS, Session.get_userId(pref), product_id, this);

                        hideLoading();
                        break;
                    case ApiServerResponse.ADD_TO_CART_NEW:
                        addToCartNewModal = (AddToCartNewModal) response.body();

                        if (addToCartNewModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            //EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));
                            ToastMsg.showShortToast(ProductDetailNew.this, getString(R.string.product_added_to_cart));
                            qty_text.setEnabled(false);

                            if (Session.getcart_id(pref).equalsIgnoreCase("") || Session.getcart_id(pref).equalsIgnoreCase("0")) {
                                Session.setcart_id(pref, String.valueOf(addToCartNewModal.getCart_info().getId()));
                            }
                            add_cart.setText(getResources().getString(R.string.open_cart));


                        } else {
                            ToastMsg.showShortToast(ProductDetailNew.this, addToCartNewModal.getResult());
                        }

                        if (!cart_id.equalsIgnoreCase("0")) {
                            ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), cart_id, this);
                        }

                        if (addToCartNewModal.getCart_info().getId() != 0) {
                            Session.setcart_id(pref, String.valueOf(addToCartNewModal.getCart_info().getId()));
                            ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                        }


                        hideLoading();
                        break;

                    case ApiServerResponse.ADD_TO_WISHLIST:
                        addToWishListModal = (AddToWishListModal) response.body();

                        if (addToWishListModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            EventBus.getDefault().post(new EventBusCategoryProductsUpdateWishlist(addToWishListModal.getStatus()));
                            ToastMsg.showShortToast(ProductDetailNew.this, addToWishListModal.getMessage());
                            Glide.with(ProductDetailNew.this)
                                    .load(R.drawable.ic_wishlist).override(45, 45)
                                    .into(img_AddToWishlist);
                            isInWishlist = 1;

                        }

                        hideLoading();
                        break;
                    case ApiServerResponse.REMOVE_FROM_WISHLIST:

                        removeFromWishlistModal = (RemoveFromWishlistModal) response.body();

                        if (removeFromWishlistModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            EventBus.getDefault().post(new EventBusCategoryProductsUpdateWishlist(removeFromWishlistModal.getStatus()));
                            ToastMsg.showShortToast(ProductDetailNew.this, removeFromWishlistModal.getMessage());
                            Glide.with(ProductDetailNew.this)
                                    .load(R.drawable.ic_wishlist_not).override(45, 45)
                                    .into(img_AddToWishlist);
                            isInWishlist = 0;
                        }

                        hideLoading();
                        break;

                    case ApiServerResponse.CART_ITEM_COUNT:
                        cartItemCountModal = (CartItemCountModal) response.body();
                        if (cartItemCountModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            EventBus.getDefault().post(new EventBusProductDetailUpdateModal(String.valueOf(cartItemCountModal.getItem_count())));
                            EventBus.getDefault().post(new EventBusCartUpdate(String.valueOf(cartItemCountModal.getItem_count())));

                            Session.setCartItemsQuantity(pref, String.valueOf(cartItemCountModal.getItem_count()));
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            tv_count.setText(String.valueOf(cartItemCountModal.getItem_count()));
                        } else {
                            tv_count.setText("0");
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(pref, "0");
                        }
                        break;
               /* case ApiServerResponse.PRODUCT_COLOURS:
                    productColourOptionsModal = (ProductColourOptionsModal) response.body();
                    if (productColourOptionsModal.getStatus().equalsIgnoreCase(Constant.OK)) {


                        if (productColourOptionsModal.getList().size() != 0) {

                            ProductColourOptionsModal.ListBean pr = new ProductColourOptionsModal.ListBean();
                            pr.setName(getString(R.string.select_colour_title));
                            pr.setId(0);

                            List<ProductColourOptionsModal.ListBean> colorList = new ArrayList<>();
                            //colorList.add(pr);

                            colorList = productColourOptionsModal.getList();
                            //List<ProductColourOptionsModal.ListBean> colorList = new ArrayList<>();
                            colorList.add(0, pr);
                            initialiseCustomSpinnerAdapter(colorList);
                        } else {
                            ArrayList<String> languages = new ArrayList<String>();
                            languages.add(getString(R.string.no_colour_options));
                            initialiseSpinnerAdapter(languages);
                        }


                    } else {
                        ArrayList<String> languages = new ArrayList<String>();
                        languages.add(getString(R.string.no_colour_options));
                        initialiseSpinnerAdapter(languages);
                    }

                    hideLoading();
                    break;*/
                    case ApiServerResponse.PRODUCT_IMAGE_ACC_COLOUR:
                        getProductImagesWithColourModal = (GetProductImagesWithColourModal) response.body();

                        if (getProductImagesWithColourModal.getStatus().equalsIgnoreCase(Constant.OK)) {


                            if (!arrayList.isEmpty()) {
                                arrayList.clear();
                            }
                            for (int i = 0; i < getProductImagesWithColourModal.getImage_list().size(); i++) {

                                arrayList.add(getProductImagesWithColourModal.getImage_list().get(i).getName());
                            }

                            init(arrayList);
                            mPager.requestFocus();
                            mPager.requestFocusFromTouch();

                            //String img = getProductImagesWithColourModal.getImage_list().toString();
                            //ToastMsg.showLongToast(ProductDetailNew.this, getProductImagesWithColourModal.getImage_list().toString());
/*                            if (!arrayList.isEmpty()) {
                                arrayList.clear();
                                arrayList.add(productDetailModal.getProduct_info().getImage());
                                arrayList.add(productDetailModal.getProduct_info().getImage2());
                                init(arrayList);


                            } else {
                                arrayList.add(productDetailModal.getProduct_info().getImage());
                                arrayList.add(getProductImagesWithColourModal.getImage_list().toString());
                                init(arrayList);
                            }*/
                        } else {
                            if (!arrayList.isEmpty()) {
                                arrayList.clear();
                            }
                            for (int i = 0; i < getProductImagesWithColourModal.getImage_list().size(); i++) {

                                arrayList.add(getProductImagesWithColourModal.getImage_list().get(i).getName());
                            }

                            init(arrayList);
                        }
                        hideLoading();
                        break;
                }

            } else {
                hideLoading();
            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }


    public void initialiseCustomSpinnerAdapter(List<ProductDetailModal.ProductInfoBean.ColorBean> colorList) {
        try {
            CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(ProductDetailNew.this, colorList);
            sp_colour_Spinner.setAdapter(customSpinnerAdapter);
            sp_colour_Spinner.setEnabled(true);
            sp_colour_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //String item = parent.getItemAtPosition(position).toString();
                    colourItemID = String.valueOf(parent.getItemIdAtPosition(position));

                    //Toast.makeText(parent.getContext(), "Android spinner size..." + sp_colour_Spinner.getAdapter().getCount(), Toast.LENGTH_LONG).show();
                    //Toast.makeText(parent.getContext(), "Android Custom Spinner Example Output..." + item + " Item ID " + itemID, Toast.LENGTH_LONG).show();
                    if (checkInternetConnection(ProductDetailNew.this)) {
                        if (!colourItemID.equalsIgnoreCase("0") && !colourItemID.equalsIgnoreCase("")) {
                            showLoading();
                            ServerAPI.getInstance().getProductImagesAccToColour(ApiServerResponse.PRODUCT_IMAGE_ACC_COLOUR, Session.get_userId(pref), colourItemID, product_id, ProductDetailNew.this);
                        } else {
                            colourItemID = "No Colour";
                        }

                    } else {
                        showAlertDialog(ProductDetailNew.this);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initialiseSpinnerAdapter(ArrayList<String> list) {
        try {
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_colour_Spinner.setAdapter(dataAdapter);
            sp_colour_Spinner.setEnabled(false);

            sp_colour_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String item = String.valueOf(parent.getItemIdAtPosition(position));


                    // Toast.makeText(parent.getContext(), "Android size" + sp_colour_Spinner.getAdapter().getCount(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }

   /* @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();

    }*/

    /*@Subscribe
    public void onItemDeletedFromCart(EventBusProductDetailUpdateModal eventBusProductDetailUpdateModal) {

        ServerAPI.getInstance().getProductDetail(ApiServerResponse.PRODUCT_DETAIL, String.valueOf(eventBusProductDetailUpdateModal.getMessage()), Session.get_userId(pref), this);

    }*/


    /*private void showDialog(ArrayList<String> arrayList) {

        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Quantity");

        View view = getLayoutInflater().inflate(R.layout.custom_dialog_layout_quantity, null);

        ListView lv = (ListView) view.findViewById(R.id.custom_list);

        // Change MyActivity.this and myListOfItems to your own values
        CustomAlertDialogAdapter clad = new CustomAlertDialogAdapter(ProductDetailNew.this, arrayList);

        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ToastMsg.showShortToast(ProductDetailNew.this, adapterView.getAdapter().getItem(i).toString());

            }
        });

        dialog.setContentView(view);

        dialog.show();

    }*/

    private void showAlertDialog(ArrayList<String> quantityList) {

        try {
            final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(getString(R.string.quantity));
            dialogBuilder.setCancelable(true);
            dialogBuilder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showAlertDialogFAQWarranty(String dialogName, String dialogMessage) {

        try {
            //final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(dialogName);
            dialogBuilder.setMessage(dialogMessage);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Connection Problem.");
            dialogBuilder.setMessage("Please check your internet connection?");
            dialogBuilder.setCancelable(true);
           *//* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();

        try {
            // Log.d("", "product_id ******   " + product_id);
            //new ProductDetailManager(ProductDetailNew.this, this).sendRequest("/".concat(product_id));
            showLoading();
            ServerAPI.getInstance().getProductDetail(ApiServerResponse.PRODUCT_DETAIL, product_id, Session.get_userId(pref), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void brandDetailsDialog(String brandName, String brandDesc, String brandImage) {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle(String.format(res.getString(R.string.about_brand), brandName));
        dialog.setContentView(R.layout.dialog_about_brand);


        //LinearLayout ll_brand_info = (LinearLayout) dialog.findViewById(R.id.ll_brand_info);

        TextView textViewBrandDesc = (TextView) dialog.findViewById(R.id.tv_brand_desc);
        if (!brandDesc.equalsIgnoreCase("")) {
            textViewBrandDesc.setText(brandDesc);
        } else {
            textViewBrandDesc.setText(R.string.no_description);
        }


        TextView textViewOK = (TextView) dialog.findViewById(R.id.text_ok);

        ImageView iv_brand_image = (ImageView) dialog.findViewById(R.id.iv_brand_image);

        Glide.with(ProductDetailNew.this)
                .load(brandImage).placeholder(R.drawable.placeholder)
                .thumbnail(0.1f).override(300, 300)
                .into(iv_brand_image);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/

        //TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        textViewOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }

/*
    private void faqDetailsDialog(String faqDesc) {
        final Dialog dialog = new Dialog(ProductDetailNew.this);
        dialog.setCancelable(false);
        dialog.setTitle(getResources().getString(R.string.title_activity_faq));
        dialog.setContentView(R.layout.dialog_layout_faq);


        TextView tv_faq = (TextView) dialog.findViewById(R.id.tv_faq);
        tv_faq.setText(faqDesc);

        TextView textViewOK = (TextView) dialog.findViewById(R.id.text_ok);

        //ImageView iv_brand_image = (ImageView) dialog.findViewById(R.id.iv_brand_image);

        */
/*Glide.with(ProductDetailNew.this)
                .load(brandImage)
                .thumbnail(0.1f).override(50, 50)
                .into(iv_brand_image);
*//*

        */
/*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*//*


        //TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        textViewOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void warrantyDetailsDialog(String warrantyDesc) {
        final Dialog dialog = new Dialog(ProductDetailNew.this);
        dialog.setCancelable(false);
        dialog.setTitle(getResources().getString(R.string.warranty));
        dialog.setContentView(R.layout.dialog_layout_faq);


        TextView tv_faq = (TextView) dialog.findViewById(R.id.tv_faq);
        tv_faq.setText(warrantyDesc);

        TextView textViewOK = (TextView) dialog.findViewById(R.id.text_ok);

        //ImageView iv_brand_image = (ImageView) dialog.findViewById(R.id.iv_brand_image);

        */
/*Glide.with(ProductDetailNew.this)
                .load(brandImage)
                .thumbnail(0.1f).override(50, 50)
                .into(iv_brand_image);
*//*

        */
/*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*//*


        //TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        textViewOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }
*/


}
