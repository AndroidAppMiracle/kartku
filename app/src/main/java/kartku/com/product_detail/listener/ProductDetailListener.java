package kartku.com.product_detail.listener;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public interface ProductDetailListener {
    void onSuccess(String response);
    void onError(String error);
}
