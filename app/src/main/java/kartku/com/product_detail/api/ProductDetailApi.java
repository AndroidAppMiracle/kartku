package kartku.com.product_detail.api;

import android.content.Context;

import kartku.com.product_category.manager.ProductCategoryManager;
import kartku.com.product_detail.manager.ProductDetailManager;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class ProductDetailApi implements CustomResponse {

    ProductDetailManager manager;
    Context context;
    RequestServer request_server;

    public ProductDetailApi(Context context, ProductDetailManager manager) {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId, String params) {
        try {
            request_server = new RequestServer(context, requestId, this, params);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
