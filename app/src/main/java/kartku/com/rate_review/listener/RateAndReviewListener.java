package kartku.com.rate_review.listener;

/**
 * Created by Kshitiz Bali on 12/28/2016.
 */

public interface RateAndReviewListener {

    void onSuccess(String response);

    void onError(String error);
}
