package kartku.com.rate_review;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.RateAndReviewAdapter;
import kartku.com.product_review.listener.RatingListener;
import kartku.com.product_review.manager.RatingManager;
import kartku.com.rate_review.listener.RateAndReviewListener;
import kartku.com.rate_review.manager.RateAndReviewManager;
import kartku.com.rate_review.model.RateAndReviewModel;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.OneProductReviews;
import kartku.com.retrofit.modal.RateProductModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

public class RateAndReview extends Utility implements View.OnClickListener, ApiServerResponse {


    EditText review_message;
    RatingBar ratingBar;
    Button bt_submit_review;
    String product_id, user_id;
    RecyclerView rv_reviews;
    SharedPreferences pref;
    List<RateAndReviewModel.ReviewsBean> reviewsBeanArrayList = new ArrayList<>();
    boolean pendingIntroAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_and_review);

        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_rate_and_review);
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        review_message = (EditText) findViewById(R.id.review_message);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setNumStars(5);
        bt_submit_review = (Button) findViewById(R.id.bt_submit_review);
        rv_reviews = (RecyclerView) findViewById(R.id.rv_reviews);
        bt_submit_review.setOnClickListener(this);

        Intent intent = getIntent();
        product_id = intent.getStringExtra(Constant.SELECTED_CATEGORY_INDEX);
        user_id = intent.getStringExtra(Constant.USER_ID);

        // Log.e("pro ID, userID", "" + product_id + " " + user_id);

        try {
         /*   Log.i("param ", product_id);
            ToastMsg.showLongToast(RateAndReview.this, product_id);
            ToastMsg.showLongToast(RateAndReview.this, "/".concat(product_id));
            Log.i("Param C", "/".concat(product_id));*/
            //String param = "/".concat(product_id);
            //Log.i("paraString ", param);
            //ToastMsg.showLongToast(RateAndReview.this, param);
            //new RateAndReviewManager(RateAndReview.this, this).sendRequest(param);

            if (checkInternetConnection(RateAndReview.this)) {
                showLoading();
                ServerAPI.getInstance().getOneProductReviews(ApiServerResponse.ONE_PRODUCT_REVIEWS,Session.get_userId(pref), product_id.trim(), this);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        try {

            if (id == R.id.bt_submit_review) {

                //Log.e("RatingBAR", "" + ratingBar.getRating());
                Utility.hideSoftKeyboard(RateAndReview.this);
                if (checkInternetConnection(RateAndReview.this)) {
                    if (Session.get_login_statuc(pref)) {
                        if (ratingBar.getRating() == 0) {
                            ToastMsg.showShortToast(RateAndReview.this, getString(R.string.provide_a_rating));
                        } else {
                            String reviewMessage = review_message.getText().toString().trim();
                            showLoading();
                            ServerAPI.getInstance().rateProduct(ApiServerResponse.RATE_PRODUCT, user_id, String.valueOf(ratingBar.getRating()), product_id, reviewMessage, this);
                            //new RatingManager(RateAndReview.this, this).sendRequest(user_id, ratingBar.getRating(), product_id, reviewMessage);
                        }
                    } else {
                        ToastMsg.showShortToast(RateAndReview.this, getString(R.string.login_to_continuew));
                    }

                } else {
                    showAlertDialog(RateAndReview.this);
                }


                   /* if (review_message.getText().toString().trim().length() == 0) {

                    } else {
                        String reviewMessage = review_message.getText().toString().trim();
                    }*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* @Override
    public void onRatingSuccess(String message) {
        ToastMsg.showShortToast(RateAndReview.this, "Rating & review saved");
        finish();
    }*/

/*
    @Override
    public void onRatingError(String error) {
        ToastMsg.showShortToast(RateAndReview.this, error);
    }

    @Override
    public void onSuccess(String response) {
        Log.i("RATEREVIEW RESP ", response);
        //ToastMsg.showLongToast(RateAndReview.this, response);
        try {
            reviewsBeanArrayList = new ArrayList<>();
            userInfoBeanArrayList = new ArrayList<>();
            JSONArray productReviews = new JSONArray(response);
            System.out.println("product wishlist *** " + productReviews);
            //ArrayList<String> reviews = new ArrayList<String>();
            //ToastMsg.showLongToast(RateAndReview.this, "" + productReviews.length());
            for (int i = 0; i < productReviews.length(); i++) {

                JSONObject jsonObject = productReviews.getJSONObject(i);
                //RateAndReviewModel rateAndReviewModel = new RateAndReviewModel();
                Log.d("", "review id " + jsonObject.getString(Constant.ID));
                RateAndReviewModel.ReviewsBean reviewsBean = new RateAndReviewModel.ReviewsBean();
                reviewsBean.setCreated_at(jsonObject.getString(Constant.CREATED_AT));
                reviewsBean.setRating(jsonObject.getString(Constant.RATING));
                reviewsBean.setReview(jsonObject.getString(Constant.REVIEW));
                */
/*reviewsBean.setUser_info(jsonObject.getJSONObject(Constant.USER_INFO));*//*

                reviewsBeanArrayList.add(reviewsBean);
                JSONObject jsonObjectUserInfo = jsonObject.getJSONObject(Constant.USER_INFO);
                RateAndReviewModel.ReviewsBean.UserInfoBean userInfoBean = new RateAndReviewModel.ReviewsBean.UserInfoBean();
                userInfoBean.setFirst_name(jsonObjectUserInfo.getString(Constant.FIRST_NAME));
                userInfoBean.setLast_name(jsonObjectUserInfo.getString(Constant.LAST_NAME));
                userInfoBeanArrayList.add(userInfoBean);
                //rateAndReviewModel.setReviews();

            }

            RateAndReviewAdapter rateAndReviewAdapter = new RateAndReviewAdapter(RateAndReview.this, reviewsBeanArrayList, userInfoBeanArrayList);
            rv_reviews.setAdapter(rateAndReviewAdapter);
            rv_reviews.setLayoutManager(new LinearLayoutManager(this));


            if (pendingIntroAnimation) {
                pendingIntroAnimation = false;
                startIntroAnimation();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
*/

  /*  @Override
    public void onError(String error) {
        Log.i("RATEREVIEW RESP ", error);
        ToastMsg.showLongToast(RateAndReview.this, error);
    }
*/

    private void startIntroAnimation() {
        rv_reviews.setTranslationY(rv_reviews.getHeight());
        rv_reviews.setAlpha(0f);
        rv_reviews.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();


    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            RateAndReviewModel rateAndReviewModel;
            RateProductModal rateProductModal;

            switch (tag) {

                case ApiServerResponse.ONE_PRODUCT_REVIEWS:

                    rateAndReviewModel = (RateAndReviewModel) response.body();

                    if (rateAndReviewModel.getStatus().equalsIgnoreCase(Constant.OK)) {
                        reviewsBeanArrayList = rateAndReviewModel.getReviews();
                        RateAndReviewAdapter rateAndReviewAdapter = new RateAndReviewAdapter(RateAndReview.this, reviewsBeanArrayList);
                        rv_reviews.setAdapter(rateAndReviewAdapter);
                        rv_reviews.setLayoutManager(new LinearLayoutManager(this));


                        if (pendingIntroAnimation) {
                            pendingIntroAnimation = false;
                            startIntroAnimation();
                        }
                    } else {

                        ToastMsg.showShortToast(RateAndReview.this, getResources().getString(R.string.no_reviews_found));
                    }

                    hideLoading();
                    break;

                case ApiServerResponse.RATE_PRODUCT:
                    rateProductModal = (RateProductModal) response.body();
                    if (rateProductModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                        ToastMsg.showShortToast(RateAndReview.this, rateProductModal.getMessage());
                        finish();
                    } else {
                        ToastMsg.showShortToast(RateAndReview.this, rateProductModal.getMessage());
                    }

                    hideLoading();
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {

            ToastMsg.showShortToast(RateAndReview.this, throwable.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
