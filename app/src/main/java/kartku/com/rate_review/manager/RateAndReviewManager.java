package kartku.com.rate_review.manager;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kartku.com.product_detail.api.ProductDetailApi;
import kartku.com.product_detail.manager.ProductDetailManager;
import kartku.com.rate_review.api.RateAndReviewApi;
import kartku.com.rate_review.listener.RateAndReviewListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;

/**
 * Created by Kshitiz Bali on 12/28/2016.
 */

public class RateAndReviewManager {

    private Context mContext;
    private RateAndReviewListener mRateAndReviewListener;

    public RateAndReviewManager(Context context, RateAndReviewListener rateAndReviewListener) {
        mContext = context;
        mRateAndReviewListener = rateAndReviewListener;

    }

    public void sendRequest(String params) {
        try {
            new RateAndReviewApi(mContext, RateAndReviewManager.this).sendRequest(API.PRODUCT_REVIEW_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                try {
                    JSONArray product_reviews = new JSONArray(res_object.getString(Constant.REVIEWS));
                    /*JSONObject product_reviews = new JSONObject(res_object.getString(Constant.REVIEWS));*/
                    mRateAndReviewListener.onSuccess("" + product_reviews);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    String message = res_object.getString(Constant.REVIEWS);
                    mRateAndReviewListener.onError(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onError(String message) {

        mRateAndReviewListener.onError(message);
    }
}
