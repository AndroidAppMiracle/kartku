package kartku.com.rate_review.api;

import android.content.Context;

import kartku.com.rate_review.manager.RateAndReviewManager;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by Kshitiz Bali on 12/28/2016.
 */

public class RateAndReviewApi implements CustomResponse {

    private Context mContext;
    private RateAndReviewManager mRateAndReviewManager;
    private RequestServer requestServer;

    public RateAndReviewApi(Context context, RateAndReviewManager rateAndReviewManager) {
        this.mContext = context;
        this.mRateAndReviewManager = rateAndReviewManager;

    }


    @Override
    public void onCustomResponse(String response) {
        mRateAndReviewManager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        mRateAndReviewManager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId, String params) {
        try {
            requestServer = new RequestServer(mContext, requestId, this, params);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
