package kartku.com.rate_review.model;

import java.util.List;

/**
 * Created by Kshitiz Bali on 12/30/2016.
 */

public class RateAndReviewModel {

    /**
     * status : OK
     * reviews : [{"id":8,"user_id":271,"user_info":{"user_id":"271","email":"ishita@gmail.com","first_name":"Ishita","last_name":"Bathla","mobile_number":"9876543210","device_id":"dZ9ZjAn0rDs:APA91bE81uKiJ5zBJYcp-ZPIhQ_FP9VByAAab8Amb4P3kaPfKcM3w2QOyuykdPX2V4ZgMtjiWbI71eCuTwVJDfUbxPOUNbQWFC_AJi6pEzUsrYP-yIoyz6Vyf5Qjtwl8aYcNGQkto3F_","device_type":"1","role":"10"},"product_id":41,"rating":"3.5","review":"","created_at":"2017-04-10 06:42:47"},{"id":9,"user_id":271,"user_info":{"user_id":"271","email":"ishita@gmail.com","first_name":"Ishita","last_name":"Bathla","mobile_number":"9876543210","device_id":"dZ9ZjAn0rDs:APA91bE81uKiJ5zBJYcp-ZPIhQ_FP9VByAAab8Amb4P3kaPfKcM3w2QOyuykdPX2V4ZgMtjiWbI71eCuTwVJDfUbxPOUNbQWFC_AJi6pEzUsrYP-yIoyz6Vyf5Qjtwl8aYcNGQkto3F_","device_type":"1","role":"10"},"product_id":41,"rating":"3.5","review":"","created_at":"2017-04-10 06:51:59"},{"id":11,"user_id":271,"user_info":{"user_id":"271","email":"ishita@gmail.com","first_name":"Ishita","last_name":"Bathla","mobile_number":"9876543210","device_id":"dZ9ZjAn0rDs:APA91bE81uKiJ5zBJYcp-ZPIhQ_FP9VByAAab8Amb4P3kaPfKcM3w2QOyuykdPX2V4ZgMtjiWbI71eCuTwVJDfUbxPOUNbQWFC_AJi6pEzUsrYP-yIoyz6Vyf5Qjtwl8aYcNGQkto3F_","device_type":"1","role":"10"},"product_id":41,"rating":"3.5","review":"","created_at":"2017-04-10 06:56:03"}]
     */

    private String status;
    private List<ReviewsBean> reviews;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ReviewsBean> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewsBean> reviews) {
        this.reviews = reviews;
    }

    public static class ReviewsBean {
        /**
         * id : 8
         * user_id : 271
         * user_info : {"user_id":"271","email":"ishita@gmail.com","first_name":"Ishita","last_name":"Bathla","mobile_number":"9876543210","device_id":"dZ9ZjAn0rDs:APA91bE81uKiJ5zBJYcp-ZPIhQ_FP9VByAAab8Amb4P3kaPfKcM3w2QOyuykdPX2V4ZgMtjiWbI71eCuTwVJDfUbxPOUNbQWFC_AJi6pEzUsrYP-yIoyz6Vyf5Qjtwl8aYcNGQkto3F_","device_type":"1","role":"10"}
         * product_id : 41
         * rating : 3.5
         * review :
         * created_at : 2017-04-10 06:42:47
         */

        private int id;
        private int user_id;
        private UserInfoBean user_info;
        private int product_id;
        private String rating;
        private String review;
        private String created_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public static class UserInfoBean {
            /**
             * user_id : 271
             * email : ishita@gmail.com
             * first_name : Ishita
             * last_name : Bathla
             * mobile_number : 9876543210
             * device_id : dZ9ZjAn0rDs:APA91bE81uKiJ5zBJYcp-ZPIhQ_FP9VByAAab8Amb4P3kaPfKcM3w2QOyuykdPX2V4ZgMtjiWbI71eCuTwVJDfUbxPOUNbQWFC_AJi6pEzUsrYP-yIoyz6Vyf5Qjtwl8aYcNGQkto3F_
             * device_type : 1
             * role : 10
             */

            private String user_id;
            private String email;
            private String first_name;
            private String last_name;
            private String mobile_number;
            private String device_id;
            private String device_type;
            private String role;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getMobile_number() {
                return mobile_number;
            }

            public void setMobile_number(String mobile_number) {
                this.mobile_number = mobile_number;
            }

            public String getDevice_id() {
                return device_id;
            }

            public void setDevice_id(String device_id) {
                this.device_id = device_id;
            }

            public String getDevice_type() {
                return device_type;
            }

            public void setDevice_type(String device_type) {
                this.device_type = device_type;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }
        }
    }
}
