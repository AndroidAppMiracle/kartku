package kartku.com.utils;

/**
 * Created by satoti.garg on 9/9/2016.
 */
public class RequestConstants {

    // register -   email,first_name,last_name,mobile,password,device_id,device_type
    // login -   email,password, device_id, device_type
    // social_media_login -   email, device_id, device_type

    public static String EMAIL = "email";
    public static String FIRST_NAME = "first_name";
    public static String LAST_NAME = "last_name";
    public static String MOBILE = "mobile";
    public static String PASSWORD = "password";
    public static String DEVICE_ID = "device_id";
    public static String DEVICE_TYPE = "device_type";
    public static String CONFIRM_PASSWORD = "confirm_password";

    // update user profile info for seller also
   // update_user - user_id,first_name,last_name,company_name,mobile_number,,identity_img,birth_month,birth_year,
    //pincode,pickup_address,bank_name,account_holder,account_no,branch_name,business_type

    public static String USERID = "user_id";
    public static String PROFILE_FIRST_NAME = "Profile[first_name]";
    public static String PROFILE_LAST_NAME = "Profile[last_name]";
    public static String PROFILE_USER_ID= "Profile[user_id]";
    public static String PROFILE_MOBILE_NUMBER = "Profile[mobile_number]";
    public static String PROFILE_Address= "Profile[address]";

    public static String IDENTITY_IMAGE = "identity_img";
    public static String COMPANY_NAME = "company_name";
    public static String BIRTH_MONTH = "birth_month";
    public static String BIRTH_YEAR = "birth_year";
    public static String PINCODE = "pincode";
    public static String PICKUP_ADDRESS = "pickup_address";
    public static String BANK_NAME = "bank_name";
    public static String ACCOUNT_HOLDER = "account_holder";
    public static String ACCOUNT_NUMBER = "account_no";
    public static String BRANCH_NAME = "branch_name";
    public static String BUSINESS_TYPE = "business_type";
    public static String ADDRESS = "address";

    // no params for home_banners, main_categories

    // product_detail -  product_id
    // product_review -  product_id
     public static String PRODUCTID = "product_id";

    // cart_detail - cart_id, user_id
    public static String CART_ID = "cart_id";
    //public static String USER_ID = "user_id";

    // product_search -

    // add_to_cart - user_id,total_amount,product_id,quantity, cart_id
    // update_cart - cart_id, user_id, total_amount,product_id,quantity
    // cart_delete - cart_id, user_id

    // delete_item  - item_id
    public static String ITEM_ID = "item_id";

    // add_to_wishlist -  user_id, product_id

    //public static String USER_ID = "user_id";
    public static String TOTAL_AMOUNT = "total_amount";
   // public static String PRODUCT_ID = "product_id";
    public static String QUANTITY = "quantity";
   // public static String CART_ID = "cart_id";


    // change_password - user_id ,current_password ,new_password
    //public static String USER_ID = "user_id";
     public static String CURRENT_PASSWORD = "current_password";
     public static String NEW_PASSWORD = "new_password";

    // category_products - category_id
    public static String CATEGORY_ID = "category_id";

    // order_detail -  order_id
    //public static String ORDER_ID = "order_id";

    // product_rewiew_ratting - user_id,  product_id, ratting, reviews
   // public static String USER_ID = "user_id";
   // public static String PRODUCT_ID = "product_id";
    public static String RATING = "ratting";
    public static String REVIEWS = "reviews";

    //order_history  order_id
    public static String ORDER_ID = "order_id";

    // wishlist  -  user_id, product_id ,remove from wishlist
    public static String USER_ID = "user_id";
    public static String PRODUCT_ID = "product_id";

    // forgot_password - email
   // public static String EMAIL = "email";

    // contact_us - name, email,subject,message
    public static String NAME = "name";
   // public static String EMAIL = "email";
    public static String SUBJECT = "subject";
    public static String MESSAGE = "message";

    public static String POST_URL = "POST";
    public static String GET_URL = "GET";

    // remove_from_wishlist post api wishlist_id params
   // public static String WISHLIST_ID = "wishlist_id";

    // brand_listing get params none
    // material_listing params none

    // search brand or material  ?query=Furniture&brnd2=Woodsworth
}
