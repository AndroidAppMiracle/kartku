package kartku.com.utils;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public class ResponseKeys {

    public static String CATEGORIES_INFO = "categories_info";
    public static String TITLE = "title";
    public static String BANNER_INFO = "banner_info";
    public static String URL = "url";
    public static String CATEGORY_IMAGE = "category_image";
    public static String SUBCAT_INFO = "subcat_info";
    public static String SUBCAT_NAME = "subcat_name";
    public static String SUBCAT_ID = "subcat_id";
    public static String SUB_SUB_CAT_INFO = "sub_subcat_info";

    public static String SUB_SUB_CAT_NAME = "sub_subcat_name";
    public static String SUB_SUB_CAT_ID = "sub_subcat_id";
}
