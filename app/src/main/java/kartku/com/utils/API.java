package kartku.com.utils;

/**
 * Created by satoti.garg on 9/9/2016.
 */
public class API {

    //url http://dev.miracleglobal.com/kartku-php/api/user_register

    /*public static String HOST_URL = "http://dev.miracleglobal.com/kartku-php/api/";*/
    /*public static String HOST_URL = "http://dev.miracleglobal.com/kartku-php/apistable/";*/
    //public static String HOST_URL = "http://dev.miracleglobal.com/kartku-php/apidev/";
    public static String HOST_URL = "http://kartku.com/api/";

    // get APi's

    public static String HOME_BANNERS = "home_banners";
    public static int BANNERS_REQUEST_ID = 3;

    public static String MAIN_CATEGORIES = "main_categories";
    public static int MAIN_CATEGORIES_REQUEST_ID = 4;

    public static String PRODUCT_DETAIL = "product_detail";
    public static int PRODUCT_DETAIL_REQUEST_ID = 10;

    public static String PRODUCT_REVIEW = "product_review";
    public static int PRODUCT_REVIEW_REQUEST_ID = 22;

    public static String CART_DETAIL = "cart_detail";
    public static int CART_DETAIL_REQUEST_ID = 19;

    public static String PRODUCT_SEARCH = "product_search";
    public static int PRODUCT_SEARCH_REQUEST_ID = 23;

    public static String BRAND_LISTING = "brand_listing";
    public static int BRAND_LISTING_REQUEST_ID = 25;


    public static String MATERIAL_LISTING = "material_listing";
    public static int MATERIAL_LISTING_REQUEST_ID = 26;


    public static String HIGH_LOW_PRICE = "product_search";
    public static int HIGH_LOW_PRICE_REQUEST_ID = 27;


    public static String LOW_HIGH_PRICE = "product_search";
    public static int LOW_HIGH_PRICE_REQUEST_ID = 28;


    public static String NEW_ARRIVALS = "new_arrivals";
    public static int NEW_ARRIVALS_REQUEST_ID = 29;


    // post Api's
    public static String USER_LOGIN = "user_login";
    public static int LOGIN_REQUEST_ID = 1;

    public static String USER_REGISTER = "user_register";
    public static int REGISTER_REQUEST_ID = 2;

    public static String CONTACT_US = "contact_us";
    public static int CONTACT_REQUEST_ID = 5;


    public static String UPDATE_USER = "update_user";
    public static int UPDATE_USER_REQUEST_ID = 6;

    public static String CHANGE_PASSWORD = "change_password";
    public static int CHANGE_PASSWORD_REQUEST_ID = 7;

    public static String FORGOT_PASSWORD = "forget_password";
    public static int FORGOT_PASSWORD_PASSWORD_REQUEST_ID = 8;

    public static String CATEGORY_PRODUCTS = "category_products";
    public static int CATEGORY_PRODUCTS_REQUEST_ID = 9;

    public static String SOCIAL_MEDIA_LOGIN = "social_media_login";
    public static int SOCIAL_LOGIN_REQUEST_ID = 11;

    public static String ORDER_DETAIL = "order_detail";
    public static int ORDER_DETAIL_REQUEST_ID = 12;

    public static String ORDER_HISTORY = "order_history";
    public static int ORDER_HISTORY_REQUEST_ID = 13;

    public static String WISHLIST = "wishlist";
    public static int WISHLIST_REQUEST_ID = 14;

    public static String ADD_TO_WISHLIST = "add_to_wishlist";
    public static int ADD_TO_WISHLIST_REQUEST_ID = 15;

    public static String ADD_TO_CART = "add_to_cart";
    public static int ADD_TO_CART_REQUEST_ID = 16;

    public static String UPDATE_CART = "update_cart";
    public static int UPDATE_CART_REQUEST_ID = 17;

    public static String CART_DELETE = "delete_cart";
    public static int CART_DELETE_REQUEST_ID = 18;

    public static String DELETE_ITEM = "delete_item";
    public static int DELETE_ITEM_REQUEST_ID = 20;

    public static String PRODUCT_REVIEW_RATING = "product_rewiew_ratting";
    public static int PRODUCT_REVIEW_RATING_REQUEST_ID = 21;

    public static String REMOVE_FROM_WISHLIST = "remove_from_wishlist";
    public static int REMOVE_FROM_WISHLIST_REQUEST_ID = 24;

    public static String CART_ITEMS = "cart_items";
    public static int CART_ITEMS_REQUEST_ID = 30;


    public static String MY_ORDERS = "my_orders";
    public static int MY_ORDERS_REQUEST_ID = 31;

    public static String MY_PRODUCT_REVIEWS = "my_product_reviews";
    public static int MY_PRODUCT_REVIEWS_REQUEST_ID = 32;





}
