package kartku.com.utils;

/**
 * Created by satoti.garg on 9/7/2016.
 */
public class Constant {

    public static int SPLASH_TIME_OUT = 3000;
    public static String STATUS = "status";
    public static String STATUS1 = "Status";
    public static String OK = "OK";
    public static String NOK = "NOK";
    public static String MESSAGE = "message";
    public static String INFO = "info";
    public static String USER_DATA = "user_data";
    public static String USER_ID = "user_id";
    public static String USER_PROFILE_DATA = "user_profile_data";
    public static String USERNAME = "username";

    public static String EMAIL = "email";
    public static String PROFILE_IMAGE_PATH = "profile_image_path";
    public static String FIRST_NAME = "first_name";
    public static String LAST_NAME = "last_name";
    public static String ADDRESS = "address";
    public static String MOBILE_NUMBER = "mobile_number";

    public static String PRODUCTS_DATA = "products_data";
    public static String PRODUCT_INFO = "product_info";

    public static String POSITION = "position";

    public static String ORDER_DETAIL = "order_detail";
    public static String ORDER_HISTORY = "order_History";


    public static String ID = "id";
    public static String TITTLE = "title";
    public static String DESCRIPTION = "description";
    public static String IMAGE_URL = "product_image";
    public static String PRICE = "products_price";
    public static String BRAND = "products_brand";
    public static String SELECTED_CATEGORY_INDEX = "category_index";


    public static String IMAGE = "image";
    public static String NAME = "name";
    public static String PRICE_KEY = "price";

    public static String RESULT = "result";
    public static String ITEM_INFO = "item_info";
    public static String CART_ID = "cart_id";

    public static String BRAND_INFO = "brand_info";
    public static String MATERIAL_INFO = "material_info";

    public static String WISHLIST = "wishlist";

    public static String REVIEWS = "reviews";

    public static String PRODUCT_NAME = "product_name";
    public static String PRODUCT_PRICE = "product_price";

    public static String CREATED_AT = "created_at";

    public static String RATING = "rating";

    public static String REVIEW = "review";

    public static String USER_INFO = "user_info";

    public static String MATERIAl = "product_material";

    public static String RESULTS = "results";

    public static String GRAND_TOTAL = "grand_total";
    public static String TOTAL_AMOUNT = "total_amount";

    public static String PRODUCT_TITLE = "product_title";

    public static String ORDER_ID = "order_id";
    public static String ORDER_STATUS = "order_status";
    public static String TAX = "tax";
    public static String ROLE = "role";

    public static String SHIPPING_COST = "shipping_cost";
    public static String ROLE_USER = "10";
    public static String ROLE_SELLER = "20";
    public static String CATEGORY_OPTIONS = "category_options";
    public static String CATEGORY_IMAGES = "category_images";
    public static String CATEGORY_ALL = "category_all";

    public static String CLIENT_KEY = "VT-server-aK7sofB0UpEKon5nYVzCxQh0";

    public static String CATEGORY_ID = "category_id";
    public static String CATEGORY_NAME = "category_name";
    public static String HOME_BANNER = "home_banner";
    public static String PAYMENT_PENDING = "PAYMENT_PENDING";
    public static String PICKUP_CITY = "pickup_city";
    public static String PICKUP_CITY_CODE = "pickup_city_code";
    public static String DEST_CITY = "dest_city";
    public static String DEST_CITY_CODE = "dest_city_code";

    public static String PENDING = "PENDING";
    public static String ORDER_CONFIRMATION_STATUS = "";
    public static String SUCCESS = "SUCCESS";
    public static String FAILED = "FAILED";
    public static String CANCELLED = "CANCELLED";
    public static String WEB_ADDRESS = "web_address";
    public static String WEB_PAYMENT_SUCCESS = "checkout/finish";
    public static String WEB_PAYMENT_FAILED = "checkout/error";
    public static String WEB_PAYMENT_CANCEL = "checkout/cancel";

    public static String PRIVACY_POLICY_URL = "http://kartku.com/site/terms?id=15";
    public static String ABOUT_US_URl = "http://kartku.com/site/terms?id=1";
    public static String TERMS_CONDITIONS = "http://kartku.com/site/terms?id=17";
    public static String PAYMENT_URL = "http://kartku.com/checkout/snap?order_id=";
    public static String SHIPPING_URL = "http://kartku.com/product/order/paymentapp?orderId=";

    // URL links
    public static String FAQ_URL = "http://kartku.com/site/terms?id=16";

    public static String LANGUAGE_ENGLISH = "ENGLISH";

    public static String LANGUAGE_INDONESIAN = "INDONESIAN";

}

