package kartku.com.utils.prefrences;

import android.content.SharedPreferences;

/**
 * Created by satoti on 9/12/216
 */
public class Session {

    public static String PREFERENCES = "SharedPrefrences";

    // save main_categories api response
    public static String main_categories_key = "main_categories";
    public static String main_categories_value = "";

    public static void set_main_categories_res(SharedPreferences pref, String main_categories) {
        pref.edit().putString(Session.main_categories_key, main_categories).apply();
    }

    public static String get_main_categories_res(SharedPreferences pref) {
        return pref.getString(Session.main_categories_key, Session.main_categories_value);
    }

    public static String login_flag = "login_status";
    public static boolean login_flag_value = false;
    public static String login_type = "login_type";
    public static String login_type_def_value = "email";

    public static void set_login_status(SharedPreferences pref, boolean login_status) {
        pref.edit().putBoolean(Session.login_flag, login_status).apply();
    }

    public static boolean get_login_statuc(SharedPreferences pref) {
        return pref.getBoolean(Session.login_flag, Session.login_flag_value);
    }

    public static String getLogin_type(SharedPreferences pref) {
        return pref.getString(Session.login_type, Session.login_type_def_value);
    }

    public static void setLogin_type(SharedPreferences pref, String login_type) {
        pref.edit().putString(Session.login_type, login_type).apply();
    }

    public static String loginTypeButton = "login_user_button";
    public static String loginTypeButtonDefValue = "email";

    public static void setLoginTypeButton(SharedPreferences pref, String loginTypeButton) {
        pref.edit().putString(Session.loginTypeButton, loginTypeButton).apply();
    }

    public static String getLoginTypeButton(SharedPreferences pref) {
        return pref.getString(Session.loginTypeButton, Session.loginTypeButtonDefValue);
    }


    // save user id generated in user registeration response.
    public static String user_id = "user_id";
    public static String user_id_value = "";

    public static void set_userId(SharedPreferences pref, String user_id) {
        pref.edit().putString(Session.user_id, user_id).apply();
    }

    public static String get_userId(SharedPreferences pref) {
        return pref.getString(Session.user_id, Session.user_id_value);
    }

    // save login user details

    // save user id generated in user registeration response.
    public static String login_user_details = "user_details";
    public static String login_user_details_value = "";


    public static void setlogin_user_details(SharedPreferences pref, String user_details) {
        pref.edit().putString(Session.login_user_details, user_details).apply();
    }

    public static String getlogin_user_details(SharedPreferences pref) {
        return pref.getString(Session.login_user_details, Session.login_user_details_value);
    }


    // save cart id generated in user registeration response.
    public static String cart_id = "cart_id";
    public static String cart_id_value = "0";

    public static void setcart_id(SharedPreferences pref, String cart_id) {
        pref.edit().putString(Session.cart_id, cart_id).apply();
    }

    public static String getcart_id(SharedPreferences pref) {
        return pref.getString(Session.cart_id, Session.cart_id_value);
    }

    // selected category id
    // save cart id generated in user registeration response.
    public static String selected_category_id = "category_id";
    public static String selected_category_id_value = "";

    public static void set_selected_category_id(SharedPreferences pref, String category_id) {
        pref.edit().putString(Session.selected_category_id, category_id).apply();
    }

    public static String get_selected_category_id(SharedPreferences pref) {
        return pref.getString(Session.selected_category_id, Session.selected_category_id_value);
    }

    public static String selected_product_id = "product_id";
    public static String selected_product_id_value = "";

    public static void set_selected_product_id(SharedPreferences pref, String product_id) {
        pref.edit().putString(Session.selected_product_id, product_id).apply();
    }

    public static String get_selected_product_id(SharedPreferences pref) {
        return pref.getString(Session.selected_product_id, Session.selected_product_id_value);
    }


    public static String selected_category_index = "selected_category_id";
    public static String selected_category_index_value = "";

    public static void set_selected_category_index(SharedPreferences pref, String category_index) {
        pref.edit().putString(Session.selected_category_index, category_index).apply();
    }

    public static String get_selected_category_index(SharedPreferences pref) {
        return pref.getString(Session.selected_category_index, Session.selected_category_index_value);
    }


    public static String google_signin_user_profile_pic = "google_display_pic";
    public static String google_signin_user_profile_pic_value = "";

    public static void set_google_signin_user_profile_pic(SharedPreferences pref, String google_display_pic) {
        pref.edit().putString(Session.google_signin_user_profile_pic, google_display_pic).apply();
    }

    public static String get_google_signin_user_profile_pic(SharedPreferences pref) {
        return pref.getString(Session.google_signin_user_profile_pic, Session.google_signin_user_profile_pic_value);
    }

    public static String google_signin_emailid = "google_email_id";
    public static String google_signin_emailid_value = "";

    public static void setGooglEmailID(SharedPreferences pref, String googleEmailId) {
        pref.edit().putString(Session.google_signin_emailid, googleEmailId).apply();
    }

    public static String getGooglEmailID(SharedPreferences pref) {
        return pref.getString(Session.google_signin_emailid, Session.google_signin_emailid_value);
    }

    public static String google_signin_name = "google_name";
    public static String google_signin_name_value = "";

    public static void setGooglName(SharedPreferences pref, String googleName) {
        pref.edit().putString(Session.google_signin_name, googleName).apply();
    }

    public static String getGooglName(SharedPreferences pref) {
        return pref.getString(Session.google_signin_name, Session.google_signin_name_value);
    }


    public static String facebook_signin_user_profile_pic = "facebook_display_pic";
    public static String facebook_signin_user_profile_pic_value = "";

    public static void set_facebook_signin_user_profile_pic(SharedPreferences pref, String facebook_display_pic) {
        pref.edit().putString(Session.facebook_signin_user_profile_pic, facebook_display_pic).apply();
    }

    public static String get_facebook_signin_user_profile_pic(SharedPreferences pref) {
        return pref.getString(Session.facebook_signin_user_profile_pic, Session.facebook_signin_user_profile_pic_value);
    }

    public static String facebookName = "facebook_name";
    public static String facebookNameValue = "";

    public static void set_facebookName(SharedPreferences pref, String facebook_name) {
        pref.edit().putString(Session.facebookName, facebook_name).apply();
    }

    public static String get_facebookName(SharedPreferences pref) {
        return pref.getString(Session.facebookName, Session.facebookNameValue);
    }


    public static String facebookEmail = "facebook_email";
    public static String facebookEmailValue = "";

    public static void set_facebookEmail(SharedPreferences pref, String facebook_email) {
        pref.edit().putString(Session.facebookEmail, facebook_email).apply();
    }

    public static String get_facebookEmail(SharedPreferences pref) {
        return pref.getString(Session.facebookEmail, Session.facebookEmailValue);
    }

    public static String selectedCategory = "selected_category";
    public static String selectedCategoryValue = "";

    public static void setselectedCategory(SharedPreferences pref, String selectedCategory) {
        pref.edit().putString(Session.selectedCategory, selectedCategory).apply();
    }

    public static String getselectedCategory(SharedPreferences pref) {
        return pref.getString(Session.selectedCategory, Session.selectedCategoryValue);
    }


    public static String selectedSubCategory = "selected_subcategory";
    public static String selectedSubCategoryValue = "";

    public static void setselectedSubCategory(SharedPreferences pref, String selectedSubCategory) {
        pref.edit().putString(Session.selectedSubCategory, selectedSubCategory).apply();
    }

    public static String getselectedSubCategory(SharedPreferences pref) {
        return pref.getString(Session.selectedSubCategory, Session.selectedSubCategoryValue);
    }


    public static String selectedSubCategoryID = "selected_subcategory_id";
    public static String selectedSubCategoryIDValue = "";

    public static void setselectedSubCategoryID(SharedPreferences pref, String selectedSubCategoryID) {
        pref.edit().putString(Session.selectedSubCategoryID, selectedSubCategoryID).apply();
    }

    public static String getselectedSubCategoryID(SharedPreferences pref) {
        return pref.getString(Session.selectedSubCategoryID, Session.selectedSubCategoryIDValue);
    }


    public static String selectedSub_SubCategory = "selected_sub_subcategory";
    public static String selectedSub_SubCategoryValue = "";

    public static void setSelectedSub_SubCategory(SharedPreferences pref, String selectedSub_SubCategory) {
        pref.edit().putString(Session.selectedSub_SubCategory, selectedSub_SubCategory).apply();
    }

    public static String getSelectedSub_SubCategory(SharedPreferences pref) {
        return pref.getString(Session.selectedSub_SubCategory, Session.selectedSub_SubCategoryValue);
    }


    public static String selectedSub_SubCategoryJSON = "selected_sub_subcategory_json";
    public static String selectedSub_SubCategoryJSONValue = "";

    public static void setselectedSub_SubCategoryJSON(SharedPreferences pref, String selectedSub_SubCategoryJSON) {
        pref.edit().putString(Session.selectedSub_SubCategoryJSON, selectedSub_SubCategoryJSON).apply();
    }

    public static String getselectedSub_SubCategoryJSON(SharedPreferences pref) {
        return pref.getString(Session.selectedSub_SubCategoryJSON, Session.selectedSub_SubCategoryJSONValue);
    }


    private static String userRoleType = "role";
    private static String userRoleTypeValue = "";


    public static void setUserRoleType(SharedPreferences pref, String selectedUserRoleType) {
        pref.edit().putString(Session.userRoleType, selectedUserRoleType).apply();
    }

    public static String getUserRoleType(SharedPreferences pref) {
        return pref.getString(Session.userRoleType, Session.userRoleTypeValue);
    }


    public static String cartItemsQuantity = "cart_quantity";
    public static String cartItemsQuantityValue = "";


    public static void setCartItemsQuantity(SharedPreferences pref, String cart_quantity) {
        pref.edit().putString(Session.cartItemsQuantity, cart_quantity).apply();
    }

    public static String getCartItemsQuantity(SharedPreferences pref) {
        return pref.getString(Session.cartItemsQuantity, Session.cartItemsQuantityValue);
    }

    public static String homeScreenBanner = "home_screen_banner";
    public static String homeScreenBannerValue = "";

    public static void setHomeScreenBanner(SharedPreferences pref, String home_banner) {
        pref.edit().putString(Session.homeScreenBanner, home_banner).apply();
    }

    public static String getHomeScreenBanner(SharedPreferences pref) {
        return pref.getString(Session.homeScreenBanner, Session.homeScreenBannerValue);
    }


    public static String firstName = "first_name";
    public static String firstNameValue = "";

    public static void setFirstName(SharedPreferences pref, String firstName) {
        pref.edit().putString(Session.firstName, firstName).apply();
    }

    public static String getFirstName(SharedPreferences pref) {
        return pref.getString(Session.firstName, Session.firstNameValue);
    }


    public static String lastName = "last_name";
    public static String lastNameValue = "";

    public static void setLastName(SharedPreferences pref, String lastName) {
        pref.edit().putString(Session.lastName, lastName).apply();

    }

    public static String getLastName(SharedPreferences pref) {
        return pref.getString(Session.lastName, Session.lastNameValue);
    }


    public static String address = "address";
    public static String addressValue = "";

    public static void setAddress(SharedPreferences pref, String address) {
        pref.edit().putString(Session.address, address).apply();

    }

    public static String getAddress(SharedPreferences pref) {
        return pref.getString(Session.address, Session.addressValue);
    }


    public static String email = "emailID";
    public static String emailValue = "";

    public static void setUserEmail(SharedPreferences pref, String email) {
        pref.edit().putString(Session.email, email).apply();

    }

    public static String getUserEmail(SharedPreferences pref) {
        return pref.getString(Session.email, Session.emailValue);
    }


    public static String mobile = "user_mobile";
    public static String mobileValue = "";

    public static void setUserMobile(SharedPreferences pref, String mobile) {
        pref.edit().putString(Session.mobile, mobile).apply();

    }

    public static String getUserMobile(SharedPreferences pref) {
        return pref.getString(Session.mobile, Session.mobileValue);
    }


    public static String fcmToken = "fcm_token";
    public static String fcmTokenValue = "";


    public static void setSessionToken(SharedPreferences pref, String sessionToken) {
        pref.edit().putString(Session.fcmToken, sessionToken).apply();
    }

    public static String getSessionToken(SharedPreferences pref) {
        return pref.getString(Session.fcmToken, Session.fcmTokenValue);
    }


    public static String appLanguage = "app_language";
    public static String appLanguageValue = "1";

    public static void setAppLanguage(SharedPreferences pref, String selectedLanguage) {
        pref.edit().putString(Session.appLanguage, selectedLanguage).apply();
    }

    public static String getAppLanguage(SharedPreferences pref) {
        return pref.getString(Session.appLanguage, Session.appLanguageValue);
    }




    /*public static String selected_product_id_details = "product_id";
    public static String selected_product_id_details_value = "";

    public static void set_selected_product_id_details(SharedPreferences pref, String product_id) {
        pref.edit().putString(Session.selected_product_id_details, product_id).apply();
    }

    public static String get_selected_product_id_details(SharedPreferences pref) {
        return pref.getString(Session.selected_product_id_details, Session.selected_product_id_details_value);
    }*/


}
