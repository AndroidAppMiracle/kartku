package kartku.com.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.SubProductCategoryDetailAdapter;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.ProductCategoryModal;
import kartku.com.retrofit.modal.SubProductCategoryListModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 4/20/2017.
 */

public class SubProductCategoryListFragment extends Fragment implements ApiServerResponse {
    private SharedPreferences prefrence;
    ImageView iv_category_banner_image;
    TextView tv_shop_by_category_label;
    List<SubProductCategoryListModal.SubcategoryDetailBean.SubcategoriesBean> list = new ArrayList<>();
    RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }*/
        setHasOptionsMenu(true);
        prefrence = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_subcategory, container, false);


        tv_shop_by_category_label = (TextView) view.findViewById(R.id.tv_shop_by_category_label);
        iv_category_banner_image = (ImageView) view.findViewById(R.id.iv_category_banner_image);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);


        Glide.with(getActivity())
                .load(Session.getHomeScreenBanner(prefrence))
                .thumbnail(0.1f)
                .into(iv_category_banner_image);

        try {

            if (Utility.checkInternetConnection(getActivity())) {
                if (getArguments().getString("SubSubCategoryID") != null) {
                    String value = getArguments().getString("SubSubCategoryID");
                    Log.i("value ", "" + value);
                    ((Utility) getActivity()).showLoading();
                    ServerAPI.getInstance().getSubProductCategoryDetails(ApiServerResponse.SUB_PRODUCT_CATEGORY, Session.get_userId(prefrence), value, this);

                }
            } else {
                ((Utility) getActivity()).showAlertDialog(getActivity());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                ProductCategoryModal productCategoryModal;
                SubProductCategoryListModal subProductCategoryListModal;
                //MyCartDetailModal myCartDetailModal;
                //CartItemCountModal cartItemCountModal;

                switch (tag) {
                    case ApiServerResponse.SUB_PRODUCT_CATEGORY:
                        subProductCategoryListModal = (SubProductCategoryListModal) response.body();
                        if (subProductCategoryListModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(subProductCategoryListModal.getSubcategory_detail().getTitle());
                            tv_shop_by_category_label.setText(subProductCategoryListModal.getSubcategory_detail().getTitle());
                            list = subProductCategoryListModal.getSubcategory_detail().getSubcategories();
                            SubProductCategoryDetailAdapter adapter = new SubProductCategoryDetailAdapter(getActivity(), list);
                            recyclerView.setAdapter(adapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                           /* if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                            }*/

                        } /*else {
                            if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                            }
                        }
*/
                        ((Utility) getActivity()).hideLoading();
                        break;
                    /*case ApiServerResponse.PRODUCT_CATEGORY:
                        productCategoryModal = (ProductCategoryModal) response.body();
                        if (productCategoryModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                           *//* if (!Session.getcart_id(prefrence).equalsIgnoreCase("")) {
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.getcart_id(prefrence), this);
                            }*//*
                            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(productCategoryModal.getCategory_detail().getTitle());
                            tv_shop_by_category_label.setText(productCategoryModal.getCategory_detail().getTitle());
                            list = productCategoryModal.getCategory_detail().getSubcategories();
                            ProductCategoryDetailAdapter adapter = new ProductCategoryDetailAdapter(getActivity(), list);
                            rv_categories.setAdapter(adapter);
                            rv_categories.setLayoutManager(new LinearLayoutManager(getActivity()));
                            *//*if (!Session.getcart_id(pref).equalsIgnoreCase("")) {
                                ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);
                            }*//*
                        } *//*else {
                            if (!Session.getcart_id(prefrence).equalsIgnoreCase("")) {
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.getcart_id(prefrence), this);
                            }
                        }*//*
                        ((Utility) getActivity()).hideLoading();
                        break;*/

                  /*  case ApiServerResponse.MY_CART_DETAILS:
                        myCartDetailModal = (MyCartDetailModal) response.body();
                        EventBus.getDefault().post(new EventBusProductDetailUpdateModal(String.valueOf(myCartDetailModal.getItems().size())));
                        EventBus.getDefault().post(new EventBusCartUpdate(String.valueOf(myCartDetailModal.getItems().size())));
                        if (myCartDetailModal.getItems().size() == 0) {
                            tv_count.setText("0");
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(pref, "0");
                        } else {
                            Session.setCartItemsQuantity(pref, String.valueOf(myCartDetailModal.getItems().size()));
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            tv_count.setText(String.valueOf(myCartDetailModal.getItems().size()));
                        }
                        break;*/
                }

            } else {
                ((Utility) getActivity()).hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
