package kartku.com.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.adapter.SubCategoryAdapter;
import kartku.com.retrofit.modal.SubCategoryModel;
import kartku.com.utils.ResponseKeys;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 1/12/2017.
 */


public class SubCategory extends Fragment {

    private SharedPreferences prefrence;
    ArrayList<SubCategoryModel> arrayList;
    RecyclerView recyclerView;
    boolean pendingIntroAnimation;
    JSONArray products_wishlist;
    ImageView iv_category_banner_image;
    TextView tv_shop_by_category_label;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        setHasOptionsMenu(true);
        prefrence = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.activity_subcategory, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        iv_category_banner_image = (ImageView) view.findViewById(R.id.iv_category_banner_image);
        tv_shop_by_category_label = (TextView) view.findViewById(R.id.tv_shop_by_category_label);

        try {
            String title = Session.getselectedCategory(prefrence);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);


            Glide.with(getActivity())
                    .load(Session.getHomeScreenBanner(prefrence))
                    .thumbnail(0.1f)
                    .into(iv_category_banner_image);


        } catch (Exception e) {
            e.printStackTrace();
        }

        String subSubCategoryJSON = Session.getselectedSub_SubCategoryJSON(prefrence);
        /*Log.i("subsub", subSubCategoryJSON);*/
        //Log.i("HelloJSOn", Session.getselectedCategory(prefrence));
        tv_shop_by_category_label.setText(Session.getselectedSubCategory(prefrence));
        SubCategoryModel subCategoryModel;
        arrayList = new ArrayList<SubCategoryModel>();
        if (!subSubCategoryJSON.isEmpty() || !subSubCategoryJSON.equals("")) {
            try {
                products_wishlist = new JSONArray(subSubCategoryJSON);
                for (int i = 0; i < products_wishlist.length(); i++) {
                    subCategoryModel = new SubCategoryModel();
                    JSONObject jsonObject = products_wishlist.getJSONObject(i);
                    subCategoryModel.setSub_subcat_name(jsonObject.getString(ResponseKeys.SUB_SUB_CAT_NAME));
                    subCategoryModel.setSub_subcat_id(jsonObject.getString(ResponseKeys.SUB_SUB_CAT_ID));
                    arrayList.add(subCategoryModel);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        SubCategoryAdapter adapter = new SubCategoryAdapter(getActivity(), arrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (pendingIntroAnimation) {
            pendingIntroAnimation = false;
            startIntroAnimation();
        }

        return view;
    }

    private void startIntroAnimation() {
        recyclerView.setTranslationY(recyclerView.getHeight());
        recyclerView.setAlpha(0f);
        recyclerView.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();


    }
}
