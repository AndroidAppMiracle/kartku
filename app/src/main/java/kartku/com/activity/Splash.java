package kartku.com.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.style.Pulse;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import kartku.com.R;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.sellermodule.SellerHomeDashBoard;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

public class Splash extends Utility /*implements MainCategoriesListener*/ {
    ProgressBar progressBar;
    private SharedPreferences prefrence;
    Handler handler = new Handler();
    // declared before onCreate
    Runnable myRunnable;
    //RelativeLayout rl_splash;
    ImageView iv_splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*printKeyHash();*/

        iv_splash = (ImageView) findViewById(R.id.iv_splash);

        //rl_splash = (RelativeLayout)findViewById(R.id.rl_splash);
        Picasso.with(Splash.this)
                .load(R.drawable.splash_bg)
                /*.placeholder(R.drawable.placeholder) */  // optional
                /*.error(R.drawable.placeholder)*/
                .fit().centerCrop()
                .into(iv_splash);

        //Glide.with(Splash.this).load(R.drawable.splash_bg).fitCenter().into(iv_splash);

        prefrence = new ObscuredSharedPreferences(getApplicationContext(), getApplicationContext().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        if (Session.getAppLanguage(prefrence).equalsIgnoreCase("")) {
            Session.setAppLanguage(prefrence, "1");
        }


        String token = FirebaseInstanceId.getInstance().getToken();
        /*Log.d("Firebase", "token " + token);*/

        try {
            progressBar = (ProgressBar) findViewById(R.id.progress_bar);
            Pulse circle = new Pulse();
            progressBar.setIndeterminateDrawable(circle);
            progressBar.setVisibility(View.VISIBLE);


           /* if (NetworkConnection.checkInternetConnection(Splash.this)) {
                new MainCategoriesManager(Splash.this, this).sendRequest();
            } else {

                //ToastMsg.showShortToast(Splash.this, "Not connected to internet.");
                //showAlertDialog();
                showAlertDialog();


            }*/

            myRunnable = new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);

                    if (checkInternetConnection(Splash.this)) {
                        callIntent();
                    } else {
                        showAlertDialog();
                    }

                }
            };
            handler.postDelayed(myRunnable, 3000);

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                callIntent();

            }
        }, 3000);*/


    }

    private void callIntent() {
        /*Intent intent = new Intent(Splash.this, MainActivity.class);*/

        if (Session.getUserRoleType(prefrence).equals(Constant.ROLE_SELLER)) {


            Intent intent = new Intent(Splash.this, SellerHomeDashBoard.class);
        /*Intent intent = new Intent(Splash.this, Home_dashboard.class);*/
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            /*Intent intent = new Intent(Splash.this, SellerHomeDashBoard.class);*/
            Intent intent = new Intent(Splash.this, DemoHomeDashBoard.class);
        /*Intent intent = new Intent(Splash.this, Home_dashboard.class);*/
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }

    }

/*    @Override
    public void onCategoriesSuccess(String response) {
        try {
            //Session.set_main_categories_res(prefrence, response);

          *//*  myRunnable = new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);

                    callIntent();
                }
            };
            handler.postDelayed(myRunnable, 3000);*//*

           *//* handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);

                    callIntent();

                }
            }, 3000);*//*


            //Log.d("HASH KEY ", printKeyHash(Splash.this));


            //callIntent();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCategoriesError(String error) {
        try {
            //progressBar.setVisibility(View.GONE);
            //ToastMsg.showShortToast(Splash.this, error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(myRunnable);
        finish();
    }


    private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(getResources().getString(R.string.connection_problem));
            dialogBuilder.setMessage(getResources().getString(R.string.check_internet_connection));
            dialogBuilder.setCancelable(false);
           /* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*/
            dialogBuilder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    finish();
                }
            });
           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* @Override
    protected void attachBaseContext(Context newBase) {
        Locale locale = new Locale("en");
        super.attachBaseContext(ContextWrapper.wrap(newBase, locale));
    }*/


   /* public void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "kartku.com",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }*/
}
