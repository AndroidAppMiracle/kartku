package kartku.com.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.SearchAdapter;
import kartku.com.adapter.SearchResultListAdapter;
import kartku.com.cart.CartDetailsNew;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.faq_module.FAQ;
import kartku.com.login_module.Login;
import kartku.com.product_category.GridSpacingItemDecoration;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.EventBusCategoryProductsUpdateWishlist;
import kartku.com.retrofit.modal.EventBusLogout;
import kartku.com.retrofit.modal.EventBusProductDetailUpdateModal;
import kartku.com.retrofit.modal.ProductSearchBrandModal;
import kartku.com.retrofit.modal.SearchModal;
import kartku.com.sort.BrandListActivity;
import kartku.com.sort.MaterialList;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/30/2017.
 */


public class SearchProductsBrandActivity extends Utility implements View.OnClickListener, ApiServerResponse {

    RecyclerView recyclerView;
    ImageView search_icon;
    EditText search_text;
    Button button_sort, button_filter;
    SharedPreferences pref;
    TextView cancel, tv_message;
    LinearLayout search_layout, filter_layout;

    boolean pendingIntroAnimation;
    TextView tv_count;
    static final int PICK_BRAND_LIST = 1;
    static final int PICK_MATERIAL_LIST = 2;
    private boolean filterable;
    private int brandMaterialSearched = 0;
    private String searchQuery;
    List<SearchModal.ProductsDataBean> searchListResult = new ArrayList<>();
    List<ProductSearchBrandModal.ProductsDataBean> searchResultList = new ArrayList<>();
    private int selectedApi = 0;
    private String filterBrandMaterialQuery;
    private SearchAdapter searchAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_search);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        int spanCount = 2; // 3 columns
        int spacing = 10; // 50px
        boolean includeEdge = false;
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        search_icon = (ImageView) findViewById(R.id.search_icon);
        search_text = (EditText) findViewById(R.id.search_text);

        search_layout = (LinearLayout) findViewById(R.id.top_wrapper);
        cancel = (TextView) findViewById(R.id.cancel_text);
        filter_layout = (LinearLayout) findViewById(R.id.filterWrapper);
        tv_message = (TextView) findViewById(R.id.tv_message);
        search_icon.setOnClickListener(this);
        button_sort = (Button) findViewById(R.id.button_sort);
        button_filter = (Button) findViewById(R.id.button_filter);
        cancel.setOnClickListener(this);
        button_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogforMasterCat();
            }
        });
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        button_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    filterDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tv_message.setVisibility(View.GONE);

        /*String cart_id = Session.getcart_id(pref);
        if (!cart_id.equalsIgnoreCase("")) {
            ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, cart_id, this);
        }*/

        try {
            String cart_id = Session.getcart_id(pref);
            if (!cart_id.equalsIgnoreCase("")) {
                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), cart_id, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        search_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    try {


                        if (checkInternetConnection(SearchProductsBrandActivity.this)) {
                            searchQuery = search_text.getText().toString().trim();
                            if (searchQuery.equalsIgnoreCase("")) {
                                ToastMsg.showShortToast(SearchProductsBrandActivity.this, "Please enter keyword to search products");
                            } else {
                                showLoading();
                                ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, "", "", "", "", Session.get_userId(pref), SearchProductsBrandActivity.this);
                                //ServerAPI.getInstance().searchProducts(ApiServerResponse.SEARCH, search_text.getText().toString().trim(), SearchProductsBrandActivity.this);
                                //searchAction(search_text.getText().toString());
                                InputMethodManager imm = (InputMethodManager) getSystemService(
                                        Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(search_text.getApplicationWindowToken(), 0);
                                selectedApi = 1;
                            }
                        } else {
                            showAlertDialog(SearchProductsBrandActivity.this);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });


    }


    public void selectedApiToRun() {

        try {

            try {
                String cart_id = Session.getcart_id(pref);
                if (!cart_id.equalsIgnoreCase("")) {
                    ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), cart_id, this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (selectedApi == 1) {
                showLoading();
                ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, "", "", "", "", Session.get_userId(pref), SearchProductsBrandActivity.this);
            } else if (selectedApi == 2) {
                showLoading();
                ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, "", "", "", "1", Session.get_userId(pref), SearchProductsBrandActivity.this);
            } else if (selectedApi == 3) {
                lowToHigh();

            } else if (selectedApi == 4) {
                highToLow();

            } else if (selectedApi == 5) {

                showLoading();
                //String asdasd = categoryName.replace(" ", "");
                ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, filterBrandMaterialQuery, "", "", "", Session.get_userId(pref), SearchProductsBrandActivity.this);
            } else if (selectedApi == 6) {

                showLoading();
                selectedApi = 6;
                ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, "", filterBrandMaterialQuery, "", "", Session.get_userId(pref), SearchProductsBrandActivity.this);
            } else if (selectedApi == 7) {

                showLoading();
                Utility.hideSoftKeyboard(SearchProductsBrandActivity.this);
                //ServerAPI.getInstance().searchProducts(ApiServerResponse.SEARCH, searchQuery, SearchProductsBrandActivity.this);
                ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, "", "", "", "", Session.get_userId(pref), SearchProductsBrandActivity.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void openDialogforMasterCat() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle(R.string.sort_by);

        TextView new_arrivals = (TextView) dialog.findViewById(R.id.new_arrivals);

        TextView LowHigh = (TextView) dialog.findViewById(R.id.lowHigh);

        TextView HighLow = (TextView) dialog.findViewById(R.id.Highlow);

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);

        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        new_arrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                    if (checkInternetConnection(SearchProductsBrandActivity.this)) {
                        showLoading();
                        ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, "", "", "", "1", Session.get_userId(pref), SearchProductsBrandActivity.this);

                        selectedApi = 2;
                        //ServerAPI.getInstance().searchByNewArriaval(ApiServerResponse.SEARCH_BY_NEW_ARRIVAL, searchQuery, SearchProductsBrandActivity.this);
                    } else {
                        showAlertDialog(SearchProductsBrandActivity.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LowHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                    lowToHigh();

                    /*Collections.sort(searchResultList, new Comparator<ProductSearchBrandModal.ProductsDataBean>() {
                        @Override
                        public int compare(ProductSearchBrandModal.ProductsDataBean o1, ProductSearchBrandModal.ProductsDataBean o2) {
                            return Integer.valueOf(o1.getProducts_price()) - Integer.valueOf(o2.getProducts_price());
                        }
                    });
*/


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        HighLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    // if (Utility.checkInternetConnection(SearchProductsBrandActivity.this)) {

                    dialog.dismiss();
                    highToLow();
                    // }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }


    private void lowToHigh() {
        try {
            if (searchResultList.size() != 0) {

                showLoading();
                Collections.sort(searchResultList, new Comparator<ProductSearchBrandModal.ProductsDataBean>() {
                    @Override
                    public int compare(ProductSearchBrandModal.ProductsDataBean o1, ProductSearchBrandModal.ProductsDataBean o2) {
                        return Integer.valueOf(o1.getProducts_price()) - Integer.valueOf(o2.getProducts_price());
                    }
                });

                        /*Collections.sort(searchListResult, new Comparator<SearchModal.ProductsDataBean>() {
                            @Override
                            public int compare(SearchModal.ProductsDataBean o1, SearchModal.ProductsDataBean o2) {
                                return Integer.valueOf(o1.getProducts_price()) - Integer.valueOf(o2.getProducts_price());
                            }
                        });*/
                SearchResultListAdapter adapter = new SearchResultListAdapter(SearchProductsBrandActivity.this, searchResultList);
                recyclerView.setAdapter(adapter);
                          /*  recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                if (pendingIntroAnimation) {
                    pendingIntroAnimation = false;
                    startIntroAnimation();
                }
                hideLoading();
            } else if (searchListResult.size() != 0) {

                showLoading();
                Collections.sort(searchListResult, new Comparator<SearchModal.ProductsDataBean>() {
                    @Override
                    public int compare(SearchModal.ProductsDataBean o1, SearchModal.ProductsDataBean o2) {
                        return Integer.valueOf(o1.getProducts_price()) - Integer.valueOf(o2.getProducts_price());

                    }
                });

                SearchAdapter adapter = new SearchAdapter(SearchProductsBrandActivity.this, searchListResult);
                recyclerView.setAdapter(adapter);
                if (pendingIntroAnimation) {
                    pendingIntroAnimation = false;
                    startIntroAnimation();
                }
                hideLoading();
            }

            selectedApi = 3;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void highToLow() {
        try {
            if (searchResultList.size() != 0) {
                showLoading();

                //Collections.reverse(searchResultList);


                Collections.sort(searchResultList, new Comparator<ProductSearchBrandModal.ProductsDataBean>() {
                    @Override
                    public int compare(ProductSearchBrandModal.ProductsDataBean o1, ProductSearchBrandModal.ProductsDataBean o2) {
                        return Integer.valueOf(o2.getProducts_price()) - Integer.valueOf(o1.getProducts_price());
                    }
                });


                SearchResultListAdapter adapter = new SearchResultListAdapter(SearchProductsBrandActivity.this, searchResultList);
                recyclerView.setAdapter(adapter);
                          /*  recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                if (pendingIntroAnimation) {
                    pendingIntroAnimation = false;
                    startIntroAnimation();
                }
                hideLoading();
            } else if (searchListResult.size() != 0) {
                showLoading();
                Collections.sort(searchListResult, new Comparator<SearchModal.ProductsDataBean>() {
                    @Override
                    public int compare(SearchModal.ProductsDataBean o1, SearchModal.ProductsDataBean o2) {
                        return Integer.valueOf(o2.getProducts_price()) - Integer.valueOf(o1.getProducts_price());

                    }
                });

                SearchAdapter adapter = new SearchAdapter(SearchProductsBrandActivity.this, searchListResult);
                recyclerView.setAdapter(adapter);
                if (pendingIntroAnimation) {
                    pendingIntroAnimation = false;
                    startIntroAnimation();
                }
                hideLoading();
            }

            selectedApi = 4;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void filterDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);

        TextView new_arrivals = (TextView) dialog.findViewById(R.id.new_arrivals);
        new_arrivals.setText(R.string.brands);

        TextView LowHigh = (TextView) dialog.findViewById(R.id.lowHigh);
        LowHigh.setText(R.string.materials);

        TextView HighLow = (TextView) dialog.findViewById(R.id.Highlow);
        HighLow.setVisibility(View.GONE);

        View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
               /* try {
                    showLoading();
                    ServerAPI.getInstance().searchByNewArrivalOnly(ApiServerResponse.SEARCH_BY_NEW_ARRIVAL_ONLY, searchQuery, SearchProductsBrandActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
        });

        new_arrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (checkInternetConnection(SearchProductsBrandActivity.this)) {
                        dialog.dismiss();
                        Intent intent = new Intent(SearchProductsBrandActivity.this, BrandListActivity.class);
                        intent.putExtra("search_key", searchQuery);
                                /*intent.putExtra("search_key", search_text.getText().toString());*/

                        filterable = true;
                        brandMaterialSearched = 1;
                        startActivityForResult(intent, PICK_BRAND_LIST);
                    } else {
                        showAlertDialog(SearchProductsBrandActivity.this);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LowHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if (checkInternetConnection(SearchProductsBrandActivity.this)) {
                        dialog.dismiss();
                        Intent intent = new Intent(SearchProductsBrandActivity.this, MaterialList.class);
                        intent.putExtra("search_key", searchQuery);
                        filterable = false;
                        brandMaterialSearched = 2;
                        startActivityForResult(intent, PICK_MATERIAL_LIST);
                    } else {
                        showAlertDialog(SearchProductsBrandActivity.this);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });






        /*SeekBar seek_bar = (SeekBar)dialog.findViewById(R.id.price_seekbar);
        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });*/
        dialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Session.get_login_statuc(pref)) {
            getMenuInflater().inflate(R.menu.menu_search_login, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_search_logout, menu);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SearchProductsBrandActivity.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {

            case R.id.menu_home:

                Intent intentHome = new Intent(SearchProductsBrandActivity.this, DemoHomeDashBoard.class);
                intentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentHome);
                break;

           /* case R.id.search:

                Intent search = new Intent(SearchProductsBrandActivity.this, SearchProductsBrandActivity.class);
                startActivity(search);

                break;*/
            case R.id.menu_count:
                Intent in = new Intent(SearchProductsBrandActivity.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
                /*Intent intent1 = new Intent(DemoHomeDashBoard.this, SellerHomeDashBoard.class);
                startActivity(intent1);*/
                break;
            case R.id.menu_my_orders:


                Intent intent1 = new Intent(SearchProductsBrandActivity.this, MyOrdersActivity.class);
                startActivity(intent1);
                //MyOrdersFragment ordersFragment = new MyOrdersFragment();
                            /*OrdersFragment ordersFragment = new OrdersFragment();*/
                //tag = "My Orders";
                /*ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);*/
                //swtichFragement(R.id.content_frame, ordersFragment, tag);

                break;
            case R.id.menu_my_product_review:

                Intent intentReviews = new Intent(SearchProductsBrandActivity.this, MyProductReviewsActivity.class);
                startActivity(intentReviews);

                break;
            case R.id.menu_my_wishlist:
                Intent intentWishlist = new Intent(SearchProductsBrandActivity.this, WishlistActivity.class);
                startActivity(intentWishlist);
                break;
            case R.id.menu_my_profile:
                Intent intentProfile = new Intent(SearchProductsBrandActivity.this, ProfileActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.menu_contact_us:

                Intent intentContact = new Intent(SearchProductsBrandActivity.this, ContactActivity.class);
                startActivity(intentContact);

                break;
            case R.id.menu_logout:

                //Session.getLogin_type(pref);

                EventBus.getDefault().post(new EventBusLogout(Session.getLogin_type(pref)));
                /*switch (loginType) {
                    case "google":
                        if (mGoogleApiClient.isConnected()) {
                            signOut();
                            drawerLayout.closeDrawers();
                            //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                            mGoogleApiClient.disconnect();
                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            LoginFragment fragmentLoginLogout = new LoginFragment();
                            tag = "Login";
                            swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);

                        }
                        break;
                    case "facebook": {
                        LoginManager.getInstance().logOut();
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        break;
                    }
                    default: {
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        //prepareListData();
                        Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                        startActivity(i);
                        finish();
                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        break;
                    }
                }*/

                break;

            case R.id.menu_login:
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                Intent intent = new Intent(SearchProductsBrandActivity.this, Login.class);
                startActivity(intent);
                break;
            case R.id.menu_faq:

                Intent intentFAQ = new Intent(SearchProductsBrandActivity.this, FAQ.class);
                startActivity(intentFAQ);

                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.search_icon:
                try {
                    if (checkInternetConnection(SearchProductsBrandActivity.this)) {
                        searchQuery = search_text.getText().toString().trim();
                        if (searchQuery.length() <= 0) {
                            ToastMsg.showShortToast(SearchProductsBrandActivity.this, getString(R.string.enter_keyword_to_search));
                        } else {

                            showLoading();
                            Utility.hideSoftKeyboard(SearchProductsBrandActivity.this);
                            //ServerAPI.getInstance().searchProducts(ApiServerResponse.SEARCH, searchQuery, SearchProductsBrandActivity.this);
                            ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, "", "", "", "", Session.get_userId(pref), SearchProductsBrandActivity.this);

                            selectedApi = 7;

                        }
                    } else {
                        showAlertDialog(SearchProductsBrandActivity.this);
                    }
                    //String search_input = search_text.getText().toString();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.cancel_text:
                try {
                    search_text.setText("");
                    searchQuery = "";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }


    }


    private void startIntroAnimation() {
        recyclerView.setTranslationY(recyclerView.getHeight());
        recyclerView.setAlpha(0f);
        recyclerView.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {
                ProductSearchBrandModal productSearchBrandModal;
                SearchModal searchModal;
                CartItemCountModal cartItemCountModal;


                switch (tag) {

                    case ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS:
                        searchModal = (SearchModal) response.body();

                        if (searchModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                                /*if (searchAdapter != null) {
                                    searchAdapter.notifyDataSetChanged();
                                }*/
                            }
                            if (searchListResult.size() != 0) {
                                searchListResult.clear();
                                if (searchAdapter != null) {
                                    searchAdapter.notifyDataSetChanged();
                                }
                            }
                            button_sort.setEnabled(true);
                            button_filter.setEnabled(true);
                            filter_layout.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                button_sort.setBackgroundColor(getResources().getColor(R.color.colorLogoRed, null));
                                button_filter.setBackgroundColor(getResources().getColor(R.color.colorPrimary, null));
                            } else {
                                button_sort.setBackgroundColor(getResources().getColor(R.color.colorLogoRed));
                                button_filter.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            }
                            tv_message.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            searchListResult = searchModal.getProducts_data();
                            searchAdapter = new SearchAdapter(SearchProductsBrandActivity.this, searchListResult);
                            searchAdapter.notifyDataSetChanged();
                            recyclerView.setAdapter(searchAdapter);
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }
                        } else {
                            tv_message.setVisibility(View.VISIBLE);
                            tv_message.setText(R.string.no_products_found);
                            recyclerView.setVisibility(View.GONE);
                            filter_layout.setVisibility(View.GONE);
                            button_sort.setEnabled(false);
                            button_filter.setEnabled(false);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                button_sort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
                                button_filter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
                            } else {
                                button_sort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
                                button_filter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
                            }

                        }


                        hideLoading();
                        break;

                    case ApiServerResponse.SEARCH:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            tv_message.setVisibility(View.GONE);


                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(SearchProductsBrandActivity.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                          /*  recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }
                        } else {

                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();
                        break;
                    case ApiServerResponse.SEARCH_BY_BRAND:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            /*if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            if (productList.size() != 0) {
                                productList.clear();
                            }*/
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(SearchProductsBrandActivity.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                           /* recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }

                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }


                        hideLoading();
                        break;

                    case ApiServerResponse.SEARCH_BY_MATERIAL:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(SearchProductsBrandActivity.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                          /*  recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();
                        break;


                    case ApiServerResponse.SEARCH_BY_NEW_ARRIVAL_ONLY:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(SearchProductsBrandActivity.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                          /*  recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();
                        break;
                    case ApiServerResponse.SEARCH_BY_NEW_ARRIVAL:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(SearchProductsBrandActivity.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                     /*       recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();

                        break;


                    case ApiServerResponse.CART_ITEM_COUNT:
                        cartItemCountModal = (CartItemCountModal) response.body();
                        if (cartItemCountModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            tv_count.setText(cartItemCountModal.getItem_count());
                            Session.setCartItemsQuantity(pref, cartItemCountModal.getItem_count());
                        } else {
                            tv_count.setText("0");
                            Session.setCartItemsQuantity(pref, "0");
                        }


                        break;

                }

            } else {
                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        tv_message.setVisibility(View.VISIBLE);
        tv_message.setText(R.string.no_products_found);
        recyclerView.setVisibility(View.GONE);
        filter_layout.setVisibility(View.GONE);
        button_sort.setEnabled(false);
        button_filter.setEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            button_sort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
            button_filter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
        } else {
            button_sort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
            button_filter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
        }
    }


    @Subscribe
    public void onCartUpdate(EventBusProductDetailUpdateModal eventBusProductDetailUpdateModal) {

        try {
            tv_count.setText(eventBusProductDetailUpdateModal.getMessage());
            /*if (eventBusProductDetailUpdateModal.getMessage() == 1) {
                String cart_id = Session.getcart_id(pref);
                if (!cart_id.equalsIgnoreCase("")) {
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, cart_id, this);
                }
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_BRAND_LIST || requestCode == PICK_MATERIAL_LIST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                //ToastMsg.showLongToast(DemoProductList.this, data.getData().toString());

                if (checkInternetConnection(SearchProductsBrandActivity.this)) {
                    String filerBrandOrMaterial = data.getData().toString();
                    filterBrandMaterialQuery = filerBrandOrMaterial;
                    searchByBrandOrMaterial(filterable, filerBrandOrMaterial, searchQuery);
                } else {
                    showAlertDialog(SearchProductsBrandActivity.this);
                }


                /*if (filterable) {
                    filterQueryBrand = data.getData().toString();
                    ToastMsg.showShortToast(CategoryProducts.this, filterQueryBrand);
                    // if (checkInternetConnection(CategoryProducts.this)) {
                    showLoading();
                    ServerAPI.getInstance().searchByBrand(ApiServerResponse.SEARCH_BY_BRAND, categoryName, filterQueryBrand, this);
                    //} else {
                    // ToastMsg.showShortToast(CategoryProducts.this, "Please check your internet connection");
                    // }


                } else {
                    filterQueryMaterial = data.getData().toString();
                    ToastMsg.showShortToast(CategoryProducts.this, categoryName + " " + filterQueryMaterial);

                    if (checkInternetConnection(CategoryProducts.this)) {
                        showLoading();
                        ServerAPI.getInstance().searchByMaterial(ApiServerResponse.SEARCH_BY_MATERIAL, categoryName, filterQueryMaterial, this);
                    } else {
                        ToastMsg.showShortToast(CategoryProducts.this, "Please check your internet connection");
                    }

                }*/

                //ServerAPI.getInstance().searchByBrand(ApiServerResponse.SEARCH_BY_BRAND, categoryName, filterQuery, this);
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                /*ToastMsg.showShortToast(CategoryProducts.this, categoryName + " " + filterQuery);*/

                // Do something with the contact here (bigger example below)
            }
        }
    }


    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }


    @Subscribe
    public void onProductDetailPageUpdate(EventBusCategoryProductsUpdateWishlist eventBusCategoryProductsUpdateWishlist) {
        try {
            if (eventBusCategoryProductsUpdateWishlist.getMessage().equalsIgnoreCase(Constant.OK)) {
                selectedApiToRun();
                //ToastMsg.showLongToast(SearchProductsBrandActivity.this, "Hello");
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void searchByBrandOrMaterial(boolean filterable, String filterQuery, String categoryName) {

        if (filterable) {
            //filterQueryBrand = data.getData().toString();
            //ToastMsg.showShortToast(SearchProductsBrandActivity.this, filterQuery);
            // if (checkInternetConnection(CategoryProducts.this)) {
            showLoading();
            //String asdasd = categoryName.replace(" ", "");
            selectedApi = 5;
            ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, filterQuery, "", "", "", Session.get_userId(pref), SearchProductsBrandActivity.this);

            //ServerAPI.getInstance().searchByBrand(ApiServerResponse.SEARCH_BY_BRAND, categoryName.replace(" ", ""), filterQuery, this);
            //} else {
            // ToastMsg.showShortToast(CategoryProducts.this, "Please check your internet connection");
            // }


        } else {
            // filterQueryMaterial = data.getData().toString();
            //ToastMsg.showShortToast(SearchProductsBrandActivity.this, categoryName + " " + filterQuery);

            if (checkInternetConnection(SearchProductsBrandActivity.this)) {
                showLoading();
                selectedApi = 6;
                ServerAPI.getInstance().searchSortFilterProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, searchQuery, "", filterQuery, "", "", Session.get_userId(pref), SearchProductsBrandActivity.this);

                //ServerAPI.getInstance().searchByMaterial(ApiServerResponse.SEARCH_BY_MATERIAL, categoryName.replace(" ", ""), filterQuery, this);
            } else {
                ToastMsg.showShortToast(SearchProductsBrandActivity.this, getString(R.string.check_internet_connection));
            }

        }
    }


    /*private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Connection Problem.");
            dialogBuilder.setMessage("Please check your internet connection?");
            dialogBuilder.setCancelable(true);
           *//* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


}
