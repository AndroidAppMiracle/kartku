package kartku.com.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.ProductCategoryDetailAdapter;
import kartku.com.cart.CartDetailsNew;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.faq_module.FAQ;
import kartku.com.login_module.Login;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.EventBusCartUpdate;
import kartku.com.retrofit.modal.EventBusLogout;
import kartku.com.retrofit.modal.EventBusProductDetailUpdateModal;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.retrofit.modal.ProductCategoryModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/15/2017.
 */

public class ProductCategoryList extends Utility implements ApiServerResponse {


    TextView tv_shop_by_category_label;
    ImageView iv_category_banner_image;
    RecyclerView rv_categories;
    SharedPreferences pref;
    List<ProductCategoryModal.CategoryDetailBean.SubcategoriesBean> list = new ArrayList<>();
    Toolbar toolbar;
    TextView tv_count;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_category_list);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        pref = new ObscuredSharedPreferences(ProductCategoryList.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        tv_shop_by_category_label = (TextView) findViewById(R.id.tv_shop_by_category_label);
        iv_category_banner_image = (ImageView) findViewById(R.id.iv_category_banner_image);
        rv_categories = (RecyclerView) findViewById(R.id.rv_categories);

        Glide.with(ProductCategoryList.this)
                .load(Session.getHomeScreenBanner(pref))
                .thumbnail(0.1f)
                .into(iv_category_banner_image);

        try {

            if (checkInternetConnection(ProductCategoryList.this)) {
                if (getIntent().getExtras().getString(Constant.CATEGORY_ID) != null) {
                    String categoryID = getIntent().getExtras().getString(Constant.CATEGORY_ID);

                    showLoading();
                    ServerAPI.getInstance().getProductCategoryDetails(ApiServerResponse.PRODUCT_CATEGORY, Session.get_userId(pref), categoryID, this);

                }
            } else {
                showAlertDialog(ProductCategoryList.this);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Session.get_login_statuc(pref)) {
            getMenuInflater().inflate(R.menu.menu_universal_login, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_universal_logout, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ProductCategoryList.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_home:

                Intent intentHome = new Intent(ProductCategoryList.this, DemoHomeDashBoard.class);
                intentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentHome);
                break;
            case R.id.search:

                Intent search = new Intent(ProductCategoryList.this, SearchProductsBrandActivity.class);
                startActivity(search);

                break;
            case R.id.menu_count:
                Intent in = new Intent(ProductCategoryList.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
                /*Intent intent1 = new Intent(DemoHomeDashBoard.this, SellerHomeDashBoard.class);
                startActivity(intent1);*/
                break;
            case R.id.menu_my_orders:


                Intent intent1 = new Intent(ProductCategoryList.this, MyOrdersActivity.class);
                startActivity(intent1);
                //MyOrdersFragment ordersFragment = new MyOrdersFragment();
                            /*OrdersFragment ordersFragment = new OrdersFragment();*/
                //tag = "My Orders";
                /*ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);*/
                //swtichFragement(R.id.content_frame, ordersFragment, tag);

                break;
            case R.id.menu_my_product_review:

                Intent intentReviews = new Intent(ProductCategoryList.this, MyProductReviewsActivity.class);
                startActivity(intentReviews);

                break;
            case R.id.menu_my_wishlist:
                Intent intentWishlist = new Intent(ProductCategoryList.this, WishlistActivity.class);
                startActivity(intentWishlist);
                break;
            case R.id.menu_my_profile:
                Intent intentProfile = new Intent(ProductCategoryList.this, ProfileActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.menu_contact_us:

                Intent intentContact = new Intent(ProductCategoryList.this, ContactActivity.class);
                startActivity(intentContact);

                break;
            case R.id.menu_logout:

                //Session.getLogin_type(pref);

                EventBus.getDefault().post(new EventBusLogout(Session.getLogin_type(pref)));
                /*switch (loginType) {
                    case "google":
                        if (mGoogleApiClient.isConnected()) {
                            signOut();
                            drawerLayout.closeDrawers();
                            //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                            mGoogleApiClient.disconnect();
                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            LoginFragment fragmentLoginLogout = new LoginFragment();
                            tag = "Login";
                            swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);

                        }
                        break;
                    case "facebook": {
                        LoginManager.getInstance().logOut();
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        break;
                    }
                    default: {
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        //prepareListData();
                        Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                        startActivity(i);
                        finish();
                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        break;
                    }
                }*/

                break;

            case R.id.menu_login:
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                Intent intent = new Intent(ProductCategoryList.this, Login.class);
                startActivity(intent);
                break;
            case R.id.menu_faq:

                Intent intentFAQ = new Intent(ProductCategoryList.this, FAQ.class);
                startActivity(intentFAQ);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {
                ProductCategoryModal productCategoryModal;
                MyCartDetailModal myCartDetailModal;
                CartItemCountModal cartItemCountModal;

                switch (tag) {
                    case ApiServerResponse.CART_ITEM_COUNT:
                        cartItemCountModal = (CartItemCountModal) response.body();
                        if (cartItemCountModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            tv_count.setText(cartItemCountModal.getItem_count());
                            Session.setCartItemsQuantity(pref, cartItemCountModal.getItem_count());
                        } else {
                            tv_count.setText("0");
                            Session.setCartItemsQuantity(pref, "0");
                        }


                        break;
                    case ApiServerResponse.PRODUCT_CATEGORY:
                        productCategoryModal = (ProductCategoryModal) response.body();
                        if (productCategoryModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            if (!Session.getcart_id(pref).equalsIgnoreCase("0")) {
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                            }

                            getSupportActionBar().setTitle(productCategoryModal.getCategory_detail().getTitle());
                            tv_shop_by_category_label.setText(productCategoryModal.getCategory_detail().getTitle());
                            list = productCategoryModal.getCategory_detail().getSubcategories();
                            ProductCategoryDetailAdapter adapter = new ProductCategoryDetailAdapter(ProductCategoryList.this, list);
                            rv_categories.setAdapter(adapter);
                            rv_categories.setLayoutManager(new LinearLayoutManager(this));
                            /*if (!Session.getcart_id(pref).equalsIgnoreCase("")) {
                                ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);
                            }*/
                        } else {
                            if (!Session.getcart_id(pref).equalsIgnoreCase("0")) {
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                            }
                        }
                        hideLoading();
                        break;

                    case ApiServerResponse.MY_CART_DETAILS:
                        myCartDetailModal = (MyCartDetailModal) response.body();
                        EventBus.getDefault().post(new EventBusProductDetailUpdateModal(String.valueOf(myCartDetailModal.getItems().size())));
                        EventBus.getDefault().post(new EventBusCartUpdate(String.valueOf(myCartDetailModal.getItems().size())));
                        if (myCartDetailModal.getItems().size() == 0) {
                            tv_count.setText("0");
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(pref, "0");
                        } else {
                            Session.setCartItemsQuantity(pref, String.valueOf(myCartDetailModal.getItems().size()));
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            tv_count.setText(String.valueOf(myCartDetailModal.getItems().size()));
                        }
                        break;
                }

            } else {
                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }


    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Subscribe
    public void onCartUpdate(EventBusCartUpdate eventBusCartUpdate) {

        try {
            tv_count.setText(eventBusCartUpdate.getMessage());
           /* if (eventBusCartUpdate.getMessage() == 1) {
                String cart_id = Session.getcart_id(pref);
                if (!cart_id.equalsIgnoreCase("")) {
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, cart_id, this);
                }
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
