package kartku.com.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kartku.com.R;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.OrderConfirmationModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/28/2017.
 */

public class OrderConfirmation extends Utility implements ApiServerResponse {

    String orderID, webAddress;
    SharedPreferences pref;
    ImageView iv_order_confirmation;
    TextView tv_orderID, tv_orderComplete, tv_orderPaymentStatus, text_Done;
    String orderConfirmationStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        iv_order_confirmation = (ImageView) findViewById(R.id.iv_order_confirmation);
        tv_orderID = (TextView) findViewById(R.id.tv_orderID);
        tv_orderComplete = (TextView) findViewById(R.id.tv_orderComplete);
        tv_orderPaymentStatus = (TextView) findViewById(R.id.tv_orderPaymentStatus);
        text_Done = (TextView) findViewById(R.id.text_Done);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.order_confirmation);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                  /*  Intent i = new Intent(OrderConfirmation.this, DemoHomeDashBoard.class);
                    startActivity(i);*/
                Intent i = new Intent(OrderConfirmation.this, DemoHomeDashBoard.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });
        /*setListener();*/
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (getIntent().getExtras().getString(Constant.ORDER_ID) != null) {
            orderID = getIntent().getExtras().getString(Constant.ORDER_ID);
        }
        if (getIntent().getExtras().getString(Constant.WEB_ADDRESS) != null) {
            webAddress = getIntent().getExtras().getString(Constant.WEB_ADDRESS);
        }


        try {
            if (webAddress != null && webAddress.contains(Constant.WEB_PAYMENT_SUCCESS)) {
                if (checkInternetConnection(OrderConfirmation.this)) {
                    showLoading();
                    ServerAPI.getInstance().getOrderConfirmation(ApiServerResponse.ORDER_CONFIRMATION, Session.get_userId(pref), orderID, this);
                } else {

                }


            } else if (webAddress != null && webAddress.contains(Constant.WEB_PAYMENT_FAILED)) {
                Glide.with(OrderConfirmation.this)
                        .load(R.drawable.failed_payment_image)
                        .into(iv_order_confirmation);
                tv_orderID.setVisibility(View.GONE);
                tv_orderComplete.setText(R.string.transaction_failed);
                tv_orderPaymentStatus.setText(R.string.payment_not_completed_successfully);
                tv_orderPaymentStatus.setTextColor(ContextCompat.getColor(OrderConfirmation.this, R.color.transactionRed));
                //Session.setcart_id(pref, "");
                orderConfirmationStatus = Constant.FAILED;

            } else if (webAddress != null && webAddress.contains(Constant.WEB_PAYMENT_CANCEL)) {
                Glide.with(OrderConfirmation.this)
                        .load(R.drawable.failed_payment_image)
                        .into(iv_order_confirmation);
                tv_orderID.setVisibility(View.GONE);
                tv_orderComplete.setText(R.string.transaction_cancelled);
                tv_orderPaymentStatus.setText(R.string.payment_not_completed_successfully);
                tv_orderPaymentStatus.setTextColor(ContextCompat.getColor(OrderConfirmation.this, R.color.transactionRed));
                //Session.setcart_id(pref, "");
                orderConfirmationStatus = Constant.CANCELLED;
            }


            text_Done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                  /*  Intent i = new Intent(OrderConfirmation.this, DemoHomeDashBoard.class);
                    startActivity(i);*/
                    if (orderConfirmationStatus != null
                            && !orderConfirmationStatus.equalsIgnoreCase("") && orderConfirmationStatus.equalsIgnoreCase(Constant.SUCCESS)) {
                        Intent i = new Intent(OrderConfirmation.this, OrderDetail.class);
                        i.putExtra(Constant.ORDER_ID, orderID);
                        i.putExtra(Constant.ORDER_CONFIRMATION_STATUS, orderConfirmationStatus);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(OrderConfirmation.this, DemoHomeDashBoard.class);
                        //i.putExtra(Constant.ORDER_ID, orderID);
                        //i.putExtra(Constant.ORDER_CONFIRMATION_STATUS, orderConfirmationStatus);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {

            if (response.isSuccessful()) {
                OrderConfirmationModal orderConfirmationModal;

                switch (tag) {

                    case ApiServerResponse.ORDER_CONFIRMATION:

                        orderConfirmationModal = (OrderConfirmationModal) response.body();
                        if (orderConfirmationModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            hideLoading();
                            Session.setcart_id(pref, "0");
                            Glide.with(OrderConfirmation.this)
                                    .load(R.drawable.payment_success)
                                    .into(iv_order_confirmation);
                            tv_orderID.setText("Order ID #" + orderConfirmationModal.getDetail().getOrder_id());
                            tv_orderComplete.setText(R.string.order_complete);
                            tv_orderPaymentStatus.setText(R.string.payment_completed_successfully);

                            orderConfirmationStatus = Constant.SUCCESS;
                        } else {
                            hideLoading();
                        }


                        break;
                }

            } else {
                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

/*    private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Connection Problem");
            dialogBuilder.setMessage("Please check your internet connection.");
            dialogBuilder.setCancelable(true);
           *//* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    Glide.with(OrderConfirmation.this)
                            .load(R.drawable.failed_payment_image)
                            .into(iv_order_confirmation);
                    tv_orderID.setVisibility(View.GONE);
                    tv_orderComplete.setText("Transaction Failed");
                    tv_orderPaymentStatus.setText("Your payment was not completed successfully.");
                    tv_orderPaymentStatus.setTextColor(ContextCompat.getColor(OrderConfirmation.this, R.color.transactionRed));
                    //Session.setcart_id(pref, "");
                    orderConfirmationStatus = Constant.FAILED;


                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
