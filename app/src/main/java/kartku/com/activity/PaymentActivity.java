package kartku.com.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;

import kartku.com.R;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;

/**
 * Created by Kshitiz Bali on 3/21/2017.
 */

public class PaymentActivity extends Utility /*implements TransactionFinishedCallback*/ {


    private String orderID;
    private WebView mWebView;
    private SharedPreferences prefrence;
    // private boolean onBackExitFlag = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        try {

            setSupportActionBar(toolbar);
           /* toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertDialog();
                }
            });*/
        /*setListener();*/
           /* try {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
            }*/

            if (getIntent().getExtras().getString(Constant.ORDER_ID) != null) {
                orderID = getIntent().getExtras().getString(Constant.ORDER_ID);
            }


            mWebView = (WebView) findViewById(R.id.activity_main_webview);

            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);


            if (checkInternetConnection(PaymentActivity.this)) {
                String urlStart = Constant.PAYMENT_URL + orderID;
        /*String urlStart = "http://dev.miracleglobal.com/kartku-php/web/product/order/payment?orderId=" + orderID + "&user_id=" + Session.get_userId(prefrence);*/
                //String shippingURL = urlStart.concat(orderID).concat("&user_id=").concat(Session.get_userId(prefrence));

                mWebView.loadUrl(urlStart);

                mWebView.setWebViewClient(new PaymentWebViewClient(PaymentActivity.this, orderID));
            } else {

                showAlertDialog(PaymentActivity.this);
            }

        /*mWebView.loadUrl("http://dev.miracleglobal.com/kartku-php/web/checkout/snap?order_id=1");*/



            /*http://dev.miracleglobal.com/kartku-php/web/checkout/snap?order_id=150*/


         /*   UIKitCustomSetting uisetting = new UIKitCustomSetting();
            uisetting.setShowPaymentStatus(true);

            // SDK initiation for UIflow
            SdkUIFlowBuilder.init(this, CLIENT_KEY, "https://api.sandbox.midtrans.com/v2", this)
                    //.setExternalScanner(new ScanCard()) // initialization for using external scancard
                    .enableLog(true)
                    .useBuiltInTokenStorage(true) // enable built in token storage
                    .setDefaultText("open_sans_regular.ttf")
                    .setSemiBoldText("open_sans_semibold.ttf")
                    .setBoldText("open_sans_bold.ttf")
                    .setUIkitCustomSetting(uisetting) //optional
                    .buildSDK();
*/


        /*    SdkUIFlowBuilder.init(PaymentActivity.this, BuildConfig.CLIENT_KEY, BuildConfig.BASE_URL, new TransactionFinishedCallback() {
                @Override
                public void onTransactionFinished(TransactionResult result) {
                    // Handle finished transaction here.
                    ToastMsg.showLongToast(PaymentActivity.this, result.getStatus());

                    Log.i("Payment status", result.getStatus());
                }
            }).buildSDK();


            TransactionRequest transactionRequestNew = new
                    TransactionRequest(158 + "", 1903270);
            // Define item details
            ItemDetails itemDetails = new ItemDetails("1", 1000, 1, "Trekking Shoes");
            ItemDetails itemDetails1 = new ItemDetails("2", 1000, 2, "Casual Shoes");
            ItemDetails itemDetails2 = new ItemDetails("3", 1000, 3, "Formal Shoes");

            // Add item details into item detail list.
            ArrayList<ItemDetails> itemDetailsArrayList = new ArrayList<>();
            itemDetailsArrayList.add(itemDetails);
            itemDetailsArrayList.add(itemDetails1);
            itemDetailsArrayList.add(itemDetails2);
            transactionRequestNew.setItemDetails(itemDetailsArrayList);


            MidtransSDK.getInstance().setTransactionRequest(transactionRequestNew);


            MidtransSDK.getInstance().startPaymentUiFlow(PaymentActivity.this);*/
            /*MidtransSDK.getInstance().startCreditCardUIFlow(PaymentActivity.this);*/

/*            SdkCoreFlowBuilder.init(PaymentActivity.this, CLIENT_KEY, "https://api.sandbox.midtrans.com/v2")
                    .enableLog(true)
                    .buildSDK();

            TransactionRequest transactionRequest = new TransactionRequest("1211232", 1500);

            CustomerDetails customer = new CustomerDetails("abcdef", "abcdef", "abcdef@abcdef.com", "9999999999");

            transactionRequest.setCustomerDetails(customer);


            MidtransSDK.getInstance().setTransactionRequest(transactionRequest);

            MidtransSDK.getInstance().checkout(new CheckoutCallback() {
                @Override
                public void onSuccess(Token token) {
                    // Checkout token will be used to charge the transaction later
                    String checkoutToken = token.getTokenId();
                    ToastMsg.showShortToast(PaymentActivity.this, checkoutToken);
                    // Action when succeded
                }

                @Override
                public void onFailure(Token token, String reason) {
                    // Action when failed
                }

                @Override
                public void onError(Throwable error) {
                    // Action when error
                }
            });

            //getPayment();
            SdkUIFlowBuilder.init(PaymentActivity.this, CLIENT_KEY, "https://api.sandbox.midtrans.com/v2", new TransactionFinishedCallback() {
                @Override
                public void onTransactionFinished(TransactionResult result) {
                    // Handle finished transaction here.
                }
            })
                    //.setExternalScanner(new ScanCard()) // initialization for using external scancard
                    .enableLog(true)
                    *//*.setDefaultText("open_sans_regular.ttf") // optional to set custom font
                    .setSemiBoldText("open_sans_semibold.ttf") // optional to set custom font
                    .setBoldText("open_sans_bold.ttf") // optional to set custom font*//*
                    .buildSDK();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onBackPressed() {


        //showAlertDialog();

       /* if (onBackExitFlag) {
            super.onBackPressed();
        }*/
    }

/*    private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage("Are you sure you want to cancel payment transaction?");
            dialogBuilder.setCancelable(true);
            dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });
            dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    finish();
                    Intent intent = new Intent(PaymentActivity.this, OrderConfirmation.class);
                    intent.putExtra(Constant.ORDER_ID, orderID.trim());
                    intent.putExtra(Constant.WEB_ADDRESS, Constant.WEB_PAYMENT_CANCEL);
                    startActivity(intent);
                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /*private void showAlertDialogNetworkConnection() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Connection Problem.");
            dialogBuilder.setMessage("Please check your internet connection?");
            dialogBuilder.setCancelable(true);
           *//* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    finish();
                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/



  /*  private void getPayment() {

        try {
            UIKitCustomSetting uisetting = new UIKitCustomSetting();
            uisetting.setShowPaymentStatus(true);
            SdkUIFlowBuilder.init(this, CLIENT_KEY, "https://api.sandbox.midtrans.com/v2", new TransactionFinishedCallback() {
                @Override
                public void onTransactionFinished(TransactionResult transactionResult) {
                    ToastMsg.showShortToast(PaymentActivity.this, transactionResult.getStatus());
                }
            })
                    //.setExternalScanner(new ScanCard())
                    .enableLog(true)
                    .useBuiltInTokenStorage(true)
                    .setDefaultText("open_sans_regular.ttf")
                    .setSemiBoldText("open_sans_semibold.ttf")
                    .setBoldText("open_sans_bold.ttf")
                    .setUIkitCustomSetting(uisetting)
                    .buildSDK();
            TransactionRequest transactionRequestNew = new TransactionRequest(System.currentTimeMillis() + "", 800000);
            MidtransSDK.getInstance().setTransactionRequest(transactionRequestNew);
            MidtransSDK.getInstance().startPaymentUiFlow(PaymentActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

   /* @Override
    public void onTransactionFinished(TransactionResult transactionResult) {

    }*/


}
