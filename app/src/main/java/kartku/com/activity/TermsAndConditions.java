package kartku.com.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kartku.com.R;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;

/**
 * Created by Kshitiz Bali on 3/28/2017.
 */

public class TermsAndConditions extends Utility {

    WebView webview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.terms_conditions);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        webview = (WebView) findViewById(R.id.activity_main_webview);

        String url = Constant.TERMS_CONDITIONS;

        try {
            loadUrlWithWebView(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void loadUrlWithWebView(String url) {
        try {
            webview.setWebViewClient(webViewClient);
            webview.loadUrl(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // Loading started for URL
            showLoading();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // Redirecting to URL
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // Loading finished for URL
            hideLoading();
        }
    };
}
