package kartku.com.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kartku.com.utils.Constant;
import kartku.com.utils.Utility;

/**
 * Created by Kshitiz Bali on 3/18/2017.
 */

public class ShippingWebViewClient extends WebViewClient {


    private Context mContext;
    private String orderID;

    public ShippingWebViewClient(Context context, String orderid) {
        this.mContext = context;
        this.orderID = orderid;
    }


    @SuppressWarnings("deprecation")
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

//        Log.i("URL ", url);
        if (Uri.parse(url).getHost().endsWith("miracleglobal.com")) {
            return false;
        }



        /*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        view.getContext().startActivity(intent);*/
        Intent intent = new Intent(mContext, PaymentActivity.class);
        intent.putExtra(Constant.ORDER_ID, orderID.trim());
        mContext.startActivity(intent);
        return true;
    }

    @RequiresApi(Build.VERSION_CODES.N)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//        Log.i("URL ", request.getUrl().getPath());
        return super.shouldOverrideUrlLoading(view, request);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        ((Utility) mContext).showLoading();
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        ((Utility) mContext).hideLoading();
    }
}