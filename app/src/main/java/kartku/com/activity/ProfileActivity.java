package kartku.com.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import kartku.com.R;
import kartku.com.profile.change_password.ChangePassword;
import kartku.com.profile.listener.ProfileListener;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.UpdateUserModal;
import kartku.com.utils.CircleTransform;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/29/2017.
 */

public class ProfileActivity extends Utility implements View.OnClickListener, ProfileListener, ApiServerResponse {

    EditText first_name, last_name, address, contact_number;
    TextView email;
    Button update_profile;
    String user_first_name, user_lName, user_email, user_address, user_number;
    String profile_url;
    SharedPreferences pref;
    String user_id, loginType;
    ImageView profile_pic;

    FloatingActionButton fab_edit;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_activity);
        pref = new ObscuredSharedPreferences(ProfileActivity.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        loginType = Session.getLogin_type(pref);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        fab_edit = (FloatingActionButton) findViewById(R.id.fab_edit);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.my_profile);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                //showAlertDialog();
            }
        });


        try {
            first_name = (EditText) findViewById(R.id.first_name);
            last_name = (EditText) findViewById(R.id.last_name);
            email = (TextView) findViewById(R.id.email);
            address = (EditText) findViewById(R.id.address);
            contact_number = (EditText) findViewById(R.id.contact_no);
            update_profile = (Button) findViewById(R.id.btn_update_profile);
            profile_pic = (ImageView) findViewById(R.id.profile_pic);
            update_profile.setOnClickListener(this);
            fab_edit.setOnClickListener(this);
          /*  if (loginType.equalsIgnoreCase("email")) {
                fab_edit.setVisibility(View.VISIBLE);
            } else {
                fab_edit.setVisibility(View.GONE);
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        // get values from session saved in login response to set as profile details.

        try {

            try {
                //String response = Session.getlogin_user_details(pref);
                //JSONObject user_details = new JSONObject(response);
                switch (loginType) {
                    case "email":


                        first_name.setText(Session.getFirstName(pref));
                        last_name.setText(Session.getLastName(pref));
                        email.setText(Session.getUserEmail(pref));
                        contact_number.setText(Session.getUserMobile(pref));
                        update_profile.setVisibility(View.GONE);
                        fab_edit.setVisibility(View.VISIBLE);
                        address.setText(Session.getAddress(pref));
                        /*if (user_address.equalsIgnoreCase("null") || user_address.equalsIgnoreCase(null)) {
                            user_address = "";
                        }*/
                        //address.setText(Session.getAddress(pref));



                       /* user_first_name = user_details.getString(Constant.FIRST_NAME);
                        user_lName = user_details.getString(Constant.LAST_NAME);
                        user_email = user_details.getString(Constant.EMAIL);
                        user_address = user_details.getString(Constant.ADDRESS);
                        user_number = user_details.getString(Constant.MOBILE_NUMBER);
                        profile_url = user_details.getString(Constant.PROFILE_IMAGE_PATH);
                        first_name.setText(user_first_name);
                        last_name.setText(user_lName);
                        email.setText(user_email);
                        contact_number.setText(user_number);
                        update_profile.setVisibility(View.VISIBLE);
                        if (user_address.equalsIgnoreCase("null") || user_address.equalsIgnoreCase(null)) {
                            user_address = "";
                        }
                        address.setText(user_address);*/
                        break;
                    case "google":
                        update_profile.setVisibility(View.GONE);
                        last_name.setVisibility(View.GONE);
                        contact_number.setVisibility(View.GONE);
                        address.setVisibility(View.GONE);
                        first_name.setFocusable(false);
                        first_name.setFocusableInTouchMode(false);
                        fab_edit.setVisibility(View.GONE);
                        String displayPicGoogle = Session.get_google_signin_user_profile_pic(pref);
                        //Log.e("ProfilePic", displayPicGoogle);
                        //Toast.makeText(getActivity(), "Pic" + displayPicGoogle, Toast.LENGTH_LONG).show();
                        if (!displayPicGoogle.isEmpty()) {
                            Picasso.with(ProfileActivity.this)
                                    .load(displayPicGoogle)
                                    .placeholder(R.drawable.placeholder)   // optional
                                    .error(R.drawable.placeholder).resize(100, 100).transform(new CircleTransform()).centerCrop()
                                    .into(profile_pic);
                        }

                        // user_first_name = user_details.getString(Constant.USERNAME);
                        //user_email = user_details.getString(Constant.EMAIL);
                        first_name.setHint(R.string.username);
                        first_name.setText(Session.getGooglName(pref));
                        email.setText(Session.getGooglEmailID(pref));

                        break;
                    case "facebook":
                        update_profile.setVisibility(View.GONE);
                        last_name.setVisibility(View.GONE);
                        contact_number.setVisibility(View.GONE);
                        address.setVisibility(View.GONE);
                        fab_edit.setVisibility(View.GONE);
                        first_name.setFocusable(false);
                        first_name.setFocusableInTouchMode(false);
                        String displayPicFacebook = Session.get_facebook_signin_user_profile_pic(pref);
                        //Log.e("ProfilePic", displayPicGoogle);
                        //Toast.makeText(getActivity(), "Pic" + displayPicGoogle, Toast.LENGTH_LONG).show();
                        if (!displayPicFacebook.isEmpty()) {
                            Picasso.with(ProfileActivity.this)
                                    .load(displayPicFacebook)
                                    .placeholder(R.drawable.placeholder)   // optional
                                    .error(R.drawable.placeholder).transform(new CircleTransform())
                                    .into(profile_pic);
                        }


                        //user_first_name = user_details.getString(Constant.USERNAME);
                        //user_email = user_details.getString(Constant.EMAIL);
                        first_name.setHint(R.string.username);
                        first_name.setText(Session.get_facebookName(pref));
                        email.setText(Session.get_facebookEmail(pref));

                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

                /*first_name.setText(user_first_name);
                last_name.setText(user_lName);
                email.setText(user_email);*/


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onSuccess(String response) {
        try {
            ToastMsg.showShortToast(ProfileActivity.this, getString(R.string.profile_success_message));
            /*finish();*/
            /*Intent intent = new Intent(Profile.this, Home_dashboard.class);
            startActivity(intent);
            finish();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String error) {
        try {
            ToastMsg.showShortToast(ProfileActivity.this, error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update_profile:
                try {

                    if (checkInternetConnection(ProfileActivity.this)) {
                        if (validateFields(first_name.getText().toString(), last_name.getText().toString(), contact_number.getText().toString())) {
                            showLoading();
                            ServerAPI.getInstance().updateUserProfile(ApiServerResponse.UPDATE_USER_PROFILE, address.getText().toString(), user_id, first_name.getText().toString(), last_name.getText().toString(), contact_number.getText().toString(), this);
                        }


                    } else {
                        showAlertDialog(ProfileActivity.this);
                    }

                    //new ProfileManager(getActivity(), this).sendRequest(first_name.getText().toString(), last_name.getText().toString(), user_id, address.getText().toString(), contact_number.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.fab_edit:
                if (update_profile.getVisibility() == View.GONE) {
                    update_profile.setVisibility(View.VISIBLE);
                    fab_edit.setVisibility(View.GONE);
                }


                break;

        }
    }


    private boolean validateFields(String first_name, String last_name, String mobile_no) {
        if (first_name.length() <= 0 || first_name.length() <= 2) {
            ToastMsg.showShortToast(ProfileActivity.this, getResources().getString(R.string.first_name));
            return false;
        } else if (last_name.length() <= 0 || first_name.length() <= 2) {

            ToastMsg.showShortToast(ProfileActivity.this, getResources().getString(R.string.last_name));
            return false;
        } else if (mobile_no.length() <= 0 || mobile_no.length() < 10) {

            ToastMsg.showShortToast(ProfileActivity.this, getResources().getString(R.string.mobile_no));
            return false;
        } else {
            return true;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (loginType) {
            case "email":
                //inflater.inflate(R.menu.profile_menu, menu);
                menu.add(0, R.id.change_pwd, 0,
                        getResources().getString(R.string.change_password))
                        .setTitle(getResources().getString(R.string.change_password))
                        .setIcon(R.drawable.ic_password_change)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                break;
            case "facebook":
                menu.add(0, R.id.change_pwd, 0,
                        getResources().getString(R.string.facebook))
                        .setTitle(getResources().getString(R.string.facebook))
                        .setIcon(R.mipmap.ic_facebook_login)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                break;
            case "google":
                menu.add(0, R.id.change_pwd, 0,
                        getResources().getString(R.string.google))
                        .setTitle(getResources().getString(R.string.google))
                        .setIcon(R.mipmap.ic_google_signin)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                break;
            default:
                menu.add(0, R.id.change_pwd, 0,
                        getResources().getString(R.string.change_password))
                        .setTitle(getResources().getString(R.string.change_password))
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                break;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (loginType.equals("email")) {
            switch (item.getItemId()) {

                case R.id.change_pwd:
                    try {
                        Intent intent = new Intent(ProfileActivity.this, ChangePassword.class);
                        startActivity(intent);
                        //getActivity().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }

        return true;
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {

            if (response.isSuccessful()) {
                UpdateUserModal updateUserModal;

                switch (tag) {

                    case ApiServerResponse.UPDATE_USER_PROFILE:

                        updateUserModal = (UpdateUserModal) response.body();

                        if (updateUserModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            ToastMsg.showShortToast(ProfileActivity.this, updateUserModal.getMessage());
                            Session.set_userId(pref, updateUserModal.getUser_data().getUser_id());
                            Session.setFirstName(pref, updateUserModal.getUser_data().getFirst_name());
                            Session.setLastName(pref, updateUserModal.getUser_data().getLast_name());
                            Session.setUserEmail(pref, updateUserModal.getUser_data().getEmail());
                            Session.setAddress(pref, updateUserModal.getUser_data().getAddress());
                            Session.setUserMobile(pref, updateUserModal.getUser_data().getMobile_number());
                            update_profile.setVisibility(View.GONE);
                            fab_edit.setVisibility(View.VISIBLE);

                        } else {
                            ToastMsg.showShortToast(ProfileActivity.this, updateUserModal.getMessage());
                        }
                        hideLoading();
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
