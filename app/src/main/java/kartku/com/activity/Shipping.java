package kartku.com.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import kartku.com.R;

public class Shipping extends AppCompatActivity {


    EditText address, pincode, landmark, phone_no_, promo_code;
    Button but_apply_promocode, btn_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        address = (EditText) findViewById(R.id.address);
        pincode = (EditText) findViewById(R.id.pincode);
        landmark = (EditText) findViewById(R.id.landmark);
        phone_no_ = (EditText) findViewById(R.id.phone_no);
        promo_code = (EditText) findViewById(R.id.promo_code);


        but_apply_promocode = (Button) findViewById(R.id.but_apply_promocode);
        btn_contact = (Button) findViewById(R.id.btn_contact);

    }

}
