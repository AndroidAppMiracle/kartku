package kartku.com.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kartku.com.R;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;

import static kartku.com.utils.Utility.checkInternetConnection;

/**
 * Created by Kshitiz Bali on 4/12/2017.
 */

public class AboutUsFragment extends Fragment {

    WebView webview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.faq_layout, container, false);
        webview = (WebView) view.findViewById(R.id.web_view);

        // Toast.makeText(getActivity(), "HELLO", Toast.LENGTH_LONG).show();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.about_us);
        try {
            if (checkInternetConnection(getActivity())) {
                loadUrlWithWebView(Constant.ABOUT_US_URl);
            } else {
                ((Utility) getActivity()).showAlertDialog(getActivity());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void loadUrlWithWebView(String url) {
        try {
            webview.setWebViewClient(webViewClient);
            webview.loadUrl(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // Loading started for URL
            ((Utility) getActivity()).showLoading();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // Redirecting to URL
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // Loading finished for URL
            ((Utility) getActivity()).hideLoading();
        }
    };
}
