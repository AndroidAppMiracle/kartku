package kartku.com.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kartku.com.MyOrders.modal.MyOrderModal;
import kartku.com.R;
import kartku.com.adapter.MyOrdersAdapter;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/29/2017.
 */

public class MyOrdersActivity extends Utility implements ApiServerResponse {

    SharedPreferences pref;
    TextView textView;
    RecyclerView recycler_view_my_orders;
    MyOrdersAdapter adapter;


    List<MyOrderModal.ResultsBean> myOrdersList;


    boolean pendingIntroAnimation;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders_activity);
        //setHasOptionsMenu(true);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.my_orders));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                //showAlertDialog();
            }
        });
        pref = new ObscuredSharedPreferences(MyOrdersActivity.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        textView = (TextView) findViewById(R.id.textView);
        recycler_view_my_orders = (RecyclerView) findViewById(R.id.recycler_view_my_orders);

        try {

            if (checkInternetConnection(MyOrdersActivity.this)) {
                showLoading();
                ServerAPI.getInstance().getMyOrders(ApiServerResponse.MY_ORDERS, Session.get_userId(pref), this);
            } else {
                showAlertDialog(MyOrdersActivity.this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {
            hideLoading();
            if (response.isSuccessful()) {

                MyOrderModal myOrderModal;

                myOrdersList = new ArrayList<>();

                switch (tag) {
                    case ApiServerResponse.MY_ORDERS:

                        myOrderModal = (MyOrderModal) response.body();

                        if (myOrderModal.getStatus().equals(Constant.OK)) {

                            if (myOrderModal.getResults().isEmpty()) {
                                recycler_view_my_orders.setVisibility(View.GONE);
                                textView.setVisibility(View.VISIBLE);
                                textView.setText(R.string.no_orders);
                            } else {
                                myOrdersList = myOrderModal.getResults();
                            }

                            adapter = new MyOrdersAdapter(MyOrdersActivity.this, myOrdersList);
                        }

                        recycler_view_my_orders.setAdapter(adapter);
                        recycler_view_my_orders.setItemAnimator(new DefaultItemAnimator());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyOrdersActivity.this);
                        recycler_view_my_orders.setLayoutManager(mLayoutManager);
                        if (pendingIntroAnimation) {
                            pendingIntroAnimation = false;
                            startIntroAnimation();
                        }


                        break;
                    default:
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case ApiServerResponse.MY_ORDERS:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startIntroAnimation() {
        recycler_view_my_orders.setTranslationY(recycler_view_my_orders.getHeight());
        recycler_view_my_orders.setAlpha(0f);
        recycler_view_my_orders.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }
}