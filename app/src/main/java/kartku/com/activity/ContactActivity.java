package kartku.com.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import kartku.com.R;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.ContactUsModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/29/2017.
 */

public class ContactActivity extends Utility implements View.OnClickListener, ApiServerResponse {

    EditText name, email, subject, message;
    Button btn_send;
    SharedPreferences pref;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us_activity);

        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.contact_us);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                //showAlertDialog();
            }
        });
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        subject = (EditText) findViewById(R.id.subject);
        message = (EditText) findViewById(R.id.message);
        btn_send = (Button) findViewById(R.id.contact_btn);

        btn_send.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.contact_btn:
                try {
                    if (checkInternetConnection(ContactActivity.this)) {
                        if (validateFields(name.getText().toString().trim(), email.getText().toString().trim(), subject.getText().toString().trim(), message.getText().toString().trim())) {
                            showLoading();
                            ServerAPI.getInstance().contactUs(ApiServerResponse.CONTACT_US, Session.get_userId(pref), name.getText().toString().trim(), email.getText().toString().trim(), subject.getText().toString().trim(), message.getText().toString().trim(), this);
                        }

                        //new ContactManager(ContactActivity.this, this).sendRequest(name.getText().toString().trim(), email.getText().toString().trim(), subject.getText().toString().trim(), message.getText().toString().trim());
                        //hideLoading();
                    } else {
                        showAlertDialog(ContactActivity.this);
                    }

                    //Toast.makeText(Contact.this,"clicked event triggerrrr" , Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

 /*   @Override
    public void onContactSuccess(String response) {
        try {
            ToastMsg.showShortToast(ContactActivity.this, getResources().getString(R.string.contact_us_success));
            name.setText("");
            email.setText("");
            subject.setText("");
            message.setText("");
            //Intent intent = new Intent(getActivity(), DemoHomeDashBoard.class);
            // startActivity(intent);
            //  getActivity().finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
*/

   /* @Override
    public void onContactError(String error) {
        ToastMsg.showShortToast(ContactActivity.this, error);
    }*/

    private boolean validateFields(String name, String email, String subject, String message) {
        if (name.length() <= 0) {
            ToastMsg.showShortToast(ContactActivity.this, getResources().getString(R.string.username));
            return false;
        } else if (email.length() <= 0) {
            ToastMsg.showShortToast(ContactActivity.this, getResources().getString(R.string.email));
            return false;
        } else if (email.length() > 0 && !Validation.validateEmail(email)) {
            ToastMsg.showShortToast(ContactActivity.this, getResources().getString(R.string.email_validation));
            return false;
        } else if (subject.length() <= 0) {
            ToastMsg.showShortToast(ContactActivity.this, getResources().getString(R.string.subject));
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            ContactUsModal contactUsModal;

            switch (tag) {

                case ApiServerResponse.CONTACT_US:

                    contactUsModal = (ContactUsModal) response.body();
                    if (contactUsModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                        ToastMsg.showShortToast(ContactActivity.this, contactUsModal.getMessage());
                        name.setText("");
                        email.setText("");
                        subject.setText("");
                        message.setText("");
                    } else {
                        ToastMsg.showShortToast(ContactActivity.this, contactUsModal.getMessage());
                    }
                    hideLoading();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            ToastMsg.showShortToast(ContactActivity.this, throwable.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                   *//* Intent intent = new Intent(getActivity(), Home_dashboard.class);
                    startActivity(intent);
                    getActivity().finish();*//*
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }
*/
   /* @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent( getActivity(), Home_dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }*/
}
