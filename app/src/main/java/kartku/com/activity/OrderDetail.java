package kartku.com.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.OrderDetailsAdapter;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.orders_module.modal.OrderDetailsModal;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

public class OrderDetail extends Utility implements ApiServerResponse {

    TextView username, contact_no, email_id, address;
    RecyclerView recycleviewList;
    String orderID, orderStatus;
    Button btn_confirm_payment;

    OrderDetailsAdapter adapter;
    //Amount Calculation Section
    TextView tv_amount, tv_shipping_cost, tv_tax, /*tv_total_amount,*/
            tv_grand_total;

    List<OrderDetailsModal.OrderDetailBean.OrderItemsBean> myOrderProductsList;
    List<OrderDetailsModal.OrderDetailBean.ShippingDetailBean> myOrderShippingDetails;
    boolean pendingIntroAnimation;
    String orderConfirmationStatus;
    SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        btn_confirm_payment = (Button) findViewById(R.id.btn_confirm_payment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_order_detail);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderConfirmationStatus != null && !orderConfirmationStatus.equalsIgnoreCase("") && orderConfirmationStatus.equalsIgnoreCase(Constant.SUCCESS)) {
                    Intent home = new Intent(OrderDetail.this, DemoHomeDashBoard.class);
                    startActivity(home);
                    finish();

                } else if (getIntent().getExtras().getString("Notification_Flag") != null) {
                    if (getIntent().getExtras().getString("Notification_Flag").equalsIgnoreCase("true")) {
                        Intent home = new Intent(OrderDetail.this, DemoHomeDashBoard.class);
                        startActivity(home);
                        finish();
                    }
                } else {
                    finish();
                }

            }
        });
        /*setListener();*/
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.title_activity_order_detail);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Intent intent = getIntent();
        //String str = getIntent().getStringExtra(Constant.ORDER_ID);

        if (getIntent().getExtras() != null) {
            orderID = getIntent().getExtras().getString(Constant.ORDER_ID);
        }

       /* if (getIntent().getExtras().getString(Constant.ORDER_ID) != null) {

            orderID = getIntent().getExtras().getString(Constant.ORDER_ID);
        }*/
        if (getIntent().getExtras().getString(Constant.ORDER_CONFIRMATION_STATUS) != null) {
            orderConfirmationStatus = getIntent().getExtras().getString(Constant.ORDER_CONFIRMATION_STATUS);
        }

        if (getIntent().getExtras().getString(Constant.ORDER_STATUS) != null) {

            orderStatus = getIntent().getExtras().getString(Constant.ORDER_STATUS);
        }

        if (orderStatus != null && orderStatus.contains(Constant.PENDING)) {
            btn_confirm_payment.setVisibility(View.VISIBLE);
        } else {
            btn_confirm_payment.setVisibility(View.GONE);
        }

        //ToastMsg.showLongToast(OrderDetail.this, orderID);

        username = (TextView) findViewById(R.id.username);
        contact_no = (TextView) findViewById(R.id.contact_no);
        email_id = (TextView) findViewById(R.id.email_id);
        address = (TextView) findViewById(R.id.address);
        recycleviewList = (RecyclerView) findViewById(R.id.recycleviewList);


        tv_amount = (TextView) findViewById(R.id.tv_amount);
        tv_shipping_cost = (TextView) findViewById(R.id.tv_shipping_cost);
        tv_tax = (TextView) findViewById(R.id.tv_tax);
        //tv_total_amount = (TextView) findViewById(R.id.tv_total_amount);
        tv_grand_total = (TextView) findViewById(R.id.tv_grand_total);

        btn_confirm_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent intent = new Intent(OrderDetail.this, PaymentActivity.class);
                    intent.putExtra(Constant.ORDER_ID, orderID.trim());
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        try {

            if (checkInternetConnection(OrderDetail.this)) {
                showLoading();
                ServerAPI.getInstance().getOrderDetail(ApiServerResponse.ORDER_DETAIL, Session.get_userId(pref), orderID, this);
            } else {
                showAlertDialog(OrderDetail.this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            hideLoading();
            if (response.isSuccessful()) {
                //ToastMsg.showLongToast(OrderDetail.this, response.toString());
                OrderDetailsModal orderDetailsModal;
                myOrderProductsList = new ArrayList<>();
                Resources resources = getResources();

                switch (tag) {
                    case ApiServerResponse.ORDER_DETAIL:

                        orderDetailsModal = (OrderDetailsModal) response.body();
                        if (orderDetailsModal.getStatus().equals(Constant.OK)) {
                            //username.setText(Session.getuser);

                            //Log.i("Tax", "" + orderDetailsModal.getOrder_detail().getTax());

                        /*    Log.i("Data", orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getFirstname() + " " + orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getLastname() +
                                    " " + orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getMobile() + " " +
                                    orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getEmail() + " " +
                                    orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getAddress() + " " +
                                    orderDetailsModal.getOrder_detail().getTotal_amount() + " " +
                                    orderDetailsModal.getOrder_detail().getShipping_cost() + " " +
                                    orderDetailsModal.getOrder_detail().getTax() + " " +
                                    orderDetailsModal.getOrder_detail().getGrand_total());
*/

                            username.setText(String.format(resources.getString(R.string.three_parameters_options_semi_colon), getString(R.string.name), orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getFirstname(), orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getLastname()));
                            contact_no.setText(String.format(resources.getString(R.string.two_parameters), getString(R.string.contact_no), String.valueOf(orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getMobile())));
                            email_id.setText(String.format(resources.getString(R.string.two_parameters), getString(R.string.email_id), orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getEmail()));
                            address.setText(String.format(resources.getString(R.string.two_parameters), getString(R.string.address_simple), orderDetailsModal.getOrder_detail().getShipping_detail().get(0).getAddress()));


                            tv_amount.setText(String.format(resources.getString(R.string.two_parameters), getString(R.string.amount), orderDetailsModal.getOrder_detail().getTotal_amount()));

                            tv_shipping_cost.setText(String.format(resources.getString(R.string.two_parameters), getString(R.string.shipping_cost), orderDetailsModal.getOrder_detail().getShipping_cost()));
                            tv_tax.setText(String.format(resources.getString(R.string.two_parameters), getString(R.string.tax), String.valueOf(orderDetailsModal.getOrder_detail().getTax())));
                            //tv_total_amount.setText(String.format(resources.getString(R.string.two_parameters), "Total Amount", orderDetailsModal.getOrder_detail().getTotal_amount()));
                            tv_grand_total.setText(String.format(resources.getString(R.string.two_parameters), getString(R.string.grand_total), String.valueOf(orderDetailsModal.getOrder_detail().getGrand_total())));

                            /*tv_amount.setText(orderDetailsModal.getOrder_detail().getTotal_amount());
                            tv_shipping_cost.setText(orderDetailsModal.getOrder_detail().getShipping_cost());
                            tv_tax.setText(String.valueOf(orderDetailsModal.getOrder_detail().getTax()));
                            tv_total_amount.setText(orderDetailsModal.getOrder_detail().getTotal_amount());
                            tv_grand_total.setText(String.valueOf(orderDetailsModal.getOrder_detail().getGrand_total()));*/
                            myOrderProductsList = orderDetailsModal.getOrder_detail().getOrder_items();

                            adapter = new OrderDetailsAdapter(OrderDetail.this, myOrderProductsList);
                            recycleviewList.setAdapter(adapter);
                            recycleviewList.setItemAnimator(new DefaultItemAnimator());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderDetail.this);
                            recycleviewList.setLayoutManager(mLayoutManager);
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }
                        }


                        break;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case ApiServerResponse.ORDER_DETAIL:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void startIntroAnimation() {
        recycleviewList.setTranslationY(recycleviewList.getHeight());
        recycleviewList.setAlpha(0f);
        recycleviewList.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    @Override
    public void onBackPressed() {
        if (orderConfirmationStatus != null && !orderConfirmationStatus.equalsIgnoreCase("") && orderConfirmationStatus.equalsIgnoreCase(Constant.SUCCESS)) {
            Intent home = new Intent(OrderDetail.this, DemoHomeDashBoard.class);
            startActivity(home);
            finish();

        } else if (getIntent().getExtras().getString("Notification_Flag") != null) {
            if (getIntent().getExtras().getString("Notification_Flag").equalsIgnoreCase("true")) {
                Intent home = new Intent(OrderDetail.this, DemoHomeDashBoard.class);
                startActivity(home);
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }
}
