package kartku.com.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.SubProductCategoryDetailAdapter;
import kartku.com.cart.CartDetailsNew;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.faq_module.FAQ;
import kartku.com.login_module.Login;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.EventBusLogout;
import kartku.com.retrofit.modal.SubProductCategoryListModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitizb on 15-03-2017.
 */

public class SubProductCategoryList extends Utility implements ApiServerResponse {


    TextView tv_shop_by_category_label, tv_count;
    ImageView iv_category_banner_image;
    RecyclerView rv_categories;
    Toolbar toolbar;

    SharedPreferences pref;
    List<SubProductCategoryListModal.SubcategoryDetailBean.SubcategoriesBean> list = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_category_list);


        pref = new ObscuredSharedPreferences(SubProductCategoryList.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.shop_by_category);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tv_shop_by_category_label = (TextView) findViewById(R.id.tv_shop_by_category_label);
        iv_category_banner_image = (ImageView) findViewById(R.id.iv_category_banner_image);
        rv_categories = (RecyclerView) findViewById(R.id.rv_categories);

        Glide.with(SubProductCategoryList.this)
                .load(Session.getHomeScreenBanner(pref))
                .thumbnail(0.1f)
                .into(iv_category_banner_image);


        try {

            if (checkInternetConnection(SubProductCategoryList.this)) {
                if (getIntent().getExtras().getString(Constant.CATEGORY_ID) != null) {
                    String categoryID = getIntent().getExtras().getString(Constant.CATEGORY_ID);

                    showLoading();
                    ServerAPI.getInstance().getSubProductCategoryDetails(ApiServerResponse.SUB_PRODUCT_CATEGORY, Session.get_userId(pref), categoryID, this);

                }

            } else {
                showAlertDialog(SubProductCategoryList.this);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Session.get_login_statuc(pref)) {
            getMenuInflater().inflate(R.menu.menu_universal_login, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_universal_logout, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SubProductCategoryList.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.menu_home:

                Intent intentHome = new Intent(SubProductCategoryList.this, DemoHomeDashBoard.class);
                intentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentHome);
                break;
            case R.id.search:

                Intent search = new Intent(SubProductCategoryList.this, SearchProductsBrandActivity.class);
                startActivity(search);

                break;
            case R.id.menu_count:
                Intent in = new Intent(SubProductCategoryList.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
                /*Intent intent1 = new Intent(DemoHomeDashBoard.this, SellerHomeDashBoard.class);
                startActivity(intent1);*/
                break;
            case R.id.menu_my_orders:


                Intent intent1 = new Intent(SubProductCategoryList.this, MyOrdersActivity.class);
                startActivity(intent1);
                //MyOrdersFragment ordersFragment = new MyOrdersFragment();
                            /*OrdersFragment ordersFragment = new OrdersFragment();*/
                //tag = "My Orders";
                /*ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);*/
                //swtichFragement(R.id.content_frame, ordersFragment, tag);

                break;
            case R.id.menu_my_product_review:

                Intent intentReviews = new Intent(SubProductCategoryList.this, MyProductReviewsActivity.class);
                startActivity(intentReviews);

                break;
            case R.id.menu_my_wishlist:
                Intent intentWishlist = new Intent(SubProductCategoryList.this, WishlistActivity.class);
                startActivity(intentWishlist);
                break;
            case R.id.menu_my_profile:
                Intent intentProfile = new Intent(SubProductCategoryList.this, ProfileActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.menu_contact_us:

                Intent intentContact = new Intent(SubProductCategoryList.this, ContactActivity.class);
                startActivity(intentContact);

                break;
            case R.id.menu_logout:

                //Session.getLogin_type(pref);

                EventBus.getDefault().post(new EventBusLogout(Session.getLogin_type(pref)));
                /*switch (loginType) {
                    case "google":
                        if (mGoogleApiClient.isConnected()) {
                            signOut();
                            drawerLayout.closeDrawers();
                            //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                            mGoogleApiClient.disconnect();
                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            LoginFragment fragmentLoginLogout = new LoginFragment();
                            tag = "Login";
                            swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);

                        }
                        break;
                    case "facebook": {
                        LoginManager.getInstance().logOut();
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        break;
                    }
                    default: {
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        //prepareListData();
                        Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                        startActivity(i);
                        finish();
                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        break;
                    }
                }*/

                break;

            case R.id.menu_login:
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                Intent intent = new Intent(SubProductCategoryList.this, Login.class);
                startActivity(intent);
                break;
            case R.id.menu_faq:

                Intent intentFAQ = new Intent(SubProductCategoryList.this, FAQ.class);
                startActivity(intentFAQ);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                SubProductCategoryListModal subProductCategoryListModal;
                CartItemCountModal cartItemCountModal;

                switch (tag) {
                    case ApiServerResponse.SUB_PRODUCT_CATEGORY:
                        subProductCategoryListModal = (SubProductCategoryListModal) response.body();
                        if (subProductCategoryListModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            getSupportActionBar().setTitle(subProductCategoryListModal.getSubcategory_detail().getTitle());
                            tv_shop_by_category_label.setText(subProductCategoryListModal.getSubcategory_detail().getTitle());
                            list = subProductCategoryListModal.getSubcategory_detail().getSubcategories();
                            SubProductCategoryDetailAdapter adapter = new SubProductCategoryDetailAdapter(SubProductCategoryList.this, list);
                            rv_categories.setAdapter(adapter);
                            rv_categories.setLayoutManager(new LinearLayoutManager(this));
                            if (!Session.getcart_id(pref).equalsIgnoreCase("0")) {
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                            }

                        } else {
                            if (!Session.getcart_id(pref).equalsIgnoreCase("0")) {
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                            }
                        }

                        hideLoading();
                        break;

                    case ApiServerResponse.CART_ITEM_COUNT:
                        cartItemCountModal = (CartItemCountModal) response.body();
                        if (cartItemCountModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            tv_count.setText(cartItemCountModal.getItem_count());
                            Session.setCartItemsQuantity(pref, cartItemCountModal.getItem_count());
                        } else {
                            tv_count.setText("0");
                            Session.setCartItemsQuantity(pref, "0");
                        }


                        break;
                }
            } else {

                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }
}
