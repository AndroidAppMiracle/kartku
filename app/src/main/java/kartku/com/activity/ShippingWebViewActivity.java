package kartku.com.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import kartku.com.R;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 3/18/2017.
 */

public class ShippingWebViewActivity extends Utility {

    private WebView mWebView;
    String orderID;

    private SharedPreferences prefrence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_webview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        prefrence = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.shipping);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //finish();
                showAlertDialog();
            }
        });
        /*setListener();*/
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (getIntent().getExtras().getString(Constant.ORDER_ID) != null) {
                orderID = getIntent().getExtras().getString(Constant.ORDER_ID);
            }

            mWebView = (WebView) findViewById(R.id.activity_main_webview);

            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            if (checkInternetConnection(ShippingWebViewActivity.this)) {
                /*String urlStart = "http://dev.miracleglobal.com/kartku-php/web/product/order/payment?orderId=" + orderID + "&user_id=" + Session.get_userId(prefrence);*/
                String urlStart = Constant.SHIPPING_URL + orderID + "&user_id=" + Session.get_userId(prefrence);
        /*String urlStart = "http://dev.miracleglobal.com/kartku-php/web/product/order/payment?orderId=" + orderID + "&user_id=" + Session.get_userId(prefrence);*/
                //String shippingURL = urlStart.concat(orderID).concat("&user_id=").concat(Session.get_userId(prefrence));

                mWebView.loadUrl(urlStart);
        /*mWebView.loadUrl("http://dev.miracleglobal.com/kartku-php/web/checkout/snap?order_id=1");*/
        /*mWebView.loadUrl("http://dev.miracleglobal.com/kartku-php/web/product/order/payment?orderId=158&user_id=181");*/
                // Force links and redirects to open in the WebView instead of in a browser
                mWebView.setWebViewClient(new ShippingWebViewClient(ShippingWebViewActivity.this, orderID));

            } else {
                showAlertDialog(ShippingWebViewActivity.this);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            //super.onBackPressed();
            showAlertDialog();
        }
    }

    private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage(R.string.sure_you_want_to_exit);
            dialogBuilder.setCancelable(true);
            dialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });
            dialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    finish();
                   /* Intent intent = new Intent(ShippingWebViewActivity.this, OrderConfirmation.class);
                    intent.putExtra(Constant.ORDER_ID, orderID.trim());
                    intent.putExtra(Constant.WEB_ADDRESS, Constant.WEB_PAYMENT_CANCEL);
                    startActivity(intent);*/
                }
            });
           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*private void showAlertDialogNetworkConnection() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Connection Problem.");
            dialogBuilder.setMessage("Please check your internet connection?");
            dialogBuilder.setCancelable(true);
           *//* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    finish();
                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
