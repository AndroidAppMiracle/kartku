package kartku.com.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import kartku.com.R;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.EventBusPromoCodeApplied;
import kartku.com.retrofit.modal.PromoCodeModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 4/21/2017.
 */

public class PromoCodeDialog extends Utility implements ApiServerResponse {


    EditText et_promo_code;
    TextView tv_skip_prromo_code, tv_apply_promo_code, tv_submit, tv_promo_code_discount, tv_total_amount, tv_promo_code_applied_response_message;
    LinearLayout ll_calculation_and_discount, ll_skip_apply;
    private SharedPreferences prefrence;
    String orderID;
    Resources res;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_promo_code);
        this.setFinishOnTouchOutside(false);
        prefrence = new ObscuredSharedPreferences(PromoCodeDialog.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        res = getResources();
        et_promo_code = (EditText) findViewById(R.id.et_promo_code);

        tv_skip_prromo_code = (TextView) findViewById(R.id.tv_skip_prromo_code);
        tv_apply_promo_code = (TextView) findViewById(R.id.tv_apply_promo_code);
        tv_submit = (TextView) findViewById(R.id.tv_submit);
        tv_promo_code_discount = (TextView) findViewById(R.id.tv_promo_code_discount);
        tv_total_amount = (TextView) findViewById(R.id.tv_total_amount);
        tv_promo_code_applied_response_message = (TextView) findViewById(R.id.tv_promo_code_applied_response_message);
        ll_skip_apply = (LinearLayout) findViewById(R.id.ll_skip_apply);
        ll_calculation_and_discount = (LinearLayout) findViewById(R.id.ll_calculation_and_discount);

        if (getIntent().getExtras().getString(Constant.ORDER_ID) != null) {
            orderID = getIntent().getExtras().getString(Constant.ORDER_ID);
        }

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(PromoCodeDialog.this, ShippingWebViewActivity.class);
                i.putExtra(Constant.ORDER_ID, orderID);
                startActivity(i);
                finish();
            }
        });

        tv_skip_prromo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent i = new Intent(PromoCodeDialog.this, ShippingWebViewActivity.class);
                i.putExtra(Constant.ORDER_ID, orderID);
                startActivity(i);*/
                finish();

            }
        });

        tv_apply_promo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (et_promo_code.getText().toString().equalsIgnoreCase("")) {
                        tv_promo_code_applied_response_message.setText(getResources().getString(R.string.empty_promo_code_error));
                    } else {
                        showLoading();
                        if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                            showLoading();
                            //ServerAPI.getInstance().placeOrder(ApiServerResponse.PLACE_ORDER_PROMO_CODE, Session.getcart_id(prefrence), CartDetailsNew.this);
                            ServerAPI.getInstance().setPromoCode(ApiServerResponse.SET_PROMOCODE, Session.get_userId(prefrence), Session.getcart_id(prefrence), et_promo_code.getText().toString(), PromoCodeDialog.this);


                        }

                        // if (!orderIDToShipping.equalsIgnoreCase("")){
                        //  }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            PromoCodeModal promoCodeModal;

            switch (tag) {

                case ApiServerResponse.SET_PROMOCODE:
                    promoCodeModal = (PromoCodeModal) response.body();
//                    Log.i("CODE", promoCodeModal.getStatus());
                    if (promoCodeModal.getStatus().equalsIgnoreCase(Constant.OK)) {


                        //orderIDToShipping= String.valueOf(promoCodeModal.getCart_info().getId());

                          /*  Intent i = new Intent(CartDetailsNew.this, ShippingWebViewActivity.class);
                            i.putExtra(Constant.ORDER_ID, orderID.trim());
                            startActivity(i);*/

                        setPromoCodeResponse(promoCodeModal.getCart_info().getDiscount(), promoCodeModal.getCart_info().getTotal_amount());

                        //orderID = String.valueOf(promoCodeModal.getCart_info().getId());

                        //amountDiscountPromoCode = promoCodeModal.getCart_info().getDiscount();
                        //totalAmountAfterDiscountPromoCode = promoCodeModal.getCart_info().getTotal_amount();

                         /*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                tv_promo_code_applied_response_message.setText(getResources().getString(R.string.promo_code_applied_succ));
                                tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorPrimary, null));
                            } else {
                                tv_promo_code_applied_response_message.setText(getResources().getString(R.string.promo_code_applied_succ));
                                tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorPrimary));
                            }*/
                    } else {
                        setPromoCodeResponse("", "");

/*
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                tv_promo_code_applied_response_message.setText(getResources().getString(R.string.invalid_promo_code_error_msg));
                                tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorLogoRed, null));
                            } else {
                                tv_promo_code_applied_response_message.setText(getResources().getString(R.string.invalid_promo_code_error_msg));
                                tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorLogoRed));
                            }*/
                    }


                    hideLoading();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

        hideLoading();
    }

    public void setPromoCodeResponse(String discountAmount, String totalAmount) {
        try {
            if (!discountAmount.equalsIgnoreCase("") && !totalAmount.equalsIgnoreCase("")) {
                //tv_dialog_close.setVisibility(View.GONE);
                ll_calculation_and_discount.setVisibility(View.VISIBLE);
                et_promo_code.setEnabled(false);
                tv_promo_code_discount.setText(String.format(res.getString(R.string.promo_code_discount_in_RP), discountAmount));
                tv_total_amount.setText(String.format(res.getString(R.string.price_in_RP), totalAmount));
                //total_amount.setText(String.format(res.getString(R.string.total_price_in_RP), totalAmountAfterDiscountPromoCode));
                ll_skip_apply.setVisibility(View.GONE);
                tv_submit.setVisibility(View.VISIBLE);
                tv_promo_code_applied_response_message.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_promo_code_applied_response_message.setText(getResources().getString(R.string.promo_code_applied_succ));
                    tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorPrimary, null));
                } else {
                    tv_promo_code_applied_response_message.setText(getResources().getString(R.string.promo_code_applied_succ));
                    tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorPrimary));
                }

                EventBus.getDefault().post(new EventBusPromoCodeApplied(Constant.OK));
            } else {
                //tv_dialog_close.setVisibility(View.VISIBLE);
                ll_calculation_and_discount.setVisibility(View.GONE);
                //tv_promo_code_discount.setText(amountDiscountPromoCode);
                //tv_total_amount.setText(totalAmountAfterDiscountPromoCode);
                ll_skip_apply.setVisibility(View.VISIBLE);
                tv_submit.setVisibility(View.GONE);
                tv_promo_code_applied_response_message.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    tv_promo_code_applied_response_message.setText(getResources().getString(R.string.invalid_promo_code_error_msg));
                    tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorLogoRed, null));
                } else {
                    tv_promo_code_applied_response_message.setText(getResources().getString(R.string.invalid_promo_code_error_msg));
                    tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorLogoRed));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

