package kartku.com.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.adapter.AllCategoriesAdapter;
import kartku.com.retrofit.modal.HomeScreenModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;

/**
 * Created by Kshitiz Bali on 3/14/2017.
 */

public class AllCategoriesActivity extends Utility {

    RecyclerView rv_all_categories;
    AllCategoriesAdapter adapter;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.shop_by_category));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rv_all_categories = (RecyclerView) findViewById(R.id.rv_all_categories);

        try {


            ArrayList<HomeScreenModal.CategoryListBean> list = new ArrayList<HomeScreenModal.CategoryListBean>();
            list = getIntent().getExtras().getParcelableArrayList(Constant.CATEGORY_ALL);

            adapter = new AllCategoriesAdapter(AllCategoriesActivity.this, list);
            rv_all_categories.setAdapter(adapter);
            rv_all_categories.setItemAnimator(new DefaultItemAnimator());
            rv_all_categories.setLayoutManager(new LinearLayoutManager(AllCategoriesActivity.this));


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
