package kartku.com.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import kartku.com.R;
import kartku.com.adapter.CategoryProductsAdapter;
import kartku.com.adapter.SearchAdapter;
import kartku.com.adapter.SearchResultListAdapter;
import kartku.com.cart.CartDetailsNew;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.faq_module.FAQ;
import kartku.com.login_module.Login;
import kartku.com.product_category.GridSpacingItemDecoration;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.CategoryProductsModal;
import kartku.com.retrofit.modal.EventBusCategoryProductsUpdateWishlist;
import kartku.com.retrofit.modal.EventBusLogout;
import kartku.com.retrofit.modal.EventBusProductDetailUpdateModal;
import kartku.com.retrofit.modal.ProductSearchBrandModal;
import kartku.com.retrofit.modal.SearchModal;
import kartku.com.sort.BrandListActivity;
import kartku.com.sort.MaterialList;
import kartku.com.utils.Constant;
import kartku.com.utils.ResponseKeys;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/15/2017.
 */

public class CategoryProducts extends Utility implements ApiServerResponse {


    Toolbar toolbar;
    RecyclerView recyclerView;
    int spanCount = 2; // 3 columns
    int spacing = 10; // 50px
    SharedPreferences pref;
    boolean includeEdge = false;
    boolean pendingIntroAnimation;
    String id;
    String textPathList;
    TextView tv_productPath, tv_productListPath;
    Button buttonFilter, buttonSort;

    List<CategoryProductsModal.ProductsDataBean> productList = new ArrayList<>();
    List<ProductSearchBrandModal.ProductsDataBean> searchResultList = new ArrayList<>();
    String categoryName;
    static final int PICK_BRAND_LIST = 1;
    static final int PICK_MATERIAL_LIST = 2;
    private boolean filterable;
    private int brandMaterialSearched = 0;
    private String filterQueryBrand, filterQueryMaterial;
    TextView tv_message;
    String categoryID;
    TextView tv_count;
    LinearLayout ll_filters;
    List<SearchModal.ProductsDataBean> searchListResult = new ArrayList<>();
    CategoryProductsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_products);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.products);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        recyclerView.setItemAnimator(new SlideInUpAnimator());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
        recyclerView.setLayoutManager(layoutManager);
        tv_productPath = (TextView) findViewById(R.id.tv_productPath);
        tv_productListPath = (TextView) findViewById(R.id.tv_productListPath);
        ll_filters = (LinearLayout) findViewById(R.id.ll_filters);

        buttonFilter = (Button) findViewById(R.id.buttonFilter);
        buttonSort = (Button) findViewById(R.id.buttonSort);
        tv_message = (TextView) findViewById(R.id.tv_message);


        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));


        id = Session.get_selected_category_id(pref);


//        Log.d("", "selected cat id " + id);
        //ShowingProgressDialog


        //Utility.includeProgressDialog(DemoProductList.this, true);


        try {
            if (checkInternetConnection(CategoryProducts.this)) {

                if (!Session.getcart_id(pref).equalsIgnoreCase("0")) {
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                }


                if (getIntent().getExtras().getString(Constant.CATEGORY_ID) != null && getIntent().getExtras().getString(Constant.CATEGORY_NAME) != null) {
                    categoryID = getIntent().getExtras().getString(Constant.CATEGORY_ID);
                    categoryName = getIntent().getExtras().getString(Constant.CATEGORY_NAME);
                    showLoading();
                    //ServerAPI.getInstance().getProductDetail(ApiServerResponse.PRODUCT_DETAIL, "17", this);
                    ServerAPI.getInstance().getCategoryProducts(ApiServerResponse.CATEGORY_PRODUCTS, categoryID, Session.get_userId(pref), this);
                    String cart_id = Session.getcart_id(pref);

                }
            } else {
                showAlertDialog(CategoryProducts.this);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Intent intent = getIntent();
            String search_query = intent.getStringExtra("search_query");
            /*String sub_cat_heading = intent.getStringExtra("sub_cat_heading");*/
            String cat_name = intent.getStringExtra(ResponseKeys.SUB_SUB_CAT_NAME);
            // String search_cat = intent.getStringExtra("search_cat");
            //int search_id = intent.getIntExtra("search_id",0);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Resources res = getResources();

            String text = String.format(res.getString(R.string.product_path), Session.getselectedCategory(pref));
            textPathList = String.format(res.getString(R.string.product_path_list_name), cat_name);
            //ToastMsg.showLongToast(DemoProductList.this, "Cat " + text + " SubCat " + textPathList);
            CharSequence styledText = Html.fromHtml(textPathList);
            //setMultipleColourText(Session.getselectedCategory(pref), Session.getselectedSubCategory(pref), tv_productPath);
            tv_productPath.setText(text);
            if (tv_productListPath.getVisibility() == View.GONE) {
                tv_productListPath.setVisibility(View.VISIBLE);
            }
            tv_productListPath.setText(styledText);

            //new SearchManager(getApplicationContext(), this).sendRequest(search_query);

            buttonFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterDialog();
                }
            });

            buttonSort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    openDialogforMasterCat();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.clear();
        if (Session.get_login_statuc(pref)) {
            getMenuInflater().inflate(R.menu.menu_universal_extra_options_login, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_universal_extra_options_logout, menu);
        }
        //getMenuInflater().inflate(R.menu.menu_cart_count_product_list_login, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CategoryProducts.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case R.id.menu_home:

                Intent intentHome = new Intent(CategoryProducts.this, DemoHomeDashBoard.class);
                intentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentHome);
                break;

            case R.id.search:

                Intent search = new Intent(CategoryProducts.this, SearchProductsBrandActivity.class);
                startActivity(search);

                break;
            case R.id.menu_count:
                Intent in = new Intent(CategoryProducts.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
                /*Intent intent1 = new Intent(DemoHomeDashBoard.this, SellerHomeDashBoard.class);
                startActivity(intent1);*/
                break;
            case R.id.menu_my_orders:


                Intent intent1 = new Intent(CategoryProducts.this, MyOrdersActivity.class);
                startActivity(intent1);
                //MyOrdersFragment ordersFragment = new MyOrdersFragment();
                            /*OrdersFragment ordersFragment = new OrdersFragment();*/
                //tag = "My Orders";
                /*ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);*/
                //swtichFragement(R.id.content_frame, ordersFragment, tag);

                break;
            case R.id.menu_my_product_review:

                Intent intentReviews = new Intent(CategoryProducts.this, MyProductReviewsActivity.class);
                startActivity(intentReviews);

                break;
            case R.id.menu_my_wishlist:
                Intent intentWishlist = new Intent(CategoryProducts.this, WishlistActivity.class);
                startActivity(intentWishlist);
                break;
            case R.id.menu_my_profile:
                Intent intentProfile = new Intent(CategoryProducts.this, ProfileActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.menu_contact_us:

                Intent intentContact = new Intent(CategoryProducts.this, ContactActivity.class);
                startActivity(intentContact);

                break;
            case R.id.menu_privacy_policy:


                //if (checkInternetConnection(DemoHomeDashBoard.this)) {

                Intent intentPrivacyPolicy = new Intent(CategoryProducts.this, PrivacyPolicyActivity.class);
                startActivity(intentPrivacyPolicy);

                // swtichFragement(R.id.content_frame, privacyPolicyFragment, tag);


                break;
            case R.id.menu_about_us:

                Intent intentAboutUs = new Intent(CategoryProducts.this, AboutUsActivity.class);
                startActivity(intentAboutUs);
                break;
            case R.id.menu_logout:

                //Session.getLogin_type(pref);

                EventBus.getDefault().post(new EventBusLogout(Session.getLogin_type(pref)));
                /*switch (loginType) {
                    case "google":
                        if (mGoogleApiClient.isConnected()) {
                            signOut();
                            drawerLayout.closeDrawers();
                            //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                            mGoogleApiClient.disconnect();
                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            LoginFragment fragmentLoginLogout = new LoginFragment();
                            tag = "Login";
                            swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);

                        }
                        break;
                    case "facebook": {
                        LoginManager.getInstance().logOut();
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        break;
                    }
                    default: {
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        //prepareListData();
                        Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                        startActivity(i);
                        finish();
                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        break;
                    }
                }*/

                break;

            case R.id.menu_login:
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                Intent intent = new Intent(CategoryProducts.this, Login.class);
                startActivity(intent);
                break;
            case R.id.menu_faq:

                Intent intentFAQ = new Intent(CategoryProducts.this, FAQ.class);
                startActivity(intentFAQ);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {


            if (response.isSuccessful()) {

                CategoryProductsModal categoryProductsModal;
                ProductSearchBrandModal productSearchBrandModal;
                CartItemCountModal cartItemCountModal;
                SearchModal searchModal;

                switch (tag) {

                    case ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS:
                        searchModal = (SearchModal) response.body();

                        if (searchModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            tv_message.setVisibility(View.GONE);
                            buttonSort.setEnabled(true);
                            buttonFilter.setEnabled(true);
                            ll_filters.setVisibility(View.VISIBLE);
                            if (productList.size() != 0) {
                                productList.clear();
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }


                            }
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }

                            }
                            if (searchListResult.size() != 0) {
                                searchListResult.clear();
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }

                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                buttonSort.setBackgroundColor(getResources().getColor(R.color.colorLogoRed, null));
                                buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorPrimary, null));
                            } else {
                                buttonSort.setBackgroundColor(getResources().getColor(R.color.colorLogoRed));
                                buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            }

                            recyclerView.setVisibility(View.VISIBLE);
                            searchListResult = searchModal.getProducts_data();
                            SearchAdapter searchAdapter = new SearchAdapter(CategoryProducts.this, searchListResult);
                            recyclerView.setAdapter(searchAdapter);
                            searchAdapter.notifyDataSetChanged();
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }
                        } else {
                            tv_message.setVisibility(View.VISIBLE);
                            tv_message.setText(R.string.no_products_found);
                            recyclerView.setVisibility(View.GONE);
                            buttonSort.setEnabled(false);
                            buttonFilter.setEnabled(false);
                            ll_filters.setVisibility(View.GONE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                buttonSort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
                                buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
                            } else {
                                buttonSort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
                                buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
                            }

                        }


                        hideLoading();
                        break;

                    case ApiServerResponse.CART_ITEM_COUNT:
                        cartItemCountModal = (CartItemCountModal) response.body();
                        if (cartItemCountModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            Session.setCartItemsQuantity(pref, String.valueOf(cartItemCountModal.getItem_count()));
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            tv_count.setText(String.valueOf(cartItemCountModal.getItem_count()));
                        } else {
                            tv_count.setText("0");
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(pref, "0");
                        }
                        break;
                    case ApiServerResponse.CATEGORY_PRODUCTS:

                        categoryProductsModal = (CategoryProductsModal) response.body();

                        if (categoryProductsModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            if (getSupportActionBar() != null){

                                getSupportActionBar().setTitle(categoryName);
                            }
                            productList = categoryProductsModal.getProducts_data();

                            if (productList.size() != 0) {
                                tv_message.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                            } else {
                                tv_message.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }

                            buttonSort.setEnabled(true);
                            ll_filters.setVisibility(View.VISIBLE);
                            buttonFilter.setEnabled(true);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                buttonSort.setBackgroundColor(getResources().getColor(R.color.colorLogoRed, null));
                                buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorPrimary, null));
                            } else {
                                buttonSort.setBackgroundColor(getResources().getColor(R.color.colorLogoRed));
                                buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            }

                            /*if (productList.size() == 0) {

                                buttonSort.setEnabled(false);
                                buttonFilter.setEnabled(false);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    buttonSort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
                                    buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
                                } else {
                                    buttonSort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
                                    buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
                                }

                                tv_message.setVisibility(View.VISIBLE);
                                tv_message.setText("No Products Found");
                            } else {
                                tv_message.setVisibility(View.GONE);
                                buttonSort.setEnabled(true);
                                buttonFilter.setEnabled(true);
                            }
*/

                            adapter = new CategoryProductsAdapter(CategoryProducts.this, productList);
                            recyclerView.setAdapter(adapter);

                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            //if (productList.size() == 0) {

                            buttonSort.setEnabled(false);
                            buttonFilter.setEnabled(false);
                            ll_filters.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                buttonSort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
                                buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
                            } else {
                                buttonSort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
                                buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
                            }

                            tv_message.setVisibility(View.VISIBLE);
                            tv_message.setText(getResources().getString(R.string.no_products_found));
                            //} else {
                            //tv_message.setVisibility(View.GONE);
                            //buttonSort.setEnabled(true);
                            //buttonFilter.setEnabled(true);
                            //}
                        }

                        hideLoading();
                        break;

                    case ApiServerResponse.SEARCH_BY_BRAND:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                          /*  if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }*/
                            if (productList.size() != 0) {
                                productList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(CategoryProducts.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                           /* recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }

                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }


                        hideLoading();
                        break;

                    case ApiServerResponse.SEARCH_BY_MATERIAL:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                           /* if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }*/

                            if (productList.size() != 0) {
                                productList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(CategoryProducts.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                          /*  recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();
                        break;

                    case ApiServerResponse.SEARCH_BY_BRAND_AND_LOW:


                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            /*if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }*/

                            if (productList.size() != 0) {
                                productList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(CategoryProducts.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                           /* recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();

                        break;

                    case ApiServerResponse.SEARCH_BY_BRAND_AND_HIGH:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            /*if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }*/

                            if (productList.size() != 0) {
                                productList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(CategoryProducts.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                          /*  recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();

                        break;

                    case ApiServerResponse.SEARCH_BY_MATERIAL_AND_LOW:
                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                           /* if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }*/

                            if (productList.size() != 0) {
                                productList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(CategoryProducts.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                      /*      recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();

                        break;

                    case ApiServerResponse.SEARCH_BY_MATERIAL_AND_HIGH:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            /*if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }*/

                            if (productList.size() != 0) {
                                productList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(CategoryProducts.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                         /*   recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();


                        break;
                    case ApiServerResponse.SEARCH_BY_NEW_ARRIVAL:

                        productSearchBrandModal = (ProductSearchBrandModal) response.body();

                        if (productSearchBrandModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            /*if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }*/

                            if (productList.size() != 0) {
                                productList.clear();
                            }
                            searchResultList = productSearchBrandModal.getProducts_data();

                            SearchResultListAdapter adapter = new SearchResultListAdapter(CategoryProducts.this, searchResultList);
                            recyclerView.setAdapter(adapter);
                     /*       recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                            recyclerView.setItemAnimator(new SlideInUpAnimator());
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(CategoryProducts.this, 2);
                            recyclerView.setLayoutManager(layoutManager);*/
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            if (searchResultList.size() != 0) {
                                searchResultList.clear();
                            }
                            recyclerView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();

                        break;


                }

            } else {

                hideLoading();
                /*Log.i("Afas", "Adsas");*/
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {

        //Log.i("Afas", "Adsas");
        //switch (tag) {

        // case ApiServerResponse.CATEGORY_PRODUCTS:
        tv_message.setVisibility(View.VISIBLE);
        tv_message.setText(getResources().getString(R.string.no_products_found));
        buttonSort.setEnabled(false);
        buttonFilter.setEnabled(false);
        ll_filters.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            buttonSort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
            buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg, null));
        } else {
            buttonSort.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
            buttonFilter.setBackgroundColor(getResources().getColor(R.color.colorGreyBg));
        }


        hideLoading();
        //  break;
        // }

    }

    private void startIntroAnimation() {
        recyclerView.setTranslationY(recyclerView.getHeight());
        recyclerView.setAlpha(0f);
        recyclerView.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();


    }


    public void filterDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle(R.string.filter_by_small);

        TextView new_arrivals = (TextView) dialog.findViewById(R.id.new_arrivals);
        new_arrivals.setText(R.string.brands);

        TextView LowHigh = (TextView) dialog.findViewById(R.id.lowHigh);
        LowHigh.setText(R.string.materials);

        TextView HighLow = (TextView) dialog.findViewById(R.id.Highlow);
        HighLow.setVisibility(View.GONE);

        View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        new_arrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (checkInternetConnection(CategoryProducts.this)) {
                        dialog.dismiss();
                        Intent intent = new Intent(CategoryProducts.this, BrandListActivity.class);
                        intent.putExtra("search_key", categoryName.trim());
                                /*intent.putExtra("search_key", search_text.getText().toString());*/

                        filterable = true;
                        brandMaterialSearched = 1;
                        startActivityForResult(intent, PICK_BRAND_LIST);
                    } else {
                        showAlertDialog(CategoryProducts.this);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LowHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if (checkInternetConnection(CategoryProducts.this)) {
                        dialog.dismiss();
                        Intent intent = new Intent(CategoryProducts.this, MaterialList.class);
                        intent.putExtra("search_key", categoryName.trim());
                        filterable = false;
                        brandMaterialSearched = 2;
                        startActivityForResult(intent, PICK_MATERIAL_LIST);
                    } else {
                        showAlertDialog(CategoryProducts.this);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        /*SeekBar seek_bar = (SeekBar)dialog.findViewById(R.id.price_seekbar);
        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });*/
        dialog.show();
    }


    public void openDialogforMasterCat() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle(R.string.sort_by_small);

        TextView new_arrivals = (TextView) dialog.findViewById(R.id.new_arrivals);

        TextView LowHigh = (TextView) dialog.findViewById(R.id.lowHigh);

        TextView HighLow = (TextView) dialog.findViewById(R.id.Highlow);

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);

        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        new_arrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    dialog.dismiss();
                    if (checkInternetConnection(CategoryProducts.this)) {
                        showLoading();
                        ServerAPI.getInstance().sortFilterCategoryProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, categoryID.trim(), "", "", "", "1", Session.get_userId(pref), CategoryProducts.this);


                        //ServerAPI.getInstance().searchByNewArriaval(ApiServerResponse.SEARCH_BY_NEW_ARRIVAL, categoryName, CategoryProducts.this);

                    } else {
                        showAlertDialog(CategoryProducts.this);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*try {
                    String link = "?query=" + textPathList.trim() + "&less=less";
                    Log.i("query ", link);
                    ToastMsg.showLongToast(DemoProductList.this, link);
                    new NewArrivalsManager(getApplicationContext(), new NewArrivalsListener() {
                        @Override
                        public void onnewArrivalsListenerSuccess(String response) {

                            try {

                                bindData(response);
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(DemoProductList.this, error);
                        }
                    }).sendRequest("?query=" + textPathList.trim());
                    *//*.sendRequest("?query=" + search_text.getText().toString());*//*
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
        });

        LowHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    if (checkInternetConnection(CategoryProducts.this)) {


                        if (productList.size() != 0) {
                            dialog.dismiss();
                            showLoading();

                            Collections.sort(productList, new Comparator<CategoryProductsModal.ProductsDataBean>() {
                                @Override
                                public int compare(CategoryProductsModal.ProductsDataBean o1, CategoryProductsModal.ProductsDataBean o2) {
                                    return Integer.valueOf(o1.getProducts_price()) - Integer.valueOf(o2.getProducts_price());
                                }
                            });


                            CategoryProductsAdapter adapter = new CategoryProductsAdapter(CategoryProducts.this, productList);
                            recyclerView.setAdapter(adapter);

                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }
                            hideLoading();
                        } else {

                            if (searchListResult.size() != 0) {
                                dialog.dismiss();
                                showLoading();
                                Collections.sort(searchListResult, new Comparator<SearchModal.ProductsDataBean>() {
                                    @Override
                                    public int compare(SearchModal.ProductsDataBean o1, SearchModal.ProductsDataBean o2) {
                                        return Integer.valueOf(o1.getProducts_price()) - Integer.valueOf(o2.getProducts_price());
                                    }
                                });


                                SearchAdapter adapter = new SearchAdapter(CategoryProducts.this, searchListResult);
                                recyclerView.setAdapter(adapter);

                                if (pendingIntroAnimation) {
                                    pendingIntroAnimation = false;
                                    startIntroAnimation();
                                }
                                hideLoading();
                            }


                        }



                        /*if (brandMaterialSearched == 1) {
                            dialog.dismiss();
                            showLoading();
                            if (!filterQueryBrand.equalsIgnoreCase("")) {
                                ServerAPI.getInstance().searchByBrandLow(ApiServerResponse.SEARCH_BY_BRAND_AND_LOW, categoryName, "", CategoryProducts.this);
                            } else {
                                ServerAPI.getInstance().searchByBrandLow(ApiServerResponse.SEARCH_BY_BRAND_AND_LOW, categoryName, filterQueryBrand, CategoryProducts.this);
                            }

                        } else if (brandMaterialSearched == 2) {
                            dialog.dismiss();
                            showLoading();
                            if (!filterQueryMaterial.equalsIgnoreCase("")) {
                                ServerAPI.getInstance().searchByMaterialLow(ApiServerResponse.SEARCH_BY_MATERIAL_AND_LOW, categoryName, filterQueryMaterial, CategoryProducts.this);
                            } else {
                                ServerAPI.getInstance().searchByMaterialLow(ApiServerResponse.SEARCH_BY_MATERIAL_AND_LOW, categoryName, "", CategoryProducts.this);
                            }


                        } else {
                            dialog.dismiss();
                            ServerAPI.getInstance().searchByBrandLow(ApiServerResponse.SEARCH_BY_BRAND_AND_LOW, categoryName, "", CategoryProducts.this);
                        }*/

                    } else {
                        dialog.dismiss();
                        showAlertDialog(CategoryProducts.this);
                    }



                  /*  String link = "?query=" + textPathList.trim() + "&less=less";
                    Log.i("query ", link);
                    ToastMsg.showLongToast(DemoProductList.this, link);
                    new LowHighManager(getApplicationContext(), new LowHighListener() {
                        @Override
                        public void onlowhighListenerSuccess(String response) {
                            try {

                                bindData(response);
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(DemoProductList.this, error);
                        }
                    }).sendRequest("?query=" + textPathList.trim() + "&less=less");
                    *//*.sendRequest("?query=" + search_text.getText().toString() + "&less=less");*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        HighLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    if (checkInternetConnection(CategoryProducts.this)) {


                        if (productList.size() != 0) {
                            dialog.dismiss();
                            showLoading();
                            Collections.sort(productList, new Comparator<CategoryProductsModal.ProductsDataBean>() {
                                @Override
                                public int compare(CategoryProductsModal.ProductsDataBean o1, CategoryProductsModal.ProductsDataBean o2) {
                                    return Integer.valueOf(o2.getProducts_price()) - Integer.valueOf(o1.getProducts_price());
                                }
                            });


                            //Collections.sort(productList, Collections.<CategoryProductsModal.ProductsDataBean>reverseOrder());


                            CategoryProductsAdapter adapter = new CategoryProductsAdapter(CategoryProducts.this, productList);
                            recyclerView.setAdapter(adapter);

                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }
                            hideLoading();
                        } else {


                            if (searchListResult.size() != 0) {
                                dialog.dismiss();
                                showLoading();
                                Collections.sort(searchListResult, new Comparator<SearchModal.ProductsDataBean>() {
                                    @Override
                                    public int compare(SearchModal.ProductsDataBean o1, SearchModal.ProductsDataBean o2) {
                                        return Integer.valueOf(o2.getProducts_price()) - Integer.valueOf(o1.getProducts_price());
                                    }
                                });


                                SearchAdapter adapter = new SearchAdapter(CategoryProducts.this, searchListResult);
                                recyclerView.setAdapter(adapter);

                                if (pendingIntroAnimation) {
                                    pendingIntroAnimation = false;
                                    startIntroAnimation();
                                }
                                hideLoading();
                            }


                        }

                        /*if (brandMaterialSearched == 1) {
                            dialog.dismiss();
                            showLoading();
                            if (!filterQueryBrand.equalsIgnoreCase("")) {
                                ServerAPI.getInstance().searchByBrandHigh(ApiServerResponse.SEARCH_BY_BRAND_AND_HIGH, categoryName, filterQueryBrand, CategoryProducts.this);
                            } else {
                                ServerAPI.getInstance().searchByBrandHigh(ApiServerResponse.SEARCH_BY_BRAND_AND_HIGH, categoryName, "", CategoryProducts.this);
                            }

                        } else if (brandMaterialSearched == 2) {
                            dialog.dismiss();
                            showLoading();
                            if (!filterQueryMaterial.equalsIgnoreCase("")) {
                                ServerAPI.getInstance().searchByMaterialHigh(ApiServerResponse.SEARCH_BY_MATERIAL_AND_HIGH, categoryName, filterQueryMaterial, CategoryProducts.this);
                            } else {
                                ServerAPI.getInstance().searchByMaterialHigh(ApiServerResponse.SEARCH_BY_MATERIAL_AND_HIGH, categoryName, "", CategoryProducts.this);
                            }


                        } else {
                            dialog.dismiss();
                            ServerAPI.getInstance().searchByBrandHigh(ApiServerResponse.SEARCH_BY_BRAND_AND_HIGH, categoryName, "", CategoryProducts.this);
                        }*/
                    } else {
                        dialog.dismiss();
                        showAlertDialog(CategoryProducts.this);
                    }


                 /*   new HighLowManager(getApplicationContext(), new HighLowListener() {
                        @Override
                        public void onhighlowListenerSuccess(String response) {
                            try {
                                bindData(response);
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(DemoProductList.this, error);
                        }
                    }).sendRequest("?query=" + textPathList.trim() + "&high=high");*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_BRAND_LIST || requestCode == PICK_MATERIAL_LIST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                //ToastMsg.showLongToast(DemoProductList.this, data.getData().toString());


                if (checkInternetConnection(CategoryProducts.this)) {
                    String filerBrandOrMaterial = data.getData().toString();
                    searchByBrandOrMaterial(filterable, filerBrandOrMaterial, categoryName);
                } else {
                    showAlertDialog(CategoryProducts.this);
                }


                /*if (filterable) {
                    filterQueryBrand = data.getData().toString();
                    ToastMsg.showShortToast(CategoryProducts.this, filterQueryBrand);
                    // if (checkInternetConnection(CategoryProducts.this)) {
                    showLoading();
                    ServerAPI.getInstance().searchByBrand(ApiServerResponse.SEARCH_BY_BRAND, categoryName, filterQueryBrand, this);
                    //} else {
                    // ToastMsg.showShortToast(CategoryProducts.this, "Please check your internet connection");
                    // }


                } else {
                    filterQueryMaterial = data.getData().toString();
                    ToastMsg.showShortToast(CategoryProducts.this, categoryName + " " + filterQueryMaterial);

                    if (checkInternetConnection(CategoryProducts.this)) {
                        showLoading();
                        ServerAPI.getInstance().searchByMaterial(ApiServerResponse.SEARCH_BY_MATERIAL, categoryName, filterQueryMaterial, this);
                    } else {
                        ToastMsg.showShortToast(CategoryProducts.this, "Please check your internet connection");
                    }

                }*/

                //ServerAPI.getInstance().searchByBrand(ApiServerResponse.SEARCH_BY_BRAND, categoryName, filterQuery, this);
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                /*ToastMsg.showShortToast(CategoryProducts.this, categoryName + " " + filterQuery);*/

                // Do something with the contact here (bigger example below)
            }
        }
    }


   /* @Override
    protected void onResume() {
        super.onResume();
        try {
            if (checkInternetConnection(CategoryProducts.this)) {
                showLoading();
                //ServerAPI.getInstance().getProductDetail(ApiServerResponse.PRODUCT_DETAIL, "17", this);
                ServerAPI.getInstance().getCategoryProducts(ApiServerResponse.CATEGORY_PRODUCTS, categoryID, Session.get_userId(pref), this);
            } else {
                showAlertDialog(CategoryProducts.this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    @Subscribe
    public void onCartUpdate(EventBusProductDetailUpdateModal eventBusProductDetailUpdateModal) {

        try {
            tv_count.setText(eventBusProductDetailUpdateModal.getMessage());
            /*if (eventBusProductDetailUpdateModal.getMessage() == 1) {
                String cart_id = Session.getcart_id(pref);
                if (!cart_id.equalsIgnoreCase("")) {
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, cart_id, this);
                }
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Subscribe
    public void onProductDetailWishListUpdate(EventBusCategoryProductsUpdateWishlist eventBusCategoryProductsUpdateWishlist) {
        try {
            if (eventBusCategoryProductsUpdateWishlist.getMessage().equalsIgnoreCase(Constant.OK)) {
                if (checkInternetConnection(CategoryProducts.this)) {
                    showLoading();
                    //ServerAPI.getInstance().getProductDetail(ApiServerResponse.PRODUCT_DETAIL, "17", this);
                    ServerAPI.getInstance().getCategoryProducts(ApiServerResponse.CATEGORY_PRODUCTS, categoryID, Session.get_userId(pref), this);
                } else {
                    showAlertDialog(CategoryProducts.this);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }


    private void searchByBrandOrMaterial(boolean filterable, String filterQuery, String categoryName) {

        if (filterable) {
            //filterQueryBrand = data.getData().toString();
            //ToastMsg.showShortToast(CategoryProducts.this, filterQuery);
            // if (checkInternetConnection(CategoryProducts.this)) {
            showLoading();
            //String asdasd = categoryName.replace(" ", "");
            ServerAPI.getInstance().sortFilterCategoryProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, categoryID.trim(), filterQuery.trim(), "","","",Session.get_userId(pref), CategoryProducts.this);


            //ServerAPI.getInstance().searchByBrand(ApiServerResponse.SEARCH_BY_BRAND, categoryName.replace(" ", ""), filterQuery, this);
            //} else {
            // ToastMsg.showShortToast(CategoryProducts.this, "Please check your internet connection");
            // }


        } else {
            // filterQueryMaterial = data.getData().toString();
            //ToastMsg.showShortToast(CategoryProducts.this, categoryName + " " + filterQuery);

            if (checkInternetConnection(CategoryProducts.this)) {
                showLoading();
                ServerAPI.getInstance().sortFilterCategoryProducts(ApiServerResponse.SEARCH_SOR_FILTER_PRODUCTS, categoryID.trim(), "", filterQuery.trim(), "", "", Session.get_userId(pref), CategoryProducts.this);

                //ServerAPI.getInstance().searchByMaterial(ApiServerResponse.SEARCH_BY_MATERIAL, categoryName.replace(" ", ""), filterQuery, this);
            } else {
                showAlertDialog(CategoryProducts.this);
            }

        }
    }
}
