package kartku.com.contact;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kartku.com.R;
import kartku.com.contact.listener.Contactlistener;
import kartku.com.contact.manager.ContactManager;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.faq_module.FAQ;
import kartku.com.utils.ToastMsg;

public class Contact extends AppCompatActivity implements Contactlistener, View.OnClickListener {

    EditText name, email, subject, message;
    Button btn_send;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        subject = (EditText) findViewById(R.id.subject);
        message = (EditText) findViewById(R.id.message);
        btn_send = (Button) findViewById(R.id.contact_btn);
        btn_send.setOnClickListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Contact.this, Home_dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.contact_btn:
                try {
                    //btn_send.setTranslationY(2 * getResources().getDimensionPixelOffset(R.dimen.btn_fab_size));
                    new ContactManager(Contact.this, this).sendRequest(name.getText().toString(), email.getText().toString(), subject.getText().toString(), message.getText().toString());
                    //Toast.makeText(Contact.this,"clicked event triggerrrr" , Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onContactSuccess(String response) {
        try {
            ToastMsg.showShortToast(Contact.this, getResources().getString(R.string.success_messgae));
            Intent intent = new Intent(Contact.this, Home_dashboard.class);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onContactError(String error) {
        ToastMsg.showShortToast(Contact.this, error);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                    Intent intent = new Intent(Contact.this, Home_dashboard.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Contact.this, Home_dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
