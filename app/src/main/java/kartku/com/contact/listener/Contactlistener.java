package kartku.com.contact.listener;

/**
 * Created by satoti.garg on 9/13/2016.
 */
public interface Contactlistener {

    void onContactSuccess(String response);
    void onContactError(String error);
}
