package kartku.com.contact.api;

import android.content.Context;

import java.util.Map;

import kartku.com.contact.manager.ContactManager;
import kartku.com.dashboard_module.manager.HomeBannersManager;
import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/13/2016.
 */
public class ContactApi implements CustomResponse {

    ContactManager manager ;
    Context context;
    RequestServer request_server;

    public ContactApi(ContactManager manager , Context context)
    {
        this.manager = manager;
        this.context = context;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId ,  Map<String, String> params){
        try {
            request_server = new RequestServer(context, requestId, params,this , RequestConstants.POST_URL);
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
