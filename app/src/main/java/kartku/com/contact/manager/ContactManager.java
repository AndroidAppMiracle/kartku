package kartku.com.contact.manager;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.contact.api.ContactApi;
import kartku.com.contact.listener.Contactlistener;
import kartku.com.dashboard_module.api.HomeBannersApi;
import kartku.com.dashboard_module.listener.HomeBannersListener;
import kartku.com.login_module.api.LoginAPI;
import kartku.com.utils.API;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;

/**
 * Created by satoti.garg on 9/13/2016.
 */
public class ContactManager {

    private Context context;
    private Contactlistener listener;

    public ContactManager(Context context, Contactlistener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void sendRequest(String name, String email, String subject, String message) {
        if (validateFields(name, email, subject, message)) {
            try {
                Map<String, String> params = new HashMap<>();
                try {
                    try {
                        params.put(RequestConstants.NAME, "" + name);
                        params.put(RequestConstants.EMAIL, "" + email);
                        params.put(RequestConstants.SUBJECT, "" + subject);
                        params.put(RequestConstants.MESSAGE, "" + message);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new ContactApi(ContactManager.this, context).sendRequest(API.CONTACT_REQUEST_ID, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validateFields(String name, String email, String subject, String message) {
        if (name.length() <= 0) {
            listener.onContactError(context.getResources().getString(R.string.username));
            return false;
        } else if (email.length() <= 0) {
            listener.onContactError(context.getResources().getString(R.string.email));
            return false;
        } else if (email.length() > 0 && !Validation.validateEmail(email)) {
            listener.onContactError(context.getResources().getString(R.string.email_validation));
            return false;
        } else if (subject.length() <= 0) {
            listener.onContactError(context.getResources().getString(R.string.subject));
            return false;
        } else {
            return true;
        }

    }


    public void onSuccess(String response) {
        listener.onContactSuccess(response);
    }

    public void onError(String message) {

        listener.onContactError(message);
    }
}
