package kartku.com.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.FloatingExpandableAdapter;
import kartku.com.floatingexpandable.FloatingGroupExpandableListView;
import kartku.com.floatingexpandable.WrapperExpandableListAdapter;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 12/12/2016.
 */

public class CategoriesFragment extends Fragment {


    FloatingExpandableAdapter adapter;
    List<String> listDataHeader;
    HashMap<String, JSONArray> listDataChild;
    FloatingGroupExpandableListView expListView;
    TextView textview;
    SharedPreferences pref;
    JSONArray sub_cat_info;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.activity_categories, container, false);
        // banner_list = (RecyclerView)view.findViewById(R.id.banner_recycleview);

       /* Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_drawer);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        expListView = (FloatingGroupExpandableListView) view.findViewById(R.id.lvExp);
        textview = (TextView) view.findViewById(R.id.txt_no_record);

        prepareListData();
        adapter = new FloatingExpandableAdapter(getActivity(), listDataHeader, listDataChild, sub_cat_info);
        WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(adapter);
        expListView.setAdapter(wrapperAdapter);
        /*** code to open all childs by default ***/
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expListView.expandGroup(i);
        }

        return view;
    }


    private void prepareListData() {
        try {
            listDataHeader = new ArrayList<String>();
            listDataChild = new HashMap<String, JSONArray>();

            //Intent intent = getActivity().getIntent();
            String index = Session.get_selected_category_index(pref);
            //int index = intent.getIntExtra(Constant.SELECTED_CATEGORY_INDEX, 0);

            JSONObject res = new JSONObject(Session.get_main_categories_res(pref));
            JSONArray categories_array = res.getJSONArray("categories_info");

            JSONObject selected_array = categories_array.getJSONObject(Integer.parseInt(index.trim()));
            Log.d("selected ", " array  " + selected_array);
            System.out.println("categories selected_array *****  " + selected_array);

            String title = selected_array.getString("title");
            try {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
            } catch (Exception e) {
                e.printStackTrace();
            }
            sub_cat_info = selected_array.getJSONArray("subcat_info");

            for (int j = 0; j < sub_cat_info.length(); j++) {
                JSONObject item_object = sub_cat_info.getJSONObject(j);
                String sub_cat_name = item_object.getString("subcat_name");
                String sub_cat_id = item_object.getString("subcat_id");
                JSONArray sub_subcat_info = item_object.getJSONArray("sub_subcat_info");

                listDataHeader.add(sub_cat_name);
                listDataChild.put(sub_cat_name, sub_subcat_info);

                //System.out.println("next ixon view clicked sub_cat_item listDataHeader 111*****  " + listDataHeader);
                //System.out.println("next ixon view clicked sub_cat_item listChildData *1111 ****  " + listDataChild);
            }

        } catch (NullPointerException e) {

            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {

            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tool_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }*/


}
