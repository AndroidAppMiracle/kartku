package kartku.com.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kartku.com.MyOrders.listener.MyOrderListener;
import kartku.com.MyOrders.manager.MyOrderManager;
import kartku.com.MyOrders.modal.MyOrderModal;
import kartku.com.R;
import kartku.com.adapter.MyOrdersAdapter;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 12/13/2016.
 */

public class OrdersFragment extends Fragment implements MyOrderListener {

    TextView textView;
    RecyclerView recycler_view_my_orders;

    SharedPreferences pref;

    List<MyOrderModal.ResultsBean> myOrders;
    boolean pendingIntroAnimation;

    public OrdersFragment() {
        //Constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.activity_orders, container, false);


        textView = (TextView) view.findViewById(R.id.textView);
        recycler_view_my_orders = (RecyclerView) view.findViewById(R.id.recycler_view_my_orders);


        try {
            new MyOrderManager(getActivity(), this).sendRequest("/" + Session.get_userId(pref));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                   /* Intent intent = new Intent(getActivity(), Home_dashboard.class);
                    startActivity(intent);
                    getActivity().finish();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onMyOrderSuccess(String response) {

        try {
            myOrders = new ArrayList<>();

            //JSONObject res_object = new JSONObject(response);
            JSONArray order_items = new JSONArray(response);
            //JSONArray order_items = res_object.getJSONArray(Constant.RESULTS);


            for (int i = 0; i < order_items.length(); i++) {

                JSONObject item = order_items.getJSONObject(i);
                MyOrderModal.ResultsBean myOrderModal = new MyOrderModal.ResultsBean();
                myOrderModal.setId(item.getString(Constant.ID));
                myOrderModal.setCreated_at(item.getString(Constant.CREATED_AT));
                myOrderModal.setStatus(item.getString(Constant.STATUS));
                myOrderModal.setGrand_total(item.getString(Constant.GRAND_TOTAL));
                myOrders.add(myOrderModal);

            }

            MyOrdersAdapter adapter = new MyOrdersAdapter(getActivity(), myOrders);
            recycler_view_my_orders.setAdapter(adapter);
            recycler_view_my_orders.setLayoutManager(new LinearLayoutManager(getActivity()));
            if (pendingIntroAnimation) {
                pendingIntroAnimation = false;
                startIntroAnimation();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMyOrderError(String error) {
        ToastMsg.showShortToast(getActivity(), error);
    }


    private void startIntroAnimation() {
        recycler_view_my_orders.setTranslationY(recycler_view_my_orders.getHeight());
        recycler_view_my_orders.setAlpha(0f);
        recycler_view_my_orders.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }
}
