package kartku.com.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kartku.com.R;
import kartku.com.adapter.HomeBannerAdapter;

/**
 * Created by satoti.garg on 9/8/2016.
 */
public class HomeFragment extends Fragment {

    private RecyclerView banner_list;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        // banner_list = (RecyclerView)view.findViewById(R.id.banner_recycleview);
        return view;
    }

   /* private void setAdapter()
    {
        HomeBannerAdapter adapter = new HomeBannerAdapter();
        banner_list.setAdapter(adapter);
    }*/
}
