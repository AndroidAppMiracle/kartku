package kartku.com.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import java.util.List;

import kartku.com.R;
import kartku.com.adapter.MyProductReviewsAdapter;
import kartku.com.my_product_reviews_module.MyProductReviewsModal;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

import static kartku.com.utils.Utility.checkInternetConnection;

/**
 * Created by Kshitiz Bali on 1/20/2017.
 */

public class MyProductReviewsFragment extends Fragment implements ApiServerResponse {

    SharedPreferences pref;
    TextView tv_msg;
    RecyclerView recycler_view_my_reviews;

    List<MyProductReviewsModal.ResultsBean> myProductReviewsModalList;

    MyProductReviewsAdapter adapter;


    boolean pendingIntroAnimation;

    public MyProductReviewsFragment() {
        //Constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.activity_product_review, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.my_product_review);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }

        tv_msg = (TextView) view.findViewById(R.id.tv_msg);
        recycler_view_my_reviews = (RecyclerView) view.findViewById(R.id.recycler_view_my_reviews);

        try {


            if (checkInternetConnection(getActivity())) {
                ((Utility) getActivity()).showLoading();
                ServerAPI.getInstance().getMyProductReviews(ApiServerResponse.MY_PRODUCT_REVIEWS, Session.get_userId(pref), this);

            } else {
                ((Utility) getActivity()).showAlertDialog(getActivity());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            ((Utility) getActivity()).hideLoading();
            if (response.isSuccessful()) {
                MyProductReviewsModal myProductReviewsModal;
                switch (tag) {
                    case ApiServerResponse.MY_PRODUCT_REVIEWS:

                        myProductReviewsModal = (MyProductReviewsModal) response.body();

                        if (myProductReviewsModal.getStatus().equals(Constant.OK)) {
                            if (myProductReviewsModal.getResults().isEmpty()) {
                                recycler_view_my_reviews.setVisibility(View.GONE);
                                tv_msg.setVisibility(View.VISIBLE);
                            } else {
                                recycler_view_my_reviews.setVisibility(View.VISIBLE);
                                tv_msg.setVisibility(View.GONE);
                                myProductReviewsModalList = myProductReviewsModal.getResults();
                            }

                            adapter = new MyProductReviewsAdapter(getActivity(), myProductReviewsModalList);
                            recycler_view_my_reviews.setAdapter(adapter);
                            recycler_view_my_reviews.setItemAnimator(new DefaultItemAnimator());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            recycler_view_my_reviews.setLayoutManager(mLayoutManager);
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }

                        } else {
                            recycler_view_my_reviews.setVisibility(View.GONE);
                            tv_msg.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            ((Utility) getActivity()).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case ApiServerResponse.MY_PRODUCT_REVIEWS:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void startIntroAnimation() {
        recycler_view_my_reviews.setTranslationY(recycler_view_my_reviews.getHeight());
        recycler_view_my_reviews.setAlpha(0f);
        recycler_view_my_reviews.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }
}
