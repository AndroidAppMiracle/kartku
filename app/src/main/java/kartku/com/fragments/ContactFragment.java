package kartku.com.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import kartku.com.R;
import kartku.com.activity.ContactActivity;
import kartku.com.contact.listener.Contactlistener;
import kartku.com.contact.manager.ContactManager;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.ContactUsModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

import static kartku.com.utils.Utility.checkInternetConnection;

/**
 * Created by Kshitiz Bali on 12/12/2016.
 */

public class ContactFragment extends Fragment implements View.OnClickListener, ApiServerResponse {

    EditText name, email, subject, message;
    Button btn_send;
    View view;
    private SharedPreferences pref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.activity_contact, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.contact_us);
        name = (EditText) view.findViewById(R.id.name);
        email = (EditText) view.findViewById(R.id.email);
        subject = (EditText) view.findViewById(R.id.subject);
        message = (EditText) view.findViewById(R.id.message);
        btn_send = (Button) view.findViewById(R.id.contact_btn);

        btn_send.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.contact_btn:
                try {
                    if (checkInternetConnection(getActivity())) {
                        if (validateFields(name.getText().toString().trim(), email.getText().toString().trim(), subject.getText().toString().trim(), message.getText().toString().trim())) {
                            ((Utility) getActivity()).showLoading();
                            ServerAPI.getInstance().contactUs(ApiServerResponse.CONTACT_US,  Session.get_userId(pref),name.getText().toString().trim(), email.getText().toString().trim(), subject.getText().toString().trim(), message.getText().toString().trim(), this);
                        }

                        //new ContactManager(ContactActivity.this, this).sendRequest(name.getText().toString().trim(), email.getText().toString().trim(), subject.getText().toString().trim(), message.getText().toString().trim());
                        //hideLoading();
                    } else {
                        ((Utility) getActivity()).showAlertDialog(getActivity());
                    }

                    //Toast.makeText(Contact.this,"clicked event triggerrrr" , Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private boolean validateFields(String name, String email, String subject, String message) {
        if (name.length() <= 0) {
            ToastMsg.showShortToast(getActivity(), getResources().getString(R.string.username));
            return false;
        } else if (email.length() <= 0) {
            ToastMsg.showShortToast(getActivity(), getResources().getString(R.string.email));
            return false;
        } else if (email.length() > 0 && !Validation.validateEmail(email)) {
            ToastMsg.showShortToast(getActivity(), getResources().getString(R.string.email_validation));
            return false;
        } else if (subject.length() <= 0) {
            ToastMsg.showShortToast(getActivity(), getResources().getString(R.string.subject));
            return false;
        } else {
            return true;
        }

    }

   /* @Override
    public void onContactSuccess(String response) {
        try {
            ToastMsg.showShortToast(getActivity(), getResources().getString(R.string.contact_us_success));
            name.setText("");
            email.setText("");
            subject.setText("");
            message.setText("");
            //Intent intent = new Intent(getActivity(), DemoHomeDashBoard.class);
            // startActivity(intent);
            //  getActivity().finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
*/

   /* @Override
    public void onContactError(String error) {
        ToastMsg.showShortToast(getActivity(), error);
    }*/

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            ContactUsModal contactUsModal;

            switch (tag) {

                case ApiServerResponse.CONTACT_US:

                    contactUsModal = (ContactUsModal) response.body();
                    if (contactUsModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                        ToastMsg.showShortToast(getActivity(), contactUsModal.getMessage());
                        name.setText("");
                        email.setText("");
                        subject.setText("");
                        message.setText("");
                    } else {
                        ToastMsg.showShortToast(getActivity(), contactUsModal.getMessage());
                    }
                    ((Utility) getActivity()).hideLoading();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            ToastMsg.showShortToast(getActivity(), throwable.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                   *//* Intent intent = new Intent(getActivity(), Home_dashboard.class);
                    startActivity(intent);
                    getActivity().finish();*//*
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }
*/
   /* @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent( getActivity(), Home_dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }*/
}
