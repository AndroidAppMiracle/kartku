package kartku.com.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import kartku.com.R;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.login_module.forgot_password.ForgotPassword;
import kartku.com.login_module.listener.LoginListener;
import kartku.com.login_module.social_login.listener.SocialLoginListener;
import kartku.com.registeration_module.Register;
import kartku.com.sellermodule.SellerHomeDashBoard;
import kartku.com.utils.Constant;
import kartku.com.utils.NetworkConnection;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 12/12/2016.
 */

public class LoginFragment extends Fragment implements View.OnClickListener, LoginListener, SocialLoginListener, GoogleApiClient.OnConnectionFailedListener {

    private EditText user_email, password;
    // private Button login;
    //private LoginButton facebook;
    //private SignInButton google;
    private TextView forget_password;
    private LinearLayout signup_here;
    private ProgressBar progressBar;
    private SharedPreferences pref;

    // for facebbok
    private CallbackManager callbackManager;

    private int RC_SIGN_IN = 2;
    private GoogleApiClient mGoogleApiClient;
    private RelativeLayout rl_submit, rl_fb_login, rl_google_login;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.secondary_content_login, container, false);
        // banner_list = (RecyclerView)view.findViewById(R.id.banner_recycleview);

        try {
            // find id's
            user_email = (EditText) view.findViewById(R.id.username);
            password = (EditText) view.findViewById(R.id.password);

            //login = (Button) view.findViewById(R.id.btn_login);

            rl_submit = (RelativeLayout) view.findViewById(R.id.rl_submit);
            rl_fb_login = (RelativeLayout) view.findViewById(R.id.rl_fb_login);
            rl_google_login = (RelativeLayout) view.findViewById(R.id.rl_google_login);

            //facebook = (LoginButton) view.findViewById(R.id.btn_facebook);
            //facebook.setReadPermissions(Arrays.asList("email", "public_profile"));
            //facebook.setFragment(this);

            //google = (SignInButton) view.findViewById(R.id.btn_google);
            //google.setSize(SignInButton.SIZE_STANDARD);

            forget_password = (TextView) view.findViewById(R.id.forget_password);
            signup_here = (LinearLayout) view.findViewById(R.id.signup_wrapper);
            progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);


            // set listeners
            rl_submit.setOnClickListener(this);
            rl_fb_login.setOnClickListener(this);
            rl_google_login.setOnClickListener(this);
            forget_password.setOnClickListener(this);
            signup_here.setOnClickListener(this);

            pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(getActivity());
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {

                case R.id.rl_submit:
                    try {

                        if (NetworkConnection.checkInternetConnection(getActivity())) {
                            if (user_email.getText().toString().length() > 0 && password.getText().toString().length() > 0) {
                                progressBar.setVisibility(View.VISIBLE);
                                Session.setLoginTypeButton(pref, "email");
                                Utility.hideSoftKeyboard(getActivity());
                                new kartku.com.login_module.manager.LoginManager(getActivity(), this).sendRequest(user_email.getText().toString(), password.getText().toString());
                            } else {
                                ToastMsg.showShortToast(getActivity(), "Please provide valid username and password.");
                            }

                        } else {

                            ToastMsg.showShortToast(getActivity(), "Not connected to internet.");

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.rl_fb_login:
                    try {

                        if (NetworkConnection.checkInternetConnection(getActivity())) {
                            Session.setLoginTypeButton(pref, "facebook");
                            //loginFacebook(callbackManager, facebook);
                        } else {
                            ToastMsg.showShortToast(getActivity(), "Not connected to internet.");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.rl_google_login:

                    if (NetworkConnection.checkInternetConnection(getActivity())) {
                        Session.setLoginTypeButton(pref, "google");
                        signIn();
                    } else {
                        ToastMsg.showShortToast(getActivity(), "Not connected to internet.");
                    }

                    break;
                case R.id.forget_password:
                    try {
                        Intent intent = new Intent(getActivity(), ForgotPassword.class);
                        startActivity(intent);
                        getActivity().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.signup_wrapper:
                    try {
                        Intent intent = new Intent(getActivity(), Register.class);
                        startActivity(intent);
                        getActivity().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loginFacebook(CallbackManager callbackManager, final LoginButton loginButton) {

       /* FacebookSdk.sdkInitialize(Login.this);
        callbackManager = CallbackManager.Factory.create();*/
        //LoginManager.getInstance().logOut();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code

                                if (response != null) {
                                    try {

                                        Log.e("JOSNFB ", object.toString());

                                        String mFbid = object.getString("id");
                                        String name = object.getString("name");
                                        // String fName = object.getString("first_name");
                                        //String lName = object.getString("last_name");

                                        String email = object.getString("email");
                                        JSONObject picture = object.getJSONObject("picture");
                                        JSONObject data = picture.getJSONObject("data");
                                        String fbProfilePic = data.getString("url");
                                        Session.set_facebook_signin_user_profile_pic(pref, fbProfilePic);
                                        //Session.set_facebookName(pref, name);
                                        // Log.i("F and L Name ", fName + " " + lName);
                                        Log.i("*******imgFB", fbProfilePic);
                                        callSocialApi(email);
                                        //Toast.makeText(Login.this, name + "---" + email + " " + mFbid, Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }


                                // Toast.makeText(Login.this,""+response,Toast.LENGTH_LONG).show();


                                Log.d("----fb response----", "" + response);

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {
                Log.d("onCancel", "Login");

            }

            @Override
            public void onError(FacebookException error) {
                Log.d("onError", "Login");
                ToastMsg.showLongToast(getActivity(), error.getMessage());
            }
        });


/*        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code

                                if (response != null) {
                                    try {
                                        String mFbid = object.getString("id");
                                        String name = object.getString("name");

                                        // String fName = object.getString("first_name");
                                        //String lName = object.getString("last_name");

                                        String email = object.getString("email");
                                        // Log.i("F and L Name ", fName + " " + lName);
                                        callSocialApi(email);
                                        //Toast.makeText(Login.this, name + "---" + email + " " + mFbid, Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }


                                // Toast.makeText(Login.this,""+response,Toast.LENGTH_LONG).show();


                                Log.d("----fb response----", "" + response);

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }


            @Override
            public void onCancel() {
                Log.d("onCancel", "Login");
                // Toast.makeText(Login.this,"on cancel",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("onError", "Login");
                // Toast.makeText(Login.this,"on Error",Toast.LENGTH_LONG).show();
            }
        });*/
    }


    @Override
    public void onError(String error) {
        try {
            progressBar.setVisibility(View.GONE);
            ToastMsg.showShortToast(getActivity(), error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(String message) {
        try {
            progressBar.setVisibility(View.GONE);
            ToastMsg.showShortToast(getActivity(), message);
            Session.set_login_status(pref, true);
            /*Intent intent = new Intent(Login.this, Home_dashboard.class);
            startActivity(intent);
            finish();*/
            if (Session.getUserRoleType(pref).equals(Constant.ROLE_USER)) {
                Intent intent = new Intent(getActivity(), DemoHomeDashBoard.class);
            /*Intent intent = new Intent(getActivity(), Home_dashboard.class);*/
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            } else {
                Intent intent = new Intent(getActivity(), SellerHomeDashBoard.class);
            /*Intent intent = new Intent(getActivity(), Home_dashboard.class);*/
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }

            /*Intent intent = new Intent(getActivity(), DemoHomeDashBoard.class);
            *//*Intent intent = new Intent(getActivity(), Home_dashboard.class);*//*
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            getActivity().finish();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onSocialSuccess(String response) {
        progressBar.setVisibility(View.GONE);
        ToastMsg.showShortToast(getActivity(), response);
        Session.set_login_status(pref, true);
        Intent intent = new Intent(getActivity(), DemoHomeDashBoard.class);
        /*Intent intent = new Intent(getActivity(), Home_dashboard.class);*/
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }*/


    public void loginWithGoogle() {

        try {
            // Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            //        revokeAccess();
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                /*.enableAutoManage(getActivity() *//* FragmentActivity *//*, this *//* OnConnectionFailedListener *//*)*/
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
            mGoogleApiClient.connect();
            //revokeAccess();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            ConnectionResult mConnectionResult = connectionResult;
            /*mConnectionResult.getErrorMessage();*/
            /*if (mResolveOnFail) {
                // This is a local helper function that starts
                // the resolution of the problem, which may be
                // showing the user an account chooser or similar.
                //startResolution();
            }*/
        }
    }

    private void signIn() {
        try {
            loginWithGoogle();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void handleSignInResult(GoogleSignInResult result) {

        try {
            Log.d("", "handleSignInResult:" + result.isSuccess());
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();
                if (!acct.getDisplayName().isEmpty()) {
                    String name = acct.getDisplayName();
                }

                String email = acct.getEmail();
                if (acct.getPhotoUrl() != null) {
                    String photoUrl = acct.getPhotoUrl().toString();
                    Log.i("PhotoUri", "" + photoUrl);
                    Session.set_google_signin_user_profile_pic(pref, photoUrl);
                }
                callSocialApi(email);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void callSocialApi(String email) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            new kartku.com.login_module.social_login.manager.SocialLoginManager(getActivity(), this).sendRequest(email);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

  /*  @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            //mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            //mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }
    /* @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected() && mGoogleApiClient != null) {
            revokeAccess();
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }*/

    /*private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Toast.makeText(getActivity(), "StatusRevoke " + status.toString(), Toast.LENGTH_LONG).show();
                        // ...
                    }
                });
    }*/

    /*@Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected() && mGoogleApiClient != null) {
            revokeAccess();
            mGoogleApiClient.stopAutoManage(Home_dashboard.this);
            mGoogleApiClient.disconnect();
        }

    }*/

   /* @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getActivity(), Home_dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }*/

}
