package kartku.com.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kartku.com.MyOrders.modal.MyOrderModal;
import kartku.com.R;
import kartku.com.adapter.MyOrdersAdapter;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

import static kartku.com.utils.Utility.checkInternetConnection;

/**
 * Created by Kshitiz Bali on 1/20/2017.
 */

public class MyOrdersFragment extends Fragment implements ApiServerResponse {

    SharedPreferences pref;
    TextView textView;
    RecyclerView recycler_view_my_orders;
    MyOrdersAdapter adapter;


    List<MyOrderModal.ResultsBean> myOrdersList;

    public MyOrdersFragment() {
        //Constructor
    }

    boolean pendingIntroAnimation;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.activity_orders, container, false);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.my_orders);

        textView = (TextView) view.findViewById(R.id.textView);
        recycler_view_my_orders = (RecyclerView) view.findViewById(R.id.recycler_view_my_orders);


        try {

            if (checkInternetConnection(getActivity())) {
                ((Utility) getActivity()).showLoading();
                ServerAPI.getInstance().getMyOrders(ApiServerResponse.MY_ORDERS, Session.get_userId(pref), this);
            } else {
                ((Utility) getActivity()).showAlertDialog(getActivity());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            ((Utility) getActivity()).hideLoading();
            if (response.isSuccessful()) {

                MyOrderModal myOrderModal;

                myOrdersList = new ArrayList<>();

                switch (tag) {
                    case ApiServerResponse.MY_ORDERS:

                        myOrderModal = (MyOrderModal) response.body();

                        if (myOrderModal.getStatus().equals(Constant.OK)) {

                            if (myOrderModal.getResults().isEmpty()) {
                                recycler_view_my_orders.setVisibility(View.GONE);
                                textView.setVisibility(View.VISIBLE);
                                textView.setText("No Orders To View");
                            } else {
                                myOrdersList = myOrderModal.getResults();
                            }

                            adapter = new MyOrdersAdapter(getActivity(), myOrdersList);
                        }

                        recycler_view_my_orders.setAdapter(adapter);
                        recycler_view_my_orders.setItemAnimator(new DefaultItemAnimator());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        recycler_view_my_orders.setLayoutManager(mLayoutManager);
                        if (pendingIntroAnimation) {
                            pendingIntroAnimation = false;
                            startIntroAnimation();
                        }


                        break;
                    default:
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            ((Utility) getActivity()).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case ApiServerResponse.MY_ORDERS:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startIntroAnimation() {
        recycler_view_my_orders.setTranslationY(recycler_view_my_orders.getHeight());
        recycler_view_my_orders.setAlpha(0f);
        recycler_view_my_orders.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }
}
