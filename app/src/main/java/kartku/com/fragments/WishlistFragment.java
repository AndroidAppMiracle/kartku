package kartku.com.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.WishlistAdapterNew;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.WishlistModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.model.WishlistBean;
import retrofit2.Response;

import static kartku.com.utils.Utility.checkInternetConnection;

/**
 * Created by Kshitiz Bali on 12/13/2016.
 */

public class WishlistFragment extends Fragment implements /*WishlistListener,*/ ApiServerResponse {

    RecyclerView wishlist_view;
    String user_id = "";
    SharedPreferences pref;
    ArrayList<WishlistBean> arrayList = new ArrayList<>();
    TextView tv_empty_text;
    boolean pendingIntroAnimation;
    TextView tv_count;
    List<WishlistModal.ProductInfoBean> wishlist = new ArrayList<>();
    //private Menu mMenu;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.activity_wishlist, container, false);
        // banner_list = (RecyclerView)view.findViewById(R.id.banner_recycleview);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_activity_wishlist);
        wishlist_view = (RecyclerView) view.findViewById(R.id.wishlist_view);
        tv_empty_text = (TextView) view.findViewById(R.id.tv_empty_text);
        try {


            if (checkInternetConnection(getActivity())) {
                ((Utility) getActivity()).showLoading();
                // new WishlistManager(getActivity(), this).sendRequest(user_id, "");


                ServerAPI.getInstance().getWishlist(ApiServerResponse.WISHLIST, user_id, this);

            } else {
                ((Utility) getActivity()).showAlertDialog(getActivity());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

/*    @Override
    public void onwishlistError(String error) {
        ToastMsg.showShortToast(getActivity(), error);
    }


    @Override
    public void onwishlistSuccess(String response) {

        Log.d("", "onwishlistSuccess res " + response);

        try {
            JSONArray products_list = new JSONArray(response);
            for (int i = 0; i < products_list.length(); i++) {
                JSONObject jsonObject = products_list.getJSONObject(i);
                WishlistBean list_model = new WishlistBean();
                list_model.setId(jsonObject.getString(Constant.ID));
                list_model.setName(jsonObject.getString(Constant.NAME));
                list_model.setPrice(jsonObject.getString(Constant.PRICE_KEY));
                list_model.setDescription(jsonObject.getString(Constant.DESCRIPTION));
                list_model.setImage(jsonObject.getString(Constant.IMAGE));
                arrayList.add(list_model);
            }
            if (arrayList.size() == 0) {
                wishlist_view.setVisibility(View.GONE);
                empty_text.setVisibility(View.VISIBLE);
            } else {
                wishlist_view.setVisibility(View.VISIBLE);
                empty_text.setVisibility(View.GONE);
            }
            WishlistAdapter adapter = new WishlistAdapter(getActivity(), arrayList, empty_text, tv_count);
            wishlist_view.setAdapter(adapter);
            ((Utility) getActivity()).hideLoading();
            wishlist_view.setLayoutManager(new LinearLayoutManager(getActivity()));
            if (pendingIntroAnimation) {
                pendingIntroAnimation = false;
                startIntroAnimation();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


/*    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        *//*inflater.inflate(R.menu.tool_menu, menu);*//*
        menu.add(0, R.id.miCompose, 0,
                getResources().getString(R.string.compose))
                .setIcon(R.mipmap.cart).setActionView(R.layout.action_bar_notifitcation_icon)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        //this.mMenu = menu;
        super.onCreateOptionsMenu(menu, inflater);

    }*/


/*
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.miCompose);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), CartDetailsNew.class);
                */
/*Intent in = new Intent(getActivity(), Cart.class);*//*

                startActivity(in);
            }
        });
    }
*/


    @Override
    public void onResume() {
        super.onResume();

        try {
            ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.get_userId(pref), Session.getcart_id(pref), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                try {
                   *//* Intent intent = new Intent(getActivity(), Home_dashboard.class);
                    startActivity(intent);
                    getActivity().finish();*//*
                } catch (Exception e) {
                    e.printStackTrace();
                }
            case R.id.miCompose:
                Intent in = new Intent(getActivity(), CartDetailsNew.class);
                *//*Intent in = new Intent(getActivity(), Cart.class);*//*
                startActivity(in);
                break;
        }
        return (super.onOptionsItemSelected(item));
    }*/

    private void startIntroAnimation() {
        wishlist_view.setTranslationY(wishlist_view.getHeight());
        wishlist_view.setAlpha(0f);
        wishlist_view.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {

            if (response.isSuccessful()) {
                // MyCartDetailModal myCartDetailModal;
                WishlistModal wishlistModal;
                switch (tag) {
                    case ApiServerResponse.MY_CART_DETAILS:
                       /* myCartDetailModal = (MyCartDetailModal) response.body();
                        if (myCartDetailModal.getItems().isEmpty()) {
                            tv_count.setText("0");
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(pref, "0");
                        } else {
                            Session.setCartItemsQuantity(pref, String.valueOf(myCartDetailModal.getItems().size()));
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            tv_count.setText(String.valueOf(myCartDetailModal.getItems().size()));
                            //supportInvalidateOptionsMenu();
                        }*/
                        ((Utility) getActivity()).hideLoading();
                        break;

                    case ApiServerResponse.WISHLIST:
                        wishlistModal = (WishlistModal) response.body();

                        if (wishlistModal.getStatus().equalsIgnoreCase(Constant.OK)) {


                            wishlist = wishlistModal.getProduct_info();

                            if (wishlist.isEmpty() && wishlist.size() == 0) {
                                tv_empty_text.setVisibility(View.VISIBLE);
                                wishlist_view.setVisibility(View.GONE);
                            } else {
                                WishlistAdapterNew adapterNew = new WishlistAdapterNew(getActivity(), wishlist);
                                wishlist_view.setAdapter(adapterNew);
                                wishlist_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                                if (pendingIntroAnimation) {
                                    pendingIntroAnimation = false;
                                    startIntroAnimation();
                                }
                            }


                        } else {
                            tv_empty_text.setVisibility(View.VISIBLE);
                            wishlist_view.setVisibility(View.GONE);
                        }
                        ((Utility) getActivity()).hideLoading();
                        break;
                }
            } else {
                ((Utility) getActivity()).hideLoading();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            ((Utility) getActivity()).hideLoading();
            switch (tag) {
                case ApiServerResponse.MY_CART_DETAILS:
                    System.out.println("Error");
                    ((Utility) getActivity()).hideLoading();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
