package kartku.com.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.my_product_reviews_module.MyProductReviewsListener;
import kartku.com.my_product_reviews_module.MyProductReviewsManager;
import kartku.com.my_product_reviews_module.MyProductReviewsModal;
import kartku.com.product_review.ProductReview;
import kartku.com.utils.Constant;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 12/13/2016.
 */

public class ProductReviewFragment extends Fragment implements MyProductReviewsListener {


    RecyclerView recycler_view_my_reviews;
    TextView tv_msg;

    SharedPreferences pref;

    List<MyProductReviewsModal> myProductReviewsList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.activity_product_review, container, false);
        recycler_view_my_reviews = (RecyclerView) view.findViewById(R.id.recycler_view_my_reviews);
        tv_msg = (TextView) view.findViewById(R.id.tv_msg);

        // banner_list = (RecyclerView)view.findViewById(R.id.banner_recycleview);
        try {
            new MyProductReviewsManager(getActivity(), this).sendRequest("/" + Session.get_userId(pref));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    @Override
    public void onMyProductReviewSuccess(String response) {

        try {

            myProductReviewsList = new ArrayList<>();
            JSONArray order_items = new JSONArray(response);

            for (int i = 0; i < order_items.length(); i++) {

                JSONObject item = order_items.getJSONObject(i);
                MyProductReviewsModal.ResultsBean myProductReviews = new MyProductReviewsModal.ResultsBean();
                myProductReviews.setProduct_title(item.getString(Constant.PRODUCT_TITLE));
                //myProductReviews.setpr
                myProductReviews.setStatus(item.getString(Constant.STATUS));

                //myOrders.add(myOrderModal);

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onMyProductReviewError(String error) {

    }
}
