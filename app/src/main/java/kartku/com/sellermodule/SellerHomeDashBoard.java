package kartku.com.sellermodule;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import java.util.List;

import kartku.com.R;
import kartku.com.activity.AboutUsActivity;
import kartku.com.activity.ContactActivity;
import kartku.com.activity.PrivacyPolicyActivity;
import kartku.com.adapter.SellerModuleOrdersAdapter;
import kartku.com.login_module.Login;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.LanguageSelectModal;
import kartku.com.retrofit.modal.SellerOrdersModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by satoti.garg on 1/16/2017.
 */

public class SellerHomeDashBoard extends Utility implements ApiServerResponse {

    RecyclerView recyclerViewSellerOrders;
    List<SellerOrdersModal.ResultsBean> mItems;
    //List<SellerOrdersModal.ResultsBean.OrderDetailBean> orderDetailBeanList;
    //List<SellerOrdersModal.ResultsBean.ProductDetailBean> productDetailBeanList;
    private SharedPreferences pref;
    TextView tv_message;
    SellerModuleOrdersAdapter adapter;
    boolean pendingIntroAnimation;

    Toolbar toolbar;
    String selectedLanguage;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_module_dashboard);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setTitle(R.string.my_orders);
        } catch (Exception e) {
            e.printStackTrace();
        }


        pref = new ObscuredSharedPreferences(SellerHomeDashBoard.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        recyclerViewSellerOrders = (RecyclerView) findViewById(R.id.recyclerViewSellerOrders);
        tv_message = (TextView) findViewById(R.id.tv_message);

        // ToastMsg.showShortToast(SellerHomeDashBoard.this, getResources().getString(R.string.logged_in));
        try {
            if (checkInternetConnection(SellerHomeDashBoard.this)) {
                showLoading();

                ServerAPI.getInstance().getSellerOrders(ApiServerResponse.SELLER_ORDERS, Session.get_userId(pref), Session.get_userId(pref), this);
            } else {
                showAlertDialog(SellerHomeDashBoard.this);
            }


            /*ServerAPI.getInstance().getSellerOrders(ApiServerResponse.SELLER_ORDERS, Session.get_userId(pref), this);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {

            if (response.isSuccessful()) {
                SellerOrdersModal sellerOrdersModal;
                LanguageSelectModal languageSelectModal;
                switch (tag) {
                    case ApiServerResponse.SELLER_ORDERS:
                        sellerOrdersModal = (SellerOrdersModal) response.body();
                        if (sellerOrdersModal.getStatus().equals(Constant.OK)) {


                            if (sellerOrdersModal.getResults().size() == 0) {
                                tv_message.setVisibility(View.VISIBLE);
                                recyclerViewSellerOrders.setVisibility(View.GONE);
                            } else {
                                tv_message.setVisibility(View.GONE);
                                recyclerViewSellerOrders.setVisibility(View.VISIBLE);
                                mItems = sellerOrdersModal.getResults();
                                adapter = new SellerModuleOrdersAdapter(SellerHomeDashBoard.this, mItems);
                            }

                            recyclerViewSellerOrders.setAdapter(adapter);
                            recyclerViewSellerOrders.setItemAnimator(new DefaultItemAnimator());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SellerHomeDashBoard.this);
                            recyclerViewSellerOrders.setLayoutManager(mLayoutManager);
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation();
                            }
                        }
                        hideLoading();
                        break;
                    case ApiServerResponse.SET_LANGUAGE:
                        languageSelectModal = (LanguageSelectModal) response.body();
                        if (languageSelectModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            Session.setAppLanguage(pref, selectedLanguage);
                            if (selectedLanguage.equalsIgnoreCase("1")) {
                                ToastMsg.showLongToast(SellerHomeDashBoard.this, "English set as default language.");
                            } else {
                                ToastMsg.showLongToast(SellerHomeDashBoard.this, "Indonesian set as default language.");

                            }

                            finish();
                            Intent i = new Intent(SellerHomeDashBoard.this, SellerHomeDashBoard.class);
                            startActivity(i);


                        }

                        break;
                }
            } else {
                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {

            switch (tag) {
                case ApiServerResponse.SELLER_ORDERS:
                    System.out.println("Error");
                    hideLoading();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startIntroAnimation() {
        recyclerViewSellerOrders.setTranslationY(recyclerViewSellerOrders.getHeight());
        recyclerViewSellerOrders.setAlpha(0f);
        recyclerViewSellerOrders.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_seller_login, menu);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_seller_logout) {

            Session.set_login_status(pref, false);
            pref.edit().clear().apply();

            Intent i = new Intent(SellerHomeDashBoard.this, Login.class);
            startActivity(i);
            ToastMsg.showShortToast(SellerHomeDashBoard.this, getString(R.string.logged_out));
            finish();
        } else if (item.getItemId() == R.id.menu_change_language) {
            if (checkInternetConnection(SellerHomeDashBoard.this)) {
                showSelectLanguageAlertDialog();
            } else {
                showAlertDialog(SellerHomeDashBoard.this);

            }
        } else if (item.getItemId() == R.id.menu_contact_us) {
            Intent contact = new Intent(SellerHomeDashBoard.this, ContactActivity.class);
            startActivity(contact);
        } else if (item.getItemId() == R.id.menu_privacy_policy) {
            Intent pp = new Intent(SellerHomeDashBoard.this, PrivacyPolicyActivity.class);
            startActivity(pp);

        } else if (item.getItemId() == R.id.menu_about_us) {
            Intent about = new Intent(SellerHomeDashBoard.this, AboutUsActivity.class);
            startActivity(about);
        }

        return super.onOptionsItemSelected(item);
    }

    private void showSelectLanguageAlertDialog() {

        try {
            /*final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);*/
            final CharSequence[] array = {"English", "Indonesian"};
            int preSelectedLanguage = 0;
            if (Session.getAppLanguage(pref).equalsIgnoreCase("1")) {
                preSelectedLanguage = 0;
            } else if (Session.getAppLanguage(pref).equalsIgnoreCase("2")) {
                preSelectedLanguage = 1;
            } else {
                preSelectedLanguage = 0;
            }
            selectedLanguage = String.valueOf(preSelectedLanguage);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Select Language");
            //dialogBuilder.setMessage("Please select your preferred language?");
            dialogBuilder.setCancelable(true);
            dialogBuilder.setSingleChoiceItems(array, preSelectedLanguage, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //ToastMsg.showShortToast(DemoHomeDashBoard.this, array[which].toString());
                    if (which == 0) {
                        selectedLanguage = "1";
                    } else if (which == 1) {
                        selectedLanguage = "2";
                    } else {
                        selectedLanguage = "1";
                    }

                   /* if (array[which].toString().equalsIgnoreCase(Constant.LANGUAGE_ENGLISH)) {
                        selectedLanguage = "1";
                    } else if (array[which].toString().equalsIgnoreCase(Constant.LANGUAGE_INDONESIAN)) {
                        selectedLanguage = "2";
                    } else {
                        selectedLanguage = "1";
                    }*/

                    //Session.setAppLanguage(prefrence, array.);
                }
            });
            dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //ToastMsg.showShortToast(DemoHomeDashBoard.this, array[which].toString());
                    //onBackExitFlag = false;
                }
            });

            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                   /* if (which == 0) {
                        selectedLanguage = "1";
                    } else if (which == 1) {
                        selectedLanguage = "2";
                    } else {
                        selectedLanguage = "1";
                    }*/


                    if (checkInternetConnection(SellerHomeDashBoard.this)) {

                        if (!Session.getAppLanguage(pref).equalsIgnoreCase(selectedLanguage)) {

                            showLoading();
                            ServerAPI.getInstance().selectLanguage(ApiServerResponse.SET_LANGUAGE, selectedLanguage, Session.get_userId(pref), SellerHomeDashBoard.this);
                        } else {
                            ToastMsg.showShortToast(SellerHomeDashBoard.this, getString(R.string.already_selected));
                        }


                    } else {
                        showAlertDialog(SellerHomeDashBoard.this);
                    }
                    //ToastMsg.showShortToast(DemoHomeDashBoard.this, array[which].toString());

                    //onBackExitFlag = true;
                }
            });
/* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/

            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Connection Problem.");
            dialogBuilder.setMessage("Please check your internet connection?");
            dialogBuilder.setCancelable(true);
           *//* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    //finish();
                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
