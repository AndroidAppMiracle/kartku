package kartku.com.sellermodule;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import kartku.com.R;
import kartku.com.adapter.SellerOrderDetailsAdapter;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.SellerOrdersModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 1/24/2017.
 */

public class SellerOrderDetails extends Utility implements ApiServerResponse {

    TextView username, contact_no, email_id, address, created_at;
    RecyclerView recycleviewList;

    SellerOrderDetailsAdapter adapter;

    //Amount Calculation Section
    TextView tv_amount, tv_shipping_cost, tv_tax, tv_total_amount, tv_grand_total, tv_order_id;
    SellerOrdersModal.ResultsBean resultsBean;
    Resources resources;

    List<SellerOrdersModal.ResultsBean> mItems;
    private SharedPreferences pref;

    /*  public SellerOrderDetails(SellerOrdersModal.ResultsBean resultsBean) {
          super();
          this.resultsBean = resultsBean;
      }
  */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_order_details);

        pref = new ObscuredSharedPreferences(SellerOrderDetails.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_order_detail);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        /*setListener();*/
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        resources = getResources();
        username = (TextView) findViewById(R.id.username);
        contact_no = (TextView) findViewById(R.id.contact_no);
        email_id = (TextView) findViewById(R.id.email_id);
        address = (TextView) findViewById(R.id.address);
        recycleviewList = (RecyclerView) findViewById(R.id.recycleviewList);
        created_at = (TextView) findViewById(R.id.created_at);
          /*resultsBean.getProduct_detail().get*/
        tv_amount = (TextView) findViewById(R.id.tv_amount);
        tv_shipping_cost = (TextView) findViewById(R.id.tv_shipping_cost);
        tv_tax = (TextView) findViewById(R.id.tv_tax);
        tv_total_amount = (TextView) findViewById(R.id.tv_total_amount);
        tv_grand_total = (TextView) findViewById(R.id.tv_grand_total);
        tv_order_id = (TextView) findViewById(R.id.tv_order_id);


        try {

            if (checkInternetConnection(SellerOrderDetails.this)) {

                showLoading();
                ServerAPI.getInstance().getSellerOrders(ApiServerResponse.SELLER_ORDERS, Session.get_userId(pref), Session.get_userId(pref), this);
            } else {
                showAlertDialog(SellerOrderDetails.this);
            }

            /*ServerAPI.getInstance().getSellerOrders(ApiServerResponse.SELLER_ORDERS, Session.get_userId(pref), this);*/
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {

            if (getIntent().getExtras().getInt(Constant.ORDER_ID) != 0 || getIntent().getExtras().getInt(Constant.ORDER_ID) > 0) {
                tv_order_id.setText(String.format(resources.getString(R.string.two_parameters_with_int), getResources().getString(R.string.order_id), getIntent().getExtras().getInt(Constant.ORDER_ID)));
            }


            if (getIntent().getExtras().getString(Constant.TOTAL_AMOUNT) != null) {
                tv_amount.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.amount), getIntent().getExtras().getString(Constant.TOTAL_AMOUNT)));
            }


            if (getIntent().getExtras().getString(Constant.SHIPPING_COST) != null) {
                tv_shipping_cost.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.shipping_cost), getIntent().getExtras().getString(Constant.SHIPPING_COST)));
            }

            if (getIntent().getExtras().getInt(Constant.TAX) != 0) {
                tv_tax.setText(String.format(resources.getString(R.string.two_parameters_with_int), getResources().getString(R.string.tax), getIntent().getExtras().getInt(Constant.TAX)));
            }

            if (getIntent().getExtras().getInt(Constant.GRAND_TOTAL) != 0) {
                tv_grand_total.setText(String.valueOf(getIntent().getExtras().getInt(Constant.GRAND_TOTAL)));
            }


            //tv_total_amount.setText(String.format(resources.getString(R.string.two_parameters), "Total Amount", getIntent().getExtras().getString(Constant.GRAND_TOTAL)));


            if (getIntent().getExtras().getString(Constant.PICKUP_CITY) != null) {

                username.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.pick_up_city), getIntent().getExtras().getString(Constant.PICKUP_CITY)));
            } else {
                username.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.pick_up_city), ""));
            }

            if (getIntent().getExtras().getString(Constant.PICKUP_CITY_CODE) != null) {
                contact_no.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.pick_up_city_code), getIntent().getExtras().getString(Constant.PICKUP_CITY_CODE)));
            } else {
                contact_no.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.pick_up_city_code), ""));
            }

            if (getIntent().getExtras().getString(Constant.DEST_CITY) != null) {
                email_id.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.destination_city), getIntent().getExtras().getString(Constant.DEST_CITY)));
            } else {
                email_id.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.destination_city), ""));
            }


            if (getIntent().getExtras().getString(Constant.DEST_CITY_CODE) != null) {
                address.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.destination_city_code), getIntent().getExtras().getString(Constant.DEST_CITY_CODE)));
            } else {
                address.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.destination_city_code), ""));
            }

            if (getIntent().getExtras().getString(Constant.CREATED_AT) != null) {
                created_at.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.date), getIntent().getExtras().getString(Constant.CREATED_AT)));
            } else {
                created_at.setText(String.format(resources.getString(R.string.two_parameters), getResources().getString(R.string.date), ""));
            }


        /*    adapter = new SellerOrderDetailsAdapter(SellerOrderDetails.this, resultsBean);
            recycleviewList.setAdapter(adapter);
            recycleviewList.setItemAnimator(new DefaultItemAnimator());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SellerOrderDetails.this);
            recycleviewList.setLayoutManager(mLayoutManager);*/
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                SellerOrdersModal sellerOrdersModal;
                switch (tag) {
                    case ApiServerResponse.SELLER_ORDERS:
                        sellerOrdersModal = (SellerOrdersModal) response.body();
                        if (sellerOrdersModal.getStatus().equals(Constant.OK)) {

                            mItems = sellerOrdersModal.getResults();
                          /*  if (sellerOrdersModal.getResults().isEmpty()) {
                                tv_message.setVisibility(View.VISIBLE);
                                recyclerViewSellerOrders.setVisibility(View.GONE);
                            } else {
                                recyclerViewSellerOrders.setVisibility(View.VISIBLE);
                                mItems = sellerOrdersModal.getResults();

                            }*/

                            adapter = new SellerOrderDetailsAdapter(SellerOrderDetails.this, mItems);
                            recycleviewList.setAdapter(adapter);
                            recycleviewList.setItemAnimator(new DefaultItemAnimator());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SellerOrderDetails.this);
                            recycleviewList.setLayoutManager(mLayoutManager);
                        }

                        hideLoading();
                        break;
                }

            } else {
                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }


/*
    private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Connection Problem.");
            dialogBuilder.setMessage("Please check your internet connection?");
            dialogBuilder.setCancelable(true);
           */
/* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*

            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    //finish();
                }
            });
           */
/* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*

            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/
}
