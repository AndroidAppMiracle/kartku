package kartku.com.MyOrders.listener;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public interface MyOrderListener {

    void onMyOrderSuccess(String response);

    void onMyOrderError(String error);
}
