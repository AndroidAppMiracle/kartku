package kartku.com.MyOrders.api;

import android.content.Context;

import java.util.Map;

import kartku.com.MyOrders.manager.MyOrderManager;
import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class MyOrderApi implements CustomResponse {

    private RequestServer request_server;
    private Context context;
    private MyOrderManager manager;

    public MyOrderApi(Context context, MyOrderManager manager)
    {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void onCustomResponse(String response) {
       manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId ,  Map<String, String> params){
        try {
            request_server = new RequestServer(context, requestId, params,this , RequestConstants.GET_URL);
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
