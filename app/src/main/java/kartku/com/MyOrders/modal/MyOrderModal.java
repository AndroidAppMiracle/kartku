package kartku.com.MyOrders.modal;

import java.util.List;

/**
 * Created by satoti.garg on 1/18/2017.
 */

public class MyOrderModal {


    /**
     * status : OK
     * results : [{"id":"137","user_id":"181","cart_id":"198","status":"PAYMENT_SUCCESS","total_amount":"3000.0000","shipping_cost":"0","tax":"270","grand_total":"3270","tracking_id":"","created_at":"2017-01-16"},{"id":"136","user_id":"181","cart_id":"196","status":"PAYMENT_PENDING","total_amount":"800.0000","shipping_cost":"0","tax":"72","grand_total":"872","tracking_id":"","created_at":"2017-01-16"}]
     */

    private String status;
    private List<ResultsBean> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class ResultsBean {
        /**
         * id : 137
         * user_id : 181
         * cart_id : 198
         * status : PAYMENT_SUCCESS
         * total_amount : 3000.0000
         * shipping_cost : 0
         * tax : 270
         * grand_total : 3270
         * tracking_id :
         * created_at : 2017-01-16
         */

        private String id;
        private String user_id;
        private String cart_id;
        private String status;
        private String total_amount;
        private String shipping_cost;
        private String tax;
        private String grand_total;
        private String tracking_id;
        private String created_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getCart_id() {
            return cart_id;
        }

        public void setCart_id(String cart_id) {
            this.cart_id = cart_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getShipping_cost() {
            return shipping_cost;
        }

        public void setShipping_cost(String shipping_cost) {
            this.shipping_cost = shipping_cost;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getGrand_total() {
            return grand_total;
        }

        public void setGrand_total(String grand_total) {
            this.grand_total = grand_total;
        }

        public String getTracking_id() {
            return tracking_id;
        }

        public void setTracking_id(String tracking_id) {
            this.tracking_id = tracking_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
