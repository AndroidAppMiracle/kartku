package kartku.com.MyOrders.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.MyOrders.api.MyOrderApi;
import kartku.com.MyOrders.listener.MyOrderListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class MyOrderManager {

    private Context context;
    private MyOrderListener listener;
    SharedPreferences pref;

    public MyOrderManager(Context context, MyOrderListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    public void sendRequest(String user_id) {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.USERID, "" + user_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new MyOrderApi(context, MyOrderManager.this).sendRequest(API.MY_ORDERS_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                //JSONObject order_detail = res_object.getJSONObject(Constant.ORDER_DETAIL);
                JSONArray order_items = res_object.getJSONArray(Constant.RESULTS);
                listener.onMyOrderSuccess("" + order_items);

            } else {
                String message = res_object.getString(Constant.ORDER_DETAIL);
                listener.onMyOrderError(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onError(String message) {
        listener.onMyOrderError(message);
    }
}
