package kartku.com.search;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import kartku.com.R;
import kartku.com.activity.CategoryProducts;
import kartku.com.adapter.ProductListAdapter;
import kartku.com.adapter.SearchListAdapter;
import kartku.com.cart.Cart;
import kartku.com.cart.CartDetailsNew;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.product_category.GridSpacingItemDecoration;
import kartku.com.product_category.ProductList;
import kartku.com.product_category.model.ProductListModel;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.search.listener.HighLowListener;
import kartku.com.search.listener.LowHighListener;
import kartku.com.search.listener.NewArrivalsListener;
import kartku.com.search.listener.SearchListener;
import kartku.com.search.manager.HighLowManager;
import kartku.com.search.manager.LowHighManager;
import kartku.com.search.manager.NewArrivalsManager;
import kartku.com.search.manager.SearchManager;
import kartku.com.search.model.SearchBean;
import kartku.com.sort.BrandListActivity;
import kartku.com.sort.MaterialList;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.listener.WishlistListener;
import kartku.com.wishlist.manager.WishlistManager;
import kartku.com.wishlist.model.WishlistBean;
import retrofit2.Response;

public class SearchActivity extends Utility implements View.OnClickListener, SearchListener, WishlistListener, ApiServerResponse {

    ArrayList<SearchBean> searchList = new ArrayList<SearchBean>();
    ArrayList<WishlistBean> WishlistarrayList = new ArrayList<WishlistBean>();
    RecyclerView recyclerView;
    ImageView search_icon;
    EditText search_text;
    Button button_sort, button_filter;
    ProductListModel productListModel;
    SharedPreferences pref;
    String id;
    TextView cancel;
    LinearLayout search_layout, filter_layout;
    JSONArray products_list;

    boolean pendingIntroAnimation;
    TextView tv_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        int spanCount = 2; // 3 columns
        int spacing = 10; // 50px
        boolean includeEdge = false;
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        search_icon = (ImageView) findViewById(R.id.search_icon);
        search_text = (EditText) findViewById(R.id.search_text);

        search_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    try {
                        searchAction(search_text.getText().toString());
                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(search_text.getApplicationWindowToken(), 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });
        search_layout = (LinearLayout) findViewById(R.id.top_wrapper);
        cancel = (TextView) findViewById(R.id.cancel_text);
        filter_layout = (LinearLayout) findViewById(R.id.filterWrapper);
        search_icon.setOnClickListener(this);
        button_sort = (Button) findViewById(R.id.button_sort);
        button_filter = (Button) findViewById(R.id.button_filter);
        cancel.setOnClickListener(this);
        button_sort.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogforMasterCat();
            }
        });
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        button_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    filterDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_search);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            String cart_id = Session.getcart_id(pref);
            if (!cart_id.equalsIgnoreCase("")) {
                ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS,Session.get_userId(pref), cart_id, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Intent intent = getIntent();
            String search_query = intent.getStringExtra("search_query");
            //ToastMsg.showLongToast(SearchActivity.this, search_query);
            // String search_cat = intent.getStringExtra("search_cat");
            //int search_id = intent.getIntExtra("search_id",0);

            new SearchManager(getApplicationContext(), this).sendRequest(search_query);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void openDialogforMasterCat() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("Support");

        TextView new_arrivals = (TextView) dialog.findViewById(R.id.new_arrivals);

        TextView LowHigh = (TextView) dialog.findViewById(R.id.lowHigh);

        TextView HighLow = (TextView) dialog.findViewById(R.id.Highlow);

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);

        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        new_arrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new NewArrivalsManager(getApplicationContext(), new NewArrivalsListener() {
                        @Override
                        public void onnewArrivalsListenerSuccess(String response) {

                            try {
                                dialog.dismiss();
                                bindData(response);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(SearchActivity.this, error);
                        }
                    }).sendRequest("?query=" + search_text.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LowHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new LowHighManager(getApplicationContext(), new LowHighListener() {
                        @Override
                        public void onlowhighListenerSuccess(String response) {
                            try {
                                dialog.dismiss();
                                bindData(response);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(SearchActivity.this, error);
                        }
                    }).sendRequest("?query=" + search_text.getText().toString() + "&less=less");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        HighLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new HighLowManager(getApplicationContext(), new HighLowListener() {
                        @Override
                        public void onhighlowListenerSuccess(String response) {
                            try {
                                dialog.dismiss();
                                bindData(response);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(SearchActivity.this, error);
                        }
                    }).sendRequest("?query=" + search_text.getText().toString() + "&high=high");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }

    public void filterDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);

        TextView new_arrivals = (TextView) dialog.findViewById(R.id.new_arrivals);
        new_arrivals.setText("Brands");

        TextView LowHigh = (TextView) dialog.findViewById(R.id.lowHigh);
        LowHigh.setText("Materials");

        TextView HighLow = (TextView) dialog.findViewById(R.id.Highlow);
        HighLow.setVisibility(View.GONE);

        View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        new_arrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new NewArrivalsManager(getApplicationContext(), new NewArrivalsListener() {
                        @Override
                        public void onnewArrivalsListenerSuccess(String response) {

                            try {
                                dialog.dismiss();
                                Intent intent = new Intent(SearchActivity.this, BrandListActivity.class);
                                intent.putExtra("search_key", search_text.getText().toString());
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(SearchActivity.this, error);
                        }
                    }).sendRequest("?query=" + search_text.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LowHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new LowHighManager(getApplicationContext(), new LowHighListener() {
                        @Override
                        public void onlowhighListenerSuccess(String response) {
                            try {
                                dialog.dismiss();
                                Intent intent = new Intent(SearchActivity.this, MaterialList.class);
                                intent.putExtra("search_key", search_text.getText().toString());
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(SearchActivity.this, error);
                        }
                    }).sendRequest("?query=" + search_text.getText().toString() + "&less=less");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        /*SeekBar seek_bar = (SeekBar)dialog.findViewById(R.id.price_seekbar);
        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });*/
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tool_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SearchActivity.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.miCompose:
                Intent in = new Intent(this, CartDetailsNew.class);
                /*Intent in = new Intent(this, Cart.class);*/
                startActivity(in);
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.search_icon:
                try {
                    String search_input = search_text.getText().toString();
                    if (search_input.length() <= 0) {
                        ToastMsg.showShortToast(SearchActivity.this, "Please enter keyword to search data");
                    } else {

                        if (searchList.size() != 0) {
                            searchList.clear();
                        }

                        showLoading();
                        new SearchManager(getApplicationContext(), this).sendRequest("?query=" + search_input);
                        Utility.hideSoftKeyboard(SearchActivity.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.cancel_text:
                try {
                    search_text.setText("");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }


    }


    public void onSearchSuccess(String response) {
        try {
            products_list = new JSONArray(response);
            System.out.println("search product_categories_list *****  " + products_list);
            if (searchList.size() != 0) {
                searchList.clear();
            }

            try {
                String user_id = Session.get_userId(pref);
                new WishlistManager(SearchActivity.this, this).sendRequest(user_id, "");
            } catch (Exception e) {
                e.printStackTrace();
            }

           /* for(int i=0;i<products_list.length();i++){
                JSONObject jsonObject = products_list.getJSONObject(i);
                SearchBean obj_bean = new SearchBean();
                obj_bean.setName(jsonObject.getString(Constant.TITTLE).toString());
                obj_bean.setPrice(jsonObject.getString(Constant.PRICE));
                obj_bean.setImage(jsonObject.getString(Constant.IMAGE_URL));
                obj_bean.setId(jsonObject.getString(Constant.ID));
                searchList.add(obj_bean);
            }
            SearchListAdapter adapter=new SearchListAdapter(getApplicationContext(),searchList);
            recyclerView.setAdapter(adapter);
            recyclerView.setVisibility(View.VISIBLE);
            filter_layout.setVisibility(View.VISIBLE);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onError(String error) {
        hideLoading();
        ToastMsg.showShortToast(SearchActivity.this, error);

    }

    private void bindData(String response) {
        try {
            try {
                products_list = new JSONArray(response);
                System.out.println("search product_categories_list *****  " + products_list);
                searchList.clear();
                try {
                    String user_id = Session.get_userId(pref);
                    new WishlistManager(SearchActivity.this, this).sendRequest(user_id, "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
               /* for(int i=0;i<products_list.length();i++){
                    JSONObject jsonObject = products_list.getJSONObject(i);
                    SearchBean obj_bean = new SearchBean();
                    obj_bean.setName(jsonObject.getString(Constant.TITTLE).toString());
                    obj_bean.setPrice(jsonObject.getString(Constant.PRICE));
                    obj_bean.setImage(jsonObject.getString(Constant.IMAGE_URL));
                    obj_bean.setId(jsonObject.getString(Constant.ID));
                    searchList.add(obj_bean);
                }
                SearchListAdapter adapter=new SearchListAdapter(getApplicationContext(),searchList);
                recyclerView.setAdapter(adapter);
                recyclerView.setVisibility(View.VISIBLE);
                filter_layout.setVisibility(View.VISIBLE);*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchAction(String search_input) {
        new SearchManager(getApplicationContext(), this).sendRequest("?query=" + search_input);
    }

    @Override
    public void onwishlistError(String error) {

        hideLoading();
    }

    @Override
    public void onwishlistSuccess(String response) {
        try {
            try {
                JSONArray products_wishlist = new JSONArray(response);
                System.out.println("product wishlist  " + products_wishlist);
                ArrayList<String> wishlistIds = new ArrayList<String>();
                for (int i = 0; i < products_wishlist.length(); i++) {
                    JSONObject jsonObject = products_wishlist.getJSONObject(i);
                    WishlistBean list_model = new WishlistBean();
                    Log.d("", "wishlist id " + jsonObject.getString(Constant.ID));
                    String wishlist_id = jsonObject.getString(Constant.ID);
                    wishlistIds.add(wishlist_id);
                    list_model.setId(jsonObject.getString(Constant.ID));
                    list_model.setName(jsonObject.getString(Constant.NAME));
                    list_model.setPrice(jsonObject.getString(Constant.PRICE_KEY));
                    list_model.setDescription(jsonObject.getString(Constant.DESCRIPTION));
                    list_model.setImage(jsonObject.getString(Constant.IMAGE));
                    WishlistarrayList.add(list_model);
                }

                try {
                    try {
                        System.out.println("search product_categories_list *****  " + products_list);
                        searchList.clear();
                        for (int i = 0; i < products_list.length(); i++) {
                            JSONObject jsonObject = products_list.getJSONObject(i);
                            SearchBean obj_bean = new SearchBean();
                            String product_id = jsonObject.getString(Constant.ID);

                            obj_bean.setName(jsonObject.getString(Constant.TITTLE));
                            obj_bean.setPrice(jsonObject.getString(Constant.PRICE));
                            obj_bean.setImage(jsonObject.getString(Constant.IMAGE_URL));
                            obj_bean.setId(jsonObject.getString(Constant.ID));
                            obj_bean.setBrand(jsonObject.getString(Constant.BRAND));
                            obj_bean.setMaterial(jsonObject.getString(Constant.MATERIAl));
                            obj_bean.setDescription(jsonObject.getString(Constant.DESCRIPTION));

                            if (wishlistIds.contains(product_id)) {
                                obj_bean.setWishlist("1");

                            } else {
                                obj_bean.setWishlist("0");
                            }
                            searchList.add(obj_bean);
                        }
                        SearchListAdapter adapter = new SearchListAdapter(SearchActivity.this, searchList);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setItemAnimator(new SlideInUpAnimator());
                        recyclerView.setVisibility(View.VISIBLE);
                        filter_layout.setVisibility(View.VISIBLE);
                        if (pendingIntroAnimation) {
                            pendingIntroAnimation = false;
                            startIntroAnimation();
                        }

                        hideLoading();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*for(int j=0;j<products_list.length();j++){
                    JSONObject jsonObject1 = products_list.getJSONObject(j);
                    productListModel = new ProductListModel();
                    String product_id = jsonObject1.getString(Constant.ID);

                    productListModel = new ProductListModel();
                    productListModel.setName(jsonObject1.getString(Constant.TITTLE).toString());
                    productListModel.setPrice(jsonObject1.getString(Constant.PRICE));
                    productListModel.setImage_product(jsonObject1.getString(Constant.IMAGE_URL));
                    productListModel.setId(jsonObject1.getString(Constant.ID));

                    //if(product_id!=null && wishlist_id!=null) {
                    if (wishlistIds.contains(product_id)) {
                        productListModel.setWishlist("add");

                    } else {
                        productListModel.setWishlist("remove");
                    }
                    //}
                    arrayList.add(productListModel);
                }*/

            } catch (Exception e) {
                e.printStackTrace();
            }

            /*try {
                ProductListAdapter adapter = new ProductListAdapter(getApplicationContext(), arrayList , WishlistarrayList);
                recyclerView.setAdapter(adapter);
                int spanCount = 2; // 3 columns
                int spacing = 50; // 50px
                boolean includeEdge = false;
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
                recyclerView.setLayoutManager(layoutManager);
            } catch (Exception e) {
                e.printStackTrace();
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startIntroAnimation() {
        recyclerView.setTranslationY(recyclerView.getHeight());
        recyclerView.setAlpha(0f);
        recyclerView.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {

            searchList.clear();
            try {
                String user_id = Session.get_userId(pref);
                new WishlistManager(SearchActivity.this, this).sendRequest(user_id, "");
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {
        if (response.isSuccessful()) {
            MyCartDetailModal myCartDetailModal;

            switch (tag) {

                case ApiServerResponse.MY_CART_DETAILS:

                    myCartDetailModal = (MyCartDetailModal) response.body();
                    if (myCartDetailModal.getItems().size() == 0) {
                        tv_count.setText("0");
                        //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                        Session.setCartItemsQuantity(pref, "0");
                    } else {
                        Session.setCartItemsQuantity(pref, String.valueOf(myCartDetailModal.getItems().size()));
                        //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                        tv_count.setText(String.valueOf(myCartDetailModal.getItems().size()));
                    }
                    break;

            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }



/*    public void sortList() {
        try {
            new LowHighManager(getApplicationContext(), new LowHighListener() {
                @Override
                public void onlowhighListenerSuccess(String response) {
                    try {
                        //dialog.dismiss();
                        Intent intent = new Intent(SearchActivity.this, MaterialList.class);
                        intent.putExtra("search_key", search_text.getText().toString());
                        startActivity(intent);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error) {
                    ToastMsg.showShortToast(SearchActivity.this, error);
                }
            }).sendRequest("?query=" + search_text.getText().toString() + "&less=less");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
