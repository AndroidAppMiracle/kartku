package kartku.com.search.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kartku.com.search.api.HighLowApi;
import kartku.com.search.api.SearchApi;
import kartku.com.search.listener.HighLowListener;
import kartku.com.search.listener.SearchListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by GARG1 on 10/23/2016.
 */
public class HighLowManager {

    private Context context;
    private HighLowListener listener;
    SharedPreferences pref;

    public HighLowManager(Context context , HighLowListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String params)
    {
        try {
            new HighLowApi(context,HighLowManager.this).sendRequest(API.HIGH_LOW_PRICE_REQUEST_ID, params);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response)
    {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                try {
                    JSONArray products_data = new JSONArray(res_object.getString(Constant.PRODUCTS_DATA));
                    listener.onhighlowListenerSuccess("" + products_data);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }else{
                try {
                    String message = res_object.getString(Constant.PRODUCTS_DATA);
                    listener.onError(message);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onError(String message)
    {
        listener.onError(message);
    }
}
