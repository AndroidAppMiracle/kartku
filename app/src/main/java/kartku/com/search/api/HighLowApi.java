package kartku.com.search.api;

import android.content.Context;

import kartku.com.search.manager.HighLowManager;
import kartku.com.search.manager.SearchManager;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by GARG1 on 10/23/2016.
 */
public class HighLowApi implements CustomResponse {


    private Context context;
    private HighLowManager manager;
    private RequestServer request_server;

    public HighLowApi(Context context, HighLowManager manager) {
        this.context = context;
        this.manager = manager;
    }


    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId, String params) {
        try {
            request_server = new RequestServer(context, requestId, this, params);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
