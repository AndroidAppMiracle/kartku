package kartku.com.search.api;

import android.content.Context;

import kartku.com.cart.manager.CartDetailManager;
import kartku.com.search.manager.SearchManager;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class SearchApi implements CustomResponse {


    private Context context;
    private SearchManager manager;
    private RequestServer request_server;

    public SearchApi(Context context, SearchManager manager)
    {
        this.context = context;
        this.manager = manager;
    }


    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId , String params){
        try {
            request_server = new RequestServer(context, requestId,this ,params);
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
