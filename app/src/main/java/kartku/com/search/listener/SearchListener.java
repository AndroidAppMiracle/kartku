package kartku.com.search.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface SearchListener {
    void onSearchSuccess(String response);
    void onError(String error);

}
