package kartku.com.orders_module.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.login_module.api.LoginAPI;
import kartku.com.login_module.listener.LoginListener;
import kartku.com.orders_module.api.OrderHistoryApi;
import kartku.com.orders_module.listener.OrderHistroyListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class OrderHistoryManager {

    private Context context;
    private OrderHistroyListener listener;
    SharedPreferences pref;

    public OrderHistoryManager(Context context , OrderHistroyListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String order_id)
    {
            try {
                Map<String, String> params = new HashMap<>();
                try {
                    try {
                        params.put(RequestConstants.ORDER_ID, ""+order_id);
                    }catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
               new OrderHistoryApi(context,OrderHistoryManager.this).sendRequest(API.ORDER_HISTORY_REQUEST_ID, params);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
    }

    public void onSuccess(String response)
    {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                JSONObject order_history = res_object.getJSONObject(Constant.ORDER_HISTORY);
                listener.onHistorySuccess(""+order_history);

            }else{
                String message = res_object.getString(Constant.ORDER_HISTORY);
                listener.onHistoryError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void onError(String message)
    {
       listener.onHistoryError(message);
    }
}
