package kartku.com.orders_module.listener;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public interface OrderHistroyListener {

   void  onHistorySuccess(String response);
   void onHistoryError(String error);
}
