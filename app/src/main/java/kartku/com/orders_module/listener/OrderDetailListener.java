package kartku.com.orders_module.listener;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public interface OrderDetailListener {

    void onOrderDetailSuccess(String response);
    void onOrderDetailError(String error);
}
