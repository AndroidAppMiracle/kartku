package kartku.com.orders_module.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/19/2017.
 */

public class OrderDetailsModal {


    /**
     * status : OK
     * order_detail : {"id":158,"user_id":181,"user":"abcdef49","status":"PAYMENT_PENDING","total_amount":"3000.0000","tax":270,"shipping_cost":"0","grand_total":3270,"created_at":"2017-03-18","shipping_detail":[{"id":84,"user_id":null,"order_id":158,"firstname":"Abcdef","lastname":"abcdef","email":"abcdef@abcdef.com","mobile":2147483647,"address":"gwyaiivoaoft\r\nasjsjsywww","pincode":345667,"area":"Malang","landmark":null,"city":0,"state":null,"country":null,"dest_city_code":"","created_at":"2017-03-18"}],"order_items":[{"id":654,"cart_id":292,"product_id":17,"quantity":"1","shipping_status":"Confirmed","total_price":"180000","product_detail":{"id":17,"title":"My new product","description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","rating":"2.5384615384615","brand_id":3,"category_id":13,"is_featured":1,"user_id":1,"short_desc":"hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk","sku":"777777","slug":"my-new-product-17","status":"Enable","is_admin_allowed":1,"create_at":"2016-04-04 17:16:27","update_at":"2016-04-04 18:00:17","stock_availability":"AVAILABLE","pre_order":0,"pre_order_duration":"","delivery_time":"","comment":null,"is_deleted":0,"hits_count":88,"product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/mediumQQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg"}},{"id":691,"cart_id":292,"product_id":28,"quantity":"3","shipping_status":"Confirmed","total_price":"3000","product_detail":{"id":28,"title":"test product5","description":"test product5","rating":null,"brand_id":1,"category_id":13,"is_featured":0,"user_id":114,"short_desc":"sdadas","sku":"we355","slug":"test-product5-28","status":"","is_admin_allowed":1,"create_at":"2016-05-03 18:22:33","update_at":null,"stock_availability":"","pre_order":0,"pre_order_duration":"","delivery_time":"","comment":null,"is_deleted":0,"hits_count":3,"product_image":""}}]}
     */

    private String status;
    private OrderDetailBean order_detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OrderDetailBean getOrder_detail() {
        return order_detail;
    }

    public void setOrder_detail(OrderDetailBean order_detail) {
        this.order_detail = order_detail;
    }

    public static class OrderDetailBean {
        /**
         * id : 158
         * user_id : 181
         * user : abcdef49
         * status : PAYMENT_PENDING
         * total_amount : 3000.0000
         * tax : 270
         * shipping_cost : 0
         * grand_total : 3270
         * created_at : 2017-03-18
         * shipping_detail : [{"id":84,"user_id":null,"order_id":158,"firstname":"Abcdef","lastname":"abcdef","email":"abcdef@abcdef.com","mobile":2147483647,"address":"gwyaiivoaoft\r\nasjsjsywww","pincode":345667,"area":"Malang","landmark":null,"city":0,"state":null,"country":null,"dest_city_code":"","created_at":"2017-03-18"}]
         * order_items : [{"id":654,"cart_id":292,"product_id":17,"quantity":"1","shipping_status":"Confirmed","total_price":"180000","product_detail":{"id":17,"title":"My new product","description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","rating":"2.5384615384615","brand_id":3,"category_id":13,"is_featured":1,"user_id":1,"short_desc":"hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk","sku":"777777","slug":"my-new-product-17","status":"Enable","is_admin_allowed":1,"create_at":"2016-04-04 17:16:27","update_at":"2016-04-04 18:00:17","stock_availability":"AVAILABLE","pre_order":0,"pre_order_duration":"","delivery_time":"","comment":null,"is_deleted":0,"hits_count":88,"product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/mediumQQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg"}},{"id":691,"cart_id":292,"product_id":28,"quantity":"3","shipping_status":"Confirmed","total_price":"3000","product_detail":{"id":28,"title":"test product5","description":"test product5","rating":null,"brand_id":1,"category_id":13,"is_featured":0,"user_id":114,"short_desc":"sdadas","sku":"we355","slug":"test-product5-28","status":"","is_admin_allowed":1,"create_at":"2016-05-03 18:22:33","update_at":null,"stock_availability":"","pre_order":0,"pre_order_duration":"","delivery_time":"","comment":null,"is_deleted":0,"hits_count":3,"product_image":""}}]
         */

        private int id;
        private int user_id;
        private String user;
        private String status;
        private String total_amount;
        private int tax;
        private String shipping_cost;
        private int grand_total;
        private String created_at;
        private List<ShippingDetailBean> shipping_detail;
        private List<OrderItemsBean> order_items;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public int getTax() {
            return tax;
        }

        public void setTax(int tax) {
            this.tax = tax;
        }

        public String getShipping_cost() {
            return shipping_cost;
        }

        public void setShipping_cost(String shipping_cost) {
            this.shipping_cost = shipping_cost;
        }

        public int getGrand_total() {
            return grand_total;
        }

        public void setGrand_total(int grand_total) {
            this.grand_total = grand_total;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public List<ShippingDetailBean> getShipping_detail() {
            return shipping_detail;
        }

        public void setShipping_detail(List<ShippingDetailBean> shipping_detail) {
            this.shipping_detail = shipping_detail;
        }

        public List<OrderItemsBean> getOrder_items() {
            return order_items;
        }

        public void setOrder_items(List<OrderItemsBean> order_items) {
            this.order_items = order_items;
        }

        public static class ShippingDetailBean {
            /**
             * id : 84
             * user_id : null
             * order_id : 158
             * firstname : Abcdef
             * lastname : abcdef
             * email : abcdef@abcdef.com
             * mobile : 2147483647
             * address : gwyaiivoaoft
             * asjsjsywww
             * pincode : 345667
             * area : Malang
             * landmark : null
             * city : 0
             * state : null
             * country : null
             * dest_city_code :
             * created_at : 2017-03-18
             */

            private int id;
            private Object user_id;
            private int order_id;
            private String firstname;
            private String lastname;
            private String email;
            private int mobile;
            private String address;
            private int pincode;
            private String area;
            private Object landmark;
            private int city;
            private Object state;
            private Object country;
            private String dest_city_code;
            private String created_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public Object getUser_id() {
                return user_id;
            }

            public void setUser_id(Object user_id) {
                this.user_id = user_id;
            }

            public int getOrder_id() {
                return order_id;
            }

            public void setOrder_id(int order_id) {
                this.order_id = order_id;
            }

            public String getFirstname() {
                return firstname;
            }

            public void setFirstname(String firstname) {
                this.firstname = firstname;
            }

            public String getLastname() {
                return lastname;
            }

            public void setLastname(String lastname) {
                this.lastname = lastname;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getMobile() {
                return mobile;
            }

            public void setMobile(int mobile) {
                this.mobile = mobile;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public int getPincode() {
                return pincode;
            }

            public void setPincode(int pincode) {
                this.pincode = pincode;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public Object getLandmark() {
                return landmark;
            }

            public void setLandmark(Object landmark) {
                this.landmark = landmark;
            }

            public int getCity() {
                return city;
            }

            public void setCity(int city) {
                this.city = city;
            }

            public Object getState() {
                return state;
            }

            public void setState(Object state) {
                this.state = state;
            }

            public Object getCountry() {
                return country;
            }

            public void setCountry(Object country) {
                this.country = country;
            }

            public String getDest_city_code() {
                return dest_city_code;
            }

            public void setDest_city_code(String dest_city_code) {
                this.dest_city_code = dest_city_code;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
        }

        public static class OrderItemsBean {
            /**
             * id : 654
             * cart_id : 292
             * product_id : 17
             * quantity : 1
             * shipping_status : Confirmed
             * total_price : 180000
             * product_detail : {"id":17,"title":"My new product","description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","rating":"2.5384615384615","brand_id":3,"category_id":13,"is_featured":1,"user_id":1,"short_desc":"hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk","sku":"777777","slug":"my-new-product-17","status":"Enable","is_admin_allowed":1,"create_at":"2016-04-04 17:16:27","update_at":"2016-04-04 18:00:17","stock_availability":"AVAILABLE","pre_order":0,"pre_order_duration":"","delivery_time":"","comment":null,"is_deleted":0,"hits_count":88,"product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/mediumQQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg"}
             */

            private int id;
            private int cart_id;
            private int product_id;
            private String quantity;
            private String shipping_status;
            private String total_price;
            private ProductDetailBean product_detail;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCart_id() {
                return cart_id;
            }

            public void setCart_id(int cart_id) {
                this.cart_id = cart_id;
            }

            public int getProduct_id() {
                return product_id;
            }

            public void setProduct_id(int product_id) {
                this.product_id = product_id;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getShipping_status() {
                return shipping_status;
            }

            public void setShipping_status(String shipping_status) {
                this.shipping_status = shipping_status;
            }

            public String getTotal_price() {
                return total_price;
            }

            public void setTotal_price(String total_price) {
                this.total_price = total_price;
            }

            public ProductDetailBean getProduct_detail() {
                return product_detail;
            }

            public void setProduct_detail(ProductDetailBean product_detail) {
                this.product_detail = product_detail;
            }

            public static class ProductDetailBean {
                /**
                 * id : 17
                 * title : My new product
                 * description : this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg
                 * rating : 2.5384615384615
                 * brand_id : 3
                 * category_id : 13
                 * is_featured : 1
                 * user_id : 1
                 * short_desc : hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk
                 * sku : 777777
                 * slug : my-new-product-17
                 * status : Enable
                 * is_admin_allowed : 1
                 * create_at : 2016-04-04 17:16:27
                 * update_at : 2016-04-04 18:00:17
                 * stock_availability : AVAILABLE
                 * pre_order : 0
                 * pre_order_duration :
                 * delivery_time :
                 * comment : null
                 * is_deleted : 0
                 * hits_count : 88
                 * product_image : http://dev.miracleglobal.com/kartku-php/web/uploads/product/mediumQQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg
                 */

                private int id;
                private String title;
                private String description;
                private String rating;
                private int brand_id;
                private int category_id;
                private int is_featured;
                private int user_id;
                private String short_desc;
                private String sku;
                private String slug;
                private String status;
                private int is_admin_allowed;
                private String create_at;
                private String update_at;
                private String stock_availability;
                private int pre_order;
                private String pre_order_duration;
                private String delivery_time;
                private Object comment;
                private int is_deleted;
                private int hits_count;
                private String product_image;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getRating() {
                    return rating;
                }

                public void setRating(String rating) {
                    this.rating = rating;
                }

                public int getBrand_id() {
                    return brand_id;
                }

                public void setBrand_id(int brand_id) {
                    this.brand_id = brand_id;
                }

                public int getCategory_id() {
                    return category_id;
                }

                public void setCategory_id(int category_id) {
                    this.category_id = category_id;
                }

                public int getIs_featured() {
                    return is_featured;
                }

                public void setIs_featured(int is_featured) {
                    this.is_featured = is_featured;
                }

                public int getUser_id() {
                    return user_id;
                }

                public void setUser_id(int user_id) {
                    this.user_id = user_id;
                }

                public String getShort_desc() {
                    return short_desc;
                }

                public void setShort_desc(String short_desc) {
                    this.short_desc = short_desc;
                }

                public String getSku() {
                    return sku;
                }

                public void setSku(String sku) {
                    this.sku = sku;
                }

                public String getSlug() {
                    return slug;
                }

                public void setSlug(String slug) {
                    this.slug = slug;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public int getIs_admin_allowed() {
                    return is_admin_allowed;
                }

                public void setIs_admin_allowed(int is_admin_allowed) {
                    this.is_admin_allowed = is_admin_allowed;
                }

                public String getCreate_at() {
                    return create_at;
                }

                public void setCreate_at(String create_at) {
                    this.create_at = create_at;
                }

                public String getUpdate_at() {
                    return update_at;
                }

                public void setUpdate_at(String update_at) {
                    this.update_at = update_at;
                }

                public String getStock_availability() {
                    return stock_availability;
                }

                public void setStock_availability(String stock_availability) {
                    this.stock_availability = stock_availability;
                }

                public int getPre_order() {
                    return pre_order;
                }

                public void setPre_order(int pre_order) {
                    this.pre_order = pre_order;
                }

                public String getPre_order_duration() {
                    return pre_order_duration;
                }

                public void setPre_order_duration(String pre_order_duration) {
                    this.pre_order_duration = pre_order_duration;
                }

                public String getDelivery_time() {
                    return delivery_time;
                }

                public void setDelivery_time(String delivery_time) {
                    this.delivery_time = delivery_time;
                }

                public Object getComment() {
                    return comment;
                }

                public void setComment(Object comment) {
                    this.comment = comment;
                }

                public int getIs_deleted() {
                    return is_deleted;
                }

                public void setIs_deleted(int is_deleted) {
                    this.is_deleted = is_deleted;
                }

                public int getHits_count() {
                    return hits_count;
                }

                public void setHits_count(int hits_count) {
                    this.hits_count = hits_count;
                }

                public String getProduct_image() {
                    return product_image;
                }

                public void setProduct_image(String product_image) {
                    this.product_image = product_image;
                }
            }
        }
    }
}
