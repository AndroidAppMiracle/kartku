package kartku.com.registeration_module;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import kartku.com.MainCategories.manager.MainCategoriesManager;
import kartku.com.R;
import kartku.com.activity.Splash;
import kartku.com.activity.TermsAndConditions;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.login_module.Login;
import kartku.com.registeration_module.listener.RegisterListener;
import kartku.com.registeration_module.manager.RegisterManager;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.RegisterModal;
import kartku.com.utils.Constant;
import kartku.com.utils.NetworkConnection;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

public class Register extends AppCompatActivity implements View.OnClickListener, RegisterListener, ApiServerResponse {

    EditText first_name, last_name, mobile_number, email, password, confirm_password;
    Button sign_up;
    TextView tv_terms_conditions;
    LinearLayout signin_wrapper;
    TextView terms_wrapper;
    CheckBox cb_terms_and_conditions;
    ProgressBar progress_bar;
    private SharedPreferences prefrence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_register);

        /**
         * hide action bar
         */
        try {
            getSupportActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
        prefrence = new ObscuredSharedPreferences(getApplicationContext(), getApplicationContext().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));


        // find id's
        first_name = (EditText) findViewById(R.id.firstName);
        last_name = (EditText) findViewById(R.id.lastName);
        mobile_number = (EditText) findViewById(R.id.mobileNo);
        email = (EditText) findViewById(R.id.Email);
        password = (EditText) findViewById(R.id.Password);
        confirm_password = (EditText) findViewById(R.id.ConfirmPassword);
        sign_up = (Button) findViewById(R.id.btn_register);
        signin_wrapper = (LinearLayout) findViewById(R.id.signin_wrapper);
        terms_wrapper = (TextView) findViewById(R.id.terms_accepted_box);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        cb_terms_and_conditions = (CheckBox) findViewById(R.id.cb_terms_and_conditions);
        tv_terms_conditions = (TextView) findViewById(R.id.tv_terms_conditions);


        terms_wrapper.setOnClickListener(this);
        sign_up.setOnClickListener(this);
        signin_wrapper.setOnClickListener(this);
        tv_terms_conditions.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                try {
                    if (NetworkConnection.checkInternetConnection(Register.this)) {


                        if (validateFields(first_name.getText().toString(), last_name.getText().toString(), mobile_number.getText().toString(),
                                email.getText().toString(), password.getText().toString(), confirm_password.getText().toString())) {
                            progress_bar.setVisibility(View.VISIBLE);
                            ServerAPI.getInstance().registerUser(ApiServerResponse.USER_REGISTER, first_name.getText().toString(),
                                    last_name.getText().toString(), mobile_number.getText().toString(), email.getText().toString(), password.getText().toString(), Session.getSessionToken(prefrence), this);
                            /*new RegisterManager(Register.this, this).sendRequest(first_name.getText().toString(),
                                    last_name.getText().toString(), mobile_number.getText().toString(), email.getText().toString(), password.getText().toString(), confirm_password.getText().toString());*/

                        }

                    } else {
                        ToastMsg.showShortToast(Register.this, "Not connected to internet.");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.signin_wrapper:
                try {
                    Intent intent = new Intent(Register.this, Login.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.terms_accepted_box:
                break;

            case R.id.tv_terms_conditions:

                try {
                    Intent intent = new Intent(Register.this, TermsAndConditions.class);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

  /*  @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
            Intent intent = new Intent(Register.this, Home_dashboard.class);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    private boolean validateFields(String first_name, String last_name, String mobile_no, String email, String password, String confirm_password) {
        if (first_name.length() <= 0 || first_name.length() <= 2) {
            ToastMsg.showShortToast(Register.this, getResources().getString(R.string.first_name));
            return false;
        } else if (last_name.length() <= 0 || first_name.length() <= 2) {

            ToastMsg.showShortToast(Register.this, getResources().getString(R.string.last_name));
            return false;
        } else if (mobile_no.length() <= 0 || mobile_no.length() < 10) {

            ToastMsg.showShortToast(Register.this, getResources().getString(R.string.mobile_no));
            return false;
        } else if (email.length() <= 0) {
            ToastMsg.showShortToast(Register.this, getResources().getString(R.string.user_email));
            return false;
        } else if (email.length() > 0 && !Validation.validateEmail(email)) {
            ToastMsg.showShortToast(Register.this, getResources().getString(R.string.email_validation));
            return false;
        } else if (password.length() <= 0 || password.length() <= 4) {
            ToastMsg.showShortToast(Register.this, getResources().getString(R.string.password));
            return false;
        } else if (confirm_password.length() <= 0 || confirm_password.length() <= 4) {

            ToastMsg.showShortToast(Register.this, getResources().getString(R.string.confirm_password));
            return false;
        } else if (!password.equalsIgnoreCase(confirm_password)) {
            ToastMsg.showShortToast(Register.this, getResources().getString(R.string.password_validation));
            return false;
        } else if (!cb_terms_and_conditions.isChecked()) {
            ToastMsg.showShortToast(Register.this, getString(R.string.title_accept_terms_conditions));
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void onRegSuccess(String response) {
        try {
            progress_bar.setVisibility(View.GONE);
            ToastMsg.showShortToast(Register.this, "Registration Successful");
            Intent intent = new Intent(Register.this, Login.class);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRegError(String error) {
        try {
            progress_bar.setVisibility(View.GONE);
            ToastMsg.showShortToast(Register.this, error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            RegisterModal registerModal;

            switch (tag) {

                case ApiServerResponse.USER_REGISTER:

                    registerModal = (RegisterModal) response.body();
                    if (registerModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                        progress_bar.setVisibility(View.GONE);
                        //ToastMsg.showShortToast(Register.this, getString(R.string.title_registration_successful));

                        verificationEmailDialog();
                        /*Intent intent = new Intent(Register.this, Login.class);
                        startActivity(intent);
                        finish();*/
                    } else {
                        progress_bar.setVisibility(View.GONE);
                        ToastMsg.showShortToast(Register.this, registerModal.getMessage());
                    }

                    break;


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            progress_bar.setVisibility(View.GONE);
            ToastMsg.showShortToast(Register.this, throwable.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void verificationEmailDialog() {
        new AlertDialog.Builder(Register.this)

                .setTitle(getString(R.string.title_registration_successful))
                .setMessage(R.string.title_verification_mail)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Register.this, Login.class);
            /*Intent intent = new Intent(getActivity(), Home_dashboard.class);*/
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }).setIcon(R.mipmap.ic_launcher)
                .show();
    }
}
