package kartku.com.registeration_module.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.login_module.api.LoginAPI;
import kartku.com.login_module.listener.LoginListener;
import kartku.com.registeration_module.api.RegisterApi;
import kartku.com.registeration_module.listener.RegisterListener;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public class RegisterManager {

    private Context context;
    private RegisterListener listener;
    private SharedPreferences pref;

    public RegisterManager(Context context , RegisterListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String first_name, String last_name, String mobile_no , String email, String password, String confirm_password)
    {
        if(validateFields(first_name, last_name,  mobile_no , email,  password, confirm_password)) {
            try {
                Map<String, String> params = new HashMap<>();
                try {
                    try {
                        params.put(RequestConstants.FIRST_NAME, ""+first_name);
                        params.put(RequestConstants.LAST_NAME, ""+last_name);
                        params.put(RequestConstants.MOBILE, ""+mobile_no);
                        params.put(RequestConstants.EMAIL, ""+email);
                        params.put(RequestConstants.PASSWORD, ""+password);
                        params.put(RequestConstants.DEVICE_ID,"");
                        params.put(RequestConstants.DEVICE_TYPE,"");
                    }catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new RegisterApi(RegisterManager.this, context).sendRequest(2, params);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private boolean validateFields(String first_name, String last_name, String mobile_no , String email, String password, String confirm_password)
    {
        if(first_name.length()<=0)
        {
            listener.onRegError(context.getResources().getString(R.string.first_name));
            return false;
        }
        else if(last_name.length()<=0)
        {
            listener.onRegError(context.getResources().getString(R.string.last_name));
            return false;
        }
       else if(mobile_no.length()<=0)
        {
            listener.onRegError(context.getResources().getString(R.string.mobile_no));
            return false;
        }
        else if(email.length()<=0)
        {
            listener.onRegError(context.getResources().getString(R.string.email));
            return false;
        }
        else if(email.length()>0 && !Validation.validateEmail(email))
        {
            listener.onRegError(context.getResources().getString(R.string.email_validation));
            return false;
        }else if(password.length()<=0)
        {
            listener.onRegError(context.getResources().getString(R.string.password));
            return false;
        }
        else if(confirm_password.length()<=0)
        {
            listener.onRegError(context.getResources().getString(R.string.confirm_password));
            return false;
        }
        else if(!password.equalsIgnoreCase(confirm_password))
        {
            listener.onRegError(context.getResources().getString(R.string.password_validation));
            return false;
        }
        else
        {
            return true;
        }

    }

    public void onSuccess(String response)
    {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                String message = res_object.getString(Constant.MESSAGE);
                listener.onRegSuccess(message);
            }else{
                String message = res_object.getString(Constant.MESSAGE);
                listener.onRegError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void onError(String message)
    {

        listener.onRegError(message);
    }


}
