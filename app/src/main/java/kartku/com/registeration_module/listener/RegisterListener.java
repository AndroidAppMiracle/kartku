package kartku.com.registeration_module.listener;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public interface RegisterListener {

    void onRegSuccess(String response);
    void onRegError(String error);
}
