package kartku.com.place_order_module;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;


import java.util.ArrayList;

import kartku.com.R;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;

/**
 * Created by Kshitiz Bali on 1/21/2017.
 */

public class PlaceOrder extends Utility {

    String mAuthorization = "12";
    String BASE_URL = "http://dev.miracleglobal.com/kartku-php/web";

    TextView tv_grand_total;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details_mini);


      /*  tv_grand_total = (TextView) findViewById(R.id.tv_grand_total);

        SdkCoreFlowBuilder.init(PlaceOrder.this, Constant.CLIENT_KEY, BASE_URL)
                .enableLog(true)
                .buildSDK();
       *//* SdkUIFlowBuilder.init(this, Constant.CLIENT_KEY, BASE_URL, new TransactionFinishedCallback() {
            @Override
            public void onTransactionFinished(TransactionResult result) {
                // Handle finished transaction here.
            }
        })
                //.setExternalScanner(new ScanCard()) // initialization for using external scancard
                .enableLog(true)
               *//**//* .setDefaultText("open_sans_regular.ttf") // optional to set custom font
                .setSemiBoldText("open_sans_semibold.ttf") // optional to set custom font
                .setBoldText("open_sans_bold.ttf") // optional to set custom font*//**//*
                .buildSDK();*//*


        SdkUIFlowBuilder.init(this, Constant.CLIENT_KEY, BASE_URL, new TransactionFinishedCallback() {
            @Override
            public void onTransactionFinished(TransactionResult result) {
                // Handle finished transaction here.
                ToastMsg.showLongToast(PlaceOrder.this, result.toString());
            }
        });

        final TransactionRequest transactionRequest = new TransactionRequest("TRANSACTION_ID", 12312312);

        CustomerDetails customer = new CustomerDetails("FIRST_NAME", "LAST_NAME", "EMAIL", "PHONE_NUMBER");

        transactionRequest.setCustomerDetails(customer);

        ItemDetails itemDetails1 = new ItemDetails("ITEM_ID_1", 123123, 2, "ITEM_NAME_1");
        ItemDetails itemDetails2 = new ItemDetails("ITEM_ID_2", 2323, 1, "ITEM_NAME_2");
        ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();
        itemDetailsList.add(itemDetails1);
        itemDetailsList.add(itemDetails2);

// Set item details into the transaction request.
        transactionRequest.setItemDetails(itemDetailsList);


        BillingAddress billingAddress1 = new BillingAddress("FIRST_NAME_1", "LAST_NAME_1", "ADDRESS_1", "CITY_1", "POSTAL_CODE_1", "PHONE_NUMBER_1", "COUNTRY_CODE_1");

        BillingAddress billingAddress2 = new BillingAddress("FIRST_NAME_2", "LAST_NAME_2", "ADDRESS_2", "CITY_2", "POSTAL_CODE_2", "PHONE_NUMBER_2", "COUNTRY_CODE_2");


        // Create array list and add above billing details in it and then set it to transaction request.
        ArrayList<BillingAddress> billingAddressList = new ArrayList<>();
        billingAddressList.add(billingAddress1);
        billingAddressList.add(billingAddress2);

// Set billing address list into transaction request
        transactionRequest.setBillingAddressArrayList(billingAddressList);

        *//*ShippingAddress shippingAddress1 = new ShippingAddress("FIRST_NAME_1", "LAST_NAME_1", "ADDRESS_1", "CITY_1", "POSTAL_CODE_1", "PHONE_NUMBER_1", "COUNTRY_CODE_1");

        ShippingAddress shippingAddress2 = new ShippingAddress("FIRST_NAME_2", "LAST_NAME_2", "ADDRESS_2", "CITY_2", "POSTAL_CODE_2", "PHONE_NUMBER_2", "COUNTRY_CODE_2");

// Create array list and add above shipping details in it and then set it to transaction request.
        ArrayList<BillingAddress> shippingAddressList = new ArrayList<>();
        shippingAddressList.add(shippingAddress1);
        shippingAddressList.add(shippingAddress1);

// Set shipping address list into transaction request.
        transactionRequest.setShippingAddressArrayList(shippingAddressList);*//*


        BillInfoModel billInfoModel = new BillInfoModel("BILL_INFO_KEY", "BILL_INFO_VALUE");
// Set the bill info on transaction details
        transactionRequest.setBillInfoModel(billInfoModel);


        tv_grand_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MidtransSDK.getInstance().setTransactionRequest(transactionRequest);
            }
        });

        MidtransSDK.getInstance().checkout(new CheckoutCallback() {
            @Override
            public void onSuccess(Token token) {
                try {
                    ToastMsg.showLongToast(PlaceOrder.this, "" + token.getTokenId());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Token token, String s) {
                try {
                    ToastMsg.showLongToast(PlaceOrder.this, "" + s);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable throwable) {
                try {
                    //ToastMsg.showLongToast(PlaceOrder.this, "" + throwable.printStackTrace(););
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        MidtransSDK.getInstance().getTransactionOptions("CheckOut_Token", new TransactionOptionsCallback() {
            @Override
            public void onSuccess(Transaction transaction) {
                *//*List<String> enabledMethods = transaction.getTransactionData().getEnabledPayments();*//*
            }

            @Override
            public void onFailure(Transaction transaction, String s) {

            }

            @Override
            public void onError(Throwable throwable) {

            }
        });

    }*/
    }
}
