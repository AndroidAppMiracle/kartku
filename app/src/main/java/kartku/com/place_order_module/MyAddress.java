package kartku.com.place_order_module;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import kartku.com.R;
import kartku.com.utils.Utility;

/**
 * Created by Kshitiz Bali on 1/21/2017.
 */

public class MyAddress extends Utility {

    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_addresses);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setTitle("Delivery Address");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_add:
                try {
                    /*Intent intent = new Intent(ProductDetailNew.this, ProductList.class);
                    startActivity(intent);
                    finish();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return super.onOptionsItemSelected(item);

    }


    private void extraDetailsDialog() {
        final Dialog dialog = new Dialog(MyAddress.this);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.add_address);
        dialog.setContentView(R.layout.custom_dialog_address_list);

        TextInputEditText et_first_name = (TextInputEditText) dialog.findViewById(R.id.et_first_name);
        TextInputEditText et_last_name = (TextInputEditText) dialog.findViewById(R.id.et_last_name);
        TextInputEditText et_address = (TextInputEditText) dialog.findViewById(R.id.et_address);
        TextInputEditText et_area = (TextInputEditText) dialog.findViewById(R.id.et_area);
        TextInputEditText et_landmark = (TextInputEditText) dialog.findViewById(R.id.et_landmark);
        TextInputEditText et_country = (TextInputEditText) dialog.findViewById(R.id.et_country);
        TextView text_done = (TextView) dialog.findViewById(R.id.text_done);
        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        text_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        dialog.show();
    }
}
