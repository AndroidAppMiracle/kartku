package kartku.com.wishlist.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface AddToWishlistListener {

    void onAddwishlistSuccess(String response);
    void onAddwishlistError(String error);
}
