package kartku.com.wishlist.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface WishlistListener {

    void onwishlistSuccess(String response);
    void onwishlistError(String error);
}
