package kartku.com.wishlist.listener;

/**
 * Created by satoti.garg on 9/20/2016.
 */
public interface RemoveFromWishlistListener {
    void removewishlistOnSuccess(String response);
    void removewishlistOnError(String error);
}
