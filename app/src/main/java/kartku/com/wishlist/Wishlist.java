package kartku.com.wishlist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kartku.com.MainCategories.CategoriesActivity;
import kartku.com.R;
import kartku.com.adapter.ProductListAdapter;
import kartku.com.cart.Cart;
import kartku.com.cart.CartDetailsNew;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.login_module.Login;
import kartku.com.product_category.GridSpacingItemDecoration;
import kartku.com.product_category.model.ProductListModel;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.adapter.WishlistAdapter;
import kartku.com.wishlist.listener.WishlistListener;
import kartku.com.wishlist.manager.WishlistManager;
import kartku.com.wishlist.model.WishlistBean;

public class Wishlist extends AppCompatActivity implements WishlistListener {

    RecyclerView wishlist_view;
    String user_id = "";
    SharedPreferences pref;
    ArrayList<WishlistBean> arrayList = new ArrayList<>();
    TextView tv_empty_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);

       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

    /*    try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            wishlist_view = (RecyclerView) findViewById(R.id.wishlist_view);
            tv_empty_text = (TextView) findViewById(R.id.tv_empty_text);
            pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
            user_id = Session.get_userId(pref);
            new WishlistManager(getApplicationContext(), this).sendRequest(user_id, "");
           /* toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Wishlist.this, Home_dashboard.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onwishlistError(String error) {
        ToastMsg.showShortToast(Wishlist.this, error);
    }

    @Override
    public void onwishlistSuccess(String response) {

        Log.d("", "onwishlistSuccess res " + response);

        try {
            JSONArray products_list = new JSONArray(response);
            for (int i = 0; i < products_list.length(); i++) {
                JSONObject jsonObject = products_list.getJSONObject(i);
                WishlistBean list_model = new WishlistBean();
                list_model.setId(jsonObject.getString(Constant.ID));
                list_model.setName(jsonObject.getString(Constant.NAME));
                list_model.setPrice(jsonObject.getString(Constant.PRICE_KEY));
                list_model.setDescription(jsonObject.getString(Constant.DESCRIPTION));
                list_model.setImage(jsonObject.getString(Constant.IMAGE));
                arrayList.add(list_model);
            }
            if (arrayList.size() == 0) {
                wishlist_view.setVisibility(View.GONE);
                tv_empty_text.setVisibility(View.VISIBLE);
            } else {
                wishlist_view.setVisibility(View.VISIBLE);
                tv_empty_text.setVisibility(View.GONE);
            }
            WishlistAdapter adapter = new WishlistAdapter(getApplicationContext(), arrayList, tv_empty_text,tv_empty_text);
            wishlist_view.setAdapter(adapter);
            wishlist_view.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                   /* Intent intent = new Intent(Wishlist.this, Home_dashboard.class);
                    startActivity(intent);
                    finish();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.miCompose:
                Intent in = new Intent(this, CartDetailsNew.class);
                /*Intent in = new Intent(this, Cart.class);*/
                startActivity(in);
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tool_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Wishlist.this, Home_dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


}
