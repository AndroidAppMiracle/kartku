package kartku.com.wishlist.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.api.AddToWishlistApi;
import kartku.com.wishlist.api.RemoveFromWishListApi;
import kartku.com.wishlist.listener.AddToWishlistListener;
import kartku.com.wishlist.listener.RemoveFromWishlistListener;

/**
 * Created by satoti.garg on 9/20/2016.
 */
public class RemoveFromWishlistManager {

    private Context context;
    private RemoveFromWishlistListener listener;
    SharedPreferences pref;

    public RemoveFromWishlistManager(Context context, RemoveFromWishlistListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    public void sendRequest(String user_id, String product_id) {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.USER_ID, "" + user_id);
                    params.put(RequestConstants.PRODUCT_ID, "" + product_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new RemoveFromWishListApi(context, RemoveFromWishlistManager.this).sendRequest(API.REMOVE_FROM_WISHLIST_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                String message = res_object.getString(Constant.INFO);
                listener.removewishlistOnSuccess("" + message);

            } else {
                String message = res_object.getString(Constant.INFO);
                listener.removewishlistOnError(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void onError(String message) {
        listener.removewishlistOnError(message);
    }
}
