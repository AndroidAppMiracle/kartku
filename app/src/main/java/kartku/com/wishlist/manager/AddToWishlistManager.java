package kartku.com.wishlist.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.orders_module.api.OrderDetailApi;
import kartku.com.orders_module.listener.OrderDetailListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.api.AddToWishlistApi;
import kartku.com.wishlist.listener.AddToWishlistListener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class AddToWishlistManager {

    private Context context;
    private AddToWishlistListener listener;
    SharedPreferences pref;

    public AddToWishlistManager(Context context , AddToWishlistListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String user_id, String product_id)
    {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.USER_ID, ""+user_id);
                    params.put(RequestConstants.PRODUCT_ID, ""+product_id);
                    System.out.println("wishlist params *******  " + user_id  + "  product_id    " + product_id);
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new AddToWishlistApi(context,AddToWishlistManager.this).sendRequest(API.ADD_TO_WISHLIST_REQUEST_ID, params);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response)
    {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS1);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                String message = res_object.getString(Constant.MESSAGE);
                listener.onAddwishlistSuccess(""+message);

            }else{
                String message = res_object.getString(Constant.MESSAGE);
                listener.onAddwishlistError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void onError(String message)
    {
        listener.onAddwishlistError(message);
    }
}
