package kartku.com.wishlist.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.api.WishListApi;
import kartku.com.wishlist.listener.WishlistListener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class WishlistManager {

    private Context context;
    private WishlistListener listener;
    SharedPreferences pref;

    public WishlistManager(Context context , WishlistListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String user_id, String product_id)
    {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.USER_ID, ""+user_id);
                    params.put(RequestConstants.PRODUCT_ID, ""+product_id);
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
          new WishListApi(context , WishlistManager.this).sendRequest(API.WISHLIST_REQUEST_ID,params);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response)
    {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                try {
                    JSONArray product_info = (res_object.getJSONArray(Constant.PRODUCT_INFO));
                    listener.onwishlistSuccess("" + product_info);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }else{
                try {
                    String message = res_object.getString(Constant.PRODUCT_INFO);
                    listener.onwishlistError(message);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void onError(String message)
    {
        listener.onwishlistError(message);
    }
}
