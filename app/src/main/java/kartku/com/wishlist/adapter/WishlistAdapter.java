package kartku.com.wishlist.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.cart.listener.AddToCartListener;
import kartku.com.cart.manager.AddToCartManager;
import kartku.com.product_category.model.ProductListModel;
import kartku.com.product_detail.ProductDetail;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.listener.AddToWishlistListener;
import kartku.com.wishlist.listener.RemoveFromWishlistListener;
import kartku.com.wishlist.manager.AddToWishlistManager;
import kartku.com.wishlist.manager.RemoveFromWishlistManager;
import kartku.com.wishlist.model.WishlistBean;
import retrofit2.Response;

/**
 * Created by satoti.garg on 9/20/2016.
 */
public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder> implements AddToCartListener, AddToWishlistListener, RemoveFromWishlistListener, ApiServerResponse {
    ArrayList<WishlistBean> arrayList = new ArrayList<>();
    Context context;
    private AQuery aquery;
    SharedPreferences pref;
    String product_id = " ", selected_productId = "";
    String user_id, cart_id, amount;
    int pos;
    TextView textViewMessage;
    String qty = "1";
    TextView mCartItems;

    public WishlistAdapter(Context context, ArrayList<WishlistBean> items, TextView textView, TextView cartItems) {
        this.context = context;
        this.mCartItems = cartItems;
        this.arrayList = items;
        this.textViewMessage = textView;
        aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wishlist_row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.d("", "banners list size onBindViewHolder *********" + arrayList);
        holder.text_name.setText(arrayList.get(position).getName());
        holder.text_price.setText("Price " + arrayList.get(position).getPrice());
        amount = arrayList.get(position).getPrice();
        product_id = "" + arrayList.get(position).getId();
        aquery.id(holder.image_product).image(arrayList.get(position).getImage(), true, true, 0, 0, null, AQuery.FADE_IN);
        Log.d("", "banners list size ViewHolder image_product *********" + arrayList.get(position).getImage());

        holder.fvt_like.setImageResource(R.mipmap.fvlike);
        holder.fvt_like.setTag(R.mipmap.fvlike);
        holder.btn_add_tocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("", "add to cart clicked ******   ");
                ((Utility) context).showLoading();
                selected_productId = "" + arrayList.get(position).getId();
                amount = arrayList.get(position).getPrice();

                if (!Session.getcart_id(pref).equalsIgnoreCase("")) {
                    addCart(user_id, amount, selected_productId.trim(), qty, cart_id);
                } else {
                    addCart(user_id, amount, selected_productId.trim(), qty, "0");
                }

                removeWishlist(selected_productId);
                refresh(holder.getAdapterPosition());

            }
        });

        holder.fvt_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_productId = "" + arrayList.get(position).getId();
                Session.set_selected_product_id(pref, selected_productId);
                Log.d("", "mark_wishlist clicked ******   " + selected_productId);
                if (holder.fvt_like.getTag().equals(R.mipmap.like)) {
                    if (Session.get_login_statuc(pref)) {
                        holder.fvt_like.setImageResource(R.mipmap.fvlike);
                        holder.fvt_like.setTag(R.mipmap.fvlike);
                        addWishlist(selected_productId);
                    } else {
                        ToastMsg.showShortToast(context, context.getResources().getString(R.string.login_text));
                    }
                } else {
                    if (Session.get_login_statuc(pref)) {
                        holder.fvt_like.setImageResource(R.mipmap.like);
                        holder.fvt_like.setTag(R.mipmap.like);
                        removeWishlist(selected_productId);
                        pos = position;
                        refresh(holder.getAdapterPosition());

                    } else {
                        ToastMsg.showShortToast(context, context.getResources().getString(R.string.login_text));
                    }
                }

            }

        });

       /* holder.image_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                System.out.println("  NEW***** " + arrayList.get(position).getId());
                System.out.println("  ***** " + product_id);
                Intent intent = new Intent(context, ProductDetail.class);
                intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, "" + arrayList.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        */

        holder.ll_wishlist_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProductDetailNew.class);
                i.putExtra(Constant.SELECTED_CATEGORY_INDEX, String.valueOf(arrayList.get(holder.getAdapterPosition()).getId()));
                i.putExtra(Constant.WISHLIST, arrayList.get(holder.getAdapterPosition()).getWishlist());
                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                context.startActivity(i);
            }
        });

    }


    private void addWishlist(String selected_productId) {
        Log.d("", "addWishlist product_id *****  " + selected_productId);
        System.out.println("add to wishlist product id ****  " + arrayList.get(pos).getId());

        new AddToWishlistManager(context, this).sendRequest(user_id, selected_productId);
    }

    private void removeWishlist(String selected_productId) {
        Log.d("", "aremoveWishlist product_id *****  " + selected_productId);
        System.out.println("add to wishlist product id ****  " + selected_productId);

        new RemoveFromWishlistManager(context, this).sendRequest(user_id, selected_productId);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();

    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            ((Utility) context).hideLoading();
            if (response.isSuccessful()) {
                MyCartDetailModal myCartDetailModal;
                switch (tag) {
                    case ApiServerResponse.MY_CART_DETAILS:
                        myCartDetailModal = (MyCartDetailModal) response.body();
                        if (myCartDetailModal.getItems().isEmpty()) {
                            mCartItems.setText("0");
                            //mMenu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_launcher));
                            //ToastMsg.showLongToast(context, String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(pref, "0");
                        } else {
                            Session.setCartItemsQuantity(pref, String.valueOf(myCartDetailModal.getItems().size()));
                            //ToastMsg.showLongToast(context, String.valueOf(myCartDetailModal.getItems().size()));
                            mCartItems.setText(String.valueOf(myCartDetailModal.getItems().size()));
                            //supportInvalidateOptionsMenu();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            ((Utility) context).hideLoading();
            switch (tag) {
                case ApiServerResponse.MY_CART_DETAILS:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text_name, text_price;
        public ImageView image_product;
        public Button btn_add_tocart;
        public ImageView fvt_like;
        LinearLayout ll_wishlist_item;

        public ViewHolder(final View container) {
            super(container);
            text_name = (TextView) container.findViewById(R.id.title_name);
            text_price = (TextView) container.findViewById(R.id.text_desc);
            image_product = (ImageView) container.findViewById(R.id.product_image);
            btn_add_tocart = (Button) container.findViewById(R.id.btn_add_tocart);
            fvt_like = (ImageView) container.findViewById(R.id.fvt_like);
            ll_wishlist_item = (LinearLayout) container.findViewById(R.id.ll_wishlist_item);
            fvt_like.setTag(R.mipmap.fvlike);
        }
    }

    private void addCart(String user_id, String amount, String product_id, String qty, String cart_id) {


        new AddToCartManager(context, this).sendRequest(user_id, amount, product_id, qty, cart_id);

    }

    @Override
    public void onAddCartError(String error) {
        ToastMsg.showShortToast(context, error);
        Log.d("", "onAddCartError *******  " + error);


        //arrayList.remove(pos);
        //notifyDataSetChanged();
    }

    @Override
    public void onAddCartSuccess(String messgae) {
        Log.d("", "onAddCartSuccess *******  " + messgae);
        try {
            ToastMsg.showShortToast(context, messgae);
            selected_productId = Session.get_selected_product_id(pref);
            Log.i("session sel prod id", "" + selected_productId);
            //removeWishlist(selected_productId);
            //notifyDataSetChanged();
            ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS,Session.get_userId(pref), Session.getcart_id(pref), this);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onAddwishlistSuccess(String response) {
        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(context, response);
    }

    @Override
    public void onAddwishlistError(String error) {
        ToastMsg.showShortToast(context, error);
    }

    @Override
    public void removewishlistOnSuccess(String response) {
        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(context, response);
        refresh(pos);
        //arrayList.remove(pos);
        //notifyDataSetChanged();

        if (arrayList.size() == 0) {
            if (textViewMessage.getVisibility() == View.GONE) {
                textViewMessage.setVisibility(View.VISIBLE);
                textViewMessage.setText("No Product In Wishlist");
            }

        }

    }

    @Override
    public void removewishlistOnError(String error) {
        ToastMsg.showShortToast(context, error);
    }


    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        arrayList.remove(position);
        notifyItemRemoved(position);
    }
}
