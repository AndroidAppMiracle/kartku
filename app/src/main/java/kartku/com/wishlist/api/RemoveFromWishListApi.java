package kartku.com.wishlist.api;

import android.content.Context;

import java.util.Map;

import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;
import kartku.com.wishlist.manager.AddToWishlistManager;
import kartku.com.wishlist.manager.RemoveFromWishlistManager;

/**
 * Created by satoti.garg on 9/20/2016.
 */
public class RemoveFromWishListApi implements CustomResponse {

    private Context context;
    private RemoveFromWishlistManager manager;
    private RequestServer request_server;

    public RemoveFromWishListApi(Context context, RemoveFromWishlistManager manager)
    {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId ,  Map<String, String> params){
        try {
            request_server = new RequestServer(context, requestId, params,this , RequestConstants.POST_URL);
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
