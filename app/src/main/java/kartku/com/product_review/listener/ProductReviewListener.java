package kartku.com.product_review.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface ProductReviewListener {

    void onSuccess(String response);
    void onError(String error);
}
