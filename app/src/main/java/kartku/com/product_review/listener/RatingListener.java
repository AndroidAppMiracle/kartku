package kartku.com.product_review.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface RatingListener {

    void onRatingSuccess(String message);
    void onRatingError(String error);
}
