package kartku.com.product_review.api;

import android.content.Context;

import kartku.com.product_review.manager.ProductReviewManager;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class ProductReviewApi implements CustomResponse {
    private Context context;
    private ProductReviewManager manager;
    private RequestServer request_server;

    public ProductReviewApi(Context context, ProductReviewManager manager) {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void onCustomResponse(String response) {

    }

    @Override
    public void onCustomError(String error) {

    }

    /**
     * request server
     */

    public void sendRequest(int requestId, String params) {
        try {
            request_server = new RequestServer(context, requestId, this, params);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
