package kartku.com.product_review.manager;

import android.content.Context;
import android.content.SharedPreferences;

import kartku.com.cart.api.CartDetailApi;
import kartku.com.cart.listener.CartDetailListener;
import kartku.com.product_detail.api.ProductDetailApi;
import kartku.com.product_review.api.ProductReviewApi;
import kartku.com.product_review.listener.ProductReviewListener;
import kartku.com.utils.API;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class ProductReviewManager {

    private Context context;
    private ProductReviewListener listener;
    SharedPreferences pref;

    public ProductReviewManager(Context context , ProductReviewListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String params)
    {
        try {
            new ProductReviewApi(context , ProductReviewManager.this).sendRequest(API.PRODUCT_REVIEW_REQUEST_ID,params);
            //new CartDetailApi(context,ProductReviewManager.this).sendRequest(API.ADD_TO_CART_REQUEST_ID, params);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response)
    {
        listener.onSuccess(response);
       /* try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                JSONObject order_detail = res_object.getJSONObject(Constant.ORDER_DETAIL);
                listener.onDetailSuccess(""+order_detail);

            }else{
                String message = res_object.getString(Constant.ORDER_DETAIL);
                listener.onDetailError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
*/
    }

    public void onError(String message)
    {
        listener.onError(message);
    }
}
