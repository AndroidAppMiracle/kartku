package kartku.com.product_review.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.cart.api.AddToCartApi;
import kartku.com.cart.listener.AddToCartListener;
import kartku.com.product_review.api.RatingApi;
import kartku.com.product_review.listener.RatingListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class RatingManager {

    private Context context;
    private RatingListener listener;
    SharedPreferences pref;

    public RatingManager(Context context, RatingListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    public void sendRequest(String user_id, float rating, String product_id, String reviews) {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.USER_ID, "" + user_id);
                    params.put(RequestConstants.PRODUCT_ID, "" + product_id);
                    params.put(RequestConstants.RATING, "" + rating);
                    params.put(Constant.REVIEW, "" + reviews);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new RatingApi(context, RatingManager.this).sendRequest(API.PRODUCT_REVIEW_RATING_REQUEST_ID, params);
            // new AddToCartApi(context,RatingManager.this).sendRequest(API.ADD_TO_CART_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response) {

        try {

            Log.e("Response Imp", response);
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);

            if (status.equalsIgnoreCase("OK")) {
                String message = res_object.getString(Constant.MESSAGE);
                listener.onRatingSuccess(message);
            } else {
                String message = res_object.getString(Constant.MESSAGE);
                listener.onRatingError(message);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }



       /* try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                JSONObject order_detail = res_object.getJSONObject(Constant.ORDER_DETAIL);
                listener.onDetailSuccess(""+order_detail);

            }else{
                String message = res_object.getString(Constant.ORDER_DETAIL);
                listener.onDetailError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
*/
    }

    public void onError(String message) {
        listener.onRatingError(message);
    }
}
