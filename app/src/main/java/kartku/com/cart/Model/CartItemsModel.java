package kartku.com.cart.Model;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/10/2017.
 */

public class CartItemsModel {

    /**
     * status : OK
     * cart_items : [{"cart_item_id":"372","cart_id":"181","product_image":"http://kartku.s3.amazonaws.com/product/medium/VzlkF-TQudk-pwmx3e7bFBolLmNmL_Q3.jpg","product_name":"Decorative Mirrors","product_description":"Decorative Mirrors :The beautiful Shaz Living Gold Mild Steel and Glass Leaves Mirror will be pretty addition to your wall. This piece is elegant in appearance and will be the best way to fill up open spaces on the wall. ","product_price":10000,"quantity":"2"},{"cart_item_id":"373","cart_id":"181","product_image":"http://kartku.s3.amazonaws.com/product/medium/XQqm3-W8LYrHh5iQf2B-u1z-gq-jnYbA.jpg","product_name":"Safal quark smartz","product_description":"Safal quark smartz","product_price":3000,"quantity":"3"},{"cart_item_id":"374","cart_id":"181","product_image":"http://kartku.s3.amazonaws.com/product/medium/XQqm3-W8LYrHh5iQf2B-u1z-gq-jnYbA.jpg","product_name":"beds sofas","product_description":"beds sofas","product_price":0,"quantity":"0"},{"cart_item_id":"375","cart_id":"181","product_image":"http://kartku.s3.amazonaws.com/product/medium/-K5q_YEFokoLVDjRN41Y5K_MQoudPniD.jpg","product_name":"variant sofas","product_description":"this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product \r\n423423546547658769","product_price":0,"quantity":"0"}]
     * total_price : 13000
     */

    private String status;
    private int total_price;
    private List<CartItemsBean> cart_items;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public List<CartItemsBean> getCart_items() {
        return cart_items;
    }

    public void setCart_items(List<CartItemsBean> cart_items) {
        this.cart_items = cart_items;
    }

    public static class CartItemsBean {
        /**
         * cart_item_id : 372
         * cart_id : 181
         * product_image : http://kartku.s3.amazonaws.com/product/medium/VzlkF-TQudk-pwmx3e7bFBolLmNmL_Q3.jpg
         * product_name : Decorative Mirrors
         * product_description : Decorative Mirrors :The beautiful Shaz Living Gold Mild Steel and Glass Leaves Mirror will be pretty addition to your wall. This piece is elegant in appearance and will be the best way to fill up open spaces on the wall.
         * product_price : 10000
         * quantity : 2
         */

        private String cart_item_id;
        private String cart_id;
        private String product_image;
        private String product_name;
        private String product_description;
        private int product_price;
        private String quantity;

        public String getCart_item_id() {
            return cart_item_id;
        }

        public void setCart_item_id(String cart_item_id) {
            this.cart_item_id = cart_item_id;
        }

        public String getCart_id() {
            return cart_id;
        }

        public void setCart_id(String cart_id) {
            this.cart_id = cart_id;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getProduct_description() {
            return product_description;
        }

        public void setProduct_description(String product_description) {
            this.product_description = product_description;
        }

        public int getProduct_price() {
            return product_price;
        }

        public void setProduct_price(int product_price) {
            this.product_price = product_price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }
}
