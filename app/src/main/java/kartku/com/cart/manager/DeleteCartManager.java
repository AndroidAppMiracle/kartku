package kartku.com.cart.manager;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

import kartku.com.cart.api.AddToCartApi;
import kartku.com.cart.api.DeleteCartApi;
import kartku.com.cart.listener.AddToCartListener;
import kartku.com.cart.listener.DeleteCartListener;
import kartku.com.utils.API;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class DeleteCartManager {

    private Context context;
    private DeleteCartListener listener;
    SharedPreferences pref;

    public DeleteCartManager(Context context , DeleteCartListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String item_id, String cart_id)    {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                   // params.put(RequestConstants.ITEM_ID, ""+item_id);
                    params.put(RequestConstants.CART_ID, ""+cart_id);
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new DeleteCartApi(context,DeleteCartManager.this).sendRequest(API.CART_DELETE_REQUEST_ID, params);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response)
    {
        listener.onDeleteCartSuccess(response);
        System.out.println("deletecart response "+response);
    }
    public void onError(String message)
    {
        listener.onDeleteCartError(message);
    }

}
