package kartku.com.cart.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.cart.api.AddToCartApi;
import kartku.com.cart.listener.AddToCartListener;
import kartku.com.orders_module.api.OrderDetailApi;
import kartku.com.orders_module.listener.OrderDetailListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class AddToCartManager {

    private Context context;
    private AddToCartListener listener;
    SharedPreferences pref;

    public AddToCartManager(Context context, AddToCartListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    public void sendRequest(String user_id, String total_amount, String product_id, String quantity, String cart_id) {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.USER_ID, "" + user_id);
                    params.put(RequestConstants.TOTAL_AMOUNT, "" + total_amount);
                    params.put(RequestConstants.PRODUCT_ID, "" + product_id);
                    params.put(RequestConstants.QUANTITY, "" + quantity);
                    params.put(RequestConstants.CART_ID, "" + cart_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new AddToCartApi(context, AddToCartManager.this).sendRequest(API.ADD_TO_CART_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response) {
        //  listener.onAddCartSuccess(response);
        //Log.i("Addtocartresponse ", response);
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.RESULT);
            if (status.contains("Product Added to cart")) {
                String message = "";
                try {
                    message = res_object.getString(Constant.RESULT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray item_info = res_object.getJSONArray(Constant.ITEM_INFO);
                    JSONObject item_info_object = item_info.getJSONObject(0);
                    String cart_id = item_info_object.getString(Constant.CART_ID);
                    Session.setcart_id(pref, cart_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                listener.onAddCartSuccess("" + message);
            } else {
                String message = res_object.getString(Constant.RESULT);
                listener.onAddCartError(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onError(String message) {
        listener.onAddCartError(message);
    }
}
