package kartku.com.cart.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.cart.Model.CartModel;
import kartku.com.cart.api.AddToCartApi;
import kartku.com.cart.api.CartDetailApi;
import kartku.com.cart.listener.AddToCartListener;
import kartku.com.cart.listener.CartDetailListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class CartDetailManager {


    private Context context;
    private CartDetailListener listener;
    SharedPreferences pref;

    public CartDetailManager(Context context, CartDetailListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    public void sendRequest(String params) {
        try {
            new CartDetailApi(context, CartDetailManager.this).sendRequest(API.CART_DETAIL_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response) {
        // listener.onCartSuccess(response);
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                // JSONObject order_detail = res_object.getJSONObject(Constant.ORDER_DETAIL);

                // JSONArray items_array = res_object.getJSONArray("products");

                listener.onCartSuccess("" + response);

            } else {
                try {
                    String message = res_object.getString(Constant.MESSAGE);
                    listener.onCartError(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onError(String message) {
        listener.onCartError(message);
    }
}
