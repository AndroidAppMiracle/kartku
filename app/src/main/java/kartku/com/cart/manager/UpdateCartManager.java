package kartku.com.cart.manager;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

import kartku.com.cart.api.UpdateCartAPI;
import kartku.com.cart.listener.DeleteItemListener;
import kartku.com.cart.listener.UpdateCartListener;
import kartku.com.utils.API;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class UpdateCartManager {

    private Context context;
    private UpdateCartListener listener;
    SharedPreferences pref;

    public UpdateCartManager(Context context , UpdateCartListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String cart_id , String user_id, String total_amount , String product_id, String quantity )
    {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.ITEM_ID, ""+cart_id);
                    params.put(RequestConstants.ITEM_ID, ""+user_id);
                    params.put(RequestConstants.ITEM_ID, ""+total_amount);
                    params.put(RequestConstants.ITEM_ID, ""+product_id);
                    params.put(RequestConstants.ITEM_ID, ""+quantity);
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
             new UpdateCartAPI(context , UpdateCartManager.this).sendRequest(API.UPDATE_CART_REQUEST_ID,params);
            //new DeleteItemAPI(context,DeleteItemListener.this).sendRequest(API.ADD_TO_CART_REQUEST_ID, params);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response)
    {
        listener.onSuccess(response);
       /* try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                JSONObject order_detail = res_object.getJSONObject(Constant.ORDER_DETAIL);
                listener.onDetailSuccess(""+order_detail);

            }else{
                String message = res_object.getString(Constant.ORDER_DETAIL);
                listener.onDetailError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
*/}
    public void onError(String message)
    {
        listener.onError(message);
    }
}
