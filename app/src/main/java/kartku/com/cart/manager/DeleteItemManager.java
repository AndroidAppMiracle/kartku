package kartku.com.cart.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

import kartku.com.cart.Cart;
import kartku.com.cart.api.DeleteCartApi;
import kartku.com.cart.api.DeleteItemAPI;
import kartku.com.cart.listener.DeleteCartListener;
import kartku.com.cart.listener.DeleteItemListener;
import kartku.com.utils.API;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class DeleteItemManager {

    private Context context;
    private DeleteItemListener listener;
    SharedPreferences pref;

    public DeleteItemManager(Context context , DeleteItemListener listener)
    {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String item_id )
    {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.ITEM_ID, ""+item_id);
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new DeleteItemAPI(context, DeleteItemManager.this).sendRequest(API.DELETE_ITEM_REQUEST_ID,params);
            //new DeleteItemAPI(context,DeleteItemListener.this).sendRequest(API.ADD_TO_CART_REQUEST_ID, params);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response)
    {
        listener.onDeleteItemSuccess(response);
        System.out.println("deletecart response "+response);

    }
    public void onError(String message)
    {
        listener.onDeleteItemError(message);
    }
}
