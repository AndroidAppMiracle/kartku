package kartku.com.cart.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import kartku.com.cart.api.CartItemsApi;
import kartku.com.cart.listener.CartItemsListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 1/10/2017.
 */

public class CartItemsManager {

    private Context context;
    private CartItemsListener listener;
    SharedPreferences pref;


    public CartItemsManager(Context context, CartItemsListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    public void sendRequest(String params) {
        try {
            new CartItemsApi(context, CartItemsManager.this).sendRequest(API.CART_ITEMS_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSuccess(String response) {
        // listener.onCartSuccess(response);
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                // JSONObject order_detail = res_object.getJSONObject(Constant.ORDER_DETAIL);

                // JSONArray items_array = res_object.getJSONArray("products");

                listener.onCartItemsSuccess("" + response);

            } else {
                try {
                    String message = res_object.getString(Constant.MESSAGE);
                    listener.onCartItemsError(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onError(String message) {
        listener.onCartItemsError(message);
    }
}
