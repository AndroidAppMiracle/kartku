package kartku.com.cart;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kartku.com.R;
import kartku.com.activity.AboutUsActivity;
import kartku.com.activity.ContactActivity;
import kartku.com.activity.MyOrdersActivity;
import kartku.com.activity.MyProductReviewsActivity;
import kartku.com.activity.PrivacyPolicyActivity;
import kartku.com.activity.ProfileActivity;
import kartku.com.activity.PromoCodeDialog;
import kartku.com.activity.ShippingWebViewActivity;
import kartku.com.activity.WishlistActivity;
import kartku.com.adapter.MyCartDetailsAdapter;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.faq_module.FAQ;
import kartku.com.login_module.Login;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.EventBusLogout;
import kartku.com.retrofit.modal.EventBusPromoCodeApplied;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.retrofit.modal.PlaceOrderModal;
import kartku.com.retrofit.modal.PromoCodeModal;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 1/23/2017.
 */

public class CartDetailsNew extends Utility implements ApiServerResponse {

    SharedPreferences pref;

    String cart_id, user_id;
    TextView total_amount;
    MyCartDetailsAdapter adapter;
    RecyclerView cart_recycle;
    List<MyCartDetailModal.ItemsBean> itemsBeanList = new ArrayList<>();
    List<MyCartDetailModal.ProductsBean> productsBeanList = new ArrayList<>();
    Button btn_shipping;
    TextView message_text;
    LinearLayout bottomLayout;
    //TextView tv_promo_code_applied_response_message;
    String amountDiscountPromoCode;
    String totalAmountAfterDiscountPromoCode;
    String orderIDToShipping;
    Button bt_have_promo_code;

    String promoCodeFlag,
            promoCodeAmount,
            promoCodeDiscountAmount,
            promoCodeGrandTotal, promoCodeName;

    Resources res;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_details_new);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_cart);

        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        total_amount = (TextView) findViewById(R.id.total_amount);
        cart_recycle = (RecyclerView) findViewById(R.id.cart_recycle);
        btn_shipping = (Button) findViewById(R.id.btn_shipping);
        message_text = (TextView) findViewById(R.id.message_text);
        bottomLayout = (LinearLayout) findViewById(R.id.bottomLayout);
        bt_have_promo_code = (Button) findViewById(R.id.bt_have_promo_code);
        res = getResources();
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        try {
            cart_id = Session.getcart_id(pref);
            user_id = Session.get_userId(pref);
           /* Log.d("", "cart detail cart id " + cart_id);
            Log.i("user_id", user_id);
            System.out.println("cartdetailcartid " + cart_id);*/
            /*Log.d("", "cart detail cart id " + cart_id);*/
            if (checkInternetConnection(CartDetailsNew.this)) {
                if (!cart_id.equalsIgnoreCase("0")) {
                    showLoading();
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.get_userId(pref), cart_id, this);
                } else {
                    message_text.setVisibility(View.VISIBLE);
                    cart_recycle.setVisibility(View.GONE);
                    String text = String.format(res.getString(R.string.two_parameters), "Total Amount", "0");
                    total_amount.setText(text);
                }

            } else {
                showAlertDialog(CartDetailsNew.this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        bt_have_promo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (promoCodeFlag.equalsIgnoreCase("1")) {
                    showPromoCodeDetails(promoCodeName, promoCodeAmount, promoCodeDiscountAmount, promoCodeGrandTotal);
                } else {
                    if (!cart_id.equalsIgnoreCase("0")) {
                        showLoading();
                        ServerAPI.getInstance().placeOrder(ApiServerResponse.PLACE_ORDER_PROMO_CODE, Session.get_userId(pref), cart_id, CartDetailsNew.this);
                        //applyPromoCodeDialog();
                    }

                }


            }
        });


        btn_shipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    if (checkInternetConnection(CartDetailsNew.this)) {

                        /*Intent i = new Intent(CartDetailsNew.this, ShippingWebViewActivity.class);

                        startActivity(i);*/

                        if (!cart_id.equalsIgnoreCase("0")) {
                            showLoading();
                            ServerAPI.getInstance().placeOrder(ApiServerResponse.PLACE_ORDER, Session.get_userId(pref), cart_id, CartDetailsNew.this);
                        }


                    } else {
                        showAlertDialog(CartDetailsNew.this);
                    }



                    /*Intent i = new Intent(CartDetailsNew.this, Shipping.class);
                    startActivity(i);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        /*MyCartDetailsAdapter.ViewHolder viewHolder = new ;
        viewHolder.add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (Session.get_login_statuc(pref)) {
            getMenuInflater().inflate(R.menu.menu_cart_login, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_cart_logout, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_home:

                Intent intentHome = new Intent(CartDetailsNew.this, DemoHomeDashBoard.class);
                intentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentHome);
                break;
            case R.id.menu_my_orders:


                Intent intent1 = new Intent(CartDetailsNew.this, MyOrdersActivity.class);
                startActivity(intent1);
                //MyOrdersFragment ordersFragment = new MyOrdersFragment();
                            /*OrdersFragment ordersFragment = new OrdersFragment();*/
                //tag = "My Orders";
                /*ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);*/
                //swtichFragement(R.id.content_frame, ordersFragment, tag);

                break;
            case R.id.menu_my_product_review:

                Intent intentReviews = new Intent(CartDetailsNew.this, MyProductReviewsActivity.class);
                startActivity(intentReviews);

                break;
            case R.id.menu_my_wishlist:
                Intent intentWishlist = new Intent(CartDetailsNew.this, WishlistActivity.class);
                startActivity(intentWishlist);
                break;
            case R.id.menu_my_profile:
                Intent intentProfile = new Intent(CartDetailsNew.this, ProfileActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.menu_contact_us:

                Intent intentContact = new Intent(CartDetailsNew.this, ContactActivity.class);
                startActivity(intentContact);

                break;
            case R.id.menu_logout:

                //Session.getLogin_type(pref);

                EventBus.getDefault().post(new EventBusLogout(Session.getLogin_type(pref)));
                /*switch (loginType) {
                    case "google":
                        if (mGoogleApiClient.isConnected()) {
                            signOut();
                            drawerLayout.closeDrawers();
                            //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                            mGoogleApiClient.disconnect();
                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            LoginFragment fragmentLoginLogout = new LoginFragment();
                            tag = "Login";
                            swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);

                        }
                        break;
                    case "facebook": {
                        LoginManager.getInstance().logOut();
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        break;
                    }
                    default: {
                        drawerLayout.closeDrawers();
                        Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        LoginFragment fragmentLoginLogout = new LoginFragment();
                        tag = "Login";
                        //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        //prepareListData();
                        Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                        startActivity(i);
                        finish();
                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        break;
                    }
                }*/

                break;

            case R.id.menu_privacy_policy:


                //if (checkInternetConnection(DemoHomeDashBoard.this)) {

                Intent intentPrivacyPolicy = new Intent(CartDetailsNew.this, PrivacyPolicyActivity.class);
                startActivity(intentPrivacyPolicy);

                // swtichFragement(R.id.content_frame, privacyPolicyFragment, tag);


                break;
            case R.id.menu_about_us:

                Intent intentAboutUs = new Intent(CartDetailsNew.this, AboutUsActivity.class);
                startActivity(intentAboutUs);
                break;

            case R.id.menu_login:
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                Intent intent = new Intent(CartDetailsNew.this, Login.class);
                startActivity(intent);
                break;
            case R.id.menu_faq:

                Intent intentFAQ = new Intent(CartDetailsNew.this, FAQ.class);
                startActivity(intentFAQ);


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {


            if (response.isSuccessful()) {
                MyCartDetailModal myCartDetailModal;
                PlaceOrderModal placeOrderModal;
                PromoCodeModal promoCodeModal;




                /*textPathList = String.format(res.getString(R.string.product_path_list_name), cat_name);*/

                switch (tag) {

                    case ApiServerResponse.MY_CART_DETAILS:
                        myCartDetailModal = (MyCartDetailModal) response.body();


                        if (myCartDetailModal.getStatus().equals("OK")) {
//                            Log.i("CARTID", "" + myCartDetailModal.getCart_info().getId());

                            if (myCartDetailModal.getItems().size() != 0) {
                                btn_shipping.setVisibility(View.VISIBLE);


                                promoCodeFlag = myCartDetailModal.getPromo_info().getIs_promocode();
                                promoCodeAmount = myCartDetailModal.getCart_info().getTotal_amount();
                                promoCodeDiscountAmount = myCartDetailModal.getPromo_info().getDiscount();
                                promoCodeGrandTotal = myCartDetailModal.getPromo_info().getFinal_amount();
                                promoCodeName = myCartDetailModal.getPromo_info().getName();

                                /*String amount = myCartDetailModal.getCart_info().getTotal_amount();
                                String text = String.format(res.getString(R.string.total_price_in_RP), String.format(Locale.getDefault(), "%.2f", Float.parseFloat(amount)));
                                message_text.setVisibility(View.GONE);
                                cart_recycle.setVisibility(View.VISIBLE);
                                bt_have_promo_code.setVisibility(View.VISIBLE);
                                total_amount.setText(text);*/


                                if (myCartDetailModal.getPromo_info().getIs_promocode().equalsIgnoreCase("1")) {
                                    message_text.setVisibility(View.GONE);
                                    cart_recycle.setVisibility(View.VISIBLE);
                                    bt_have_promo_code.setVisibility(View.VISIBLE);
                                    bt_have_promo_code.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_done, 0);
                                    bt_have_promo_code.setEnabled(true);
                                    bt_have_promo_code.setText(R.string.code_applied);
                                    //String amountPromo = myCartDetailModal.getPromo_info().getFinal_amount();
                                    //String text = String.format(res.getString(R.string.total_price_in_RP), String.format(Locale.getDefault(), "%.2f", Float.parseFloat(amount)));
                                    total_amount.setText(String.format(res.getString(R.string.total_price_in_RP), String.format(Locale.getDefault(), "%.2f", Float.parseFloat(myCartDetailModal.getPromo_info().getFinal_amount()))));
                                } else {
                                    //String amountCart = myCartDetailModal.getCart_info().getTotal_amount();
                                    //String text = String.format(res.getString(R.string.total_price_in_RP), String.format(Locale.getDefault(), "%.2f", Float.parseFloat(amount)));
                                    message_text.setVisibility(View.GONE);
                                    cart_recycle.setVisibility(View.VISIBLE);
                                    bt_have_promo_code.setVisibility(View.VISIBLE);
                                    bt_have_promo_code.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                    bt_have_promo_code.setEnabled(true);
                                    bt_have_promo_code.setText(getString(R.string.promo_code));
                                    total_amount.setText(String.format(res.getString(R.string.total_price_in_RP), String.format(Locale.getDefault(), "%.2f", Float.parseFloat(myCartDetailModal.getCart_info().getTotal_amount()))));
                                }
                                itemsBeanList = myCartDetailModal.getItems();
                                productsBeanList = myCartDetailModal.getProducts();
                                adapter = new MyCartDetailsAdapter(CartDetailsNew.this, itemsBeanList, productsBeanList, total_amount, bottomLayout);
                                cart_recycle.setAdapter(adapter);
                                cart_recycle.setItemAnimator(new DefaultItemAnimator());
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CartDetailsNew.this);
                                cart_recycle.setLayoutManager(mLayoutManager);
                            } else {
                                //ToastMsg.showLongToast(CartDetailsNew.this, "Here");
                                btn_shipping.setVisibility(View.GONE);
                                message_text.setVisibility(View.VISIBLE);
                                cart_recycle.setVisibility(View.GONE);
                                bt_have_promo_code.setVisibility(View.GONE);
                                String text = String.format(res.getString(R.string.two_parameters), "Total Amount", "0");
                                total_amount.setText(text);
                            }


                        } else {
                            // ToastMsg.showLongToast(CartDetailsNew.this, "Here");
                            message_text.setVisibility(View.VISIBLE);
                            cart_recycle.setVisibility(View.GONE);
                            btn_shipping.setVisibility(View.GONE);
                            String text = String.format(res.getString(R.string.two_parameters), "Total Amount", "0");
                            total_amount.setText(text);
                        }

                        hideLoading();
                        break;


                    case ApiServerResponse.PLACE_ORDER:

                        placeOrderModal = (PlaceOrderModal) response.body();

//                        Log.i("MESSPLACEORDER", placeOrderModal.getMessage());
//                        Log.i("MESSPLACEORDERID", "" + placeOrderModal.getOrder_detail().getCart_id());
                        if (placeOrderModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            orderIDToShipping = String.valueOf(placeOrderModal.getOrder_detail().getId());

                            //applyPromoCodeDialog();


                            Intent i = new Intent(CartDetailsNew.this, ShippingWebViewActivity.class);
                            i.putExtra(Constant.ORDER_ID, orderIDToShipping.trim());
                            startActivity(i);

                        } else /*if (placeOrderModal.getOrder_detail().getStatus().equalsIgnoreCase(Constant.PAYMENT_PENDING)) */ {
                            orderIDToShipping = String.valueOf(placeOrderModal.getOrder_detail().getId());
                            //applyPromoCodeDialog();


                            Intent i = new Intent(CartDetailsNew.this, ShippingWebViewActivity.class);
                            i.putExtra(Constant.ORDER_ID, orderIDToShipping.trim());
                            startActivity(i);

                        }

                        hideLoading();
                        break;
                    case ApiServerResponse.PLACE_ORDER_PROMO_CODE:
                        placeOrderModal = (PlaceOrderModal) response.body();
                      /*  Log.i("MESSPLACEORDER", placeOrderModal.getMessage());
                        Log.i("MESSPLACEORDERID", "" + placeOrderModal.getOrder_detail().getCart_id());*/
                        if (placeOrderModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            orderIDToShipping = String.valueOf(placeOrderModal.getOrder_detail().getId());

                            Intent i = new Intent(CartDetailsNew.this, PromoCodeDialog.class);
                            i.putExtra(Constant.ORDER_ID, orderIDToShipping);
                            startActivity(i);
                            //applyPromoCodeDialog();


                           /* Intent i = new Intent(CartDetailsNew.this, ShippingWebViewActivity.class);
                            i.putExtra(Constant.ORDER_ID, orderIDToShipping.trim());
                            startActivity(i);
*/
                        } else /*if (placeOrderModal.getOrder_detail().getStatus().equalsIgnoreCase(Constant.PAYMENT_PENDING)) */ {
                            orderIDToShipping = String.valueOf(placeOrderModal.getOrder_detail().getId());

                            Intent i = new Intent(CartDetailsNew.this, PromoCodeDialog.class);
                            i.putExtra(Constant.ORDER_ID, orderIDToShipping);
                            startActivity(i);
                            //applyPromoCodeDialog();


                           /* Intent i = new Intent(CartDetailsNew.this, ShippingWebViewActivity.class);
                            i.putExtra(Constant.ORDER_ID, orderIDToShipping.trim());
                            startActivity(i);*/

                        }

                        hideLoading();

                        break;
                   /* case ApiServerResponse.SET_PROMOCODE:

                        promoCodeModal = (PromoCodeModal) response.body();
                        Log.i("CODE", promoCodeModal.getStatus());


                        if (promoCodeModal.getStatus().equalsIgnoreCase(Constant.OK)) {


                            //orderIDToShipping= String.valueOf(promoCodeModal.getCart_info().getId());

                          *//*  Intent i = new Intent(CartDetailsNew.this, ShippingWebViewActivity.class);
                            i.putExtra(Constant.ORDER_ID, orderID.trim());
                            startActivity(i);*//*

                            amountDiscountPromoCode = promoCodeModal.getCart_info().getDiscount();
                            totalAmountAfterDiscountPromoCode = promoCodeModal.getCart_info().getTotal_amount();

                         *//*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                tv_promo_code_applied_response_message.setText(getResources().getString(R.string.promo_code_applied_succ));
                                tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorPrimary, null));
                            } else {
                                tv_promo_code_applied_response_message.setText(getResources().getString(R.string.promo_code_applied_succ));
                                tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorPrimary));
                            }*//*
                        } else {
                            amountDiscountPromoCode = "";
                            totalAmountAfterDiscountPromoCode = "";

*//*
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                tv_promo_code_applied_response_message.setText(getResources().getString(R.string.invalid_promo_code_error_msg));
                                tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorLogoRed, null));
                            } else {
                                tv_promo_code_applied_response_message.setText(getResources().getString(R.string.invalid_promo_code_error_msg));
                                tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorLogoRed));
                            }*//*
                        }


                        hideLoading();
                        break;*/


                }
            } else {
                hideLoading();
                //ToastMsg.showLongToast(CartDetailsNew.this, "Here");
                message_text.setVisibility(View.VISIBLE);
                cart_recycle.setVisibility(View.GONE);
                btn_shipping.setVisibility(View.GONE);
                String text = String.format(res.getString(R.string.two_parameters), "Total Amount", "0");
                total_amount.setText(text);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            hideLoading();
            switch (tag) {
                case ApiServerResponse.MY_CART_DETAILS:
                    Resources res = getResources();
                    System.out.println("Error");
                    //ToastMsg.showLongToast(CartDetailsNew.this, "Here");
                    message_text.setVisibility(View.VISIBLE);
                    cart_recycle.setVisibility(View.GONE);
                    String text = String.format(res.getString(R.string.two_parameters), "Total Amount", "0");
                    total_amount.setText(text);
                    hideLoading();
                    break;
                case ApiServerResponse.PLACE_ORDER:
                    hideLoading();

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void showPromoCodeDetails(String promoCodeName, String amount, String discount, String grandTotal) {

        try {
            final Dialog dialog = new Dialog(this);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //dialog.setTitle(String.format(res.getString(R.string.about_brand), brandName));
            dialog.setContentView(R.layout.dialog_promo_code_details);

            TextView tv_promo_code_name = (TextView) dialog.findViewById(R.id.tv_promo_code_name);
            TextView tv_amount = (TextView) dialog.findViewById(R.id.tv_amount);
            TextView tv_promo_code_discount = (TextView) dialog.findViewById(R.id.tv_promo_code_discount);
            TextView tv_promo_code_grand_total = (TextView) dialog.findViewById(R.id.tv_promo_code_grand_total);
            Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);

            tv_promo_code_name.setText(promoCodeName);
            tv_amount.setText("RP " + amount);
            tv_promo_code_discount.setText("RP " + discount);
            tv_promo_code_grand_total.setText("RP" + grandTotal);

            bt_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


/*    private void applyPromoCodeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setTitle(String.format(res.getString(R.string.about_brand), brandName));
        dialog.setContentView(R.layout.dialog_promo_code);


        final EditText et_promo_code = (EditText) dialog.findViewById(R.id.et_promo_code);
        final TextView tv_promo_code_applied_response_message = (TextView) dialog.findViewById(R.id.tv_promo_code_applied_response_message);
        TextView tv_skip_prromo_code = (TextView) dialog.findViewById(R.id.tv_skip_prromo_code);
        TextView tv_apply_promo_code = (TextView) dialog.findViewById(R.id.tv_apply_promo_code);
        final TextView tv_dialog_close = (TextView) dialog.findViewById(R.id.tv_dialog_close);

        final LinearLayout ll_skip_apply = (LinearLayout) dialog.findViewById(R.id.ll_skip_apply);

        //Calculations
        final LinearLayout ll_calculation_and_discount = (LinearLayout) dialog.findViewById(R.id.ll_calculation_and_discount);
        final TextView tv_promo_code_discount = (TextView) dialog.findViewById(R.id.tv_promo_code_discount);
        final TextView tv_total_amount = (TextView) dialog.findViewById(R.id.tv_total_amount);
        final TextView tv_submit = (TextView) dialog.findViewById(R.id.tv_submit);

        //TextView textViewBrandDesc = (TextView) dialog.findViewById(R.id.tv_brand_desc);

        tv_apply_promo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (et_promo_code.getText().toString().equalsIgnoreCase("")) {
                        tv_promo_code_applied_response_message.setText(getResources().getString(R.string.empty_promo_code_error));
                    } else {
                        showLoading();
                        if (!cart_id.equalsIgnoreCase("")) {
                            showLoading();
                            ServerAPI.getInstance().placeOrder(ApiServerResponse.PLACE_ORDER_PROMO_CODE, cart_id, CartDetailsNew.this);
                            ServerAPI.getInstance().setPromoCode(ApiServerResponse.SET_PROMOCODE, Session.getcart_id(pref), et_promo_code.getText().toString(), CartDetailsNew.this);

                            if (!amountDiscountPromoCode.equalsIgnoreCase("") && !totalAmountAfterDiscountPromoCode.equalsIgnoreCase("")) {
                                tv_dialog_close.setVisibility(View.GONE);
                                ll_calculation_and_discount.setVisibility(View.VISIBLE);
                                et_promo_code.setEnabled(false);
                                tv_promo_code_discount.setText(String.format(res.getString(R.string.promo_code_discount_in_RP), amountDiscountPromoCode));
                                tv_total_amount.setText(String.format(res.getString(R.string.price_in_RP), totalAmountAfterDiscountPromoCode));
                                total_amount.setText(String.format(res.getString(R.string.total_price_in_RP), totalAmountAfterDiscountPromoCode));
                                ll_skip_apply.setVisibility(View.GONE);
                                tv_submit.setVisibility(View.VISIBLE);
                                tv_promo_code_applied_response_message.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    tv_promo_code_applied_response_message.setText(getResources().getString(R.string.promo_code_applied_succ));
                                    tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorPrimary, null));
                                } else {
                                    tv_promo_code_applied_response_message.setText(getResources().getString(R.string.promo_code_applied_succ));
                                    tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorPrimary));
                                }
                            } else {
                                tv_dialog_close.setVisibility(View.VISIBLE);
                                ll_calculation_and_discount.setVisibility(View.GONE);
                                //tv_promo_code_discount.setText(amountDiscountPromoCode);
                                //tv_total_amount.setText(totalAmountAfterDiscountPromoCode);
                                ll_skip_apply.setVisibility(View.VISIBLE);
                                tv_submit.setVisibility(View.GONE);
                                tv_promo_code_applied_response_message.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                    tv_promo_code_applied_response_message.setText(getResources().getString(R.string.invalid_promo_code_error_msg));
                                    tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorLogoRed, null));
                                } else {
                                    tv_promo_code_applied_response_message.setText(getResources().getString(R.string.invalid_promo_code_error_msg));
                                    tv_promo_code_applied_response_message.setTextColor(getResources().getColor(R.color.colorLogoRed));
                                }
                            }
                        }

                        // if (!orderIDToShipping.equalsIgnoreCase("")){
                        //  }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tv_dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cart_id.equalsIgnoreCase("")) {
                    showLoading();
                    ServerAPI.getInstance().placeOrder(ApiServerResponse.PLACE_ORDER, cart_id, CartDetailsNew.this);
                }

              *//*  Intent i = new Intent(CartDetailsNew.this, ShippingWebViewActivity.class);
                i.putExtra(Constant.ORDER_ID, orderIDToShipping.trim());
                startActivity(i);*//*
            }
        });

        tv_skip_prromo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*if (!cart_id.equalsIgnoreCase("")) {
                    showLoading();
                    ServerAPI.getInstance().placeOrder(ApiServerResponse.PLACE_ORDER, cart_id, CartDetailsNew.this);
                }*//*
                dialog.dismiss();
            }
        });


        //TextView textViewOK = (TextView) dialog.findViewById(R.id.text_ok);

        //ImageView iv_brand_image = (ImageView) dialog.findViewById(R.id.iv_brand_image);

        *//*Glide.with(ProductDetailNew.this)
                .load(brandImage).placeholder(R.drawable.placeholder)
                .thumbnail(0.1f).override(300, 300)
                .into(iv_brand_image);*//*

        *//*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*//*

        //TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        *//*textViewOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
*//*

        dialog.show();
    }*/

    /*private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Connection Problem.");
            dialogBuilder.setMessage("Please check your internet connection?");
            dialogBuilder.setCancelable(true);
           *//* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                    //finish();
                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }


    @Subscribe
    public void updateCart(EventBusPromoCodeApplied eventBusPromoCodeApplied) {

        try {
            if (eventBusPromoCodeApplied.getMessage().equalsIgnoreCase(Constant.OK)) {
                if (!cart_id.equalsIgnoreCase("0")) {
                    //showLoading();
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.get_userId(pref), cart_id, this);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
