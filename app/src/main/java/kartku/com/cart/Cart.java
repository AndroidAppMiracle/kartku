package kartku.com.cart;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.adapter.CartAdapter;
import kartku.com.cart.Model.CartModel;
import kartku.com.cart.listener.CartDetailListener;
import kartku.com.cart.listener.CartItemsListener;
import kartku.com.cart.listener.DeleteCartListener;
import kartku.com.cart.manager.CartDetailManager;
import kartku.com.cart.manager.CartItemsManager;
import kartku.com.cart.manager.DeleteCartManager;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;


public class Cart extends AppCompatActivity implements CartDetailListener, View.OnClickListener, DeleteCartListener, CartItemsListener {
    ArrayList<CartModel> arrayList_cart = new ArrayList<>();
    RecyclerView cart_recycle;
    LinearLayoutManager mRecentLayoutManager;
    String cart_id, user_id;
    SharedPreferences pref;
    public static TextView message_text;
    public static Button shipping_text;
    public static double total_value;
    public static TextView amount_text;
    boolean pendingIntroAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }

        cart_recycle = (RecyclerView) findViewById(R.id.cart_recycle);
        message_text = (TextView) findViewById(R.id.message_text);
        amount_text = (TextView) findViewById(R.id.total_amount);
        shipping_text = (Button) findViewById(R.id.btn_shipping);
        mRecentLayoutManager = new LinearLayoutManager(this);
        cart_recycle.setLayoutManager(mRecentLayoutManager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        try {
            cart_id = Session.getcart_id(pref);
            user_id = Session.get_userId(pref);
            Log.d("", "cart detail cart id " + cart_id);
            Log.i("user_id", user_id);
            System.out.println("cartdetailcartid " + cart_id);

            new CartDetailManager(Cart.this, this).sendRequest("/" + cart_id);
            new CartItemsManager(Cart.this, this).sendRequest("/" + user_id);

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*for(int i=0;i<5;i++){
            CartModel cartModel=new CartModel();
            cartModel.setProductName("Chair");
            cartModel.setProductDesc("Rounding Chair");
            cartModel.setProductPrice("Price :500 rs");
            arrayList_cart.add(cartModel);
        }
        CartAdapter adapter=new CartAdapter(this,arrayList_cart);
       cart_recycle.setAdapter(adapter);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_shipping:
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case android.R.id.home:
                try {
                    /*Intent intent = new Intent(Cart.this, Home_dashboard.class);
                    startActivity(intent);*/
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_item, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.deleteCart:
                try {
                    deleteCart();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
/*            case android.R.id.home:
                try {
                    *//*Intent intent = new Intent(Cart.this, Home_dashboard.class);
                    startActivity(intent);*//*
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;*/
        }
        return (super.onOptionsItemSelected(menuItem));
    }


    private void deleteCart() {
        new DeleteCartManager(Cart.this, this).sendRequest("", Session.getcart_id(pref));
    }

    @Override
    public void onCartError(String error) {

        ToastMsg.showLongToast(Cart.this, error);
    }

    @Override
    public void onCartSuccess(String message) {
        try {
            Log.i("Response ", message);

            if (!message.contains("No Item found cart is empty")) {
                JSONObject res_object = new JSONObject(message);
                JSONArray cart_items = res_object.getJSONArray("products");
                JSONArray items_array = res_object.getJSONArray("items");
                Log.i("response cart products ", " " + cart_items);
                Log.i("response item array", " " + items_array);
                for (int j = 0; j < cart_items.length(); j++) {
                    JSONObject item = cart_items.getJSONObject(j);
                    JSONObject item_obj = items_array.getJSONObject(j);
                    Log.d("", "cart item obj " + item);
                    CartModel model = new CartModel();
                    model.setProduct_id(item.getString("pid"));
                    model.setProductName(item.getString("product_name"));
                    model.setProductPrice(item.getString("price"));
                    model.setImagePath(item.getString("product_image"));
                    model.setItem_id(item_obj.getString("id"));
                    model.setProductQuantity(item_obj.getString("quantity"));
                    arrayList_cart.add(model);

                    try {
                        total_value = total_value + (Double.parseDouble(item.getString("price")) * Double.parseDouble(item_obj.getString("quantity")));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                message_text.setVisibility(View.GONE);
                amount_text.setVisibility(View.VISIBLE);
                shipping_text.setVisibility(View.VISIBLE);
                //amount_text.setText(" Total Amount -  Rs " + total_value);
                CartAdapter adp = new CartAdapter(getApplicationContext(), arrayList_cart, message_text);
                cart_recycle.setAdapter(adp);
                if (pendingIntroAnimation) {
                    pendingIntroAnimation = false;
                    startIntroAnimation();
                }
            } else {
                message_text.setVisibility(View.VISIBLE);
                amount_text.setVisibility(View.GONE);
                shipping_text.setVisibility(View.GONE);
                amount_text.setText(" Total Amount -  Rs " + 0);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteCartError(String error) {

    }

    @Override
    public void onDeleteCartSuccess(String message) {

        try {
            JSONObject item = new JSONObject(message);
            String status = item.getString("status");
            if (status.equalsIgnoreCase("ok")) {
                String item_info = item.getString("info");
                ToastMsg.showShortToast(Cart.this, "" + item_info);
                try {
                    arrayList_cart.clear();
                    CartAdapter adp = new CartAdapter(getApplicationContext(), arrayList_cart, message_text);
                    cart_recycle.setAdapter(adp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    String item_info = item.getString("cart_info");
                    ToastMsg.showShortToast(Cart.this, "" + item_info);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void startIntroAnimation() {
        cart_recycle.setTranslationY(cart_recycle.getHeight());
        cart_recycle.setAlpha(0f);
        cart_recycle.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    @Override
    public void onCartItemsSuccess(String message) {

        try {
            JSONObject item = new JSONObject(message);
            String status = item.getString("status");
            if (status.equalsIgnoreCase("ok")) {
                String total_price = item.getString("total_price");
                amount_text.setText(total_price);
            } else {
                amount_text.setText("0");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCartItemsError(String error) {
        ToastMsg.showLongToast(Cart.this, error);

    }
}
