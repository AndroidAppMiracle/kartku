package kartku.com.cart.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface DeleteItemListener {

    void onDeleteItemSuccess(String message);
    void onDeleteItemError(String error);
}
