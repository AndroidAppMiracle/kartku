package kartku.com.cart.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface UpdateCartListener {

    void onSuccess(String response);
    void onError(String error);

}
