package kartku.com.cart.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface AddToCartListener {

    void onAddCartSuccess(String messgae);
    void onAddCartError(String error);
}
