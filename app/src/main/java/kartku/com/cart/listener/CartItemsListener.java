package kartku.com.cart.listener;

/**
 * Created by Kshitiz Bali on 1/10/2017.
 */

public interface CartItemsListener {

    void onCartItemsSuccess(String message);
    void onCartItemsError(String error);
}
