package kartku.com.cart.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface CartDetailListener {

    void onCartSuccess(String message);
    void onCartError(String error);
}
