package kartku.com.cart.listener;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public interface DeleteCartListener {

    void onDeleteCartSuccess(String message);
    void onDeleteCartError(String error);
}
