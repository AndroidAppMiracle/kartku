package kartku.com.cart.api;

import android.content.Context;

import kartku.com.cart.manager.CartDetailManager;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class CartDetailApi implements CustomResponse {

    private Context context;
    private CartDetailManager manager;
    private RequestServer request_server;

    public CartDetailApi(Context context, CartDetailManager manager) {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId, String params) {
        try {
            request_server = new RequestServer(context, requestId, this, params);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
