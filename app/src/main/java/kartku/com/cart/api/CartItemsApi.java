package kartku.com.cart.api;

import android.content.Context;

import kartku.com.cart.manager.CartDetailManager;
import kartku.com.cart.manager.CartItemsManager;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by Kshitiz Bali on 1/10/2017.
 */

public class CartItemsApi implements CustomResponse {

    private Context context;
    private CartItemsManager manager;
    private RequestServer request_server;

    public CartItemsApi(Context context, CartItemsManager manager) {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    public void sendRequest(int requestId, String params) {
        try {
            request_server = new RequestServer(context, requestId, this, params);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
