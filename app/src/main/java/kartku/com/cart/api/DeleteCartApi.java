package kartku.com.cart.api;

import android.content.Context;

import java.util.Map;

import kartku.com.cart.manager.AddToCartManager;
import kartku.com.cart.manager.DeleteCartManager;
import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/16/2016.
 */
public class DeleteCartApi implements CustomResponse {

    private Context context;
    private DeleteCartManager manager;
    private RequestServer request_server;

    public DeleteCartApi(Context context, DeleteCartManager manager) {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }
    /**
     * request server
     */

    public void sendRequest(int requestId ,  Map<String, String> params){
        try {
            request_server = new RequestServer(context, requestId, params,this , RequestConstants.POST_URL);
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
