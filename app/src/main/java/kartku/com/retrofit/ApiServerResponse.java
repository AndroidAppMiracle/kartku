package kartku.com.retrofit;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 1/20/2017.
 */

public interface ApiServerResponse {


    int MY_ORDERS = 1;
    int ORDER_DETAIL = 2;
    int MY_PRODUCT_REVIEWS = 3;
    int MY_CART_DETAILS = 4;
    int ADD_TO_CART_NEW = 5;
    int SELLER_ORDERS = 6;
    int HOME_SCREEN = 7;
    int PRODUCT_DETAIL = 8;
    int CATEGORY_PRODUCTS = 9;
    int ADD_TO_WISHLIST = 10;
    int REMOVE_FROM_WISHLIST = 11;
    int PRODUCT_CATEGORY = 12;
    int SUB_PRODUCT_CATEGORY = 13;
    int SUB_SUB_PRODUCT_CATEGORY = 14;
    int CARTS_BY_USER = 15;
    int DELETE_CART_ITEM = 16;
    int PLACE_ORDER = 17;
    int UPDATE_CART_ITEM = 18;
    int SEARCH_BY_BRAND = 19;
    int SEARCH_BY_BRAND_AND_HIGH = 20;
    int SEARCH_BY_BRAND_AND_LOW = 21;
    int SEARCH_BY_MATERIAL = 22;
    int SEARCH_BY_MATERIAL_AND_HIGH = 23;
    int SEARCH_BY_MATERIAL_AND_LOW = 24;
    int SEARCH_BY_NEW_ARRIVAL = 25;
    int SEARCH_BY_NEW_ARRIVAL_AND_LOW = 26;
    int SEARCH_BY_NEW_ARRIVAL_AND_HIGH = 27;
    int ORDER_CONFIRMATION = 28;
    int UPDATE_USER_PROFILE = 29;
    int WISHLIST = 30;
    int SEARCH = 31;
    int SEARCH_BY_NEW_ARRIVAL_ONLY = 32;
    int MAIN_CATEGORIES = 33;
    int CART_ITEM_COUNT = 34;
    int SEARCH_SOR_FILTER_PRODUCTS = 35;
    int CATEGORY_SORT_FILER = 36;
    int SET_LANGUAGE = 37;
    int SET_PROMOCODE = 38;
    int PRODUCT_COLOURS = 39;
    int PRODUCT_IMAGE_ACC_COLOUR = 40;
    int USER_LOGIN = 41;
    int USER_SOCIAL_LOGIN = 42;
    int USER_REGISTER = 43;
    int CONTACT_US = 44;
    int ONE_PRODUCT_REVIEWS = 45;
    int RATE_PRODUCT= 46;
    int PLACE_ORDER_PROMO_CODE = 47;
    int MATERIAL_LIST = 48;
    int BRAND_LIST = 49;
    int LOGOUT = 50;
    int LOGOUT_FRAGMENT = 51;
    int CHANGE_PASSWORD = 52;
    int FORGOR_PASSWORD = 53;


    void onSuccess(int tag, Response response);

    void onError(int tag, Throwable throwable);
}
