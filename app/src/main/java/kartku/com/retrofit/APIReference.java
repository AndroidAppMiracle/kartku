package kartku.com.retrofit;

import java.util.Map;

import kartku.com.MyOrders.modal.MyOrderModal;
import kartku.com.my_product_reviews_module.MyProductReviewsModal;
import kartku.com.orders_module.modal.OrderDetailsModal;
import kartku.com.rate_review.model.RateAndReviewModel;
import kartku.com.retrofit.modal.AddToCartNewModal;
import kartku.com.retrofit.modal.AddToWishListModal;
import kartku.com.retrofit.modal.BrandListModal;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.CartsByUserModal;
import kartku.com.retrofit.modal.CategoryProductsModal;
import kartku.com.retrofit.modal.ChangePasswordModal;
import kartku.com.retrofit.modal.ContactUsModal;
import kartku.com.retrofit.modal.DeleteCartItemModal;
import kartku.com.retrofit.modal.ForgotPasswordModal;
import kartku.com.retrofit.modal.GetProductImagesWithColourModal;
import kartku.com.retrofit.modal.HomeScreenModal;
import kartku.com.retrofit.modal.LanguageSelectModal;
import kartku.com.retrofit.modal.LogOutModal;
import kartku.com.retrofit.modal.LoginModal;
import kartku.com.retrofit.modal.MainCategoriesModal;
import kartku.com.retrofit.modal.MaterialListingModal;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.retrofit.modal.OneProductReviews;
import kartku.com.retrofit.modal.OrderConfirmationModal;
import kartku.com.retrofit.modal.PlaceOrderModal;
import kartku.com.retrofit.modal.ProductCategoryModal;
import kartku.com.retrofit.modal.ProductColourOptionsModal;
import kartku.com.retrofit.modal.ProductDetailModal;
import kartku.com.retrofit.modal.ProductSearchBrandModal;
import kartku.com.retrofit.modal.PromoCodeModal;
import kartku.com.retrofit.modal.RateProductModal;
import kartku.com.retrofit.modal.RegisterModal;
import kartku.com.retrofit.modal.RemoveFromWishlistModal;
import kartku.com.retrofit.modal.SearchModal;
import kartku.com.retrofit.modal.SellerOrdersModal;
import kartku.com.retrofit.modal.SocialUserLoginModal;
import kartku.com.retrofit.modal.SubProductCategoryListModal;
import kartku.com.retrofit.modal.SubSubProductCategoryDetailModal;
import kartku.com.retrofit.modal.UpdateCartItemModal;
import kartku.com.retrofit.modal.UpdateUserModal;
import kartku.com.retrofit.modal.WishlistModal;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Kshitiz Bali on 1/20/2017.
 */

public interface APIReference {


    @GET("my_orders/{user_id}")
    Call<MyOrderModal> getMyOrders(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("user_id") String userID);

    @GET("order_detail/{order_id}")
    Call<OrderDetailsModal> getOrderDetail(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("order_id") String orderID);

    @GET("my_product_reviews/{user_id}")
    Call<MyProductReviewsModal> getMyProductReviews(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("user_id") String userID);

    @GET("cart_detail/{cart_id}")
    Call<MyCartDetailModal> getMyCartDetails(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("cart_id") String cartID);

    @FormUrlEncoded
    @POST("add_to_cart")
    Call<AddToCartNewModal> addToCartNew(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @FieldMap Map<String, String> params);

    @GET("seller_sold_orders/{user_id}")
    Call<SellerOrdersModal> getSellerOrders(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("user_id") String sellerID);

    @GET("homescreen")
    Call<HomeScreenModal> getHomeScreen(@Header("apiKey") String sessionKey, @Header("user_id") String userID);

    @FormUrlEncoded
    @POST("product_detail")
    Call<ProductDetailModal> getProductDetail(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("product_id") String productID, @Field("user_id") String userID);


    @FormUrlEncoded
    @POST("category_products")
    Call<CategoryProductsModal> getCategoryProducts(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("category_id") String categoryID, @Field("user_id") String userID);

    @FormUrlEncoded
    @POST("add_to_wishlist")
    Call<AddToWishListModal> addToWishlist(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("user_id") String userID, @Field("product_id") String productID);


    @FormUrlEncoded
    @POST("remove_from_wishlist")
    Call<RemoveFromWishlistModal> removeFromWishlist(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("user_id") String userID, @Field("product_id") String productID);

    @GET("category_detail/{category_id}")
    Call<ProductCategoryModal> getProductCategoryDetails(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("category_id") String categoryID);

    @GET("subcategory_detail/{category_id}")
    Call<SubProductCategoryListModal> getSubProductCategoryDetails(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("category_id") String categoryID);


    @GET("endcategory_detail/{category_id}")
    Call<SubSubProductCategoryDetailModal> getSubSubProductCategoryDetails(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("category_id") String categoryID);

    @GET("carts_by_user/{user_id}")
    Call<CartsByUserModal> getCartsByUser(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("user_id") String userID);

    @FormUrlEncoded
    @POST("delete_item")
    Call<DeleteCartItemModal> deleteCartItem(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("item_id") String itemID);

    @FormUrlEncoded
    @POST("place_order")
    Call<PlaceOrderModal> placeOrder(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("cart_id") String cartID);

    @FormUrlEncoded
    @POST("update_item")
    Call<UpdateCartItemModal> updateCartItem(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @FieldMap Map<String, String> params);

    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchByBrand(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword, @Query("brand") String brand);

    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchByMaterial(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword, @Query("material") String material);

    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchByBrandHighToLow(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword, @Query("brand") String brand, @Query("high") String high);

    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchByBrandLowToHigh(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword, @Query("brand") String brand, @Query("less") String low);

    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchByMaterialHighToLow(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword, @Query("material") String material, @Query("high") String high);

    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchByMaterialLowToHigh(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword, @Query("material") String material, @Query("high") String low);

    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchByNewArrivalLow(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword, @Query("less") String less);

    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchByNewArrivalHigh(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword, @Query("high") String high);


    @GET("product_search" + "?" + "{keyword}" + "&newarrival")
    Call<ProductSearchBrandModal> searchByNewArrival(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("keyword") String keyword);


    @GET("get_status/{order_id}")
    Call<OrderConfirmationModal> getOrderConfirmation(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("order_id") String orderID);

    @FormUrlEncoded
    @POST("update_user")
    Call<UpdateUserModal> updateUserProfile(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @FieldMap Map<String, String> params);


    @FormUrlEncoded
    @POST("wishlist")
    Call<WishlistModal> getWishlist(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("user_id") String userID);


    @GET("product_search" + "?")
    Call<ProductSearchBrandModal> searchProducts(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Query("query") String keyword);


    @GET("main_categories")
    Call<MainCategoriesModal> getMainCategories(@Header("apiKey") String sessionKey, @Header("user_id") String user_id);

    @GET("get_item_count/{cart_id}")
    Call<CartItemCountModal> getCartItemCount(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("cart_id") String cartID);

    @FormUrlEncoded
    @POST("search")
    Call<SearchModal> searchProducts(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("category_search")
    Call<SearchModal> sortFilterCategoryProducts(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @FieldMap Map<String, String> params);


    @FormUrlEncoded
    @POST("set_language")
    Call<LanguageSelectModal> selectLanguage(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("language") String language, @Field("user_id") String userID);

    @FormUrlEncoded
    @POST("check_promocode")
    Call<PromoCodeModal> setPromoCode(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Field("cart_id") String cartID, @Field("promocode") String promoCode);

    @GET("color_list/{product_id}")
    Call<ProductColourOptionsModal> getProductColours(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("product_id") String ProductID);

    @GET("get_product_image/{colour_id}/{product_id}")
    Call<GetProductImagesWithColourModal> getProductImagesAccToColour(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @Path("colour_id") String colourID, @Path("product_id") String productID);


    @FormUrlEncoded
    @POST("user_login")
    Call<LoginModal> userLogin(@Header("apiKey") String sessionKey, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("social_login")
    Call<SocialUserLoginModal> userSocialLogin(@Header("apiKey") String sessionKey, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("user_register")
    Call<RegisterModal> registerUser(@Header("apiKey") String sessionKey, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("contact_us")
    Call<ContactUsModal> contactUs(@Header("apiKey") String sessionKey, @Header("user_id") String user_id, @FieldMap Map<String, String> params);

    @GET("product_review/{product_id}")
    Call<RateAndReviewModel> getOneProductReviews(@Header("apiKey") String sessionkey, @Header("user_id") String user_id, @Path("product_id") String productID);

    @FormUrlEncoded
    @POST("product_rewiew_ratting")
    Call<RateProductModal> rateProduct(@Header("apiKey") String sessionkey, @Header("user_id") String user_id, @FieldMap Map<String, String> params);

    @GET("material_listing")
    Call<MaterialListingModal> getMaterialList(@Header("apiKey") String sessionKey, @Header("user_id") String userID);

    @GET("brand_listing")
    Call<BrandListModal> getBrandList(@Header("apiKey") String sessionKey, @Header("user_id") String userID);

    @GET("logout/{user_id}")
    Call<LogOutModal> logOut(@Header("apiKey") String sessionKey, @Header("user_id") String userID, @Path("user_id") String user_id);

    @FormUrlEncoded
    @POST("change_password")
    Call<ChangePasswordModal> changePassword(@Header("apiKey") String sessionKey, @Header("user_id") String userID, @FieldMap Map<String,String> params);

    @FormUrlEncoded
    @POST("forget_password")
    Call<ForgotPasswordModal> forgotPassword(@Header("apiKey") String sessionKey, @Header("user_id") String userID, @Field("email") String email);

}