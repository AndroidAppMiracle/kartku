package kartku.com.retrofit;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import kartku.com.MyOrders.modal.MyOrderModal;
import kartku.com.my_product_reviews_module.MyProductReviewsModal;
import kartku.com.orders_module.modal.OrderDetailsModal;
import kartku.com.rate_review.model.RateAndReviewModel;
import kartku.com.retrofit.modal.AddToCartNewModal;
import kartku.com.retrofit.modal.AddToWishListModal;
import kartku.com.retrofit.modal.BrandListModal;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.CartsByUserModal;
import kartku.com.retrofit.modal.CategoryProductsModal;
import kartku.com.retrofit.modal.ChangePasswordModal;
import kartku.com.retrofit.modal.ContactUsModal;
import kartku.com.retrofit.modal.DeleteCartItemModal;
import kartku.com.retrofit.modal.ForgotPasswordModal;
import kartku.com.retrofit.modal.GetProductImagesWithColourModal;
import kartku.com.retrofit.modal.HomeScreenModal;
import kartku.com.retrofit.modal.LanguageSelectModal;
import kartku.com.retrofit.modal.LogOutModal;
import kartku.com.retrofit.modal.LoginModal;
import kartku.com.retrofit.modal.MainCategoriesModal;
import kartku.com.retrofit.modal.MaterialListingModal;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.retrofit.modal.OneProductReviews;
import kartku.com.retrofit.modal.OrderConfirmationModal;
import kartku.com.retrofit.modal.PlaceOrderModal;
import kartku.com.retrofit.modal.ProductCategoryModal;
import kartku.com.retrofit.modal.ProductColourOptionsModal;
import kartku.com.retrofit.modal.ProductDetailModal;
import kartku.com.retrofit.modal.ProductSearchBrandModal;
import kartku.com.retrofit.modal.PromoCodeModal;
import kartku.com.retrofit.modal.RateProductModal;
import kartku.com.retrofit.modal.RegisterModal;
import kartku.com.retrofit.modal.RemoveFromWishlistModal;
import kartku.com.retrofit.modal.SearchModal;
import kartku.com.retrofit.modal.SellerOrdersModal;
import kartku.com.retrofit.modal.SocialUserLoginModal;
import kartku.com.retrofit.modal.SubProductCategoryListModal;
import kartku.com.retrofit.modal.SubSubProductCategoryDetailModal;
import kartku.com.retrofit.modal.UpdateCartItemModal;
import kartku.com.retrofit.modal.UpdateUserModal;
import kartku.com.retrofit.modal.WishlistModal;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kshitiz Bali on 1/20/2017.
 */

public class ServerAPI {

    private static ServerAPI ourInstance = new ServerAPI();
    private Retrofit mRetrofit;
    private APIReference apiReference;
    // private SharedPreferences sharedPrefs;
    final String authKey = "API000adsfYU765KARTKU2189076";
    //private String userID;

    public ServerAPI() {

        mRetrofit = new Retrofit.Builder()
                .baseUrl("http://kartku.com/api/")
                /*.baseUrl("http://dev.miracleglobal.com/kartku-php/apidev/")*/
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiReference = mRetrofit.create(APIReference.class);
        // SharedPreferences prefrence = new ObscuredSharedPreferences(getApplicationContext(), getApplicationContext().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        //userID = Session.get_userId(prefrence);
        //Log.i("headerUSerID", userID);
    }

    public static ServerAPI getInstance() {
        return ourInstance;
    }


    public void getMyOrders(final int tag, String userID, final ApiServerResponse apiServerResponse) {
        Call<MyOrderModal> call = apiReference.getMyOrders(authKey, userID, userID);
        call.enqueue(new Callback<MyOrderModal>() {
            @Override
            public void onResponse(Call<MyOrderModal> call, Response<MyOrderModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MyOrderModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getOrderDetail(final int tag, String userID, String orderID, final ApiServerResponse apiServerResponse) {

        Call<OrderDetailsModal> call = apiReference.getOrderDetail(authKey, userID, orderID);
        call.enqueue(new Callback<OrderDetailsModal>() {
            @Override
            public void onResponse(Call<OrderDetailsModal> call, Response<OrderDetailsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<OrderDetailsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getMyProductReviews(final int tag, String userID, final ApiServerResponse apiServerResponse) {
        Call<MyProductReviewsModal> call = apiReference.getMyProductReviews(authKey, userID, userID);
        call.enqueue(new Callback<MyProductReviewsModal>() {
            @Override
            public void onResponse(Call<MyProductReviewsModal> call, Response<MyProductReviewsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MyProductReviewsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getMyCartDetails(final int tag, String userID, String cartID, final ApiServerResponse apiServerResponse) {

        Call<MyCartDetailModal> call = apiReference.getMyCartDetails(authKey, userID, cartID);
        call.enqueue(new Callback<MyCartDetailModal>() {
            @Override
            public void onResponse(Call<MyCartDetailModal> call, Response<MyCartDetailModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MyCartDetailModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void addToCartNew(final int tag, String totalAmount, String product_id, String quantity, String user_id, String cart_id, String colourID, final ApiServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("total_amount", totalAmount);
        params.put("product_id", product_id);
        params.put("quantity", quantity);
        params.put("user_id", user_id);
        params.put("cart_id", cart_id);
        params.put("colour_id", colourID);
        Call<AddToCartNewModal> call = apiReference.addToCartNew(authKey, user_id, params);
        call.enqueue(new Callback<AddToCartNewModal>() {
            @Override
            public void onResponse(Call<AddToCartNewModal> call, Response<AddToCartNewModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AddToCartNewModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getSellerOrders(final int tag, String userID, String sellerID, final ApiServerResponse apiServerResponse) {

        Call<SellerOrdersModal> call = apiReference.getSellerOrders(authKey, userID, sellerID);
        call.enqueue(new Callback<SellerOrdersModal>() {
            @Override
            public void onResponse(Call<SellerOrdersModal> call, Response<SellerOrdersModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SellerOrdersModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getHomeScreen(final int tag, String userIHeader, final ApiServerResponse apiServerResponse) {
        Call<HomeScreenModal> call = apiReference.getHomeScreen(authKey, userIHeader);
        call.enqueue(new Callback<HomeScreenModal>() {
            @Override
            public void onResponse(Call<HomeScreenModal> call, Response<HomeScreenModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<HomeScreenModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getProductDetail(final int tag, String productID, String userId, final ApiServerResponse apiServerResponse) {
        Call<ProductDetailModal> call = apiReference.getProductDetail(authKey, userId, productID, userId);
        call.enqueue(new Callback<ProductDetailModal>() {
            @Override
            public void onResponse(Call<ProductDetailModal> call, Response<ProductDetailModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductDetailModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getCategoryProducts(final int tag, String categoryID, String userID, final ApiServerResponse apiServerResponse) {

        Call<CategoryProductsModal> call = apiReference.getCategoryProducts(authKey, userID, categoryID, userID);
        call.enqueue(new Callback<CategoryProductsModal>() {
            @Override
            public void onResponse(Call<CategoryProductsModal> call, Response<CategoryProductsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CategoryProductsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void addToWishlist(final int tag, String userID, String productID, final ApiServerResponse apiServerResponse) {
        Call<AddToWishListModal> call = apiReference.addToWishlist(authKey, userID, userID, productID);
        call.enqueue(new Callback<AddToWishListModal>() {
            @Override
            public void onResponse(Call<AddToWishListModal> call, Response<AddToWishListModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AddToWishListModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void removeFromWishlist(final int tag, String userID, String productId, final ApiServerResponse apiServerResponse) {
        Call<RemoveFromWishlistModal> call = apiReference.removeFromWishlist(authKey, userID, userID, productId);
        call.enqueue(new Callback<RemoveFromWishlistModal>() {
            @Override
            public void onResponse(Call<RemoveFromWishlistModal> call, Response<RemoveFromWishlistModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<RemoveFromWishlistModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getProductCategoryDetails(final int tag, String userID, String categoryID, final ApiServerResponse apiServerResponse) {
        Call<ProductCategoryModal> call = apiReference.getProductCategoryDetails(authKey, userID, categoryID);
        call.enqueue(new Callback<ProductCategoryModal>() {
            @Override
            public void onResponse(Call<ProductCategoryModal> call, Response<ProductCategoryModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductCategoryModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getSubProductCategoryDetails(final int tag, String userID, String categoryID, final ApiServerResponse apiServerResponse) {

        Call<SubProductCategoryListModal> call = apiReference.getSubProductCategoryDetails(authKey, userID, categoryID);
        call.enqueue(new Callback<SubProductCategoryListModal>() {
            @Override
            public void onResponse(Call<SubProductCategoryListModal> call, Response<SubProductCategoryListModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SubProductCategoryListModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getSubSubProductDetailCategoryDetail(final int tag, String userID, String categoryID, final ApiServerResponse apiServerResponse) {
        Call<SubSubProductCategoryDetailModal> call = apiReference.getSubSubProductCategoryDetails(authKey, userID, categoryID);
        call.enqueue(new Callback<SubSubProductCategoryDetailModal>() {
            @Override
            public void onResponse(Call<SubSubProductCategoryDetailModal> call, Response<SubSubProductCategoryDetailModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SubSubProductCategoryDetailModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getCartsByUser(final int tag, String userID, final ApiServerResponse apiServerResponse) {
        Call<CartsByUserModal> call = apiReference.getCartsByUser(authKey, userID, userID);
        call.enqueue(new Callback<CartsByUserModal>() {
            @Override
            public void onResponse(Call<CartsByUserModal> call, Response<CartsByUserModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CartsByUserModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void deleteCartItem(final int tag, String userID, String itemID, final ApiServerResponse apiServerResponse) {

        Call<DeleteCartItemModal> call = apiReference.deleteCartItem(authKey, userID, itemID);
        call.enqueue(new Callback<DeleteCartItemModal>() {
            @Override
            public void onResponse(Call<DeleteCartItemModal> call, Response<DeleteCartItemModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<DeleteCartItemModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void placeOrder(final int tag, String userID, String cartID, final ApiServerResponse apiServerResponse) {

        Call<PlaceOrderModal> call = apiReference.placeOrder(authKey, userID, cartID);
        call.enqueue(new Callback<PlaceOrderModal>() {
            @Override
            public void onResponse(Call<PlaceOrderModal> call, Response<PlaceOrderModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PlaceOrderModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void updateCartItem(final int tag, String userID, String cartID, String quantity, String productID, final ApiServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("cart_id", cartID);
        params.put("quantity", quantity);
        params.put("product_id", productID);


        Call<UpdateCartItemModal> call = apiReference.updateCartItem(authKey, userID, params);

        call.enqueue(new Callback<UpdateCartItemModal>() {
            @Override
            public void onResponse(Call<UpdateCartItemModal> call, Response<UpdateCartItemModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<UpdateCartItemModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


   /* public void searchByBrand(final int tag,String userID, String keyword, String brand, final ApiServerResponse apiServerResponse) {

        Call<ProductSearchBrandModal> call = apiReference.searchByBrand(authKey, keyword, brand);

        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void searchByBrandHigh(final int tag,String userID, String keyword, String brand, final ApiServerResponse apiServerResponse) {
        Call<ProductSearchBrandModal> call = apiReference.searchByBrandHighToLow(authKey, keyword, brand, "high");
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void searchByBrandLow(final int tag,String userID, String keyword, String brand, final ApiServerResponse apiServerResponse) {
        Call<ProductSearchBrandModal> call = apiReference.searchByBrandLowToHigh(authKey,userID, keyword, brand, "less");
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void searchByMaterial(final int tag,String userID, String keyword, String material, final ApiServerResponse apiServerResponse) {

        Call<ProductSearchBrandModal> call = apiReference.searchByMaterial(authKey, keyword, material);
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void searchByMaterialHigh(final int tag,String userID, String keyword, String material, final ApiServerResponse apiServerResponse) {

        Call<ProductSearchBrandModal> call = apiReference.searchByMaterialHighToLow(authKey, keyword, material, "high");
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void searchByMaterialLow(final int tag, String keyword, String material, final ApiServerResponse apiServerResponse) {
        Call<ProductSearchBrandModal> call = apiReference.searchByMaterialHighToLow(authKey, keyword, material, "less");
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void searchByNewArriaval(final int tag, String keyword, final ApiServerResponse apiServerResponse) {
        Call<ProductSearchBrandModal> call = apiReference.searchByNewArrivalLow(authKey, keyword + "&newarrival", "");
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }


    public void searchByNewArriavalLow(final int tag, String keyword, final ApiServerResponse apiServerResponse) {

        Call<ProductSearchBrandModal> call = apiReference.searchByNewArrivalLow(authKey, keyword + "&newarrival", "less");
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void searchByNewArriavalHigh(final int tag, String keyword, final ApiServerResponse apiServerResponse) {

        Call<ProductSearchBrandModal> call = apiReference.searchByNewArrivalHigh(authKey, keyword + "&newarrival", "high");
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }*/


    public void getOrderConfirmation(final int tag, String userID, String orderID, final ApiServerResponse apiServerResponse) {

        Call<OrderConfirmationModal> call = apiReference.getOrderConfirmation(authKey, userID, orderID);
        call.enqueue(new Callback<OrderConfirmationModal>() {
            @Override
            public void onResponse(Call<OrderConfirmationModal> call, Response<OrderConfirmationModal> response) {
                apiServerResponse.onSuccess(tag, response);

            }

            @Override
            public void onFailure(Call<OrderConfirmationModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);

            }
        });

    }

    public void updateUserProfile(final int tag, String address, String userID, String firstName, String lastName, String mobileNo, final ApiServerResponse apiServerResponse) {


        Map<String, String> params = new HashMap<>();
        params.put("Profile[address]", address);
        params.put("Profile[user_id]", userID);
        params.put("Profile[first_name]", firstName);
        params.put("Profile[last_name]", lastName);
        params.put("Profile[mobile_number]", mobileNo);

        Call<UpdateUserModal> call = apiReference.updateUserProfile(authKey, userID, params);
        call.enqueue(new Callback<UpdateUserModal>() {
            @Override
            public void onResponse(Call<UpdateUserModal> call, Response<UpdateUserModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<UpdateUserModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void getWishlist(final int tag, String userID, final ApiServerResponse apiServerResponse) {

        Call<WishlistModal> call = apiReference.getWishlist(authKey, userID, userID);

        call.enqueue(new Callback<WishlistModal>() {
            @Override
            public void onResponse(Call<WishlistModal> call, Response<WishlistModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<WishlistModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void searchProducts(final int tag, String userID, String keiword, final ApiServerResponse apiServerResponse) {


        Call<ProductSearchBrandModal> call = apiReference.searchProducts(authKey, userID, keiword);
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void searchByNewArrivalOnly(final int tag, String userID, String keyword, final ApiServerResponse apiServerResponse) {

        Call<ProductSearchBrandModal> call = apiReference.searchByNewArrival(authKey, userID, keyword);
        call.enqueue(new Callback<ProductSearchBrandModal>() {
            @Override
            public void onResponse(Call<ProductSearchBrandModal> call, Response<ProductSearchBrandModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductSearchBrandModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getMainCategories(final int tag, String userID, final ApiServerResponse apiServerResponse) {

        Call<MainCategoriesModal> call = apiReference.getMainCategories(authKey, userID);
        call.enqueue(new Callback<MainCategoriesModal>() {
            @Override
            public void onResponse(Call<MainCategoriesModal> call, Response<MainCategoriesModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MainCategoriesModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getCartItemCount(final int tag, String userID, String cartID, final ApiServerResponse apiServerResponse) {
        Call<CartItemCountModal> call = apiReference.getCartItemCount(authKey, userID, cartID);
        call.enqueue(new Callback<CartItemCountModal>() {
            @Override
            public void onResponse(Call<CartItemCountModal> call, Response<CartItemCountModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CartItemCountModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void searchSortFilterProducts(final int tag, String title, String brand, String mat, String price, String newArrival, String userID, final ApiServerResponse apiServerResponse) {


        Map<String, String> params = new HashMap<>();
        params.put("title", title);
        params.put("brand", brand);
        params.put("mat", mat);
        params.put("price", price);
        params.put("new", newArrival);
        params.put("user_id", userID);

        Call<SearchModal> call = apiReference.searchProducts(authKey, userID, params);
        call.enqueue(new Callback<SearchModal>() {
            @Override
            public void onResponse(Call<SearchModal> call, Response<SearchModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SearchModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void sortFilterCategoryProducts(final int tag, String categoryID, String brand, String mat, String price, String newArrival,final String userID, final ApiServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("category_id", categoryID);
        params.put("brand", brand);
        params.put("mat", mat);
        params.put("price", price);
        params.put("new", newArrival);
        params.put("user_id", userID);

        Call<SearchModal> call = apiReference.sortFilterCategoryProducts(authKey, userID, params);
        call.enqueue(new Callback<SearchModal>() {
            @Override
            public void onResponse(Call<SearchModal> call, Response<SearchModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SearchModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void selectLanguage(final int tag, String languageID, String userID, final ApiServerResponse apiServerResponse) {

        Call<LanguageSelectModal> call = apiReference.selectLanguage(authKey, userID, languageID, userID);

        call.enqueue(new Callback<LanguageSelectModal>() {
            @Override
            public void onResponse(Call<LanguageSelectModal> call, Response<LanguageSelectModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<LanguageSelectModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void setPromoCode(final int tag, String userID, String cartID, String promoCode, final ApiServerResponse apiServerResponse) {

        Call<PromoCodeModal> call = apiReference.setPromoCode(authKey, userID, cartID, promoCode);
        call.enqueue(new Callback<PromoCodeModal>() {
            @Override
            public void onResponse(Call<PromoCodeModal> call, Response<PromoCodeModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PromoCodeModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getProductColour(final int tag, String userID, String productID, final ApiServerResponse apiServerResponse) {
        Call<ProductColourOptionsModal> call = apiReference.getProductColours(authKey, userID, productID);
        call.enqueue(new Callback<ProductColourOptionsModal>() {
            @Override
            public void onResponse(Call<ProductColourOptionsModal> call, Response<ProductColourOptionsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProductColourOptionsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getProductImagesAccToColour(final int tag, String userID, String colourID, String productID, final ApiServerResponse apiServerResponse) {
        Call<GetProductImagesWithColourModal> call = apiReference.getProductImagesAccToColour(authKey, userID, colourID, productID);
        call.enqueue(new Callback<GetProductImagesWithColourModal>() {
            @Override
            public void onResponse(Call<GetProductImagesWithColourModal> call, Response<GetProductImagesWithColourModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GetProductImagesWithColourModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void userLogin(final int tag, String email, String password, String deviceID, final ApiServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        params.put("device_id", deviceID);
        params.put("device_type", "1");

        Call<LoginModal> call = apiReference.userLogin(authKey, params);

        call.enqueue(new Callback<LoginModal>() {
            @Override
            public void onResponse(Call<LoginModal> call, Response<LoginModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<LoginModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void userSocialLogin(final int tag, String email,String firstName,String lastName,String userName,String deviceID, final ApiServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", "");
        params.put("device_id", deviceID);
        params.put("device_type", "1");
        params.put("firstname",firstName);
        params.put("lastname",lastName);
        params.put("username",userName);

        Call<SocialUserLoginModal> call = apiReference.userSocialLogin(authKey, params);
        call.enqueue(new Callback<SocialUserLoginModal>() {
            @Override
            public void onResponse(Call<SocialUserLoginModal> call, Response<SocialUserLoginModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SocialUserLoginModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void registerUser(final int tag, String firstName, String lastName, String mobile, String email, String password, String deviceId, final ApiServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        params.put("mobile", mobile);
        params.put("email", email);
        params.put("password", password);
        params.put("device_id", deviceId);
        params.put("device_type", "1");

        Call<RegisterModal> call = apiReference.registerUser(authKey, params);
        call.enqueue(new Callback<RegisterModal>() {
            @Override
            public void onResponse(Call<RegisterModal> call, Response<RegisterModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<RegisterModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }


    public void contactUs(final int tag, String userID, String name, String email, String subject, String message, final ApiServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("email", email);
        params.put("subject", subject);
        params.put("message", message);

        Call<ContactUsModal> call = apiReference.contactUs(authKey, userID, params);
        call.enqueue(new Callback<ContactUsModal>() {
            @Override
            public void onResponse(Call<ContactUsModal> call, Response<ContactUsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ContactUsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void getOneProductReviews(final int tag, String userID, String productID, final ApiServerResponse apiServerResponse) {
        Call<RateAndReviewModel> call = apiReference.getOneProductReviews(authKey, userID, productID);
        call.enqueue(new Callback<RateAndReviewModel>() {
            @Override
            public void onResponse(Call<RateAndReviewModel> call, Response<RateAndReviewModel> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<RateAndReviewModel> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void rateProduct(final int tag, String userID, String rating, String productId, String review, final ApiServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", userID);
        params.put("ratting", rating);
        params.put("product_id", productId);
        params.put("review", review);


        Call<RateProductModal> call = apiReference.rateProduct(authKey, userID, params);
        call.enqueue(new Callback<RateProductModal>() {
            @Override
            public void onResponse(Call<RateProductModal> call, Response<RateProductModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<RateProductModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getMaterialList(final int tag, String userID, final ApiServerResponse apiServerResponse) {

        Call<MaterialListingModal> call = apiReference.getMaterialList(authKey, userID);

        call.enqueue(new Callback<MaterialListingModal>() {
            @Override
            public void onResponse(Call<MaterialListingModal> call, Response<MaterialListingModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MaterialListingModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getBrandList(final int tag, String userID, final ApiServerResponse apiServerResponse) {

        Call<BrandListModal> call = apiReference.getBrandList(authKey, userID);
        call.enqueue(new Callback<BrandListModal>() {
            @Override
            public void onResponse(Call<BrandListModal> call, Response<BrandListModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BrandListModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void logout(final int tag, String userID, final ApiServerResponse apiServerResponse) {

        Call<LogOutModal> call = apiReference.logOut(authKey, userID, userID);
        call.enqueue(new Callback<LogOutModal>() {
            @Override
            public void onResponse(Call<LogOutModal> call, Response<LogOutModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<LogOutModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void changePassword(final int tag, String user_id, String oldPass, String newPass, final ApiServerResponse apiServerResponse) {


        Map<String, String> params = new HashMap<>();
        params.put("user_id", user_id);
        params.put("current_password", oldPass);
        params.put("new_password", newPass);
        Call<ChangePasswordModal> call = apiReference.changePassword(authKey, user_id, params);
        call.enqueue(new Callback<ChangePasswordModal>() {
            @Override
            public void onResponse(Call<ChangePasswordModal> call, Response<ChangePasswordModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ChangePasswordModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void forgotPassword(final int tag, String userID, String email, final ApiServerResponse apiServerResponse) {

        Call<ForgotPasswordModal> call = apiReference.forgotPassword(authKey, userID, email);
        call.enqueue(new Callback<ForgotPasswordModal>() {
            @Override
            public void onResponse(Call<ForgotPasswordModal> call, Response<ForgotPasswordModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ForgotPasswordModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

}
