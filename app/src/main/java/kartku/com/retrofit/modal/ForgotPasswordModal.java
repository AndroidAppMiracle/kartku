package kartku.com.retrofit.modal;

/**
 * Created by satoti.garg on 8/11/2017.
 */

public class ForgotPasswordModal {


    /**
     * status : OK
     * message : An email has been sent with instructions for resetting your password
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
