package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 3/28/2017.
 */

public class OrderConfirmationModal {


    /**
     * status : OK
     * detail : {"transaction_id":"65cf3685-f282-4b04-bcd1-5a96bca046e7","order_id":"14","amount":"4360.00","transaction_status":"capture","transaction_time":"2017-03-28 19:06:41","payment_type":"credit_card","masked_card":"481111-1114","bank":"mandiri"}
     */

    private String status;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * transaction_id : 65cf3685-f282-4b04-bcd1-5a96bca046e7
         * order_id : 14
         * amount : 4360.00
         * transaction_status : capture
         * transaction_time : 2017-03-28 19:06:41
         * payment_type : credit_card
         * masked_card : 481111-1114
         * bank : mandiri
         */

        private String transaction_id;
        private String order_id;
        private String amount;
        private String transaction_status;
        private String transaction_time;
        private String payment_type;
        private String masked_card;
        private String bank;

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTransaction_status() {
            return transaction_status;
        }

        public void setTransaction_status(String transaction_status) {
            this.transaction_status = transaction_status;
        }

        public String getTransaction_time() {
            return transaction_time;
        }

        public void setTransaction_time(String transaction_time) {
            this.transaction_time = transaction_time;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getMasked_card() {
            return masked_card;
        }

        public void setMasked_card(String masked_card) {
            this.masked_card = masked_card;
        }

        public String getBank() {
            return bank;
        }

        public void setBank(String bank) {
            this.bank = bank;
        }
    }
}
