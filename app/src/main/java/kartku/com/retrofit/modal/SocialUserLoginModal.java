package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/19/2017.
 */

public class SocialUserLoginModal  {


    /**
     * Status : OK
     * message : Logged in successfully
     * user_profile_data : {"user_id":287,"username":"abcd33","language":"1","email":"abcd@abcd.com","device_id":null,"device_type":null,"created_at":1492741500,"login_type":"Social_Login"}
     */


    private String Status;
    private String message;
    private UserDataBean user_data;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDataBean getUser_profile_data() {
        return user_data;
    }

    public void setUser_profile_data(UserDataBean user_profile_data) {
        this.user_data = user_profile_data;
    }

    public static class UserDataBean {
        /**
         * user_id : 287
         * username : abcd33
         * language : 1
         * email : abcd@abcd.com
         * device_id : null
         * device_type : null
         * created_at : 1492741500
         * login_type : Social_Login
         */
        private int user_id;
        private String username;
        private String language;
        private String email;
        private Object device_id;
        private Object device_type;
        private int created_at;
        private String login_type;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getDevice_id() {
            return device_id;
        }

        public void setDevice_id(Object device_id) {
            this.device_id = device_id;
        }

        public Object getDevice_type() {
            return device_type;
        }

        public void setDevice_type(Object device_type) {
            this.device_type = device_type;
        }

        public int getCreated_at() {
            return created_at;
        }

        public void setCreated_at(int created_at) {
            this.created_at = created_at;
        }

        public String getLogin_type() {
            return login_type;
        }

        public void setLogin_type(String login_type) {
            this.login_type = login_type;
        }
    }
}
