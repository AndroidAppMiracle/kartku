package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/20/2017.
 */

public class RateProductModal {

    /**
     * status : OK
     * message : Ratting and reviews saved.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
