package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/23/2017.
 */

public class MyCartDetailModal {


    /**
     * status : OK
     * cart_info : {"id":49,"user_id":271,"session_id":null,"promocode_id":null,"discount":"0","total_amount":"2000.0000","order_status":"248","created_at":"2017-04-24","updated_at":null}
     * promo_info : {"is_promocode":"0","promocode_id":0,"name":"","discount":"0","final_amount":"2000.0000"}
     * items : [{"id":204,"cart_id":49,"product_id":7,"quantity":"1","colour":"White","shipping_status":"Confirmed","total_price":"2000"}]
     * products : [{"pid":7,"product_name":"","price":"2000.0000","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/4dVPGYORqcou4f35_J_UAvCu5sfJ_0CI.jpg","brand":"Furnicheer","category":"Ruang keluarga","delivery_time":"24 April","colours":"White","material":"Sheesham","height":"","width":"","depth":"","weight":"2","wishlist":"1","promo_discount_value":"","max_quantity":"13"}]
     */

    private String status;
    private CartInfoBean cart_info;
    private PromoInfoBean promo_info;
    private List<ItemsBean> items;
    private List<ProductsBean> products;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CartInfoBean getCart_info() {
        return cart_info;
    }

    public void setCart_info(CartInfoBean cart_info) {
        this.cart_info = cart_info;
    }

    public PromoInfoBean getPromo_info() {
        return promo_info;
    }

    public void setPromo_info(PromoInfoBean promo_info) {
        this.promo_info = promo_info;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public List<ProductsBean> getProducts() {
        return products;
    }

    public void setProducts(List<ProductsBean> products) {
        this.products = products;
    }

    public static class CartInfoBean {
        /**
         * id : 49
         * user_id : 271
         * session_id : null
         * promocode_id : null
         * discount : 0
         * total_amount : 2000.0000
         * order_status : 248
         * created_at : 2017-04-24
         * updated_at : null
         */

        private int id;
        private int user_id;
        private Object session_id;
        private Object promocode_id;
        private String discount;
        private String total_amount;
        private String order_status;
        private String created_at;
        private Object updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public Object getSession_id() {
            return session_id;
        }

        public void setSession_id(Object session_id) {
            this.session_id = session_id;
        }

        public Object getPromocode_id() {
            return promocode_id;
        }

        public void setPromocode_id(Object promocode_id) {
            this.promocode_id = promocode_id;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }
    }

    public static class PromoInfoBean {
        /**
         * is_promocode : 0
         * promocode_id : 0
         * name :
         * discount : 0
         * final_amount : 2000.0000
         */

        private String is_promocode;
        private int promocode_id;
        private String name;
        private String discount;
        private String final_amount;

        public String getIs_promocode() {
            return is_promocode;
        }

        public void setIs_promocode(String is_promocode) {
            this.is_promocode = is_promocode;
        }

        public int getPromocode_id() {
            return promocode_id;
        }

        public void setPromocode_id(int promocode_id) {
            this.promocode_id = promocode_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getFinal_amount() {
            return final_amount;
        }

        public void setFinal_amount(String final_amount) {
            this.final_amount = final_amount;
        }
    }

    public static class ItemsBean {
        /**
         * id : 204
         * cart_id : 49
         * product_id : 7
         * quantity : 1
         * colour : White
         * shipping_status : Confirmed
         * total_price : 2000
         */

        private int id;
        private int cart_id;
        private int product_id;
        private String quantity;
        private String colour;
        private String shipping_status;
        private String total_price;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCart_id() {
            return cart_id;
        }

        public void setCart_id(int cart_id) {
            this.cart_id = cart_id;
        }

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getColour() {
            return colour;
        }

        public void setColour(String colour) {
            this.colour = colour;
        }

        public String getShipping_status() {
            return shipping_status;
        }

        public void setShipping_status(String shipping_status) {
            this.shipping_status = shipping_status;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }
    }

    public static class ProductsBean {
        /**
         * pid : 7
         * product_name :
         * price : 2000.0000
         * product_image : http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/4dVPGYORqcou4f35_J_UAvCu5sfJ_0CI.jpg
         * brand : Furnicheer
         * category : Ruang keluarga
         * delivery_time : 24 April
         * colours : White
         * material : Sheesham
         * height :
         * width :
         * depth :
         * weight : 2
         * wishlist : 1
         * promo_discount_value :
         * max_quantity : 13
         */

        private int pid;
        private String product_name;
        private String price;
        private String product_image;
        private String brand;
        private String category;
        private String delivery_time;
        private String colours;
        private String material;
        private String height;
        private String width;
        private String depth;
        private String weight;
        private String wishlist;
        private String promo_discount_value;
        private String max_quantity;

        public int getPid() {
            return pid;
        }

        public void setPid(int pid) {
            this.pid = pid;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getDelivery_time() {
            return delivery_time;
        }

        public void setDelivery_time(String delivery_time) {
            this.delivery_time = delivery_time;
        }

        public String getColours() {
            return colours;
        }

        public void setColours(String colours) {
            this.colours = colours;
        }

        public String getMaterial() {
            return material;
        }

        public void setMaterial(String material) {
            this.material = material;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getDepth() {
            return depth;
        }

        public void setDepth(String depth) {
            this.depth = depth;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getWishlist() {
            return wishlist;
        }

        public void setWishlist(String wishlist) {
            this.wishlist = wishlist;
        }

        public String getPromo_discount_value() {
            return promo_discount_value;
        }

        public void setPromo_discount_value(String promo_discount_value) {
            this.promo_discount_value = promo_discount_value;
        }

        public String getMax_quantity() {
            return max_quantity;
        }

        public void setMax_quantity(String max_quantity) {
            this.max_quantity = max_quantity;
        }
    }
}
