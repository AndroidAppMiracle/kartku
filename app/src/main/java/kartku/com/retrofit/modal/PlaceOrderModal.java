package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 3/18/2017.
 */

public class PlaceOrderModal {


    /**
     * status : OK
     * message : Order placed successfully
     * order_detail : {"id":2,"user_id":null,"cart_id":1,"status":"PAYMENT_PENDING","total_amount":"0.0000","shipping_cost":"","tax":null,"grand_total":0,"tracking_id":"","created_at":"2017-03-24"}
     */

    private String status;
    private String message;
    private OrderDetailBean order_detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrderDetailBean getOrder_detail() {
        return order_detail;
    }

    public void setOrder_detail(OrderDetailBean order_detail) {
        this.order_detail = order_detail;
    }

    public static class OrderDetailBean {
        /**
         * id : 2
         * user_id : null
         * cart_id : 1
         * status : PAYMENT_PENDING
         * total_amount : 0.0000
         * shipping_cost :
         * tax : null
         * grand_total : 0
         * tracking_id :
         * created_at : 2017-03-24
         */

        private int id;
        private Object user_id;
        private int cart_id;
        private String status;
        private String total_amount;
        private String shipping_cost;
        private Object tax;
        private int grand_total;
        private String tracking_id;
        private String created_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getUser_id() {
            return user_id;
        }

        public void setUser_id(Object user_id) {
            this.user_id = user_id;
        }

        public int getCart_id() {
            return cart_id;
        }

        public void setCart_id(int cart_id) {
            this.cart_id = cart_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getShipping_cost() {
            return shipping_cost;
        }

        public void setShipping_cost(String shipping_cost) {
            this.shipping_cost = shipping_cost;
        }

        public Object getTax() {
            return tax;
        }

        public void setTax(Object tax) {
            this.tax = tax;
        }

        public int getGrand_total() {
            return grand_total;
        }

        public void setGrand_total(int grand_total) {
            this.grand_total = grand_total;
        }

        public String getTracking_id() {
            return tracking_id;
        }

        public void setTracking_id(String tracking_id) {
            this.tracking_id = tracking_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
