package kartku.com.retrofit.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/14/2017.
 */

public class HomeScreenModal {


    /**
     * status : OK
     * banner : {"url":"http://dev.miracleglobal.com/kartku-php/web/uploads/banner/GeQbRTc3rDzI6Syh4oe2KuFPe94rpo05.jpg"}
     * category_list : [{"id":1,"title":"Furniture","description":"Furniture","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/Ao_nrH0Jgv5JsD2A8CVf-T1S52-6MEri.png","is_active":"1","is_subcategory":"1"},{"id":2,"title":"Home Decor","description":"Home Decor","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/PBgTJ9W4gZLN7Foo4O2uYvIDBq0b-Tyv.png","is_active":"1","is_subcategory":"1"},{"id":3,"title":"Appliances","description":"Appliances","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/e5kOfg2xPYOwKJWOcYPzaq12qlFg4D-y.png","is_active":"1","is_subcategory":"1"},{"id":4,"title":"Furnishings","description":"Furnishings","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/Eoy2FZbZsmk9GwAt6NwoO84JZ8ajuam_.png","is_active":"1","is_subcategory":"1"},{"id":5,"title":"Housekeeping","description":"Housekeeping","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/kSUbB_FMtuvwOYB4RNwp0tqP6kLO_Hjq.png","is_active":"1","is_subcategory":"1"},{"id":6,"title":"Kitchen & Dining","description":"Kitchen & Dining","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/gh6frCKORsfqjrYF_ews_Sk6deGRA1DG.png","is_active":"1","is_subcategory":"1"},{"id":7,"title":"Lamps & Lighting","description":"Lamps & Lighting","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/Gn5cX4nbWabXHiHKFZ77JS7Rn3la_YzT.png","is_active":"1","is_subcategory":"1"},{"id":8,"title":"Bath","description":"Bath","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/nZjsXmEFa8h8vzwHBKATZ8t10H3AGdj4.png","is_active":"1","is_subcategory":"1"},{"id":16,"title":"Office Furniture","description":"Office Furniture","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/hJu9JibuhejbWhwCIJRx0Ka0CCpafUVm.png","is_active":"1","is_subcategory":"1"}]
     * sub_categories : [{"id":9,"title":"Living Room","description":"Living Room","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/SaHtD24aqfrR78nF75hZeWPB5FSvkISa.jpg","is_active":"1","is_subcategory":"1","item_count":"1","start_range":100},{"id":10,"title":"Bed Room","description":"Bed Room","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/NCf-ItSDCrjEa71j5h3tyw8WVWkO7gJk.jpg","is_active":"1","is_subcategory":"1","item_count":"1","start_range":700},{"id":22,"title":"APPLIANCES1","description":"Test appliances1","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/80GR7MTboy4PwroA260akqaT4YQgBOi6.jpg","is_active":"1","is_subcategory":"0","item_count":"2","start_range":10},{"id":26,"title":"Curtains","description":"Curtains","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/8q7vBoSebJBk1u68KvduWzo8ud7nTXlS.jpg","is_active":"1","is_subcategory":"0","item_count":"1","start_range":400},{"id":28,"title":"Kitchen Shelves","description":"Kitchen Shelves","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/2bMPrwyCKNGcjZQlq6g-enJOFY2NhuQB.jpg","is_active":"1","is_subcategory":"0","item_count":"1","start_range":200},{"id":30,"title":"LAMP AND LIGHTING1","description":"Test lamp and lighting1","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/3gjdudCggLdTV23oi1_scziVAL7uX-in.jpg","is_active":"1","is_subcategory":"0","item_count":"4","start_range":2000},{"id":35,"title":"Office Chairs","description":"Office Chairs","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/i7VruBrNAzX6_D4jxxb1_n-Rop_6oTHA.jpg","is_active":"1","is_subcategory":"0","item_count":"0","start_range":4500}]
     */

    private String status;
    private BannerBean banner;
    private List<CategoryListBean> category_list;
    private List<SubCategoriesBean> sub_categories;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BannerBean getBanner() {
        return banner;
    }

    public void setBanner(BannerBean banner) {
        this.banner = banner;
    }

    public List<CategoryListBean> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(List<CategoryListBean> category_list) {
        this.category_list = category_list;
    }

    public List<SubCategoriesBean> getSub_categories() {
        return sub_categories;
    }

    public void setSub_categories(List<SubCategoriesBean> sub_categories) {
        this.sub_categories = sub_categories;
    }

    public static class BannerBean {
        /**
         * url : http://dev.miracleglobal.com/kartku-php/web/uploads/banner/GeQbRTc3rDzI6Syh4oe2KuFPe94rpo05.jpg
         */

        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class CategoryListBean  implements Parcelable {
        /**
         * id : 1
         * title : Furniture
         * description : Furniture
         * image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/Ao_nrH0Jgv5JsD2A8CVf-T1S52-6MEri.png
         * is_active : 1
         * is_subcategory : 1
         */

        protected CategoryListBean(Parcel in) {
            id = in.readInt();
            title = in.readString();
            description = in.readString();
            image = in.readString();
            is_subcategory = in.readString();
        }
        public static final Creator<CategoryListBean> CREATOR = new Creator<CategoryListBean>() {
            @Override
            public CategoryListBean createFromParcel(Parcel in) {
                return new CategoryListBean(in);
            }

            @Override
            public CategoryListBean[] newArray(int size) {
                return new CategoryListBean[size];
            }
        };
        private int id;
        private String title;
        private String description;
        private String image;
        private String is_active;
        private String is_subcategory;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getIs_subcategory() {
            return is_subcategory;
        }

        public void setIs_subcategory(String is_subcategory) {
            this.is_subcategory = is_subcategory;
        }
        @Override
        public int describeContents() {
            return 0;
        }
        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(title);
            dest.writeString(description);
            dest.writeString(image);
            dest.writeString(is_subcategory);
        }
    }

    public static class SubCategoriesBean {
        /**
         * id : 9
         * title : Living Room
         * description : Living Room
         * image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/SaHtD24aqfrR78nF75hZeWPB5FSvkISa.jpg
         * is_active : 1
         * is_subcategory : 1
         * item_count : 1
         * start_range : 100
         */

        private int id;
        private String title;
        private String description;
        private String image;
        private String is_active;
        private String is_subcategory;
        private String item_count;
        private int start_range;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getIs_subcategory() {
            return is_subcategory;
        }

        public void setIs_subcategory(String is_subcategory) {
            this.is_subcategory = is_subcategory;
        }

        public String getItem_count() {
            return item_count;
        }

        public void setItem_count(String item_count) {
            this.item_count = item_count;
        }

        public int getStart_range() {
            return start_range;
        }

        public void setStart_range(int start_range) {
            this.start_range = start_range;
        }
    }
}
