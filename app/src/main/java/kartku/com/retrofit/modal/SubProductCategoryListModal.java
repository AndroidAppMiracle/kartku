package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitizb on 15-03-2017.
 */

public class SubProductCategoryListModal {


    /**
     * status : OK
     * subcategory_detail : {"id":9,"title":"Living Room","description":"Living Room","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/SaHtD24aqfrR78nF75hZeWPB5FSvkISa.jpg","is_active":"1","is_subcategory":"1","subcategories":[{"id":11,"title":"Sofas","description":"Sofas","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/77F8BbDFaNj6UJWzsVh9mJus4WlzALoQ.jpg","is_active":"1","is_subcategory":"1"},{"id":36,"title":"Recliners","description":"Recliners","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb","is_active":"1","is_subcategory":"1"}],"item_count":"1"}
     */

    private String status;
    private SubcategoryDetailBean subcategory_detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SubcategoryDetailBean getSubcategory_detail() {
        return subcategory_detail;
    }

    public void setSubcategory_detail(SubcategoryDetailBean subcategory_detail) {
        this.subcategory_detail = subcategory_detail;
    }

    public static class SubcategoryDetailBean {
        /**
         * id : 9
         * title : Living Room
         * description : Living Room
         * image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/SaHtD24aqfrR78nF75hZeWPB5FSvkISa.jpg
         * is_active : 1
         * is_subcategory : 1
         * subcategories : [{"id":11,"title":"Sofas","description":"Sofas","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/77F8BbDFaNj6UJWzsVh9mJus4WlzALoQ.jpg","is_active":"1","is_subcategory":"1"},{"id":36,"title":"Recliners","description":"Recliners","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb","is_active":"1","is_subcategory":"1"}]
         * item_count : 1
         */

        private int id;
        private String title;
        private String description;
        private String image;
        private String is_active;
        private String is_subcategory;
        private String item_count;
        private List<SubcategoriesBean> subcategories;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getIs_subcategory() {
            return is_subcategory;
        }

        public void setIs_subcategory(String is_subcategory) {
            this.is_subcategory = is_subcategory;
        }

        public String getItem_count() {
            return item_count;
        }

        public void setItem_count(String item_count) {
            this.item_count = item_count;
        }

        public List<SubcategoriesBean> getSubcategories() {
            return subcategories;
        }

        public void setSubcategories(List<SubcategoriesBean> subcategories) {
            this.subcategories = subcategories;
        }

        public static class SubcategoriesBean {
            /**
             * id : 11
             * title : Sofas
             * description : Sofas
             * image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/77F8BbDFaNj6UJWzsVh9mJus4WlzALoQ.jpg
             * is_active : 1
             * is_subcategory : 1
             */

            private int id;
            private String title;
            private String description;
            private String image;
            private String is_active;
            private String is_subcategory;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getIs_active() {
                return is_active;
            }

            public void setIs_active(String is_active) {
                this.is_active = is_active;
            }

            public String getIs_subcategory() {
                return is_subcategory;
            }

            public void setIs_subcategory(String is_subcategory) {
                this.is_subcategory = is_subcategory;
            }
        }
    }
}
