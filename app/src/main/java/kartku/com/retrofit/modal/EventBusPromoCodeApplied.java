package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/24/2017.
 */

public class EventBusPromoCodeApplied {

    private final String status;

    public EventBusPromoCodeApplied(String count) {
        this.status = count;
    }

    public String getMessage() {
        return status;
    }
}
