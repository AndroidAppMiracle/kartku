package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/15/2017.
 */

public class ProductDetailModal {


    /**
     * status : OK
     * product_info : {"id":41,"image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/jTmlRaepy7so8mBtuvkHEpvJbjJqo-yS.jpg","image2":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/orignal/jTmlRaepy7so8mBtuvkHEpvJbjJqo-yS.jpg","name":"housekeeping product","material":"wooden","color":[{"name":"White","id":13},{"name":"Red","id":14},{"name":"Blue","id":15}],"brand":"CasaCraft","brand_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/brandimages/thumb/ep7V0j57acPAeJ11yB7UuQLtcZfh42uR.jpg","brand_description":"","height":"34","width":"65","depth":"55","weight":"25","price":"2600","description":"test housekeeping product description","faq":"<p>test faq<\/p>","warranty":"<p>test terms<\/p>","is_cart":"0","wishlist":"0","max_quantity":"20","share_url":"http://dev.miracleglobal.com/kartku-php/web/product/product/view?id=41","discounted_price":""}
     * related_products : [{"id":48,"title":"Gorden biru","description":"Gorden biru","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/Fk8mpY4NLUBzyvAXaLSqze1okczCVDqH.jpg","products_price":"400","products_brand":"Signature Blankets","product_material":"Silk hard stuff","wishlist":"0","max_quantity":"50"}]
     */

    private String status;
    private ProductInfoBean product_info;
    private List<RelatedProductsBean> related_products;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProductInfoBean getProduct_info() {
        return product_info;
    }

    public void setProduct_info(ProductInfoBean product_info) {
        this.product_info = product_info;
    }

    public List<RelatedProductsBean> getRelated_products() {
        return related_products;
    }

    public void setRelated_products(List<RelatedProductsBean> related_products) {
        this.related_products = related_products;
    }

    public static class ProductInfoBean {
        /**
         * id : 41
         * image : http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/jTmlRaepy7so8mBtuvkHEpvJbjJqo-yS.jpg
         * image2 : http://dev.miracleglobal.com/kartku-php/web/uploads/product/orignal/jTmlRaepy7so8mBtuvkHEpvJbjJqo-yS.jpg
         * name : housekeeping product
         * material : wooden
         * color : [{"name":"White","id":13},{"name":"Red","id":14},{"name":"Blue","id":15}]
         * brand : CasaCraft
         * brand_image : http://dev.miracleglobal.com/kartku-php/web/uploads/brandimages/thumb/ep7V0j57acPAeJ11yB7UuQLtcZfh42uR.jpg
         * brand_description :
         * height : 34
         * width : 65
         * depth : 55
         * weight : 25
         * price : 2600
         * description : test housekeeping product description
         * faq : <p>test faq</p>
         * warranty : <p>test terms</p>
         * is_cart : 0
         * wishlist : 0
         * max_quantity : 20
         * share_url : http://dev.miracleglobal.com/kartku-php/web/product/product/view?id=41
         * discounted_price :
         */

        private int id;
        private String image;
        private String image2;
        private String name;
        private String material;
        private String brand;
        private String brand_image;
        private String brand_description;
        private String height;
        private String width;
        private String depth;
        private String weight;
        private String price;
        private String description;
        private String faq;
        private String warranty;
        private String is_cart;
        private String wishlist;
        private String max_quantity;
        private String share_url;
        private String discounted_price;
        private List<ColorBean> color;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getImage2() {
            return image2;
        }

        public void setImage2(String image2) {
            this.image2 = image2;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMaterial() {
            return material;
        }

        public void setMaterial(String material) {
            this.material = material;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getBrand_image() {
            return brand_image;
        }

        public void setBrand_image(String brand_image) {
            this.brand_image = brand_image;
        }

        public String getBrand_description() {
            return brand_description;
        }

        public void setBrand_description(String brand_description) {
            this.brand_description = brand_description;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getDepth() {
            return depth;
        }

        public void setDepth(String depth) {
            this.depth = depth;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getFaq() {
            return faq;
        }

        public void setFaq(String faq) {
            this.faq = faq;
        }

        public String getWarranty() {
            return warranty;
        }

        public void setWarranty(String warranty) {
            this.warranty = warranty;
        }

        public String getIs_cart() {
            return is_cart;
        }

        public void setIs_cart(String is_cart) {
            this.is_cart = is_cart;
        }

        public String getWishlist() {
            return wishlist;
        }

        public void setWishlist(String wishlist) {
            this.wishlist = wishlist;
        }

        public String getMax_quantity() {
            return max_quantity;
        }

        public void setMax_quantity(String max_quantity) {
            this.max_quantity = max_quantity;
        }

        public String getShare_url() {
            return share_url;
        }

        public void setShare_url(String share_url) {
            this.share_url = share_url;
        }

        public String getDiscounted_price() {
            return discounted_price;
        }

        public void setDiscounted_price(String discounted_price) {
            this.discounted_price = discounted_price;
        }

        public List<ColorBean> getColor() {
            return color;
        }

        public void setColor(List<ColorBean> color) {
            this.color = color;
        }

        public static class ColorBean {
            /**
             * name : White
             * id : 13
             */

            private String name;
            private int id;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }
    }

    public static class RelatedProductsBean {
        /**
         * id : 48
         * title : Gorden biru
         * description : Gorden biru
         * product_image : http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/Fk8mpY4NLUBzyvAXaLSqze1okczCVDqH.jpg
         * products_price : 400
         * products_brand : Signature Blankets
         * product_material : Silk hard stuff
         * wishlist : 0
         * max_quantity : 50
         */

        private int id;
        private String title;
        private String description;
        private String product_image;
        private String products_price;
        private String products_brand;
        private String product_material;
        private String wishlist;
        private String max_quantity;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getProducts_price() {
            return products_price;
        }

        public void setProducts_price(String products_price) {
            this.products_price = products_price;
        }

        public String getProducts_brand() {
            return products_brand;
        }

        public void setProducts_brand(String products_brand) {
            this.products_brand = products_brand;
        }

        public String getProduct_material() {
            return product_material;
        }

        public void setProduct_material(String product_material) {
            this.product_material = product_material;
        }

        public String getWishlist() {
            return wishlist;
        }

        public void setWishlist(String wishlist) {
            this.wishlist = wishlist;
        }

        public String getMax_quantity() {
            return max_quantity;
        }

        public void setMax_quantity(String max_quantity) {
            this.max_quantity = max_quantity;
        }
    }
}
