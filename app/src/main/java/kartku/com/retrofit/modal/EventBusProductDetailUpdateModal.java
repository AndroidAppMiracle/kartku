package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 3/18/2017.
 */

public class EventBusProductDetailUpdateModal {


    private final String status;

    public EventBusProductDetailUpdateModal(String count) {
        this.status = count;
    }

    public String getMessage() {
        return status;
    }
}
