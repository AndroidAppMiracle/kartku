package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/21/2017.
 */

public class ProductSearchBrandModal {


    /**
     * status : OK
     * MIN_PRICE : 0
     * MAX_PRICE : 180000
     * products_data : [{"id":17,"title":"My new product","description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/mediumQQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg","products_price":"180000","products_brand":"","product_material":"","wishlist":"0","max_quantity":"15"},{"id":18,"title":"My second product ","description":"hjghj hkghkghjkghjghj hgjghjgfjgfjgfjgj","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/mediumYieS5L2v09QlsCzweFwDvDD340c8IDsi.jpg","products_price":"800","products_brand":"","product_material":"","wishlist":"0","max_quantity":"1"},{"id":27,"title":"Bed Room3","description":"Test description","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/mediumnxA2u4hy0-1A9Anu82VkyPx1lGLux8Um.jpg","products_price":"0","products_brand":"","product_material":"","wishlist":"0","max_quantity":"89"}]
     */

    private String status;
    private String MIN_PRICE;
    private String MAX_PRICE;
    private List<ProductsDataBean> products_data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMIN_PRICE() {
        return MIN_PRICE;
    }

    public void setMIN_PRICE(String MIN_PRICE) {
        this.MIN_PRICE = MIN_PRICE;
    }

    public String getMAX_PRICE() {
        return MAX_PRICE;
    }

    public void setMAX_PRICE(String MAX_PRICE) {
        this.MAX_PRICE = MAX_PRICE;
    }

    public List<ProductsDataBean> getProducts_data() {
        return products_data;
    }

    public void setProducts_data(List<ProductsDataBean> products_data) {
        this.products_data = products_data;
    }

    public static class ProductsDataBean {
        /**
         * id : 17
         * title : My new product
         * description : this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg
         * product_image : http://dev.miracleglobal.com/kartku-php/web/uploads/product/mediumQQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg
         * products_price : 180000
         * products_brand :
         * product_material :
         * wishlist : 0
         * max_quantity : 15
         */

        private int id;
        private String title;
        private String description;
        private String product_image;
        private String products_price;
        private String products_brand;
        private String product_material;
        private String wishlist;
        private String max_quantity;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getProducts_price() {
            return products_price;
        }

        public void setProducts_price(String products_price) {
            this.products_price = products_price;
        }

        public String getProducts_brand() {
            return products_brand;
        }

        public void setProducts_brand(String products_brand) {
            this.products_brand = products_brand;
        }

        public String getProduct_material() {
            return product_material;
        }

        public void setProduct_material(String product_material) {
            this.product_material = product_material;
        }

        public String getWishlist() {
            return wishlist;
        }

        public void setWishlist(String wishlist) {
            this.wishlist = wishlist;
        }

        public String getMax_quantity() {
            return max_quantity;
        }

        public void setMax_quantity(String max_quantity) {
            this.max_quantity = max_quantity;
        }
    }
}
