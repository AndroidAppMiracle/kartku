package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/23/2017.
 */

public class AddToCartNewModal {


    /**
     * status : OK
     * result : Product Added to cart
     * cart_info : {"id":62,"user_id":260,"session_id":null,"promocode_id":null,"discount":"","total_amount":5800,"order_status":"58","created_at":"2017-03-31","updated_at":null}
     * item_info : [{"id":184,"cart_id":62,"product_id":25,"quantity":"1","shipping_status":"Confirmed","pickup_city":"jakarta","pickup_city_code":"","dest_city":"JAKARTA","dest_city_code":"CGK10000","shipping_cost":"0","commission":"0","created_at":"2017-03-31","updated_at":null},{"id":192,"cart_id":62,"product_id":18,"quantity":"1","shipping_status":"Confirmed","pickup_city":null,"pickup_city_code":null,"dest_city":null,"dest_city_code":null,"shipping_cost":"","commission":"0","created_at":"2017-03-31","updated_at":null},{"id":193,"cart_id":62,"product_id":31,"quantity":"1","shipping_status":"Confirmed","pickup_city":null,"pickup_city_code":null,"dest_city":null,"dest_city_code":null,"shipping_cost":"","commission":"0","created_at":"2017-03-31","updated_at":null}]
     */

    private String status;
    private String result;
    private CartInfoBean cart_info;
    private List<ItemInfoBean> item_info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public CartInfoBean getCart_info() {
        return cart_info;
    }

    public void setCart_info(CartInfoBean cart_info) {
        this.cart_info = cart_info;
    }

    public List<ItemInfoBean> getItem_info() {
        return item_info;
    }

    public void setItem_info(List<ItemInfoBean> item_info) {
        this.item_info = item_info;
    }

    public static class CartInfoBean {
        /**
         * id : 62
         * user_id : 260
         * session_id : null
         * promocode_id : null
         * discount :
         * total_amount : 5800
         * order_status : 58
         * created_at : 2017-03-31
         * updated_at : null
         */

        private int id;
        private int user_id;
        private Object session_id;
        private Object promocode_id;
        private String discount;
        private int total_amount;
        private String order_status;
        private String created_at;
        private Object updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public Object getSession_id() {
            return session_id;
        }

        public void setSession_id(Object session_id) {
            this.session_id = session_id;
        }

        public Object getPromocode_id() {
            return promocode_id;
        }

        public void setPromocode_id(Object promocode_id) {
            this.promocode_id = promocode_id;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public int getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(int total_amount) {
            this.total_amount = total_amount;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }
    }

    public static class ItemInfoBean {
        /**
         * id : 184
         * cart_id : 62
         * product_id : 25
         * quantity : 1
         * shipping_status : Confirmed
         * pickup_city : jakarta
         * pickup_city_code :
         * dest_city : JAKARTA
         * dest_city_code : CGK10000
         * shipping_cost : 0
         * commission : 0
         * created_at : 2017-03-31
         * updated_at : null
         */

        private int id;
        private int cart_id;
        private int product_id;
        private String quantity;
        private String shipping_status;
        private String pickup_city;
        private String pickup_city_code;
        private String dest_city;
        private String dest_city_code;
        private String shipping_cost;
        private String commission;
        private String created_at;
        private Object updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCart_id() {
            return cart_id;
        }

        public void setCart_id(int cart_id) {
            this.cart_id = cart_id;
        }

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getShipping_status() {
            return shipping_status;
        }

        public void setShipping_status(String shipping_status) {
            this.shipping_status = shipping_status;
        }

        public String getPickup_city() {
            return pickup_city;
        }

        public void setPickup_city(String pickup_city) {
            this.pickup_city = pickup_city;
        }

        public String getPickup_city_code() {
            return pickup_city_code;
        }

        public void setPickup_city_code(String pickup_city_code) {
            this.pickup_city_code = pickup_city_code;
        }

        public String getDest_city() {
            return dest_city;
        }

        public void setDest_city(String dest_city) {
            this.dest_city = dest_city;
        }

        public String getDest_city_code() {
            return dest_city_code;
        }

        public void setDest_city_code(String dest_city_code) {
            this.dest_city_code = dest_city_code;
        }

        public String getShipping_cost() {
            return shipping_cost;
        }

        public void setShipping_cost(String shipping_cost) {
            this.shipping_cost = shipping_cost;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }
    }
}
