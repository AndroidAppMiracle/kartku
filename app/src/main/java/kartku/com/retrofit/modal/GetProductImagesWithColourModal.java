package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 4/18/2017.
 */

public class GetProductImagesWithColourModal {

    /**
     * status : OK
     * image_list : [{"name":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/jTmlRaepy7so8mBtuvkHEpvJbjJqo-yS.jpg"}]
     */

    private String status;
    private List<ImageListBean> image_list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ImageListBean> getImage_list() {
        return image_list;
    }

    public void setImage_list(List<ImageListBean> image_list) {
        this.image_list = image_list;
    }

    public static class ImageListBean {
        /**
         * name : http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/jTmlRaepy7so8mBtuvkHEpvJbjJqo-yS.jpg
         */

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
