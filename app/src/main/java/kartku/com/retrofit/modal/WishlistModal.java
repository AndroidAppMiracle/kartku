package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/28/2017.
 */

public class WishlistModal {

    /**
     * status : OK
     * product_info : [{"id":52,"image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/L3hFXS4JftIrvZtKLd-85hURq4EjWRK8.jpg","name":"Styla Wall clock","price":"310","description":"Stylish Black colored wall clock, best for drawing room decoration.","max_quantity":"48","wishlist":"1","is_cart":"1"}]
     */

    private String status;
    private List<ProductInfoBean> product_info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProductInfoBean> getProduct_info() {
        return product_info;
    }

    public void setProduct_info(List<ProductInfoBean> product_info) {
        this.product_info = product_info;
    }

    public static class ProductInfoBean {
        /**
         * id : 52
         * image : http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/L3hFXS4JftIrvZtKLd-85hURq4EjWRK8.jpg
         * name : Styla Wall clock
         * price : 310
         * description : Stylish Black colored wall clock, best for drawing room decoration.
         * max_quantity : 48
         * wishlist : 1
         * is_cart : 1
         */

        private int id;
        private String image;
        private String name;
        private String price;
        private String description;
        private String max_quantity;
        private String wishlist;
        private String is_cart;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMax_quantity() {
            return max_quantity;
        }

        public void setMax_quantity(String max_quantity) {
            this.max_quantity = max_quantity;
        }

        public String getWishlist() {
            return wishlist;
        }

        public void setWishlist(String wishlist) {
            this.wishlist = wishlist;
        }

        public String getIs_cart() {
            return is_cart;
        }

        public void setIs_cart(String is_cart) {
            this.is_cart = is_cart;
        }
    }
}
