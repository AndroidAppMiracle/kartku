package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/25/2017.
 */

public class ChangePasswordModal {


    /**
     * status : OK
     * message : Kata sandi akun Anda telah diperbarui
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
