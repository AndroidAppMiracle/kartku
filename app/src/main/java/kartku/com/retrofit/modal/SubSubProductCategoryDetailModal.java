package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/16/2017.
 */

public class SubSubProductCategoryDetailModal {


    /**
     * status : OK
     * subcategory_detail : {"id":11,"title":"Sofas","description":"Sofas","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/77F8BbDFaNj6UJWzsVh9mJus4WlzALoQ.jpg","is_active":"1","is_subcategory":"1","subcategories":[{"id":12,"title":"Three Seater Sofas","description":"Three Seater Sofas","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/FKNNyV2dkp8njezZRL-RqsgyNtNksiVB.jpg","is_active":"1","is_subcategory":"0","item_count":"1"},{"id":13,"title":"Two Seater Sofas","description":"Two Seater Sofas ","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/e9UxYl32UGNJbOK3S3ZTS0Z2OaldK2jZ.jpg","is_active":"1","is_subcategory":"0","item_count":"7"},{"id":14,"title":"One Seater Sofas","description":"One Seater Sofas","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/jq5GzfiPSG8vCfRPGdYvLFmtg9qUsSt9.jpg","is_active":"1","is_subcategory":"0","item_count":"4"},{"id":15,"title":"Sofa Sectionals","description":"Sofa Sectionals","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/2aEPg6hUYy27_npiLoFmVWgC8ny8E51q.jpg","is_active":"1","is_subcategory":"0","item_count":"0"}]}
     */

    private String status;
    private SubcategoryDetailBean subcategory_detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SubcategoryDetailBean getSubcategory_detail() {
        return subcategory_detail;
    }

    public void setSubcategory_detail(SubcategoryDetailBean subcategory_detail) {
        this.subcategory_detail = subcategory_detail;
    }

    public static class SubcategoryDetailBean {
        /**
         * id : 11
         * title : Sofas
         * description : Sofas
         * image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/77F8BbDFaNj6UJWzsVh9mJus4WlzALoQ.jpg
         * is_active : 1
         * is_subcategory : 1
         * subcategories : [{"id":12,"title":"Three Seater Sofas","description":"Three Seater Sofas","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/FKNNyV2dkp8njezZRL-RqsgyNtNksiVB.jpg","is_active":"1","is_subcategory":"0","item_count":"1"},{"id":13,"title":"Two Seater Sofas","description":"Two Seater Sofas ","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/e9UxYl32UGNJbOK3S3ZTS0Z2OaldK2jZ.jpg","is_active":"1","is_subcategory":"0","item_count":"7"},{"id":14,"title":"One Seater Sofas","description":"One Seater Sofas","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/jq5GzfiPSG8vCfRPGdYvLFmtg9qUsSt9.jpg","is_active":"1","is_subcategory":"0","item_count":"4"},{"id":15,"title":"Sofa Sectionals","description":"Sofa Sectionals","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/2aEPg6hUYy27_npiLoFmVWgC8ny8E51q.jpg","is_active":"1","is_subcategory":"0","item_count":"0"}]
         */

        private int id;
        private String title;
        private String description;
        private String image;
        private String is_active;
        private String is_subcategory;
        private List<SubcategoriesBean> subcategories;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getIs_subcategory() {
            return is_subcategory;
        }

        public void setIs_subcategory(String is_subcategory) {
            this.is_subcategory = is_subcategory;
        }

        public List<SubcategoriesBean> getSubcategories() {
            return subcategories;
        }

        public void setSubcategories(List<SubcategoriesBean> subcategories) {
            this.subcategories = subcategories;
        }

        public static class SubcategoriesBean {
            /**
             * id : 12
             * title : Three Seater Sofas
             * description : Three Seater Sofas
             * image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/FKNNyV2dkp8njezZRL-RqsgyNtNksiVB.jpg
             * is_active : 1
             * is_subcategory : 0
             * item_count : 1
             */

            private int id;
            private String title;
            private String description;
            private String image;
            private String is_active;
            private String is_subcategory;
            private String item_count;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getIs_active() {
                return is_active;
            }

            public void setIs_active(String is_active) {
                this.is_active = is_active;
            }

            public String getIs_subcategory() {
                return is_subcategory;
            }

            public void setIs_subcategory(String is_subcategory) {
                this.is_subcategory = is_subcategory;
            }

            public String getItem_count() {
                return item_count;
            }

            public void setItem_count(String item_count) {
                this.item_count = item_count;
            }
        }
    }
}
