package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/12/2017.
 */

public class EventBusLogout {

    private final String status;

    public EventBusLogout(String count) {
        this.status = count;
    }

    public String getMessage() {
        return status;
    }
}
