package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/17/2017.
 */

public class PromoCodeModal {


    /**
     * status : OK
     * cart_info : {"id":27,"user_id":271,"session_id":"0dff1","promocode_id":"5","discount":"1474","total_amount":"13266","order_status":"224","created_at":"2017-04-11","updated_at":"2017-04-12"}
     * order_info : {"id":224,"user_id":271,"user":"ishita0","status":"ORDER_CANCELLED","total_amount":"14740.0000","tax":57,"shipping_cost":"0","grand_total":697,"created_at":"2017-04-11"}
     */

    private String status;
    private CartInfoBean cart_info;
    private OrderInfoBean order_info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CartInfoBean getCart_info() {
        return cart_info;
    }

    public void setCart_info(CartInfoBean cart_info) {
        this.cart_info = cart_info;
    }

    public OrderInfoBean getOrder_info() {
        return order_info;
    }

    public void setOrder_info(OrderInfoBean order_info) {
        this.order_info = order_info;
    }

    public static class CartInfoBean {
        /**
         * id : 27
         * user_id : 271
         * session_id : 0dff1
         * promocode_id : 5
         * discount : 1474
         * total_amount : 13266
         * order_status : 224
         * created_at : 2017-04-11
         * updated_at : 2017-04-12
         */

        private int id;
        private int user_id;
        private String session_id;
        private String promocode_id;
        private String discount;
        private String total_amount;
        private String order_status;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getSession_id() {
            return session_id;
        }

        public void setSession_id(String session_id) {
            this.session_id = session_id;
        }

        public String getPromocode_id() {
            return promocode_id;
        }

        public void setPromocode_id(String promocode_id) {
            this.promocode_id = promocode_id;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

    public static class OrderInfoBean {
        /**
         * id : 224
         * user_id : 271
         * user : ishita0
         * status : ORDER_CANCELLED
         * total_amount : 14740.0000
         * tax : 57
         * shipping_cost : 0
         * grand_total : 697
         * created_at : 2017-04-11
         */

        private int id;
        private int user_id;
        private String user;
        private String status;
        private String total_amount;
        private int tax;
        private String shipping_cost;
        private int grand_total;
        private String created_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public int getTax() {
            return tax;
        }

        public void setTax(int tax) {
            this.tax = tax;
        }

        public String getShipping_cost() {
            return shipping_cost;
        }

        public void setShipping_cost(String shipping_cost) {
            this.shipping_cost = shipping_cost;
        }

        public int getGrand_total() {
            return grand_total;
        }

        public void setGrand_total(int grand_total) {
            this.grand_total = grand_total;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
