package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/6/2017.
 */

public class CartItemCountModal {

    /**
     * status : OK
     * item_count : 3
     */

    private String status;
    private String item_count;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItem_count() {
        return item_count;
    }

    public void setItem_count(String item_count) {
        this.item_count = item_count;
    }
}
