package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/20/2017.
 */

public class ContactUsModal {

    /**
     * status : OK
     * message : Thank you for contacting us. We will respond to you as soon as possible.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
