package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/15/2017.
 */

public class ProductCategoryModal {

    /**
     * status : OK
     * category_detail : {"id":1,"title":"Furniture","description":"xcvxv","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/Ao_nrH0Jgv5JsD2A8CVf-T1S52-6MEri.png","is_active":"1","is_subcategory":"1","subcategories":[{"id":9,"title":"Living Room","description":"Living Room","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/SaHtD24aqfrR78nF75hZeWPB5FSvkISa.jpg","is_active":"1","is_subcategory":"1","item_count":"1"},{"id":10,"title":"Bed Room","description":"Bed Room","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/NCf-ItSDCrjEa71j5h3tyw8WVWkO7gJk.jpg","is_active":"1","is_subcategory":"1","item_count":"11"}]}
     */

    private String status;
    private CategoryDetailBean category_detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CategoryDetailBean getCategory_detail() {
        return category_detail;
    }

    public void setCategory_detail(CategoryDetailBean category_detail) {
        this.category_detail = category_detail;
    }

    public static class CategoryDetailBean {
        /**
         * id : 1
         * title : Furniture
         * description : xcvxv
         * image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/Ao_nrH0Jgv5JsD2A8CVf-T1S52-6MEri.png
         * is_active : 1
         * is_subcategory : 1
         * subcategories : [{"id":9,"title":"Living Room","description":"Living Room","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/SaHtD24aqfrR78nF75hZeWPB5FSvkISa.jpg","is_active":"1","is_subcategory":"1","item_count":"1"},{"id":10,"title":"Bed Room","description":"Bed Room","image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/NCf-ItSDCrjEa71j5h3tyw8WVWkO7gJk.jpg","is_active":"1","is_subcategory":"1","item_count":"11"}]
         */

        private int id;
        private String title;
        private String description;
        private String image;
        private String is_active;
        private String is_subcategory;
        private List<SubcategoriesBean> subcategories;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getIs_subcategory() {
            return is_subcategory;
        }

        public void setIs_subcategory(String is_subcategory) {
            this.is_subcategory = is_subcategory;
        }

        public List<SubcategoriesBean> getSubcategories() {
            return subcategories;
        }

        public void setSubcategories(List<SubcategoriesBean> subcategories) {
            this.subcategories = subcategories;
        }

        public static class SubcategoriesBean {
            /**
             * id : 9
             * title : Living Room
             * description : Living Room
             * image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/thumb/SaHtD24aqfrR78nF75hZeWPB5FSvkISa.jpg
             * is_active : 1
             * is_subcategory : 1
             * item_count : 1
             */

            private int id;
            private String title;
            private String description;
            private String image;
            private String is_active;
            private String is_subcategory;
            private String item_count;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getIs_active() {
                return is_active;
            }

            public void setIs_active(String is_active) {
                this.is_active = is_active;
            }

            public String getIs_subcategory() {
                return is_subcategory;
            }

            public void setIs_subcategory(String is_subcategory) {
                this.is_subcategory = is_subcategory;
            }

            public String getItem_count() {
                return item_count;
            }

            public void setItem_count(String item_count) {
                this.item_count = item_count;
            }
        }
    }
}
