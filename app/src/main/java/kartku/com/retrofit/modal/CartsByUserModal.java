package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/18/2017.
 */

public class CartsByUserModal {


    /**
     * status : OK
     * carts : [{"id":3,"user_id":214,"session_id":null,"promocode_id":null,"discount":"","total_amount":"40000.0000","order_status":"1","created_at":"2017-03-24","updated_at":null}]
     */

    private String status;
    private List<CartsBean> carts;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CartsBean> getCarts() {
        return carts;
    }

    public void setCarts(List<CartsBean> carts) {
        this.carts = carts;
    }

    public static class CartsBean {
        /**
         * id : 3
         * user_id : 214
         * session_id : null
         * promocode_id : null
         * discount :
         * total_amount : 40000.0000
         * order_status : 1
         * created_at : 2017-03-24
         * updated_at : null
         */

        private int id;
        private int user_id;
        private Object session_id;
        private Object promocode_id;
        private String discount;
        private String total_amount;
        private String order_status;
        private String created_at;
        private Object updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public Object getSession_id() {
            return session_id;
        }

        public void setSession_id(Object session_id) {
            this.session_id = session_id;
        }

        public Object getPromocode_id() {
            return promocode_id;
        }

        public void setPromocode_id(Object promocode_id) {
            this.promocode_id = promocode_id;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }
    }
}
