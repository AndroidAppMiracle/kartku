package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/19/2017.
 */

public class LoginModal {

    /**
     * status : OK
     * message : Logged in successfully
     * user_data : {"user_id":"271","email":"ishita@gmail.com","language":"1","device_id":"","device_type":"","profile_image_path":"http://dev.miracleglobal.com/kartku-php/web/uploads/profileimages/orignal/oUGvCGN-nK55mcT6302Om7x3m-eHOijf.png","first_name":"Ishita","last_name":"Bathla","company_name":"","country":"","city":"","address":"CHANDIGARH","landline_number":"","mobile_number":"9876543210","role":"10","birth_date":"","birth_month":"","birth_year":"","pickup_country":"","pickup_city":"","pickup_address":"","business_type":"Select Business,Factory,Distributor,Wholesaler,Retailer,Home Handicraft","newsletter":"","sms":""}
     */

    private String status;
    private String message;
    private UserDataBean user_data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDataBean getUser_data() {
        return user_data;
    }

    public void setUser_data(UserDataBean user_data) {
        this.user_data = user_data;
    }

    public static class UserDataBean {
        /**
         * user_id : 271
         * email : ishita@gmail.com
         * language : 1
         * device_id :
         * device_type :
         * profile_image_path : http://dev.miracleglobal.com/kartku-php/web/uploads/profileimages/orignal/oUGvCGN-nK55mcT6302Om7x3m-eHOijf.png
         * first_name : Ishita
         * last_name : Bathla
         * company_name :
         * country :
         * city :
         * address : CHANDIGARH
         * landline_number :
         * mobile_number : 9876543210
         * role : 10
         * birth_date :
         * birth_month :
         * birth_year :
         * pickup_country :
         * pickup_city :
         * pickup_address :
         * business_type : Select Business,Factory,Distributor,Wholesaler,Retailer,Home Handicraft
         * newsletter :
         * sms :
         */

        private String user_id;
        private String email;
        private String language;
        private String device_id;
        private String device_type;
        private String profile_image_path;
        private String first_name;
        private String last_name;
        private String company_name;
        private String country;
        private String city;
        private String address;
        private String landline_number;
        private String mobile_number;
        private String role;
        private String birth_date;
        private String birth_month;
        private String birth_year;
        private String pickup_country;
        private String pickup_city;
        private String pickup_address;
        private String business_type;
        private String newsletter;
        private String sms;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getProfile_image_path() {
            return profile_image_path;
        }

        public void setProfile_image_path(String profile_image_path) {
            this.profile_image_path = profile_image_path;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLandline_number() {
            return landline_number;
        }

        public void setLandline_number(String landline_number) {
            this.landline_number = landline_number;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getBirth_date() {
            return birth_date;
        }

        public void setBirth_date(String birth_date) {
            this.birth_date = birth_date;
        }

        public String getBirth_month() {
            return birth_month;
        }

        public void setBirth_month(String birth_month) {
            this.birth_month = birth_month;
        }

        public String getBirth_year() {
            return birth_year;
        }

        public void setBirth_year(String birth_year) {
            this.birth_year = birth_year;
        }

        public String getPickup_country() {
            return pickup_country;
        }

        public void setPickup_country(String pickup_country) {
            this.pickup_country = pickup_country;
        }

        public String getPickup_city() {
            return pickup_city;
        }

        public void setPickup_city(String pickup_city) {
            this.pickup_city = pickup_city;
        }

        public String getPickup_address() {
            return pickup_address;
        }

        public void setPickup_address(String pickup_address) {
            this.pickup_address = pickup_address;
        }

        public String getBusiness_type() {
            return business_type;
        }

        public void setBusiness_type(String business_type) {
            this.business_type = business_type;
        }

        public String getNewsletter() {
            return newsletter;
        }

        public void setNewsletter(String newsletter) {
            this.newsletter = newsletter;
        }

        public String getSms() {
            return sms;
        }

        public void setSms(String sms) {
            this.sms = sms;
        }
    }
}
