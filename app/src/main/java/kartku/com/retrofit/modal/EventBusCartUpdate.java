package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 3/29/2017.
 */

public class EventBusCartUpdate {

    private final String status;

    public EventBusCartUpdate(String count) {
        this.status = count;
    }

    public String getMessage() {
        return status;
    }
}
