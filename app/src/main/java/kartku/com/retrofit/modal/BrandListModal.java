package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 4/24/2017.
 */

public class BrandListModal {


    /**
     * status : OK
     * brand_info : [{"id":1,"name":"Furnicheer"},{"id":2,"name":"Woodsworth"},{"id":3,"name":"Hometown"},{"id":4,"name":"DecorNation"},{"id":5,"name":"CasaCraft"},{"id":6,"name":"Amberville"},{"id":7,"name":"Titan"},{"id":12,"name":"Gardenia"},{"id":13,"name":"Woodie"},{"id":16,"name":"Signature Blankets"}]
     */

    private String status;
    private List<BrandInfoBean> brand_info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BrandInfoBean> getBrand_info() {
        return brand_info;
    }

    public void setBrand_info(List<BrandInfoBean> brand_info) {
        this.brand_info = brand_info;
    }

    public static class BrandInfoBean {
        /**
         * id : 1
         * name : Furnicheer
         */

        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
