package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 3/15/2017.
 */

public class AddToWishListModal {


    /**
     * status : OK
     * message : Product Added to wishlist.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
