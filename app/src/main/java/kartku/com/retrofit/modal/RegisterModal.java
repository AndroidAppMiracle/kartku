package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/20/2017.
 */

public class RegisterModal {

    /**
     * status : OK
     * message : Your account has been created, Please check your email.
     * user_id : 287
     * email : pause_urself@yahoo.in
     * first_name : Kshitiz
     * last_name : Kshitiz
     * mobile_number : 9876543210
     * device_id : vwe-asd22asd2hjasda02-32-sdnase23-asda
     * device_type : 1
     * role : 10
     */

    private String status;
    private String message;
    private int user_id;
    private String email;
    private String first_name;
    private String last_name;
    private String mobile_number;
    private String device_id;
    private String device_type;
    private String role;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
