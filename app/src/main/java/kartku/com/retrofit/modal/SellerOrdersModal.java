package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/23/2017.
 */

public class SellerOrdersModal {


    /**
     * status : OK
     * results : [{"id":515,"cart_id":215,"product_id":17,"quantity":"1","shipping_status":"Confirmed","pickup_city":"GIANYAR","pickup_city_code":"DPS20300","dest_city":"JAYAPURA","dest_city_code":"DJJ10000","shipping_cost":"166000","commission":"0","created_at":"2016-12-28","updated_at":null,"product_detail":{"id":17,"title":"My new product","description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","rating":"2.5384615384615","brand_id":3,"category_id":13,"is_featured":1,"user_id":1,"short_desc":"hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk","sku":"777777","slug":"my-new-product-17","status":"Enable","is_admin_allowed":1,"create_at":"2016-04-04 17:16:27","update_at":"2016-04-04 18:00:17","stock_availability":"AVAILABLE","pre_order":0,"pre_order_duration":"","delivery_time":"","is_deleted":0,"hits_count":86,"product_image":"http://kartku.s3.amazonaws.com/product/medium/QQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg"},"order_detail":{"id":135,"user_id":185,"cart_id":215,"status":"PAYMENT_SUCCESS","total_amount":"180000.0000","shipping_cost":"166000","tax":16200,"grand_total":362200,"tracking_id":"","created_at":"2016-12-28"}},{"id":330,"cart_id":149,"product_id":7,"quantity":"1","shipping_status":"Confirmed","pickup_city":"GIANYAR","pickup_city_code":"DPS20300","dest_city":"HAMPANG,KOTABARUPULAULAUT","dest_city_code":"BDJ10302","shipping_cost":"138000","commission":"0","created_at":"2016-09-15","updated_at":null,"product_detail":{"id":7,"title":"Sofa cum beds","description":"Auspicious","rating":"3","brand_id":1,"category_id":9,"is_featured":0,"user_id":1,"short_desc":"","sku":"star","slug":null,"status":"Enable","is_admin_allowed":null,"create_at":"2016-01-18 18:34:53","update_at":"2016-02-05 06:14:03","stock_availability":"AVAILABLE","pre_order":0,"pre_order_duration":"","delivery_time":"","is_deleted":0,"hits_count":47,"product_image":"http://kartku.s3.amazonaws.com/product/medium/H9fiFyAGv_fI2XOQkK1H94Rru4rJmknl.jpg"},"order_detail":{"id":124,"user_id":2,"cart_id":149,"status":"PAYMENT_SUCCESS","total_amount":"9600.0000","shipping_cost":"630000","tax":864,"grand_total":640464,"tracking_id":"","created_at":"2016-09-15"}}]
     */

    private String status;
    private List<ResultsBean> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class ResultsBean {
        /**
         * id : 515
         * cart_id : 215
         * product_id : 17
         * quantity : 1
         * shipping_status : Confirmed
         * pickup_city : GIANYAR
         * pickup_city_code : DPS20300
         * dest_city : JAYAPURA
         * dest_city_code : DJJ10000
         * shipping_cost : 166000
         * commission : 0
         * created_at : 2016-12-28
         * updated_at : null
         * product_detail : {"id":17,"title":"My new product","description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","rating":"2.5384615384615","brand_id":3,"category_id":13,"is_featured":1,"user_id":1,"short_desc":"hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk","sku":"777777","slug":"my-new-product-17","status":"Enable","is_admin_allowed":1,"create_at":"2016-04-04 17:16:27","update_at":"2016-04-04 18:00:17","stock_availability":"AVAILABLE","pre_order":0,"pre_order_duration":"","delivery_time":"","is_deleted":0,"hits_count":86,"product_image":"http://kartku.s3.amazonaws.com/product/medium/QQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg"}
         * order_detail : {"id":135,"user_id":185,"cart_id":215,"status":"PAYMENT_SUCCESS","total_amount":"180000.0000","shipping_cost":"166000","tax":16200,"grand_total":362200,"tracking_id":"","created_at":"2016-12-28"}
         */

        private int id;
        private int cart_id;
        private int product_id;
        private String quantity;
        private String shipping_status;
        private String pickup_city;
        private String pickup_city_code;
        private String dest_city;
        private String dest_city_code;
        private String shipping_cost;
        private String commission;
        private String created_at;
        private Object updated_at;
        private ProductDetailBean product_detail;
        private OrderDetailBean order_detail;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCart_id() {
            return cart_id;
        }

        public void setCart_id(int cart_id) {
            this.cart_id = cart_id;
        }

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getShipping_status() {
            return shipping_status;
        }

        public void setShipping_status(String shipping_status) {
            this.shipping_status = shipping_status;
        }

        public String getPickup_city() {
            return pickup_city;
        }

        public void setPickup_city(String pickup_city) {
            this.pickup_city = pickup_city;
        }

        public String getPickup_city_code() {
            return pickup_city_code;
        }

        public void setPickup_city_code(String pickup_city_code) {
            this.pickup_city_code = pickup_city_code;
        }

        public String getDest_city() {
            return dest_city;
        }

        public void setDest_city(String dest_city) {
            this.dest_city = dest_city;
        }

        public String getDest_city_code() {
            return dest_city_code;
        }

        public void setDest_city_code(String dest_city_code) {
            this.dest_city_code = dest_city_code;
        }

        public String getShipping_cost() {
            return shipping_cost;
        }

        public void setShipping_cost(String shipping_cost) {
            this.shipping_cost = shipping_cost;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }

        public ProductDetailBean getProduct_detail() {
            return product_detail;
        }

        public void setProduct_detail(ProductDetailBean product_detail) {
            this.product_detail = product_detail;
        }

        public OrderDetailBean getOrder_detail() {
            return order_detail;
        }

        public void setOrder_detail(OrderDetailBean order_detail) {
            this.order_detail = order_detail;
        }

        public static class ProductDetailBean {
            /**
             * id : 17
             * title : My new product
             * description : this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg
             * rating : 2.5384615384615
             * brand_id : 3
             * category_id : 13
             * is_featured : 1
             * user_id : 1
             * short_desc : hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk hjjhk
             * sku : 777777
             * slug : my-new-product-17
             * status : Enable
             * is_admin_allowed : 1
             * create_at : 2016-04-04 17:16:27
             * update_at : 2016-04-04 18:00:17
             * stock_availability : AVAILABLE
             * pre_order : 0
             * pre_order_duration :
             * delivery_time :
             * is_deleted : 0
             * hits_count : 86
             * product_image : http://kartku.s3.amazonaws.com/product/medium/QQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg
             */

            private int id;
            private String title;
            private String description;
            private String rating;
            private int brand_id;
            private int category_id;
            private int is_featured;
            private int user_id;
            private String short_desc;
            private String sku;
            private String slug;
            private String status;
            private int is_admin_allowed;
            private String create_at;
            private String update_at;
            private String stock_availability;
            private int pre_order;
            private String pre_order_duration;
            private String delivery_time;
            private int is_deleted;
            private int hits_count;
            private String product_image;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getRating() {
                return rating;
            }

            public void setRating(String rating) {
                this.rating = rating;
            }

            public int getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(int brand_id) {
                this.brand_id = brand_id;
            }

            public int getCategory_id() {
                return category_id;
            }

            public void setCategory_id(int category_id) {
                this.category_id = category_id;
            }

            public int getIs_featured() {
                return is_featured;
            }

            public void setIs_featured(int is_featured) {
                this.is_featured = is_featured;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public String getShort_desc() {
                return short_desc;
            }

            public void setShort_desc(String short_desc) {
                this.short_desc = short_desc;
            }

            public String getSku() {
                return sku;
            }

            public void setSku(String sku) {
                this.sku = sku;
            }

            public String getSlug() {
                return slug;
            }

            public void setSlug(String slug) {
                this.slug = slug;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public int getIs_admin_allowed() {
                return is_admin_allowed;
            }

            public void setIs_admin_allowed(int is_admin_allowed) {
                this.is_admin_allowed = is_admin_allowed;
            }

            public String getCreate_at() {
                return create_at;
            }

            public void setCreate_at(String create_at) {
                this.create_at = create_at;
            }

            public String getUpdate_at() {
                return update_at;
            }

            public void setUpdate_at(String update_at) {
                this.update_at = update_at;
            }

            public String getStock_availability() {
                return stock_availability;
            }

            public void setStock_availability(String stock_availability) {
                this.stock_availability = stock_availability;
            }

            public int getPre_order() {
                return pre_order;
            }

            public void setPre_order(int pre_order) {
                this.pre_order = pre_order;
            }

            public String getPre_order_duration() {
                return pre_order_duration;
            }

            public void setPre_order_duration(String pre_order_duration) {
                this.pre_order_duration = pre_order_duration;
            }

            public String getDelivery_time() {
                return delivery_time;
            }

            public void setDelivery_time(String delivery_time) {
                this.delivery_time = delivery_time;
            }

            public int getIs_deleted() {
                return is_deleted;
            }

            public void setIs_deleted(int is_deleted) {
                this.is_deleted = is_deleted;
            }

            public int getHits_count() {
                return hits_count;
            }

            public void setHits_count(int hits_count) {
                this.hits_count = hits_count;
            }

            public String getProduct_image() {
                return product_image;
            }

            public void setProduct_image(String product_image) {
                this.product_image = product_image;
            }
        }

        public static class OrderDetailBean {
            /**
             * id : 135
             * user_id : 185
             * cart_id : 215
             * status : PAYMENT_SUCCESS
             * total_amount : 180000.0000
             * shipping_cost : 166000
             * tax : 16200
             * grand_total : 362200
             * tracking_id :
             * created_at : 2016-12-28
             */

            private int id;
            private int user_id;
            private int cart_id;
            private String status;
            private String total_amount;
            private String shipping_cost;
            private int tax;
            private int grand_total;
            private String tracking_id;
            private String created_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public int getCart_id() {
                return cart_id;
            }

            public void setCart_id(int cart_id) {
                this.cart_id = cart_id;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getTotal_amount() {
                return total_amount;
            }

            public void setTotal_amount(String total_amount) {
                this.total_amount = total_amount;
            }

            public String getShipping_cost() {
                return shipping_cost;
            }

            public void setShipping_cost(String shipping_cost) {
                this.shipping_cost = shipping_cost;
            }

            public int getTax() {
                return tax;
            }

            public void setTax(int tax) {
                this.tax = tax;
            }

            public int getGrand_total() {
                return grand_total;
            }

            public void setGrand_total(int grand_total) {
                this.grand_total = grand_total;
            }

            public String getTracking_id() {
                return tracking_id;
            }

            public void setTracking_id(String tracking_id) {
                this.tracking_id = tracking_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
        }
    }
}
