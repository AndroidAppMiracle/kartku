package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 4/11/2017.
 */

public class EventBusCategoryProductsUpdateWishlist {
    private final String status;

    public EventBusCategoryProductsUpdateWishlist(String count) {
        this.status = count;
    }

    public String getMessage() {
        return status;
    }
}
