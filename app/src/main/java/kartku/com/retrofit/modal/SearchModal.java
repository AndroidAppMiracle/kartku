package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 4/6/2017.
 */

public class SearchModal {

    /**
     * status : OK
     * products_data : [{"id":7,"title":"Sofa cum beds","description":"Auspicious","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/H9fiFyAGv_fI2XOQkK1H94Rru4rJmknl.jpg","products_price":"2000","products_brand":"Furnicheer","product_material":"Sheesham","wishlist":"0","max_quantity":"19"},{"id":24,"title":"variant sofas","description":"this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product \r\n423423546547658769","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/-K5q_YEFokoLVDjRN41Y5K_MQoudPniD.jpg","products_price":"1500","products_brand":"","product_material":"Sheesham, Fabric, Engeneering Wood, teek ply","wishlist":"0","max_quantity":"19"},{"id":30,"title":"beds sofas","description":"beds sofas","product_image":"","products_price":"1000","products_brand":"Furnicheer","product_material":"","wishlist":"0","max_quantity":"0"},{"id":42,"title":"Sofa","description":"7 Seater grey Coloured Sofa","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/Q0Lqac8TZXe8qBh3IHc8guPUXXaE2YqR.jpg","products_price":"50000","products_brand":"Woodie","product_material":"Engeneering Wood","wishlist":"0","max_quantity":"20"}]
     */

    private String status;
    private List<ProductsDataBean> products_data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProductsDataBean> getProducts_data() {
        return products_data;
    }

    public void setProducts_data(List<ProductsDataBean> products_data) {
        this.products_data = products_data;
    }

    public static class ProductsDataBean {
        /**
         * id : 7
         * title : Sofa cum beds
         * description : Auspicious
         * product_image : http://dev.miracleglobal.com/kartku-php/web/uploads/product/medium/H9fiFyAGv_fI2XOQkK1H94Rru4rJmknl.jpg
         * products_price : 2000
         * products_brand : Furnicheer
         * product_material : Sheesham
         * wishlist : 0
         * max_quantity : 19
         */

        private int id;
        private String title;
        private String description;
        private String product_image;
        private String products_price;
        private String products_brand;
        private String product_material;
        private String wishlist;
        private String max_quantity;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getProducts_price() {
            return products_price;
        }

        public void setProducts_price(String products_price) {
            this.products_price = products_price;
        }

        public String getProducts_brand() {
            return products_brand;
        }

        public void setProducts_brand(String products_brand) {
            this.products_brand = products_brand;
        }

        public String getProduct_material() {
            return product_material;
        }

        public void setProduct_material(String product_material) {
            this.product_material = product_material;
        }

        public String getWishlist() {
            return wishlist;
        }

        public void setWishlist(String wishlist) {
            this.wishlist = wishlist;
        }

        public String getMax_quantity() {
            return max_quantity;
        }

        public void setMax_quantity(String max_quantity) {
            this.max_quantity = max_quantity;
        }
    }
}
