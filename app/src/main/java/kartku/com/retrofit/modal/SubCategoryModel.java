package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 1/12/2017.
 */

public class SubCategoryModel {

    /**
     * sub_subcat_id : 11
     * sub_subcat_name : Sofas
     */

    private String sub_subcat_id;
    private String sub_subcat_name;

    public String getSub_subcat_id() {
        return sub_subcat_id;
    }

    public void setSub_subcat_id(String sub_subcat_id) {
        this.sub_subcat_id = sub_subcat_id;
    }

    public String getSub_subcat_name() {
        return sub_subcat_name;
    }

    public void setSub_subcat_name(String sub_subcat_name) {
        this.sub_subcat_name = sub_subcat_name;
    }
}
