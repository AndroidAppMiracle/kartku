package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 4/18/2017.
 */

public class ProductColourOptionsModal {

    /**
     * status : OK
     * list : [{"name":"White","id":13},{"name":"Red","id":14},{"name":"Blue","id":15}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * name : White
         * id : 13
         */

        private String name;
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
