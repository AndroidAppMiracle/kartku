package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/15/2017.
 */

public class CategoryProductsModal {


    /**
     * status : OK
     * products_data : [{"id":9,"title":"Benches","description":"Benches","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/thumbcCQnlVVlz3cyd5CntsT8bZR2pMyzHXNk.jpg","products_price":"3000","products_brand":"test","product_material":"","max_quantity":"3","wishlist":0},{"id":17,"title":"My new product","description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/thumbQQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg","products_price":"180000","products_brand":"","product_material":"","max_quantity":"15","wishlist":1},{"id":18,"title":"My second product ","description":"hjghj hkghkghjkghjghj hgjghjgfjgfjgfjgj","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/thumbYieS5L2v09QlsCzweFwDvDD340c8IDsi.jpg","products_price":"800","products_brand":"","product_material":"","max_quantity":"1","wishlist":0},{"id":24,"title":"variant sofas","description":"this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product this is my test product \r\n423423546547658769","product_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/product/thumb-K5q_YEFokoLVDjRN41Y5K_MQoudPniD.jpg","products_price":"1500","products_brand":"","product_material":"Sheesham, Fabric, Engeneering Wood, teek ply","max_quantity":"19","wishlist":0},{"id":30,"title":"beds sofas","description":"beds sofas","product_image":"","products_price":"1000","products_brand":"","product_material":"","max_quantity":"","wishlist":1}]
     */

    private String status;
    private List<ProductsDataBean> products_data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProductsDataBean> getProducts_data() {
        return products_data;
    }

    public void setProducts_data(List<ProductsDataBean> products_data) {
        this.products_data = products_data;
    }

    public static class ProductsDataBean {
        /**
         * id : 9
         * title : Benches
         * description : Benches
         * product_image : http://dev.miracleglobal.com/kartku-php/web/uploads/product/thumbcCQnlVVlz3cyd5CntsT8bZR2pMyzHXNk.jpg
         * products_price : 3000
         * products_brand : test
         * product_material :
         * max_quantity : 3
         * wishlist : 0
         */

        private int id;
        private String title;
        private String description;
        private String product_image;
        private String products_price;
        private String products_brand;
        private String product_material;
        private String max_quantity;
        private int wishlist;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getProducts_price() {
            return products_price;
        }

        public void setProducts_price(String products_price) {
            this.products_price = products_price;
        }

        public String getProducts_brand() {
            return products_brand;
        }

        public void setProducts_brand(String products_brand) {
            this.products_brand = products_brand;
        }

        public String getProduct_material() {
            return product_material;
        }

        public void setProduct_material(String product_material) {
            this.product_material = product_material;
        }

        public String getMax_quantity() {
            return max_quantity;
        }

        public void setMax_quantity(String max_quantity) {
            this.max_quantity = max_quantity;
        }

        public int getWishlist() {
            return wishlist;
        }

        public void setWishlist(int wishlist) {
            this.wishlist = wishlist;
        }
    }
}
