package kartku.com.retrofit.modal;

/**
 * Created by Kshitiz Bali on 3/15/2017.
 */

public class RemoveFromWishlistModal {

    /**
     * status : OK
     * message : Product deleted from Wishlist.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
