package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/16/2017.
 */

public class AddToCartModal {


    /**
     * status : OK
     * result : Product Added to cart
     * cart_info : {"id":295,"user_id":189,"session_id":null,"promocode_id":null,"discount":"","total_amount":18150,"order_status":null,"created_at":"2017-03-10","updated_at":null}
     * item_info : [{"id":676,"cart_id":295,"product_id":9,"quantity":"6","shipping_status":"Confirmed","pickup_city":null,"pickup_city_code":null,"dest_city":null,"dest_city_code":null,"shipping_cost":"","commission":"60","created_at":"2017-03-15","updated_at":null},{"id":677,"cart_id":295,"product_id":21,"quantity":"1","shipping_status":"Confirmed","pickup_city":null,"pickup_city_code":null,"dest_city":null,"dest_city_code":null,"shipping_cost":"","commission":"3","created_at":"2017-03-16","updated_at":null}]
     */

    private String status;
    private String result;
    private CartInfoBean cart_info;
    private List<ItemInfoBean> item_info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public CartInfoBean getCart_info() {
        return cart_info;
    }

    public void setCart_info(CartInfoBean cart_info) {
        this.cart_info = cart_info;
    }

    public List<ItemInfoBean> getItem_info() {
        return item_info;
    }

    public void setItem_info(List<ItemInfoBean> item_info) {
        this.item_info = item_info;
    }

    public static class CartInfoBean {
        /**
         * id : 295
         * user_id : 189
         * session_id : null
         * promocode_id : null
         * discount :
         * total_amount : 18150
         * order_status : null
         * created_at : 2017-03-10
         * updated_at : null
         */

        private int id;
        private int user_id;
        private Object session_id;
        private Object promocode_id;
        private String discount;
        private int total_amount;
        private Object order_status;
        private String created_at;
        private Object updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public Object getSession_id() {
            return session_id;
        }

        public void setSession_id(Object session_id) {
            this.session_id = session_id;
        }

        public Object getPromocode_id() {
            return promocode_id;
        }

        public void setPromocode_id(Object promocode_id) {
            this.promocode_id = promocode_id;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public int getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(int total_amount) {
            this.total_amount = total_amount;
        }

        public Object getOrder_status() {
            return order_status;
        }

        public void setOrder_status(Object order_status) {
            this.order_status = order_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }
    }

    public static class ItemInfoBean {
        /**
         * id : 676
         * cart_id : 295
         * product_id : 9
         * quantity : 6
         * shipping_status : Confirmed
         * pickup_city : null
         * pickup_city_code : null
         * dest_city : null
         * dest_city_code : null
         * shipping_cost :
         * commission : 60
         * created_at : 2017-03-15
         * updated_at : null
         */

        private int id;
        private int cart_id;
        private int product_id;
        private String quantity;
        private String shipping_status;
        private Object pickup_city;
        private Object pickup_city_code;
        private Object dest_city;
        private Object dest_city_code;
        private String shipping_cost;
        private String commission;
        private String created_at;
        private Object updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCart_id() {
            return cart_id;
        }

        public void setCart_id(int cart_id) {
            this.cart_id = cart_id;
        }

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getShipping_status() {
            return shipping_status;
        }

        public void setShipping_status(String shipping_status) {
            this.shipping_status = shipping_status;
        }

        public Object getPickup_city() {
            return pickup_city;
        }

        public void setPickup_city(Object pickup_city) {
            this.pickup_city = pickup_city;
        }

        public Object getPickup_city_code() {
            return pickup_city_code;
        }

        public void setPickup_city_code(Object pickup_city_code) {
            this.pickup_city_code = pickup_city_code;
        }

        public Object getDest_city() {
            return dest_city;
        }

        public void setDest_city(Object dest_city) {
            this.dest_city = dest_city;
        }

        public Object getDest_city_code() {
            return dest_city_code;
        }

        public void setDest_city_code(Object dest_city_code) {
            this.dest_city_code = dest_city_code;
        }

        public String getShipping_cost() {
            return shipping_cost;
        }

        public void setShipping_cost(String shipping_cost) {
            this.shipping_cost = shipping_cost;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }
    }
}
