package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 4/5/2017.
 */

public class MainCategoriesModal {

    /**
     * status : OK
     * categories_info : [{"category_id":"1","title":"Furniture","is_active":"1","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/Ao_nrH0Jgv5JsD2A8CVf-T1S52-6MEri.png","subcat_info":[{"subcat_id":"9","subcat_name":"LIVING ROOM","subcat_is_active":"1","sub_subcat_info":[{"sub_subcat_id":"11","sub_subcat_is_active":"1","sub_subcat_name":"Sofas"},{"sub_subcat_id":"36","sub_subcat_is_active":"1","sub_subcat_name":"Recliners"}]},{"subcat_id":"10","subcat_name":"BED ROOM","subcat_is_active":"1","sub_subcat_info":[{"sub_subcat_id":"38","sub_subcat_is_active":"1","sub_subcat_name":"furniture wooden"}]}]},{"category_id":"2","title":"Home Decor","is_active":"1","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/PBgTJ9W4gZLN7Foo4O2uYvIDBq0b-Tyv.png","subcat_info":[{"subcat_id":"17","subcat_name":"WALL SHELVES","subcat_is_active":"1","sub_subcat_info":[{"sub_subcat_id":"18","sub_subcat_is_active":"1","sub_subcat_name":"Contemprory"}]},{"subcat_id":"19","subcat_name":"CLOCKS","subcat_is_active":"1","sub_subcat_info":[{"sub_subcat_id":"20","sub_subcat_is_active":"1","sub_subcat_name":"wall clock"}]}]},{"category_id":"3","title":"Appliances","is_active":"1","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/e5kOfg2xPYOwKJWOcYPzaq12qlFg4D-y.png","subcat_info":[{"subcat_id":"22","subcat_name":"APPLIANCES1","subcat_is_active":"1","sub_subcat_info":[]},{"subcat_id":"23","subcat_name":"APPLIANCES2","subcat_is_active":"1","sub_subcat_info":[]}]},{"category_id":"4","title":"Furnishings","is_active":"1","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/Eoy2FZbZsmk9GwAt6NwoO84JZ8ajuam_.png","subcat_info":[{"subcat_id":"24","subcat_name":"FURNISHING1","subcat_is_active":"1","sub_subcat_info":[]},{"subcat_id":"25","subcat_name":"FURNISHING2","subcat_is_active":"1","sub_subcat_info":[]}]},{"category_id":"5","title":"Housekeeping","is_active":"1","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/kSUbB_FMtuvwOYB4RNwp0tqP6kLO_Hjq.png","subcat_info":[{"subcat_id":"26","subcat_name":"CURTAINS","subcat_is_active":"1","sub_subcat_info":[]},{"subcat_id":"27","subcat_name":"WALL HANGINGS","subcat_is_active":"1","sub_subcat_info":[]}]},{"category_id":"6","title":"Kitchen & Dining","is_active":"1","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/gh6frCKORsfqjrYF_ews_Sk6deGRA1DG.png","subcat_info":[{"subcat_id":"28","subcat_name":"KITCHEN SHELVES","subcat_is_active":"1","sub_subcat_info":[]},{"subcat_id":"29","subcat_name":"DINING TABLES","subcat_is_active":"1","sub_subcat_info":[]}]},{"category_id":"7","title":"Lamps & Lighting","is_active":"1","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/Gn5cX4nbWabXHiHKFZ77JS7Rn3la_YzT.png","subcat_info":[{"subcat_id":"30","subcat_name":"LAMP AND LIGHTING1","subcat_is_active":"1","sub_subcat_info":[]},{"subcat_id":"31","subcat_name":"LAMP AND LIGHTING2","subcat_is_active":"1","sub_subcat_info":[]}]},{"category_id":"8","title":"Bath","is_active":"0","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/nZjsXmEFa8h8vzwHBKATZ8t10H3AGdj4.png","subcat_info":[{"subcat_id":"32","subcat_name":"BATH TUBS","subcat_is_active":"1","sub_subcat_info":[]},{"subcat_id":"33","subcat_name":"BATHROOM DECOR","subcat_is_active":"1","sub_subcat_info":[]}]},{"category_id":"16","title":"Office Furniture","is_active":"1","category_image":"http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/hJu9JibuhejbWhwCIJRx0Ka0CCpafUVm.png","subcat_info":[{"subcat_id":"34","subcat_name":"OFFICE TABLES","subcat_is_active":"1","sub_subcat_info":[]},{"subcat_id":"35","subcat_name":"OFFICE CHAIRS","subcat_is_active":"1","sub_subcat_info":[]}]}]
     */

    private String status;
    private List<CategoriesInfoBean> categories_info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CategoriesInfoBean> getCategories_info() {
        return categories_info;
    }

    public void setCategories_info(List<CategoriesInfoBean> categories_info) {
        this.categories_info = categories_info;
    }

    public static class CategoriesInfoBean {
        /**
         * category_id : 1
         * title : Furniture
         * is_active : 1
         * category_image : http://dev.miracleglobal.com/kartku-php/web/uploads/categoryimages/full/Ao_nrH0Jgv5JsD2A8CVf-T1S52-6MEri.png
         * subcat_info : [{"subcat_id":"9","subcat_name":"LIVING ROOM","subcat_is_active":"1","sub_subcat_info":[{"sub_subcat_id":"11","sub_subcat_is_active":"1","sub_subcat_name":"Sofas"},{"sub_subcat_id":"36","sub_subcat_is_active":"1","sub_subcat_name":"Recliners"}]},{"subcat_id":"10","subcat_name":"BED ROOM","subcat_is_active":"1","sub_subcat_info":[{"sub_subcat_id":"38","sub_subcat_is_active":"1","sub_subcat_name":"furniture wooden"}]}]
         */

        private String category_id;
        private String title;
        private String is_active;
        private String category_image;
        private List<SubcatInfoBean> subcat_info;

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getCategory_image() {
            return category_image;
        }

        public void setCategory_image(String category_image) {
            this.category_image = category_image;
        }

        public List<SubcatInfoBean> getSubcat_info() {
            return subcat_info;
        }

        public void setSubcat_info(List<SubcatInfoBean> subcat_info) {
            this.subcat_info = subcat_info;
        }

        public static class SubcatInfoBean {
            /**
             * subcat_id : 9
             * subcat_name : LIVING ROOM
             * subcat_is_active : 1
             * sub_subcat_info : [{"sub_subcat_id":"11","sub_subcat_is_active":"1","sub_subcat_name":"Sofas"},{"sub_subcat_id":"36","sub_subcat_is_active":"1","sub_subcat_name":"Recliners"}]
             */

            private String subcat_id;
            private String subcat_name;
            private String subcat_is_active;
            private List<SubSubcatInfoBean> sub_subcat_info;

            public String getSubcat_id() {
                return subcat_id;
            }

            public void setSubcat_id(String subcat_id) {
                this.subcat_id = subcat_id;
            }

            public String getSubcat_name() {
                return subcat_name;
            }

            public void setSubcat_name(String subcat_name) {
                this.subcat_name = subcat_name;
            }

            public String getSubcat_is_active() {
                return subcat_is_active;
            }

            public void setSubcat_is_active(String subcat_is_active) {
                this.subcat_is_active = subcat_is_active;
            }

            public List<SubSubcatInfoBean> getSub_subcat_info() {
                return sub_subcat_info;
            }

            public void setSub_subcat_info(List<SubSubcatInfoBean> sub_subcat_info) {
                this.sub_subcat_info = sub_subcat_info;
            }

            public static class SubSubcatInfoBean {
                /**
                 * sub_subcat_id : 11
                 * sub_subcat_is_active : 1
                 * sub_subcat_name : Sofas
                 */

                private String sub_subcat_id;
                private String sub_subcat_is_active;
                private String sub_subcat_name;

                public String getSub_subcat_id() {
                    return sub_subcat_id;
                }

                public void setSub_subcat_id(String sub_subcat_id) {
                    this.sub_subcat_id = sub_subcat_id;
                }

                public String getSub_subcat_is_active() {
                    return sub_subcat_is_active;
                }

                public void setSub_subcat_is_active(String sub_subcat_is_active) {
                    this.sub_subcat_is_active = sub_subcat_is_active;
                }

                public String getSub_subcat_name() {
                    return sub_subcat_name;
                }

                public void setSub_subcat_name(String sub_subcat_name) {
                    this.sub_subcat_name = sub_subcat_name;
                }
            }
        }
    }
}
