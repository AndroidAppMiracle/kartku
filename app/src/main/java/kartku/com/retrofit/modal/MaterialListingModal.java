package kartku.com.retrofit.modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 4/24/2017.
 */

public class MaterialListingModal {

    /**
     * status : OK
     * material_info : [{"id":1,"name":"Sheesham"},{"id":2,"name":"Fabric"},{"id":3,"name":"Melamine"},{"id":4,"name":"MDF"},{"id":5,"name":"Engeneering Wood"},{"id":6,"name":"teek ply"},{"id":7,"name":"steel"},{"id":8,"name":"plastic"},{"id":9,"name":"wooden"},{"id":10,"name":"light material"},{"id":11,"name":"applinace matrial"},{"id":12,"name":"new applience"},{"id":13,"name":"Silk hard stuff"}]
     */

    private String status;
    private List<MaterialInfoBean> material_info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MaterialInfoBean> getMaterial_info() {
        return material_info;
    }

    public void setMaterial_info(List<MaterialInfoBean> material_info) {
        this.material_info = material_info;
    }

    public static class MaterialInfoBean {
        /**
         * id : 1
         * name : Sheesham
         */

        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
