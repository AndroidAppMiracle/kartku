package kartku.com.dashboard_module.model;

/**
 * Created by satoti.garg on 9/13/2016.
 */
public class BannersModel {

    String banner_url;

    public String getBanner_url() {
        return banner_url;
    }

    public void setBanner_url(String banner_url) {
        this.banner_url = banner_url;
    }
}
