package kartku.com.dashboard_module.manager;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.dashboard_module.api.HomeBannersApi;
import kartku.com.dashboard_module.listener.HomeBannersListener;
import kartku.com.registeration_module.api.RegisterApi;
import kartku.com.registeration_module.listener.RegisterListener;
import kartku.com.registeration_module.manager.RegisterManager;
import kartku.com.utils.API;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public class HomeBannersManager {

        private Context context;
        private HomeBannersListener listener;

        public HomeBannersManager(Context context , HomeBannersListener listener)
        {
            this.context = context;
            this.listener = listener;
        }

        public void sendRequest()
        {
                try {
                    new HomeBannersApi(HomeBannersManager.this, context).sendRequest(API.BANNERS_REQUEST_ID);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
        }


        public void onSuccess(String response)
        {
            listener.onBannersSuccess(response);
        }

        public void onError(String message)
        {

            listener.onBannersError(message);
        }

}
