package kartku.com.dashboard_module.api;

import android.content.Context;

import java.util.Map;

import kartku.com.dashboard_module.manager.HomeBannersManager;
import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public class HomeBannersApi implements CustomResponse {

    HomeBannersManager manager ;
    Context context;
    RequestServer request_server;

    public HomeBannersApi(HomeBannersManager manager , Context context)
    {
        this.manager = manager;
        this.context = context;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId){
        try {
            request_server = new RequestServer(context, requestId,this,"");
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
