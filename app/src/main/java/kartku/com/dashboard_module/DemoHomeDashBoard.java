package kartku.com.dashboard_module;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import kartku.com.R;
import kartku.com.activity.AboutUsFragment;
import kartku.com.activity.CategoryProductsFragment;
import kartku.com.activity.PrivacyPolicyFragment;
import kartku.com.activity.SubProductCategoryListFragment;
import kartku.com.activity.SearchProductsBrandActivity;
import kartku.com.adapter.DemoExpandableAdapter;
import kartku.com.adapter.ExpandableListAdapter;
import kartku.com.cart.CartDetailsNew;
import kartku.com.faq_module.FAQFragment;
import kartku.com.fragments.ContactFragment;
import kartku.com.fragments.MyOrdersFragment;
import kartku.com.fragments.MyProductReviewsFragment;
import kartku.com.fragments.ProfileFragment;
import kartku.com.fragments.WishlistFragment;
import kartku.com.login_module.Login;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.CartsByUserModal;
import kartku.com.retrofit.modal.EventBusCartUpdate;
import kartku.com.retrofit.modal.EventBusLogout;
import kartku.com.retrofit.modal.EventBusProductDetailUpdateModal;
import kartku.com.retrofit.modal.LanguageSelectModal;
import kartku.com.retrofit.modal.LogOutModal;
import kartku.com.retrofit.modal.MainCategoriesModal;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.sellermodule.SellerHomeDashBoard;
import kartku.com.utils.Constant;
import kartku.com.utils.TinyDB;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;


public class DemoHomeDashBoard extends Utility implements GoogleApiClient.OnConnectionFailedListener, ApiServerResponse {
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    ExpandableListView expand_list;
    DemoExpandableAdapter listAdapter;

    HashMap<String, JSONArray> listSubSubCategory;
    HashMap<String, String> listCategoryWithID;

    //HashMap<String, List<String>> listDataChild;
    //LinkedHashMap<String, String> listHeadCategory;

    //HashMap<String, String> listHeadCategory;

    /*HashMap<String, List<String>> listSubCategory;*/

    HashMap<String, List<MainCategoriesModal.CategoriesInfoBean.SubcatInfoBean.SubSubcatInfoBean>> subSubcategoryRetrofit;


    private SharedPreferences prefrence;
    //RecyclerView banner_recycleview;

    private FragmentManager fragmentManager;
    private BannerFragment fragment;
    private String tag;
    //private SharedPreferences pref;
    private boolean login_status;
    String loginType; /*selectedCategory*/
    TextView tv_count;

    private GoogleApiClient mGoogleApiClient;

    //boolean mResolveOnFail;

    //ProgressBar progressBar;
   /* private int[] categoryImagesStatic = {
            R.drawable.ic_category_furniture,
            R.drawable.ic_category_decor,
            R.drawable.ic_category_appliances,
            R.drawable.ic_category_furnishing,
            R.drawable.ic_category_furniture,
            R.drawable.ic_category_decor,
            R.drawable.ic_category_appliances,
            R.drawable.ic_category_furnishing,
            R.drawable.ic_category_appliances,
            R.drawable.ic_category_furnishing
    };*/

    private String[] categoryImagesLink;
    ExpandableListAdapter adapter;
    private int lastExpandedPosition = -1;

    TinyDB tinydb;
    String selectedLanguage = "";


    List<String> subCategoriesName = new ArrayList<>();
    List<String> subCategoryID = new ArrayList<>();
    List<String> listDataHeader = new ArrayList<String>();
    List<String> listDataHeaderImages = new ArrayList<>();
    LinkedHashMap<String, List<String>> listSubCategory = new LinkedHashMap<String, List<String>>();

    private LinkedHashMap<String, String> subsubCatNameID = new LinkedHashMap<String, String>();
    private List<String> newSubCategoryNamesOnly = new ArrayList<>();

    HashMap<String, List<String>> demo = new HashMap<String, List<String>>();


    List<MainCategoriesModal.CategoriesInfoBean> categoriesInfoBeen = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            loginWithGoogle();
            FacebookSdk.sdkInitialize(DemoHomeDashBoard.this);
            setContentView(R.layout.activity_home_dashboard);

            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
            }

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            expand_list = (ExpandableListView) findViewById(R.id.expandable_category);
            prefrence = new ObscuredSharedPreferences(DemoHomeDashBoard.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
            fragmentManager = getSupportFragmentManager();


            // attachBaseContext(DemoHomeDashBoard.this);
            tinydb = new TinyDB(DemoHomeDashBoard.this);

            /*progressBar = new ProgressBar()*/
            //pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
            login_status = Session.get_login_statuc(prefrence);
            loginType = Session.getLogin_type(prefrence);
            Log.d("login_status", "login_status ***** " + login_status);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.title_activity_home_dashboard);

            try {
                initNavigationDrawer();
            } catch (Exception e) {
                e.printStackTrace();
            }


            /*try{
                Intent intent = new Intent(Home_dashboard.this, ProductList.class);
                startActivity(intent);
            }catch (Exception e)
            {
                e.printStackTrace();
            }*/

            // preparing list data




            /*prepareListData();*/

            if (checkInternetConnection(DemoHomeDashBoard.this)) {
                showLoading();
                ServerAPI.getInstance().getMainCategories(ApiServerResponse.MAIN_CATEGORIES, Session.get_userId(prefrence), this);
            } else {

                showAlertDialog(DemoHomeDashBoard.this);
            }


            if (checkInternetConnection(DemoHomeDashBoard.this)) {

                showLoading();

                ServerAPI.getInstance().getCartsByUser(ApiServerResponse.CARTS_BY_USER, Session.get_userId(prefrence), this);
            }

            //prepareListDataDemo();

            //adapter = new ExpandableListAdapter(DemoHomeDashBoard.this, listDataHeader, listSubCategory, listDataHeaderImages);

            //listAdapter = new DemoExpandableAdapter(DemoHomeDashBoard.this, listDataHeader, listSubCategory, fragmentManager, R.id.content_frame, login_status, listDataHeaderImages);

            // setting list adapter
           /* expand_list.setAdapter(adapter);*/

            try {
                fragment = new BannerFragment();
                tag = "Home";
                swtichFragement(R.id.content_frame, fragment, tag);
            } catch (Exception e) {
                e.printStackTrace();
            }


            expand_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {

                    // ToastMsg.showLongToast(DemoHomeDashBoard.this, "Group Clicked " + listDataHeader.get(groupPosition));

                    Session.setselectedCategory(prefrence, listDataHeader.get(groupPosition).trim());
                    switch (listDataHeader.get(groupPosition).trim()) {
                        case "Home":
                            drawerLayout.closeDrawers();
                            fragment = new BannerFragment();
                            tag = "Home";

                            swtichFragement(R.id.content_frame, fragment, tag);
                            break;

                        case "Login":
                            drawerLayout.closeDrawers();
                            //LoginFragment fragmentLogin = new LoginFragment();
                            tag = "Login";
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                            Intent intent = new Intent(DemoHomeDashBoard.this, Login.class);
                            startActivity(intent);
                            finish();
                            break;
                        case "FAQ's":
                            drawerLayout.closeDrawers();
                            FAQFragment fragmentFaq = new FAQFragment();
                            tag = "FAQ";
                            swtichFragement(R.id.content_frame, fragmentFaq, tag);

                            break;
                        case "Contact Us":
                            drawerLayout.closeDrawers();
                            ContactFragment contactFragment = new ContactFragment();
                            tag = "Contact Us";
                            swtichFragement(R.id.content_frame, contactFragment, tag);

                            break;
                        case "My Orders":
                            drawerLayout.closeDrawers();
                            MyOrdersFragment ordersFragment = new MyOrdersFragment();
                            /*OrdersFragment ordersFragment = new OrdersFragment();*/
                            tag = "My Orders";
                            swtichFragement(R.id.content_frame, ordersFragment, tag);
                            break;
                        case "My Product Review":
                            drawerLayout.closeDrawers();
                            MyProductReviewsFragment myProductReviewsFragment = new MyProductReviewsFragment();
                            /*ProductReviewFragment productReviewFragment = new ProductReviewFragment();*/
                            tag = "My Product Review";
                            swtichFragement(R.id.content_frame, myProductReviewsFragment, tag);
                            break;
                        case "My Wishlist":
                            drawerLayout.closeDrawers();
                            WishlistFragment wishlistFragment = new WishlistFragment();
                            tag = "My Wishlist";
                            swtichFragement(R.id.content_frame, wishlistFragment, tag);
                            break;
                        case "My Profile":
                            drawerLayout.closeDrawers();
                            ProfileFragment profileFragment = new ProfileFragment();
                            tag = "My Profile";
                            swtichFragement(R.id.content_frame, profileFragment, tag);
                            break;
                        case "Logout":
                            switch (loginType) {
                                case "google":
                                    if (mGoogleApiClient.isConnected()) {
                                        signOut();
                                        drawerLayout.closeDrawers();
                                        //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                                        mGoogleApiClient.disconnect();


                                        ServerAPI.getInstance().logout(ApiServerResponse.LOGOUT_FRAGMENT, Session.get_userId(prefrence), DemoHomeDashBoard.this);

                                        /*Session.set_login_status(prefrence, false);
                                        prefrence.edit().clear().apply();
                                        *//*LoginFragment fragmentLoginLogout = new LoginFragment();
                                        tag = "Login";
                                        swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);*//*
                                        tag = "Login";
                            *//*swtichFragement(R.id.content_frame, fragmentLogin, tag);*//*
                                        Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                                        startActivity(i);
                                        finish();*/

                                    }
                                    break;
                                case "facebook": {
                                    LoginManager.getInstance().logOut();
                                    drawerLayout.closeDrawers();

                                    ServerAPI.getInstance().logout(ApiServerResponse.LOGOUT_FRAGMENT, Session.get_userId(prefrence), DemoHomeDashBoard.this);

                                    /*Session.set_login_status(prefrence, false);
                                    prefrence.edit().clear().apply();
                                   *//* LoginFragment fragmentLoginLogout = new LoginFragment();
                                    tag = "Login";
                                    swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);*//*
                                    tag = "Login";
                            *//*swtichFragement(R.id.content_frame, fragmentLogin, tag);*//*
                                    Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                                    startActivity(i);
                                    finish();*/
                                    break;
                                }
                                default: {
                                    drawerLayout.closeDrawers();

                                    ServerAPI.getInstance().logout(ApiServerResponse.LOGOUT_FRAGMENT, Session.get_userId(prefrence), DemoHomeDashBoard.this);

                                   /* Session.set_login_status(prefrence, false);
                                    prefrence.edit().clear().apply();
                                    // LoginFragment fragmentLoginLogout = new LoginFragment();
                                    tag = "Login";
                                    //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                                    //prepareListData();
                                    Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                                    startActivity(i);
                                    finish();*/
                                   /* fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);*/

                                    break;
                                }
                            }
                            break;
                        default:
                           /* fragment = new BannerFragment();
                            tag = "Home";
                            swtichFragement(R.id.content_frame, fragment, tag);
                            drawerLayout.closeDrawers();*/

                            break;
                    }

                    // selectedCategory = listDataHeader.get(groupPosition).trim();
                    return false;
                }
            });

            // Listview Group expanded listener
            expand_list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                @Override
                public void onGroupExpand(int groupPosition) {
                   /* Toast.makeText(getApplicationContext(),
                            listDataHeader.get(groupPosition) + " Expanded",
                            Toast.LENGTH_SHORT).show();*/

                    //ToastMsg.showLongToast(DemoHomeDashBoard.this, listDataHeader.get(groupPosition) + " Expanded");
                    if (lastExpandedPosition != -1
                            && groupPosition != lastExpandedPosition) {
                        expand_list.collapseGroup(lastExpandedPosition);
                    }
                    lastExpandedPosition = groupPosition;
                    /*if (expand_list.isGroupExpanded(groupPosition)) {
                        expand_list.collapseGroup(groupPosition);
                    }*/
                }
            });

            // Listview Group collasped listener
            expand_list.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {
                   /* Toast.makeText(getApplicationContext(),
                            Toast.LENGTH_SHORT).show();
*/
                    //ToastMsg.showLongToast(DemoHomeDashBoard.this, listDataHeader.get(groupPosition) + " Collapsed");
                }
            });

            // Listview on child click listener
            expand_list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                                            int groupPosition, int childPosition, long id) {
                    // TODO Auto-generated method stub
                    try {


                        if (subsubCatNameID.size() != 0 && newSubCategoryNamesOnly.size() != 0) {


                            // String grp = subsubCatNameID.get("Living Room");
                            //Log.i("Exad", subsubCatNameID.get("Living Room"));
                            //String grpChild = newSubCategoryNamesOnly.get(childPosition);
                            //Log.i("Expandable SubList", newSubCategoryNamesOnly.get(childPosition));


                            if (categoriesInfoBeen.get(groupPosition).getSubcat_info().get(childPosition).getSubcat_is_active().equalsIgnoreCase("1")) {


                                if (categoriesInfoBeen.get(groupPosition).getSubcat_info().get(childPosition).getSub_subcat_info().size() != 0) {
                                    String subcat_id = categoriesInfoBeen.get(groupPosition).getSubcat_info().get(childPosition).getSubcat_id();
                                    SubProductCategoryListFragment fragment = new SubProductCategoryListFragment();
                                    tag = categoriesInfoBeen.get(groupPosition).getSubcat_info().get(childPosition).getSubcat_name();
                                    Bundle args = new Bundle();
                                    args.putString("SubSubCategoryID", subcat_id);
                                    fragment.setArguments(args);
                                    swtichFragement(R.id.content_frame, fragment, tag);
                                    expand_list.collapseGroup(groupPosition);
                                    drawerLayout.closeDrawers();
                                } else {
                                    String subcat_id = categoriesInfoBeen.get(groupPosition).getSubcat_info().get(childPosition).getSubcat_id();
                                    String subcat_name = categoriesInfoBeen.get(groupPosition).getSubcat_info().get(childPosition).getSubcat_name();
                                    CategoryProductsFragment fragment = new CategoryProductsFragment();
                                    tag = categoriesInfoBeen.get(groupPosition).getSubcat_info().get(childPosition).getSubcat_name();
                                    Bundle args = new Bundle();
                                    args.putString("SubSubCategoryID", subcat_id);
                                    args.putString("SubSubCategoryName", subcat_name);
                                    fragment.setArguments(args);
                                    swtichFragement(R.id.content_frame, fragment, tag);
                                    expand_list.collapseGroup(groupPosition);
                                    drawerLayout.closeDrawers();

                                }

                            } else {
                                showAlertCategoryNoAvailable();
                            }


                            //String pos1 = demo.get(groupPosition).get(childPosition);
                            //String pos = subsubCatNameID.get(newSubCategoryNamesOnly.get(childPosition));

                            //ToastMsg.showLongToast(DemoHomeDashBoard.this, pos);
                        }

                        /*final String selected = (String) adapter.getChild(
                                groupPosition, childPosition);

                        //ToastMsg.showLongToast(DemoHomeDashBoard.this, "child selected isChildSelectable  " + selected + " childPosition  " + childPosition);

                        Log.i("ArraSUBSUB ", "" + listSubSubCategory.get(selected));
                        Log.i("ArrSUBSUB ", "" + subSubcategoryRetrofit.get(selected));


                        //use the subsubcategorylist
                        if (checkInternetConnection(DemoHomeDashBoard.this)) {
                            if (listSubSubCategory.get(selected).length() > 0) {
                                //Session.setselectedSub_SubCategoryJSON(prefrence, listSubSubCategory.get(selected).toString());
                                //Session.setselectedCategory(prefrence, Session.getselectedCategory(prefrence));
                                //Session.setselectedSubCategory(prefrence, selected);

                                SubProductCategoryListFragment fragment = new SubProductCategoryListFragment();
                                tag = selected;
                                Bundle args = new Bundle();
                                args.putString("SubSubCategoryID", subSubcategoryRetrofit.get(selected).toString());
                                fragment.setArguments(args);
                                swtichFragement(R.id.content_frame, fragment, tag);
                                expand_list.collapseGroup(groupPosition);
                                drawerLayout.closeDrawers();
                            } else {

                                Bundle data = new Bundle();
                                data.putString(Constant.SELECTED_CATEGORY_INDEX, listCategoryWithID.get(selected));
                                Session.setselectedCategory(prefrence, Session.getselectedCategory(prefrence));
                                Session.setselectedSubCategory(prefrence, selected);
                                Session.setselectedSubCategoryID(prefrence, listCategoryWithID.get(selected));
                                CategoryProductsFragment fragment = new CategoryProductsFragment();
                                fragment.setArguments(data);
                                tag = selected;
                                swtichFragement(R.id.content_frame, fragment, tag);
                                expand_list.collapseGroup(groupPosition);
                                drawerLayout.closeDrawers();
                            }

                        } else {
                            showAlertDialog(DemoHomeDashBoard.this);

                        }*/

                        //ToastMsg.showLongToast(DemoHomeDashBoard.this, " selectedArray" + listSubSubCategory.get(selected).toString());




                        /*System.out.println("child selected isChildSelectable  " + selected + " childPosition  " + childPosition);*/
                      /*  Session.setselectedCategory(prefrence, selected.trim());
                        Session.set_selected_category_index(prefrence, String.valueOf(childPosition));
                        CategoriesFragment fragment = new CategoriesFragment();
                        tag = "Categories";
                        swtichFragement(R.id.content_frame, fragment, tag);
                        expand_list.collapseGroup(groupPosition);
                        drawerLayout.closeDrawers();
                        asdasd*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                   /* Toast.makeText(
                            getApplicationContext(),
                            listDataHeader.get(groupPosition)
                                    + " : "
                                    + listSubCategory.get(
                                    listDataHeader.get(groupPosition)).get(
                                    childPosition), Toast.LENGTH_SHORT)
                            .show();*/
                    return false;
                }
            });



/*            expand_list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    final String selected = (String) adapter.getChild(
                            groupPosition, childPosition);
                    System.out.println("child selected isChildSelectable  " + selected + " childPosition  " + childPosition);

                    try {
                        *//*Intent intent = new Intent(Home_dashboard.this, CategoriesActivity.class);
                        intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, childPosition);
                        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                            drawerLayout.closeDrawers();
                        }
                        startActivity(intent);*//*
                        //Log.i("Selected Category", )

                        Session.setselectedCategory(prefrence, selected.trim());
                        Session.set_selected_category_index(prefrence, String.valueOf(childPosition));
                        CategoriesFragment fragment = new CategoriesFragment();
                        tag = "Categories";
                        swtichFragement(R.id.content_frame, fragment, tag);
                        expand_list.collapseGroup(groupPosition);
                        drawerLayout.closeDrawers();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            });*/

/*
            expand_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                    Log.d("DEBUG", "heading clicked");

                    if (login_status) {
                        switch (i) {

                            case 0:
                                break;
                            case 1:
                                drawerLayout.closeDrawers();
                                fragment = new BannerFragment();
                                tag = "Home";
                                swtichFragement(R.id.content_frame, fragment, tag);

                                break;
                            case 2:
                                drawerLayout.closeDrawers();
                                OrdersFragment ordersFragment = new OrdersFragment();
                                tag = "My Orders";
                                swtichFragement(R.id.content_frame, ordersFragment, tag);

                                break;
                            case 3:
                                drawerLayout.closeDrawers();
                                ProductReviewFragment productReviewFragment = new ProductReviewFragment();
                                tag = "My Product Review";
                                swtichFragement(R.id.content_frame, productReviewFragment, tag);

                                break;
                            case 4:
                                drawerLayout.closeDrawers();
                                WishlistFragment wishlistFragment = new WishlistFragment();
                                tag = "My Wishlist";
                                swtichFragement(R.id.content_frame, wishlistFragment, tag);

                                break;
                            case 5:
                                drawerLayout.closeDrawers();
                                ProfileFragment profileFragment = new ProfileFragment();
                                tag = "My Profile";
                                swtichFragement(R.id.content_frame, profileFragment, tag);

                                break;
                            case 6:
                                drawerLayout.closeDrawers();
                                ContactFragment contactFragment = new ContactFragment();
                                tag = "Contact Us";
                                swtichFragement(R.id.content_frame, contactFragment, tag);

                                break;
                            case 7:

                                if (loginType.equals("google")) {
                                    if (mGoogleApiClient.isConnected()) {
                                        signOut();
                                        drawerLayout.closeDrawers();
                                        //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                                        mGoogleApiClient.disconnect();
                                        Session.set_login_status(prefrence, false);
                                        prefrence.edit().clear().apply();
                                        LoginFragment fragmentLogin = new LoginFragment();
                                        tag = "Login";
                                        swtichFragement(R.id.content_frame, fragmentLogin, tag);

                                    }
                                } else if (loginType.equals("facebook")) {
                                    LoginManager.getInstance().logOut();
                                    drawerLayout.closeDrawers();
                                    Session.set_login_status(prefrence, false);
                                    prefrence.edit().clear().apply();
                                    LoginFragment fragmentLogin = new LoginFragment();
                                    tag = "Login";
                                    swtichFragement(R.id.content_frame, fragmentLogin, tag);
                                } else {
                                    drawerLayout.closeDrawers();
                                    Session.set_login_status(prefrence, false);
                                    prefrence.edit().clear().apply();
                                    LoginFragment fragmentLogin = new LoginFragment();
                                    tag = "Login";
                                    swtichFragement(R.id.content_frame, fragmentLogin, tag);
                                }


                                break;
                            default:
                                fragment = new BannerFragment();
                                tag = "Home";
                                swtichFragement(R.id.content_frame, fragment, tag);
                                drawerLayout.closeDrawers();
                                break;
                        }
                    } else {
                        switch (i) {

                            case 0:
                                break;
                            case 1:
                                drawerLayout.closeDrawers();
                                fragment = new BannerFragment();
                                tag = "Home";
                                swtichFragement(R.id.content_frame, fragment, tag);

                                break;
                            case 2:
                                drawerLayout.closeDrawers();
                                LoginFragment fragmentLogin = new LoginFragment();
                                tag = "Login";
                                swtichFragement(R.id.content_frame, fragmentLogin, tag);

                                break;
                            case 3:
                                drawerLayout.closeDrawers();
                                FAQFragment fragmentFaq = new FAQFragment();
                                tag = "FAQ";
                                swtichFragement(R.id.content_frame, fragmentFaq, tag);

                                break;
                            case 4:
                                drawerLayout.closeDrawers();
                                ContactFragment contactFragment = new ContactFragment();
                                tag = "Contact Us";
                                swtichFragement(R.id.content_frame, contactFragment, tag);

                                break;

                            default:
                                fragment = new BannerFragment();
                                tag = "Home";
                                swtichFragement(R.id.content_frame, fragment, tag);
                                break;
                        }
                    }

                    // Toast.makeText(Home_dashboard.this, "Count BackStack " + getSupportFragmentManager().getBackStackEntryCount(), Toast.LENGTH_LONG).show();
                    return false;
                }
            });
*/


        } catch (Exception e) {
            e.printStackTrace();
        }



       /* NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);*/
    }

    public void swtichFragement(int id, Fragment fragment, String tag) {
        try {
            this.tag = tag;
            Fragment _fragment = fragmentManager.findFragmentByTag(tag);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment, tag);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            /*fragmentManager.executePendingTransactions();*/
            getSupportActionBar().setTitle(tag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*getMenuInflater().inflate(R.menu.tool_menu, menu);*/

        if (login_status) {
            getMenuInflater().inflate(R.menu.menu_home_login, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_home_logout, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DemoHomeDashBoard.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_home:
                tag = "Home";

                BannerFragment bannerFragment = new BannerFragment();
                if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                    ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                }
                swtichFragement(R.id.content_frame, bannerFragment, tag);

                break;


            case R.id.search:


                Intent search = new Intent(DemoHomeDashBoard.this, SearchProductsBrandActivity.class);
                /*Intent search = new Intent(DemoHomeDashBoard.this, SearchProductsBrandActivity.class);*/
                startActivity(search);

                break;
            case R.id.menu_count:
                Intent in = new Intent(DemoHomeDashBoard.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
                /*Intent intent1 = new Intent(DemoHomeDashBoard.this, SellerHomeDashBoard.class);
                startActivity(intent1);*/
                break;
            case R.id.menu_my_orders:
               /* Intent intent1 = new Intent(DemoHomeDashBoard.this, SellerHomeDashBoard.class);
                startActivity(intent1);*/


                            /*OrdersFragment ordersFragment = new OrdersFragment();*/

                if (checkInternetConnection(DemoHomeDashBoard.this)) {
                    tag = "My Orders";
                    MyOrdersFragment ordersFragment = new MyOrdersFragment();
                    if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    }
                    swtichFragement(R.id.content_frame, ordersFragment, tag);
                } else {
                    showAlertDialog(DemoHomeDashBoard.this);
                    ;
                }


                break;
            case R.id.menu_my_product_review:

                if (checkInternetConnection(DemoHomeDashBoard.this)) {

                    MyProductReviewsFragment myProductReviewsFragment = new MyProductReviewsFragment();
                            /*ProductReviewFragment productReviewFragment = new ProductReviewFragment();*/
                    tag = "My Product Review";
                    if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    }
                    swtichFragement(R.id.content_frame, myProductReviewsFragment, tag);
                } else {
                    showAlertDialog(DemoHomeDashBoard.this);
                }


                break;
            case R.id.menu_my_wishlist:

                if (checkInternetConnection(DemoHomeDashBoard.this)) {

                    WishlistFragment wishlistFragment = new WishlistFragment();
                    tag = "My Wishlist";
                    if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    }
                    swtichFragement(R.id.content_frame, wishlistFragment, tag);
                } else {
                    showAlertDialog(DemoHomeDashBoard.this);
                }

                break;
            case R.id.menu_my_profile:

                if (checkInternetConnection(DemoHomeDashBoard.this)) {

                    ProfileFragment profileFragment = new ProfileFragment();
                    tag = "My Profile";
                    if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    }
                    swtichFragement(R.id.content_frame, profileFragment, tag);
                } else {
                    showAlertDialog(DemoHomeDashBoard.this);
                }

                break;
            case R.id.menu_contact_us:

                if (checkInternetConnection(DemoHomeDashBoard.this)) {

                    ContactFragment contactFragment = new ContactFragment();
                    tag = "Contact Us";
                    if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    }
                    swtichFragement(R.id.content_frame, contactFragment, tag);
                } else {
                    showAlertDialog(DemoHomeDashBoard.this);

                }


                break;

            case R.id.menu_change_language:
                if (checkInternetConnection(DemoHomeDashBoard.this)) {
                    showSelectLanguageAlertDialog();
                } else {
                    showAlertDialog(DemoHomeDashBoard.this);

                }


                break;

            case R.id.menu_privacy_policy:


                if (checkInternetConnection(DemoHomeDashBoard.this)) {

                    PrivacyPolicyFragment privacyPolicyFragment = new PrivacyPolicyFragment();
                    tag = "Privacy Policy";
                    if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    }
                    swtichFragement(R.id.content_frame, privacyPolicyFragment, tag);
                } else {
                    showAlertDialog(DemoHomeDashBoard.this);

                }

                break;
            case R.id.menu_about_us:

                if (checkInternetConnection(DemoHomeDashBoard.this)) {

                    AboutUsFragment aboutUsFragment = new AboutUsFragment();
                    tag = "About Us";
                    if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    }

                    // ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(prefrence), this);
                    swtichFragement(R.id.content_frame, aboutUsFragment, tag);
                } else {
                    showAlertDialog(DemoHomeDashBoard.this);

                }


                break;
            case R.id.menu_logout:
                switch (loginType) {
                    case "google":

                        if (checkInternetConnection(DemoHomeDashBoard.this)) {

                            if (mGoogleApiClient.isConnected()) {
                                signOut();
                                drawerLayout.closeDrawers();
                                //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                                mGoogleApiClient.disconnect();
                                Session.set_login_status(prefrence, false);
                                prefrence.edit().clear().apply();
                                Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                                startActivity(i);
                                ToastMsg.showShortToast(DemoHomeDashBoard.this, getString(R.string.logged_out));
                                finish();
                                /*LoginFragment fragmentLoginLogout = new LoginFragment();
                                tag = "Login";
                                swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);*/

                            }
                        } else {
                            showAlertDialog(DemoHomeDashBoard.this);

                        }

                        break;
                    case "facebook":
                        if (checkInternetConnection(DemoHomeDashBoard.this)) {


                            /*LoginManager.getInstance().logOut();*/
                            disconnectFromFacebook();
                            drawerLayout.closeDrawers();
                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                            startActivity(i);

                            ToastMsg.showShortToast(DemoHomeDashBoard.this, getString(R.string.logged_out));
                            finish();
                            /*LoginFragment fragmentLoginLogout = new LoginFragment();
                            tag = "Login";
                            swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);*/
                            break;

                        } else {
                            showAlertDialog(DemoHomeDashBoard.this);

                        }

                    default: {

                        if (checkInternetConnection(DemoHomeDashBoard.this)) {


                            drawerLayout.closeDrawers();
                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            //LoginFragment fragmentLoginLogout = new LoginFragment();
                            //tag = "Login";
                            //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                            //prepareListData();
                            Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                            startActivity(i);
                            ToastMsg.showShortToast(DemoHomeDashBoard.this, getString(R.string.logged_out));
                            finish();
                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        } else {
                            showAlertDialog(DemoHomeDashBoard.this);
                        }


                        break;
                    }
                }

                break;

            case R.id.menu_login:

                if (checkInternetConnection(DemoHomeDashBoard.this)) {


                    tag = "Login";
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                    Intent intent = new Intent(DemoHomeDashBoard.this, Login.class);
                    startActivity(intent);
                    finish();

                } else {
                    showAlertDialog(DemoHomeDashBoard.this);
                }
                break;
            case R.id.menu_faq:
                if (checkInternetConnection(DemoHomeDashBoard.this)) {


                    FAQFragment fragmentFaq = new FAQFragment();
                    tag = "FAQ";
                    if (!Session.getcart_id(prefrence).equalsIgnoreCase("0")) {
                        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    }
                    swtichFragement(R.id.content_frame, fragmentFaq, tag);

                } else {
                    showAlertDialog(DemoHomeDashBoard.this);
                }

                break;
        }
        return (super.onOptionsItemSelected(item));
    }


    /*
         * Preparing the list data
         */
  /*  private void prepareListData() {


        listDataHeader = new ArrayList<String>();
        listDataHeaderImages = new ArrayList<>();
        listSubSubCategory = new HashMap<String, JSONArray>();
        listCategoryWithID = new HashMap<String, String>();

        //listDataChild = new HashMap<String, List<String>>();

        //demoLinkedList = new LinkedHashMap<String, String>();

        listSubCategory = new LinkedHashMap<String, List<String>>();
        *//*listSubCategory = new LinkedHashMap<String, List<String>>();*//*
        //listHeadCategory = new LinkedHashMap<String, String>();


        List<String> categories_list = new ArrayList<String>();
        List<String> imageList = new ArrayList<String>();
        List<String> subCatList = new ArrayList<String>();

        *//*LinkedList<String> subCat = new LinkedList<String>();*//*


      *//*  List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");*//*

        String response = Session.get_main_categories_res(prefrence);
        try {
            Log.i("Response Home Cate", response);
            //Toast.makeText(Home_dashboard.this, "RESPONSEHOME " + response, Toast.LENGTH_LONG).show();
            JSONObject object = new JSONObject(response);
            JSONArray categories_info = object.getJSONArray(ResponseKeys.CATEGORIES_INFO);


            int k = 0;
            for (int j = 0; j < categories_info.length(); j++) {
                try {
                    JSONObject item_object = categories_info.getJSONObject(j);

                    //  JSONObject subCategory = subCategoryInfo.getJSONObject(j);
                    String title = item_object.getString(ResponseKeys.TITLE);
                    String image = item_object.getString(ResponseKeys.CATEGORY_IMAGE);
                    JSONArray subCategory = item_object.getJSONArray(ResponseKeys.SUBCAT_INFO);

                    categories_list.add(title);
                    imageList.add(image);

                    listDataHeader.add(title);
                    listDataHeaderImages.add(image);

                    //listHeadCategory.put(title, image);
                    //demoLinkedList.put(title, image);

                    *//*if (!subCatList.isEmpty()) {
                        subCatList.clear();
                    }*//*
                    for (int i = 0; i < subCategory.length(); i++) {
                        JSONObject sub_category_item = subCategory.getJSONObject(i);

                        JSONArray subSubCategory = sub_category_item.getJSONArray(ResponseKeys.SUB_SUB_CAT_INFO);

                        String subCatName = sub_category_item.getString(ResponseKeys.SUBCAT_NAME);
                        String subCatId = sub_category_item.getString(ResponseKeys.SUBCAT_ID);

                        listSubSubCategory.put(subCatName, subSubCategory);
                        listCategoryWithID.put(subCatName, subCatId);

                        subCatList.add(subCatName);


                        //subCat.add(j, subCatName);
                        *//*dsasd.get(j)*//*
                        *//*listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(i, i + 1)));*//*
                        //listSubCategory.put(categories_list.get(j), dsasd);
                        //listHeadCategory.get(listHeadCategory.keySet().toString());

                        //listSubCategory.put(categories_list.get(j), subCatList.get(i));
                    }


                    *//*listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(i, i + 1)));*//*
                    *//*listSubCategory.put(categories_list.get(j), subCatList);*//*

                 *//*   if (j == 0) {
                        //Range 0 - 2 - > values 0,1

                        listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(k, k + 2)));
                        *//**//*subCatList.remove(j);*//**//*
                    } else {

                        listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(k, k + 2)));
                    }*//*
                   *//* listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(j, j + 2)));*//*



                   *//* if (!subCatList.isEmpty()) {
                        *//**//*listSubCategory.put(listDataHeader.get(j), subCatList);*//**//*
                        listSubCategory.put(categories_list.get(j), subCatList);
                    } *//**//*else {
                        *//**//*listSubCategory.put(listDataHeader.get(j), top250);*//**//*
                        listSubCategory.put(categories_list.get(j), top250);
                    }*//*

                    *//*listSubCategory.put(categories_list.get(j), subCatList);*//*
                    *//*listSubCategory.put(listDataHeader.get(0), subCatList);*//*


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

           *//* if (j == categories_info.length()) {
                subCatList.add("Entry1");*//*
            //subCatList.add("Entry2");
            // ToastMsg.showLongToast(DemoHomeDashBoard.this, "Size before" + subCatList.size());
            subCatList.add(subCatList.size(), "Entry1");
            //  ToastMsg.showLongToast(DemoHomeDashBoard.this, "Size after" + subCatList.size());
           *//* }*//*

            try {

                for (int subCat = 0; subCat < categories_info.length(); subCat++) {
                    if (subCat == 0) {
                        //Range 0 - 2 - > values 0,1
                        k = k + 2;
                        listSubCategory.put(categories_list.get(subCat), new ArrayList<String>(subCatList.subList(subCat, k)));

                    } else {

                        listSubCategory.put(categories_list.get(subCat), new ArrayList<String>(subCatList.subList(k, k + 2)));
                        k = k + 2;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }



             *//*listSubCategory.put(categories_list.get(0), new ArrayList<String>(subCatList.subList(1, 2 )));*//*

*//*            if (login_status) {

                //listHeadCategory.putAll(imageList, categories_list);
                listDataHeader.add("Home");
                listDataHeader.add("My Orders");
                listDataHeader.add("My Product Review");
                listDataHeader.add("My Wishlist");
                listDataHeader.add("My Profile");
                listDataHeader.add("Contact Us");
                listDataHeader.add("Logout");

                listDataHeaderImages.add(imageList.get(0));
                listDataHeaderImages.add(imageList.get(1));
                listDataHeaderImages.add(imageList.get(2));
                listDataHeaderImages.add(imageList.get(3));
                listDataHeaderImages.add(imageList.get(4));
                listDataHeaderImages.add(imageList.get(5));
                listDataHeaderImages.add(imageList.get(6));

              *//**//*  listDataHeader.addAll(categories_list);
                listDataHeader.add("Categories");
                listDataHeader.add("Home");
                listDataHeader.add("My Orders");
                listDataHeader.add("My Product Review");
                listDataHeader.add("My Wishlist");
                listDataHeader.add("My Profile");
                listDataHeader.add("Contact Us");
                listDataHeader.add("Logout");*//**//*
            } else {
                //listDataHeader.addAll(categories_list);
                // Adding child data
                //listDataHeader.add("Categories");
                *//**//*listHeadCategory.put(imageList.get(0), "Home");
                listHeadCategory.put(imageList.get(1), "Login");
                listHeadCategory.put(imageList.get(2), "FAQ");
                listHeadCategory.put(imageList.get(3), "Contact Us");*//**//*
                listDataHeader.add("Home");
                listDataHeader.add("Login");
                listDataHeader.add("FAQ's");
                listDataHeader.add("Contact Us");
                listDataHeaderImages.add(imageList.get(0));
                listDataHeaderImages.add(imageList.get(1));
                listDataHeaderImages.add(imageList.get(2));
                listDataHeaderImages.add(imageList.get(3));
            }*//*
          *//*  for (int i = 0; i < listHeadCategory.size(); i++) {
                listSubCategory.put(listHeadCategory.get(i), subCatList.get(i));
            }*//*


            //listDataChild.put(listDataHeader.get(0), categories_list); // Header, Child data

            adapter = new ExpandableListAdapter(DemoHomeDashBoard.this, listDataHeader, listSubCategory, listDataHeaderImages);
            expand_list.setAdapter(adapter);

            ArrayList<String> saveCategoryOptions = new ArrayList<String>();
            saveCategoryOptions.addAll(listDataHeader);
            ArrayList<String> saveCategoryImages = new ArrayList<String>();
            saveCategoryOptions.addAll(listDataHeaderImages);

            tinydb.putListString(Constant.CATEGORY_OPTIONS, saveCategoryOptions);
            tinydb.putListString(Constant.CATEGORY_IMAGES, saveCategoryImages);


            // packagesharedPreferences(listDataHeader, listDataHeaderImages);
            //retriveSharedValue();

            *//*adapter.notifyDataSetChanged();*//*
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Adding child data

    }*/


   /* private void prepareListRetrofit(List<MainCategoriesModal.CategoriesInfoBean> list) {

        listCategoryWithID = new HashMap<String, String>();
        subSubcategoryRetrofit = new HashMap<String, List<MainCategoriesModal.CategoriesInfoBean.SubcatInfoBean.SubSubcatInfoBean>>();
        for (int j = 0; j < list.size() - 1; j++) {
            try {
                //JSONObject item_object = categories_info.getJSONObject(j);

                //  JSONObject subCategory = subCategoryInfo.getJSONObject(j);
                //String title = item_object.getString(ResponseKeys.TITLE);
                //String image = item_object.getString(ResponseKeys.CATEGORY_IMAGE);
                //JSONArray subCategory = item_object.getJSONArray(ResponseKeys.SUBCAT_INFO);
                List<MainCategoriesModal.CategoriesInfoBean.SubcatInfoBean> subList = list.get(j).getSubcat_info();

                //categories_list.add(title);
                //imageList.add(image);

                //listDataHeader.add(title);
                //istDataHeaderImages.add(image);

                //listHeadCategory.put(title, image);
                //demoLinkedList.put(title, image);

                    *//*if (!subCatList.isEmpty()) {
                        subCatList.clear();
                    }*//*


                for (int i = 0; i < subList.size() - 1; i++) {
                    //JSONObject sub_category_item = subCategory.getJSONObject(i);

                    //JSONArray subSubCategory = sub_category_item.getJSONArray(ResponseKeys.SUB_SUB_CAT_INFO);

                    List<MainCategoriesModal.CategoriesInfoBean.SubcatInfoBean.SubSubcatInfoBean> subsubList = subList.get(i).getSub_subcat_info();


                    String subCatName = subList.get(i).getSubcat_name();
                    String subCatId = subList.get(i).getSubcat_id();


                    subSubcategoryRetrofit.put(subCatName, subsubList);

                    //listSubSubCategory.put(subCatName, subsubList);
                    listCategoryWithID.put(subCatName, subCatId);

                    //subCatList.add(subCatName);


                    //subCat.add(j, subCatName);
                        *//*dsasd.get(j)*//*
                        *//*listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(i, i + 1)));*//*
                    //listSubCategory.put(categories_list.get(j), dsasd);
                    //listHeadCategory.get(listHeadCategory.keySet().toString());

                    //listSubCategory.put(categories_list.get(j), subCatList.get(i));
                }


                    *//*listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(i, i + 1)));*//*
                    *//*listSubCategory.put(categories_list.get(j), subCatList);*//*

                 *//*   if (j == 0) {
                        //Range 0 - 2 - > values 0,1

                        listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(k, k + 2)));
                        *//**//*subCatList.remove(j);*//**//*
                    } else {

                        listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(k, k + 2)));
                    }*//*
                   *//* listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(j, j + 2)));*//*



                   *//* if (!subCatList.isEmpty()) {
                        *//**//*listSubCategory.put(listDataHeader.get(j), subCatList);*//**//*
                        listSubCategory.put(categories_list.get(j), subCatList);
                    } *//**//*else {
                        *//**//*listSubCategory.put(listDataHeader.get(j), top250);*//**//*
                        listSubCategory.put(categories_list.get(j), top250);
                    }*//*

                    *//*listSubCategory.put(categories_list.get(j), subCatList);*//*
                    *//*listSubCategory.put(listDataHeader.get(0), subCatList);*//*


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }*/

/*    private void prepareListDataDemo() {


        //listDataHeader = new ArrayList<String>();
        //listDataHeaderImages = new ArrayList<>();
        listSubSubCategory = new HashMap<String, JSONArray>();
        listCategoryWithID = new HashMap<String, String>();

        //listDataChild = new HashMap<String, List<String>>();

        //demoLinkedList = new LinkedHashMap<String, String>();

        //listSubCategory = new LinkedHashMap<String, List<String>>();
        *//*listSubCategory = new LinkedHashMap<String, List<String>>();*//*
        //listHeadCategory = new LinkedHashMap<String, String>();


        //List<String> categories_list = new ArrayList<String>();
        //List<String> imageList = new ArrayList<String>();
        //List<String> subCatList = new ArrayList<String>();

        *//*LinkedList<String> subCat = new LinkedList<String>();*//*


      *//*  List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");*//*

        String response = Session.get_main_categories_res(prefrence);
        try {
            Log.i("Response Home Cate", response);
            //Toast.makeText(Home_dashboard.this, "RESPONSEHOME " + response, Toast.LENGTH_LONG).show();
            JSONObject object = new JSONObject(response);
            JSONArray categories_info = object.getJSONArray(ResponseKeys.CATEGORIES_INFO);


            //int k = 0;
            for (int j = 0; j < categories_info.length(); j++) {
                try {
                    JSONObject item_object = categories_info.getJSONObject(j);

                    //  JSONObject subCategory = subCategoryInfo.getJSONObject(j);
                    //String title = item_object.getString(ResponseKeys.TITLE);
                    //String image = item_object.getString(ResponseKeys.CATEGORY_IMAGE);
                    JSONArray subCategory = item_object.getJSONArray(ResponseKeys.SUBCAT_INFO);

                    //categories_list.add(title);
                    //imageList.add(image);

                    //listDataHeader.add(title);
                    //istDataHeaderImages.add(image);

                    //listHeadCategory.put(title, image);
                    //demoLinkedList.put(title, image);

                    *//*if (!subCatList.isEmpty()) {
                        subCatList.clear();
                    }*//*
                    for (int i = 0; i < subCategory.length(); i++) {
                        JSONObject sub_category_item = subCategory.getJSONObject(i);

                        JSONArray subSubCategory = sub_category_item.getJSONArray(ResponseKeys.SUB_SUB_CAT_INFO);

                        String subCatName = sub_category_item.getString(ResponseKeys.SUBCAT_NAME);
                        String subCatId = sub_category_item.getString(ResponseKeys.SUBCAT_ID);

                        listSubSubCategory.put(subCatName, subSubCategory);
                        listCategoryWithID.put(subCatName, subCatId);

                        //subCatList.add(subCatName);


                        //subCat.add(j, subCatName);
                        *//*dsasd.get(j)*//*
                        *//*listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(i, i + 1)));*//*
                        //listSubCategory.put(categories_list.get(j), dsasd);
                        //listHeadCategory.get(listHeadCategory.keySet().toString());

                        //listSubCategory.put(categories_list.get(j), subCatList.get(i));
                    }


                    *//*listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(i, i + 1)));*//*
                    *//*listSubCategory.put(categories_list.get(j), subCatList);*//*

                 *//*   if (j == 0) {
                        //Range 0 - 2 - > values 0,1

                        listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(k, k + 2)));
                        *//**//*subCatList.remove(j);*//**//*
                    } else {

                        listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(k, k + 2)));
                    }*//*
                   *//* listSubCategory.put(categories_list.get(j), new ArrayList<String>(subCatList.subList(j, j + 2)));*//*



                   *//* if (!subCatList.isEmpty()) {
                        *//**//*listSubCategory.put(listDataHeader.get(j), subCatList);*//**//*
                        listSubCategory.put(categories_list.get(j), subCatList);
                    } *//**//*else {
                        *//**//*listSubCategory.put(listDataHeader.get(j), top250);*//**//*
                        listSubCategory.put(categories_list.get(j), top250);
                    }*//*

                    *//*listSubCategory.put(categories_list.get(j), subCatList);*//*
                    *//*listSubCategory.put(listDataHeader.get(0), subCatList);*//*


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

           *//* if (j == categories_info.length()) {
                subCatList.add("Entry1");*//*
            //subCatList.add("Entry2");
            // ToastMsg.showLongToast(DemoHomeDashBoard.this, "Size before" + subCatList.size());
            //subCatList.add(subCatList.size(), "Entry1");
            //  ToastMsg.showLongToast(DemoHomeDashBoard.this, "Size after" + subCatList.size());
           *//* }*//*

          *//*  try {

                for (int subCat = 0; subCat < allCategories.size(); subCat++) {
                    if (subCat == 0) {
                        //Range 0 - 2 - > values 0,1
                        k = k + 2;
                        listSubCategory.put(categories_list.get(subCat), new ArrayList<String>(subCatList.subList(subCat, k)));

                    } else {

                        listSubCategory.put(categories_list.get(subCat), new ArrayList<String>(subCatList.subList(k, k + 2)));
                        k = k + 2;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*//*



             *//*listSubCategory.put(categories_list.get(0), new ArrayList<String>(subCatList.subList(1, 2 )));*//*

*//*            if (login_status) {

                //listHeadCategory.putAll(imageList, categories_list);
                listDataHeader.add("Home");
                listDataHeader.add("My Orders");
                listDataHeader.add("My Product Review");
                listDataHeader.add("My Wishlist");
                listDataHeader.add("My Profile");
                listDataHeader.add("Contact Us");
                listDataHeader.add("Logout");

                listDataHeaderImages.add(imageList.get(0));
                listDataHeaderImages.add(imageList.get(1));
                listDataHeaderImages.add(imageList.get(2));
                listDataHeaderImages.add(imageList.get(3));
                listDataHeaderImages.add(imageList.get(4));
                listDataHeaderImages.add(imageList.get(5));
                listDataHeaderImages.add(imageList.get(6));

              *//**//*  listDataHeader.addAll(categories_list);
                listDataHeader.add("Categories");
                listDataHeader.add("Home");
                listDataHeader.add("My Orders");
                listDataHeader.add("My Product Review");
                listDataHeader.add("My Wishlist");
                listDataHeader.add("My Profile");
                listDataHeader.add("Contact Us");
                listDataHeader.add("Logout");*//**//*
            } else {
                //listDataHeader.addAll(categories_list);
                // Adding child data
                //listDataHeader.add("Categories");
                *//**//*listHeadCategory.put(imageList.get(0), "Home");
                listHeadCategory.put(imageList.get(1), "Login");
                listHeadCategory.put(imageList.get(2), "FAQ");
                listHeadCategory.put(imageList.get(3), "Contact Us");*//**//*
                listDataHeader.add("Home");
                listDataHeader.add("Login");
                listDataHeader.add("FAQ's");
                listDataHeader.add("Contact Us");
                listDataHeaderImages.add(imageList.get(0));
                listDataHeaderImages.add(imageList.get(1));
                listDataHeaderImages.add(imageList.get(2));
                listDataHeaderImages.add(imageList.get(3));
            }*//*
          *//*  for (int i = 0; i < listHeadCategory.size(); i++) {
                listSubCategory.put(listHeadCategory.get(i), subCatList.get(i));
            }*//*


            //listDataChild.put(listDataHeader.get(0), categories_list); // Header, Child data

            //adapter = new ExpandableListAdapter(DemoHomeDashBoard.this, listDataHeader, listSubCategory, listDataHeaderImages);
            //expand_list.setAdapter(adapter);

            ArrayList<String> saveCategoryOptions = new ArrayList<String>();
            saveCategoryOptions.addAll(listDataHeader);

            ArrayList<String> saveCategoryImages = new ArrayList<String>();
            saveCategoryOptions.addAll(listDataHeaderImages);

            tinydb.putListString(Constant.CATEGORY_OPTIONS, saveCategoryOptions);
            tinydb.putListString(Constant.CATEGORY_IMAGES, saveCategoryImages);


            // packagesharedPreferences(listDataHeader, listDataHeaderImages);
            //retriveSharedValue();

            *//*adapter.notifyDataSetChanged();*//*
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Adding child data

    }*/








  /*  private void packagesharedPreferences(List<String> categoryList, List<String> categoryImages) {

        SharedPreferences.Editor editor = prefrence.edit();
        Set<String> setCategoryNames = new LinkedHashSet<String>();
        setCategoryNames.addAll(categoryList);
        editor.putStringSet(Constant.CATEGORY_OPTIONS, setCategoryNames);
        Set<String> setCategoryImages = new LinkedHashSet<String>();
        setCategoryImages.addAll(categoryImages);
        editor.putStringSet(Constant.CATEGORY_IMAGES, setCategoryImages);
        editor.apply();
        retriveSharedValue();
        //Log.d("storesharedPreferences", "" + set);
    }*/

/*    private void retriveSharedValue() {

        Set<String> getCategoryName = new LinkedHashSet<String>();
        getCategoryName = prefrence.getStringSet(Constant.CATEGORY_OPTIONS, null);
        Set<String> getCategoryImages = prefrence.getStringSet(Constant.CATEGORY_IMAGES, null);
        if (getCategoryName != null) {
            //listDataHeader.clear();
            //listDataHeader.addAll(getCategoryName);
            String data = getCategoryName.toString();
            Log.i("DataHeaderCategory", " " + data);

        }
        if (getCategoryImages != null) {
            //listDataHeaderImages.clear();
            //listDataHeaderImages.addAll(getCategoryImages);
            String imageData = getCategoryImages.toString();
            Log.i("DataHeaderCategory", " " + imageData);
        }
        //Log.d("retrivesharedPreferences",""+set);
    }*/

/*    private void prepareListDataDemo() {
        listDataHeader = new ArrayList<String>();
        listSubCategory = new LinkedHashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Top 250");
        listDataHeader.add("Now Showing");
        listDataHeader.add("Coming Soon..");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");
        top250.add("The Good, the Bad and the Ugly");
        top250.add("The Dark Knight");
        top250.add("12 Angry Men");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");

        listSubCategory.put(listDataHeader.get(0), top250); // Header, Child data
        listSubCategory.put(listDataHeader.get(1), nowShowing);
        listSubCategory.put(listDataHeader.get(2), comingSoon);
    }*/

    public void initNavigationDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //drawerLayout.closeDrawers();
        //int count = getSupportFragmentManager().getBackStackEntryCount();
        //Toast.makeText(Home_dashboard.this, "Count BackStack " + count, Toast.LENGTH_LONG).show();
        try {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers();
            } else {
            /*finish();*/
                if (tag.equals("Home")) {
                    super.onBackPressed();
                    //additional code
             /*   getFragmentManager().popBackStack();*/
                    //getSupportActionBar().setTitle(tag);

                } else {
                /*getFragmentManager().popBackStack();*/
                    fragment = new BannerFragment();
                    tag = "Home";
                    ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                    //ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(prefrence), this);
                    swtichFragement(R.id.content_frame, fragment, tag);
                    getSupportActionBar().setTitle("Home");
                /*drawerLayout.closeDrawers();*/

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void signOut() {
        revokeAccess();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.i("StatusSignOut ", status.toString());
                        /*Toast.makeText(Home_dashboard.this, "Status " + status, Toast.LENGTH_LONG).show();*/
                       /* if (status.toString().equals("SUCCESS")) {
                            Toast.makeText(Home_dashboard.this, "Logged out successfully", Toast.LENGTH_LONG).show();
                        }*/
                    }
                });
    }

    /* private void revokeAccess() {
         Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                 new ResultCallback<Status>() {
                     @Override
                     public void onResult(Status status) {
                         Toast.makeText(Home_dashboard.this, "Status " + status, Toast.LENGTH_LONG).show();
                     }
                 });
     }
 */

    public void loginWithGoogle() {

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                /*.enableAutoManage(Home_dashboard.this *//* FragmentActivity *//*, Home_dashboard.this *//* OnConnectionFailedListener *//*)*/
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.i("StatusSignOut ", status.toString());
                        /*Toast.makeText(Home_dashboard.this, "StatusRevoke " + status.toString(), Toast.LENGTH_LONG).show();*/
                        //ToastMsg.showLongToast(Home_dashboard.this, "StatusRevoke " + status.toString());
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            ConnectionResult mConnectionResult = connectionResult;
            /*if (mResolveOnFail) {
                // This is a local helper function that starts
                // the resolution of the problem, which may be
                // showing the user an account chooser or similar.
                //startResolution();
            }*/
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                MyCartDetailModal myCartDetailModal;
                CartsByUserModal cartsByUserModal;
                MainCategoriesModal mainCategoriesModal;
                CartItemCountModal cartItemCountModal;
                LanguageSelectModal languageSelectModal;
                LogOutModal logOutModal;
                switch (tag) {
                    case ApiServerResponse.CART_ITEM_COUNT:
                        cartItemCountModal = (CartItemCountModal) response.body();
                        if (cartItemCountModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            tv_count.setText(cartItemCountModal.getItem_count());
                            Session.setCartItemsQuantity(prefrence, cartItemCountModal.getItem_count());
                        } else {
                            tv_count.setText("0");
                            Session.setCartItemsQuantity(prefrence, "0");
                        }


                        break;
                    case ApiServerResponse.MY_CART_DETAILS:
                        myCartDetailModal = (MyCartDetailModal) response.body();
                        if (myCartDetailModal.getItems().size() == 0) {
                            tv_count.setText("0");
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(prefrence, "0");
                        } else {
                            Session.setCartItemsQuantity(prefrence, String.valueOf(myCartDetailModal.getItems().size()));
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            tv_count.setText(String.valueOf(myCartDetailModal.getItems().size()));
                        }
                        break;
                    case ApiServerResponse.CARTS_BY_USER:
                        cartsByUserModal = (CartsByUserModal) response.body();
                        if (cartsByUserModal.getStatus().equalsIgnoreCase(Constant.OK)) {


                            if (cartsByUserModal.getCarts().get(0).getId() != 0) {
                                Session.setcart_id(prefrence, String.valueOf(cartsByUserModal.getCarts().get(0).getId()));
                                //ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(prefrence), this);
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
                            } else {
                                Session.setcart_id(prefrence, "0");
                            }

                            //ToastMsg.showShortToast(DemoHomeDashBoard.this, "card_id" + Session.getcart_id(prefrence));

                        } else {
                            Session.setcart_id(prefrence, "0");
                        }
                        break;
                    case ApiServerResponse.MAIN_CATEGORIES:

                        mainCategoriesModal = (MainCategoriesModal) response.body();

                        if (mainCategoriesModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            //List adsas = new ArrayList();
                            //List<String> mainCategory = new ArrayList<String>();
                            //List<String> mainCategoryImages = new ArrayList<String>();

                            categoriesInfoBeen = mainCategoriesModal.getCategories_info();

                            int k = 0;
                            // adsas.clear();
                            for (int i = 0; i < mainCategoriesModal.getCategories_info().size(); i++) {
                                listDataHeader.add(mainCategoriesModal.getCategories_info().get(i).getTitle());
                                listDataHeaderImages.add(mainCategoriesModal.getCategories_info().get(i).getCategory_image());

                                for (int j = 0; j < mainCategoriesModal.getCategories_info().get(i).getSubcat_info().size(); j++) {
                                    subCategoriesName.add(mainCategoriesModal.getCategories_info().get(i).getSubcat_info().get(j).getSubcat_name());
                                    subCategoryID.add(mainCategoriesModal.getCategories_info().get(i).getSubcat_info().get(j).getSubcat_id());
                                }

                            }

                            subCategoriesName.add(subCategoriesName.size(), "Entry1");


                            for (int subCat = 0; subCat < listDataHeader.size(); subCat++) {


                                //subsubCatID.add(subCategoriesName.get(subCat));
                                if (subCat == 0) {
                                    //Range 0 - 2 - > values 0,1
                                    k = k + 2;
                                    listSubCategory.put(listDataHeader.get(subCat), new ArrayList<String>(subCategoriesName.subList(subCat, k)));
                                    demo.put(listDataHeader.get(subCat), new ArrayList<String>(subCategoriesName.subList(subCat, k)));


                                } else {

                                    listSubCategory.put(listDataHeader.get(subCat), new ArrayList<String>(subCategoriesName.subList(k, k + 2)));
                                    demo.put(listDataHeader.get(subCat), new ArrayList<String>(subCategoriesName.subList(k, k + 2)));
                                    k = k + 2;
                                }
                            }

                            for (int i = 0; i < subCategoriesName.size() - 1; i++) {
                                subsubCatNameID.put(subCategoriesName.get(i), subCategoryID.get(i));
                                newSubCategoryNamesOnly.add(subCategoriesName.get(i));
                            }

                            //String res = mainCategoriesModal.getCategories_info().toString();
                            //ToastMsg.showLongToast(DemoHomeDashBoard.this, response.body().toString());
                            //prepareListRetrofit(mainCategoriesModal.getCategories_info());

                            //prepareListDataDemo();


                            adapter = new ExpandableListAdapter(DemoHomeDashBoard.this, listDataHeader, listSubCategory, listDataHeaderImages);
                            expand_list.setAdapter(adapter);

                            // Log.i("SIZE", "" + adsas.size());
                            //ToastMsg.showLongToast(DemoHomeDashBoard.this, "" + adsas.size());
                        }

                        break;

                    case ApiServerResponse.SET_LANGUAGE:
                        languageSelectModal = (LanguageSelectModal) response.body();
                        if (languageSelectModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            Session.setAppLanguage(prefrence, languageSelectModal.getUser_data().getLanguage());
                            if (languageSelectModal.getUser_data().getLanguage().equalsIgnoreCase("1")) {
                                ToastMsg.showLongToast(DemoHomeDashBoard.this, "English set as default language.");
                            } else {
                                ToastMsg.showLongToast(DemoHomeDashBoard.this, "Bahasa Indonesia ditetapkan sebagai bahasa default.");

                            }

                            finish();
                            Intent i = new Intent(DemoHomeDashBoard.this, DemoHomeDashBoard.class);
                            startActivity(i);


                        }

                        break;
                    case ApiServerResponse.LOGOUT:

                        logOutModal = (LogOutModal) response.body();
                        if (logOutModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                            ToastMsg.showShortToast(DemoHomeDashBoard.this, logOutModal.getMessage());

                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            Intent intent = new Intent(DemoHomeDashBoard.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                        break;

                    case ApiServerResponse.LOGOUT_FRAGMENT:
                        logOutModal = (LogOutModal) response.body();
                        if (logOutModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();

                            this.tag = "Login";
                                        /*LoginFragment fragmentLoginLogout = new LoginFragment();
                                        tag = "Login";
                                        swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);*/
                            /*swtichFragement(R.id.content_frame, fragmentLogin, tag);*/
                            Intent i = new Intent(DemoHomeDashBoard.this, Login.class);
                            startActivity(i);
                            finish();

                        }

                        break;
                }

                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //showLoading();
        /*ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(prefrence), this);*/
        ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(prefrence), Session.getcart_id(prefrence), this);
        //tv_count.setText(Session.getCartItemsQuantity(prefrence));
    }


    @Subscribe
    public void onCartUpdate(EventBusProductDetailUpdateModal eventBusProductDetailUpdateModal) {

        try {
            tv_count.setText(eventBusProductDetailUpdateModal.getMessage());
            /*if (eventBusProductDetailUpdateModal.getMessage() == 1) {
                String cart_id = Session.getcart_id(prefrence);
                if (!cart_id.equalsIgnoreCase("")) {
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, cart_id, this);
                }
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Subscribe
    public void onCartUpdate(EventBusCartUpdate eventBusCartUpdate) {

        try {
            tv_count.setText(eventBusCartUpdate.getMessage());
           /* if (eventBusCartUpdate.getMessage() == 1) {
                String cart_id = Session.getcart_id(prefrence);
                if (!cart_id.equalsIgnoreCase("")) {
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, cart_id, this);
                }
            }
*/
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Subscribe
    public void onLogout(EventBusLogout eventBusLogout) {
        try {

            logoutFromApp(eventBusLogout.getMessage());

            //ToastMsg.showShortToast(DemoHomeDashBoard.this, eventBusLogout.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }


    private void logoutFromApp(String loginFrom) {
        try {
            switch (loginFrom) {
                case "google":

                    if (checkInternetConnection(DemoHomeDashBoard.this)) {

                        if (mGoogleApiClient.isConnected()) {
                            signOut();
                            drawerLayout.closeDrawers();
                            //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                            mGoogleApiClient.disconnect();
                            ServerAPI.getInstance().logout(ApiServerResponse.LOGOUT, Session.get_userId(prefrence), this);

                            ////
                           /* Session.set_login_status(prefrence, false);
                            prefrence.edit().clear().apply();
                            Intent intent = new Intent(DemoHomeDashBoard.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);*/
                                /*LoginFragment fragmentLoginLogout = new LoginFragment();
                                tag = "Login";
                                swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);*/

                        } else {
                            ServerAPI.getInstance().logout(ApiServerResponse.LOGOUT, Session.get_userId(prefrence), this);

                        }
                    } else {
                        showAlertDialog(DemoHomeDashBoard.this);

                    }

                    break;
                case "facebook":
                    if (checkInternetConnection(DemoHomeDashBoard.this)) {


                        disconnectFromFacebook();
                        //LoginManager.getInstance().logOut();
                        drawerLayout.closeDrawers();
                        ServerAPI.getInstance().logout(ApiServerResponse.LOGOUT, Session.get_userId(prefrence), this);


                        /*Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        Intent intent = new Intent(DemoHomeDashBoard.this, Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);*/
                            /*LoginFragment fragmentLoginLogout = new LoginFragment();
                            tag = "Login";
                            swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);*/
                        break;

                    } else {
                        showAlertDialog(DemoHomeDashBoard.this);

                    }

                default: {

                    if (checkInternetConnection(DemoHomeDashBoard.this)) {


                        drawerLayout.closeDrawers();

                        ServerAPI.getInstance().logout(ApiServerResponse.LOGOUT, Session.get_userId(prefrence), this);


                       /* Session.set_login_status(prefrence, false);
                        prefrence.edit().clear().apply();
                        //LoginFragment fragmentLoginLogout = new LoginFragment();
                        //tag = "Login";
                        //swtichFragement(R.id.content_frame, fragmentLoginLogout, tag);
                        //prepareListData();
                        Intent intent = new Intent(DemoHomeDashBoard.this, Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);*/
                        //fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    } else {
                        showAlertDialog(DemoHomeDashBoard.this);
                    }


                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showSelectLanguageAlertDialog() {

        try {
            /*final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);*/
            final CharSequence[] array = {"English", "Indonesian"};
            int preSelectedLanguage = 0;
            String preselect = Session.getAppLanguage(prefrence);
            if (preselect.equalsIgnoreCase("1")) {
                preSelectedLanguage = 0;
            } else if (preselect.equalsIgnoreCase("2")) {
                preSelectedLanguage = 1;
            }
            /*else {
                preSelectedLanguage = 0;
            }*/
            selectedLanguage = preselect;


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Select Language");
            //dialogBuilder.setMessage("Please select your preferred language?");
            dialogBuilder.setCancelable(true);
            dialogBuilder.setSingleChoiceItems(array, preSelectedLanguage, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (array[which].toString() == array[0]) {
                        selectedLanguage = "1";
                    } else if (array[which].toString() == array[1]) {
                        selectedLanguage = "2";
                    } /*else {
                        selectedLanguage = "1";
                    }*/

                   /* if (array[which].toString().equalsIgnoreCase(Constant.LANGUAGE_ENGLISH)) {
                        selectedLanguage = "1";
                    } else if (array[which].toString().equalsIgnoreCase(Constant.LANGUAGE_INDONESIAN)) {
                        selectedLanguage = "2";
                    } else {
                        selectedLanguage = "1";
                    }*/

                    //Session.setAppLanguage(prefrence, array.);
                }
            });
            dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //ToastMsg.showShortToast(DemoHomeDashBoard.this, array[which].toString());
                    //onBackExitFlag = false;
                }
            });

            dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                   /* if (which == 0) {
                        selectedLanguage = "1";
                    } else if (which == 1) {
                        selectedLanguage = "2";
                    } else {
                        selectedLanguage = "1";
                    }*/


                    if (checkInternetConnection(DemoHomeDashBoard.this)) {

                        if (!Session.getAppLanguage(prefrence).equalsIgnoreCase(selectedLanguage)) {

                            showLoading();
                            ServerAPI.getInstance().selectLanguage(ApiServerResponse.SET_LANGUAGE, selectedLanguage, Session.get_userId(prefrence), DemoHomeDashBoard.this);
                        } else {
                            ToastMsg.showShortToast(DemoHomeDashBoard.this, getString(R.string.already_selected));
                        }


                    } else {
                        showAlertDialog(DemoHomeDashBoard.this);
                    }
                    //ToastMsg.showShortToast(DemoHomeDashBoard.this, array[which].toString());

                    //onBackExitFlag = true;
                }
            });
/* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/

            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAlertCategoryNoAvailable() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            //dialogBuilder.setTitle("Connection Problem");
            dialogBuilder.setMessage(R.string.products_not_available);
            dialogBuilder.setCancelable(true);
           /* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*/
            dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                }
            });
           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        Locale locale = new Locale("in");
        super.attachBaseContext(ContextWrapper.wrap(newBase, locale));
    }*/


   /* @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, "in"));
    }*/
    /* @Override
    protected void attachBaseContext(Context newBase) {


        String defChoosenLanguage = "en";
        String choosenLanguage = Session.getAppLanguage(prefrence);
        if (choosenLanguage.equalsIgnoreCase("1")) {
            defChoosenLanguage = "en";
        } else if (choosenLanguage.equalsIgnoreCase("2")) {
            defChoosenLanguage = "in";
        }
        Locale newLocale = new Locale(defChoosenLanguage);
        // .. create or get your new Locale object here.
        //newLocale = setDefaultLocal()
        Context context = ContextWrapper.wrap(newBase, newLocale);
        super.attachBaseContext(context);
    }*/

    /*
 * Preparing the list data
 */

}
