package kartku.com.dashboard_module;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.activity.AllCategoriesActivity;
import kartku.com.activity.SearchProductsBrandActivity;
import kartku.com.adapter.MyHomeScreenCategoriesAdapter;
import kartku.com.adapter.MyHomeScreenRecentProductsAdapter;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.HomeScreenModal;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.search.SearchActivity;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

import static kartku.com.utils.Utility.checkInternetConnection;

/**
 * Created by satoti.garg on 9/20/2016.
 */
public class BannerFragment extends Fragment implements /*HomeBannersListener,*/ ApiServerResponse {

    //RecyclerView banner_recycleview;
    //SpinKitView spinKitView;
    //ProgressBar progressBar;

    boolean pendingIntroAnimation;
    SharedPreferences pref;
    TextView tv_count, tv_view_all;
    ImageView iv_home_banner_image;
    RecyclerView rv_categories, rv_recent_products;
    List<HomeScreenModal.CategoryListBean> listCategory = new ArrayList<>();
    List<HomeScreenModal.SubCategoriesBean> listProducts = new ArrayList<>();
    FloatingActionButton fab_search;

    boolean loginStatus;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        pref = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        loginStatus = Session.get_login_statuc(pref);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.home_fragment_final_layout, container, false);
        /*View view = inflater.inflate(R.layout.banners_fragment, container, false);*/

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.home);
       /* progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        CubeGrid cubeGrid = new CubeGrid();
        progressBar.setIndeterminateDrawable(cubeGrid);
        progressBar.setVisibility(View.VISIBLE);*/

       /* spinKitView = (SpinKitView) view.findViewById(R.id.spin_kit);
        spinKitView.setVisibility(View.VISIBLE);*/


        try {
            // banner_recycleview = (RecyclerView) view.findViewById(R.id.banners_recycle_view);
            iv_home_banner_image = (ImageView) view.findViewById(R.id.iv_home_banner_image);
            tv_view_all = (TextView) view.findViewById(R.id.tv_view_all);
            rv_categories = (RecyclerView) view.findViewById(R.id.rv_categories);
            rv_recent_products = (RecyclerView) view.findViewById(R.id.rv_recent_products);
            fab_search = (FloatingActionButton) view.findViewById(R.id.fab_search);

            rv_categories.setNestedScrollingEnabled(false);
            rv_recent_products.setNestedScrollingEnabled(false);

            tv_view_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    ArrayList<HomeScreenModal.CategoryListBean> lists = new ArrayList<HomeScreenModal.CategoryListBean>();
                    for (int i = 0; i < listCategory.size(); i++) {
                        lists.add(i, listCategory.get(i));
                    }
                    Intent i = new Intent(getActivity(), AllCategoriesActivity.class);
                    Bundle b = new Bundle();
                    b.putParcelableArrayList(Constant.CATEGORY_ALL, lists);
                    i.putExtras(b);
                    startActivity(i);

                }
            });

            fab_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent search = new Intent(getActivity(), SearchProductsBrandActivity.class);
                    startActivity(search);

                }
            });

            /*((DemoHomeDashBoard) getActivity()).getSupportActionBar().setTitle("Home");*/


        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            // banners api
            if (checkInternetConnection(getActivity())) {
                ((Utility) getActivity()).showLoading();
                ServerAPI.getInstance().getHomeScreen(ApiServerResponse.HOME_SCREEN, Session.get_userId(pref), this);
            } else {
                ((Utility) getActivity()).showAlertDialog(getActivity());

            }

            // new HomeBannersManager(getActivity(), this).sendRequest();


        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

/*    @Override
    public void onBannersSuccess(String response) {
        Log.d("", "baaners success response ******  " + response);
        try {


            JSONObject object = new JSONObject(response);
            JSONArray banner_info = object.getJSONArray(ResponseKeys.BANNER_INFO);
            List<String> bannersList = new ArrayList<String>();
            for (int j = 0; j < banner_info.length(); j++) {
                try {
                    JSONObject item_object = banner_info.getJSONObject(j);
                    String url = item_object.getString(ResponseKeys.URL);
                    bannersList.add(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            BannerAdapter adapter = new BannerAdapter(getActivity(), bannersList);

            banner_recycleview.setAdapter(adapter);
            ((Utility) getActivity()).hideLoading();
           *//* RecyclerView.ItemDecoration itemDecoration = new
                    DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
            banner_recycleview.addItemDecoration(itemDecoration);*//*
            *//*StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);*//*
            *//*banner_recycleview.setLayoutManager(gridLayoutManager);*//*
            //banner_recycleview.setItemAnimator(new SlideInUpAnimator());
            banner_recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
            if (pendingIntroAnimation) {
                pendingIntroAnimation = false;
                startIntroAnimation(banner_recycleview);
            }
            *//*banner_recycleview.setItemAnimator(new SlideInUpAnimator(new OvershootInterpolator(1f)));*//*
           *//* if (progressBar.isShown()) {
                progressBar.setVisibility(View.GONE);
            }*//*
            *//*if (spinKitView.isShown()) {
                spinKitView.setVisibility(View.GONE);
            }*//*
            Log.d("", "banners list size *********" + bannersList);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBannersError(String error) {
        try {
            ((Utility) getActivity()).hideLoading();
            ToastMsg.showShortToast(getActivity(), error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

  /*  @Override
    public void onResume() {
        super.onResume();
        ToastMsg.showLongToast(getActivity(), "ONRESUME");
        ((DemoHomeDashBoard) getActivity()).getSupportActionBar().setTitle("Home");
    }

    @Override
    public void onStart() {
        super.onStart();
        ToastMsg.showLongToast(getActivity(), "ONSTART");
        ((DemoHomeDashBoard) getActivity()).getSupportActionBar().setTitle("Home");
    }
*/

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.demo_menu, menu);
      *//*  if (loginStatus) {
            inflater.inflate(R.menu.menu_home_login, menu);
        } else {
            inflater.inflate(R.menu.menu_home_logout, menu);
        }*//*


        super.onCreateOptionsMenu(menu, inflater);
    }*/

  /*  @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), CartDetailsNew.class);
                *//*Intent in = new Intent(getActivity(), Cart.class);*//*
                startActivity(in);
            }
        });
        // ToastMsg.showLongToast(getActivity(), "" + Session.get_login_statuc(pref));

        //tv_count.setText("11");

    }*/

/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_count:
                Intent in = new Intent(getActivity(), CartDetailsNew.class);
                */
/*Intent in = new Intent(getActivity(), Cart.class);*//*

                startActivity(in);
                break;
            case R.id.menu_my_orders:

                break;
            case R.id.menu_my_product_review:

                break;
            case R.id.menu_my_wishlist:

                break;
            case R.id.menu_my_profile:

                break;
            case R.id.menu_contact_us:

                break;
            case R.id.menu_logout:


                break;

            case R.id.menu_login:

                break;
            case R.id.menu_faq:

                break;
        }
        return true;
    }
*/


/*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tool_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miCompose:
                Intent in = new Intent(getActivity(), CartDetailsNew.class);
                */
/*Intent in = new Intent(getActivity(), Cart.class);*//*

                startActivity(in);
                break;
        }
        return true;
    }
*/


    private void startIntroAnimation(RecyclerView recyclerView) {
        //latestPostRecyclerview.getHeight()
        recyclerView.setTranslationY(recyclerView.getHeight());
        recyclerView.setAlpha(0f);
        recyclerView.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                CartItemCountModal cartItemCountModal;
                HomeScreenModal homeScreenModal;
                switch (tag) {
                    case ApiServerResponse.CART_ITEM_COUNT:
                        cartItemCountModal = (CartItemCountModal) response.body();
                        if (cartItemCountModal.getStatus().equalsIgnoreCase(Constant.OK)) {


                            tv_count.setText(cartItemCountModal.getItem_count());
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(pref, cartItemCountModal.getItem_count());
                        }

                        break;
                    case ApiServerResponse.HOME_SCREEN:
                        homeScreenModal = (HomeScreenModal) response.body();
                        if (homeScreenModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            Glide.with(getActivity())
                                    .load(homeScreenModal.getBanner().getUrl())
                                    .thumbnail(0.1f)
                                    .into(iv_home_banner_image);

                            Session.setHomeScreenBanner(pref, homeScreenModal.getBanner().getUrl());

                            listCategory = homeScreenModal.getCategory_list();
                            listProducts = homeScreenModal.getSub_categories();

                            List<HomeScreenModal.CategoryListBean> listCategoryToShow = new ArrayList<>();
                            for (int i = 0; i < (listCategory.size() - 1); i++) {

                                listCategoryToShow.add(i, listCategory.get(i));
                            }

                            MyHomeScreenCategoriesAdapter categoriesAdapter = new MyHomeScreenCategoriesAdapter(getActivity(), listCategory, listCategoryToShow);
                            rv_categories.setAdapter(categoriesAdapter);
                            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 4);
                            rv_categories.setLayoutManager(mLayoutManager);
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation(rv_categories);
                            }

                            MyHomeScreenRecentProductsAdapter productsAdapter = new MyHomeScreenRecentProductsAdapter(getActivity(), listProducts);
                            rv_recent_products.setAdapter(productsAdapter);
                            //RecyclerView.LayoutManager mLayoutManagerProducts = new GridLayoutManager(getActivity(), 2);

                            RecyclerView.LayoutManager mLayoutManagerProducts = new GridLayoutManager(getActivity(), 2) {
                                @Override
                                public boolean canScrollVertically() {
                                    return false;
                                }
                            };


                            rv_recent_products.setLayoutManager(mLayoutManagerProducts);
                            if (pendingIntroAnimation) {
                                pendingIntroAnimation = false;
                                startIntroAnimation(rv_recent_products);
                            }

                        }
                        //((Utility) getActivity()).showLoading();
                        if (!Session.getcart_id(pref).equalsIgnoreCase("0")) {
                            ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), this);
                        }

                        //((Utility) getActivity()).hideLoading();
                        break;
                }

                ((Utility) getActivity()).hideLoading();
            } else {
                ((Utility) getActivity()).hideLoading();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            ((Utility) getActivity()).hideLoading();
            switch (tag) {
                case ApiServerResponse.MY_CART_DETAILS:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResume() {
        super.onResume();


        ServerAPI.getInstance().getHomeScreen(ApiServerResponse.HOME_SCREEN, Session.get_userId(pref), this);
        /*try {

            if (Session.get_login_statuc(pref)) {
                //ToastMsg.showLongToast(getActivity(), "YES");
                ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);
            } else {
                //ToastMsg.showLongToast(getActivity(), "No");
                ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

/*    private void showAlertDialog() {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle("Connection Problem.");
            dialogBuilder.setMessage("Please check your internet connection?");
            dialogBuilder.setCancelable(true);
           *//* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*//*
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                }
            });
           *//* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*//*
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
