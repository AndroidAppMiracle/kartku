package kartku.com.dashboard_module;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import kartku.com.R;
import kartku.com.adapter.Expandableadapter;
import kartku.com.faq_module.FAQFragment;
import kartku.com.fragments.CategoriesFragment;
import kartku.com.fragments.ContactFragment;
import kartku.com.fragments.LoginFragment;
import kartku.com.fragments.OrdersFragment;
import kartku.com.fragments.ProductReviewFragment;
import kartku.com.fragments.ProfileFragment;
import kartku.com.fragments.WishlistFragment;
import kartku.com.login_module.Login;
import kartku.com.utils.ResponseKeys;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

public class Home_dashboard extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    ExpandableListView expand_list;
    Expandableadapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private SharedPreferences prefrence;
    //RecyclerView banner_recycleview;

    private FragmentManager fragmentManager;
    private BannerFragment fragment;
    private String tag;
    //private SharedPreferences pref;
    private boolean login_status;
    String loginType;

    private GoogleApiClient mGoogleApiClient;
    //boolean mResolveOnFail;

    //ProgressBar progressBar;
    private int[] categoryImagesStatic = {
            R.drawable.ic_category_furniture,
            R.drawable.ic_category_decor,
            R.drawable.ic_category_appliances,
            R.drawable.ic_category_furnishing,
            R.drawable.ic_category_furniture,
            R.drawable.ic_category_decor,
            R.drawable.ic_category_appliances,
            R.drawable.ic_category_furnishing,
            R.drawable.ic_category_appliances,
            R.drawable.ic_category_furnishing
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            loginWithGoogle();
            FacebookSdk.sdkInitialize(Home_dashboard.this);
            setContentView(R.layout.activity_home_dashboard);

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            expand_list = (ExpandableListView) findViewById(R.id.expandable_category);
            prefrence = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
            fragmentManager = getSupportFragmentManager();

            /*progressBar = new ProgressBar()*/
            //pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
            login_status = Session.get_login_statuc(prefrence);
            loginType = Session.getLogin_type(prefrence);
            Log.d("login_status", "login_status ***** " + login_status);
            setSupportActionBar(toolbar);
            try {
                initNavigationDrawer();

                String countryCode = Locale.getDefault().getCountry();
                ToastMsg.showLongToast(Home_dashboard.this, countryCode);
            } catch (Exception e) {
                e.printStackTrace();
            }


            /*try{
                Intent intent = new Intent(Home_dashboard.this, ProductList.class);
                startActivity(intent);
            }catch (Exception e)
            {
                e.printStackTrace();
            }*/

            // preparing list data
            prepareListData();


            listAdapter = new Expandableadapter(Home_dashboard.this, listDataHeader, listDataChild, fragmentManager, R.id.content_frame, login_status, categoryImagesStatic);


            // setting list adapter
            expand_list.setAdapter(listAdapter);

            try {
                fragment = new BannerFragment();
                tag = "Home";
                swtichFragement(R.id.content_frame, fragment, tag);
            } catch (Exception e) {
                e.printStackTrace();
            }


            expand_list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    final String selected = (String) listAdapter.getChild(
                            groupPosition, childPosition);
                    System.out.println("child selected isChildSelectable  " + selected + " childPosition  " + childPosition);

                    try {
                        /*Intent intent = new Intent(Home_dashboard.this, CategoriesActivity.class);
                        intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, childPosition);
                        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                            drawerLayout.closeDrawers();
                        }
                        startActivity(intent);*/
                        //Log.i("Selected Category", )

                        Session.setselectedCategory(prefrence, selected.trim());
                        Session.set_selected_category_index(prefrence, String.valueOf(childPosition));
                        CategoriesFragment fragment = new CategoriesFragment();
                        tag = "Categories";
                        swtichFragement(R.id.content_frame, fragment, tag);
                        expand_list.collapseGroup(groupPosition);
                        drawerLayout.closeDrawers();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            });

            expand_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                    Log.d("DEBUG", "heading clicked");

                    if (login_status) {
                        switch (i) {

                            case 0:
                                break;
                            case 1:
                                drawerLayout.closeDrawers();
                                fragment = new BannerFragment();
                                tag = "Home";
                                swtichFragement(R.id.content_frame, fragment, tag);

                                break;
                            case 2:
                                drawerLayout.closeDrawers();
                                OrdersFragment ordersFragment = new OrdersFragment();
                                tag = "My Orders";
                                swtichFragement(R.id.content_frame, ordersFragment, tag);

                                break;
                            case 3:
                                drawerLayout.closeDrawers();
                                ProductReviewFragment productReviewFragment = new ProductReviewFragment();
                                tag = "My Product Review";
                                swtichFragement(R.id.content_frame, productReviewFragment, tag);

                                break;
                            case 4:
                                drawerLayout.closeDrawers();
                                WishlistFragment wishlistFragment = new WishlistFragment();
                                tag = "My Wishlist";
                                swtichFragement(R.id.content_frame, wishlistFragment, tag);

                                break;
                            case 5:
                                drawerLayout.closeDrawers();
                                ProfileFragment profileFragment = new ProfileFragment();
                                tag = "My Profile";
                                swtichFragement(R.id.content_frame, profileFragment, tag);

                                break;
                            case 6:
                                drawerLayout.closeDrawers();
                                ContactFragment contactFragment = new ContactFragment();
                                tag = "Contact Us";
                                swtichFragement(R.id.content_frame, contactFragment, tag);

                                break;
                            case 7:

                                if (loginType.equals("google")) {
                                    if (mGoogleApiClient.isConnected()) {
                                        signOut();
                                        drawerLayout.closeDrawers();
                                        //mGoogleApiClient.stopAutoManage(Home_dashboard.this);
                                        mGoogleApiClient.disconnect();
                                        Session.set_login_status(prefrence, false);
                                        prefrence.edit().clear().apply();
                                        LoginFragment fragmentLogin = new LoginFragment();
                                        tag = "Login";
                                        swtichFragement(R.id.content_frame, fragmentLogin, tag);

                                    }
                                } else if (loginType.equals("facebook")) {
                                    LoginManager.getInstance().logOut();
                                    drawerLayout.closeDrawers();
                                    Session.set_login_status(prefrence, false);
                                    prefrence.edit().clear().apply();
                                    LoginFragment fragmentLogin = new LoginFragment();
                                    tag = "Login";
                                    swtichFragement(R.id.content_frame, fragmentLogin, tag);
                                } else {
                                    drawerLayout.closeDrawers();
                                    Session.set_login_status(prefrence, false);
                                    prefrence.edit().clear().apply();
                                    LoginFragment fragmentLogin = new LoginFragment();
                                    tag = "Login";
                                    swtichFragement(R.id.content_frame, fragmentLogin, tag);
                                }


                                break;
                            default:
                                fragment = new BannerFragment();
                                tag = "Home";
                                swtichFragement(R.id.content_frame, fragment, tag);
                                drawerLayout.closeDrawers();
                                break;
                        }
                    } else {
                        switch (i) {

                            case 0:
                                break;
                            case 1:
                                drawerLayout.closeDrawers();
                                fragment = new BannerFragment();
                                tag = "Home";
                                swtichFragement(R.id.content_frame, fragment, tag);

                                break;
                            case 2:
                                drawerLayout.closeDrawers();
                                LoginFragment fragmentLogin = new LoginFragment();
                                tag = "Login";
                                swtichFragement(R.id.content_frame, fragmentLogin, tag);

                                break;
                            case 3:
                                drawerLayout.closeDrawers();
                                FAQFragment fragmentFaq = new FAQFragment();
                                tag = "FAQ";
                                swtichFragement(R.id.content_frame, fragmentFaq, tag);

                                break;
                            case 4:
                                drawerLayout.closeDrawers();
                                ContactFragment contactFragment = new ContactFragment();
                                tag = "Contact Us";
                                swtichFragement(R.id.content_frame, contactFragment, tag);

                                break;

                            default:
                                fragment = new BannerFragment();
                                tag = "Home";
                                swtichFragement(R.id.content_frame, fragment, tag);
                                break;
                        }
                    }

                    // Toast.makeText(Home_dashboard.this, "Count BackStack " + getSupportFragmentManager().getBackStackEntryCount(), Toast.LENGTH_LONG).show();
                    return false;
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }



       /* NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);*/
    }

    public void swtichFragement(int id, Fragment fragment, String tag) {
        try {
            this.tag = tag;
            Fragment _fragment = fragmentManager.findFragmentByTag(tag);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment, tag);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            /*fragmentManager.executePendingTransactions();*/
            getSupportActionBar().setTitle(tag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tool_menu, menu);

        return true;
    }*/

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<String, List<String>>();

        if (login_status) {
            listDataHeader.add("Categories");
            listDataHeader.add("Home");
            listDataHeader.add("My Orders");
            listDataHeader.add("My Product Review");
            listDataHeader.add("My Wishlist");
            listDataHeader.add("My Profile");
            listDataHeader.add("Contact Us");
            listDataHeader.add("Logout");
        } else {
            // Adding child data
            listDataHeader.add("Categories");
            listDataHeader.add("Home");
            listDataHeader.add("Login");
            listDataHeader.add("FAQ's");
            listDataHeader.add("Contact Us");
        }

        // Adding child data
        String response = Session.get_main_categories_res(prefrence);
        try {
            Log.i("Response Home Cate", response);
            //Toast.makeText(Home_dashboard.this, "RESPONSEHOME " + response, Toast.LENGTH_LONG).show();
            JSONObject object = new JSONObject(response);
            JSONArray categories_info = object.getJSONArray(ResponseKeys.CATEGORIES_INFO);
            List<String> categories_list = new ArrayList<String>();
            for (int j = 0; j < categories_info.length(); j++) {
                try {
                    JSONObject item_object = categories_info.getJSONObject(j);
                    String title = item_object.getString(ResponseKeys.TITLE);
                    categories_list.add(title);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            listDataChild.put(listDataHeader.get(0), categories_list); // Header, Child data
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void initNavigationDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miCompose:
                Intent in = new Intent(this, Cart.class);

                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                }
                startActivity(in);
                break;
        }
        return true;
    }*/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //drawerLayout.closeDrawers();
        //int count = getSupportFragmentManager().getBackStackEntryCount();
        //Toast.makeText(Home_dashboard.this, "Count BackStack " + count, Toast.LENGTH_LONG).show();

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            /*finish();*/
            if (tag.equals("Home")) {
                super.onBackPressed();
                //additional code
             /*   getFragmentManager().popBackStack();*/

            } else {
                /*getFragmentManager().popBackStack();*/
                fragment = new BannerFragment();
                tag = "Home";
                swtichFragement(R.id.content_frame, fragment, tag);
                /*drawerLayout.closeDrawers();*/

            }
        }

    }

    private void signOut() {
        revokeAccess();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.i("StatusSignOut ", status.toString());
                        /*Toast.makeText(Home_dashboard.this, "Status " + status, Toast.LENGTH_LONG).show();*/
                       /* if (status.toString().equals("SUCCESS")) {
                            Toast.makeText(Home_dashboard.this, "Logged out successfully", Toast.LENGTH_LONG).show();
                        }*/
                    }
                });
    }

    /* private void revokeAccess() {
         Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                 new ResultCallback<Status>() {
                     @Override
                     public void onResult(Status status) {
                         Toast.makeText(Home_dashboard.this, "Status " + status, Toast.LENGTH_LONG).show();
                     }
                 });
     }
 */

    public void loginWithGoogle() {

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                /*.enableAutoManage(Home_dashboard.this *//* FragmentActivity *//*, Home_dashboard.this *//* OnConnectionFailedListener *//*)*/
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.i("StatusSignOut ", status.toString());
                        /*Toast.makeText(Home_dashboard.this, "StatusRevoke " + status.toString(), Toast.LENGTH_LONG).show();*/
                        //ToastMsg.showLongToast(Home_dashboard.this, "StatusRevoke " + status.toString());
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            ConnectionResult mConnectionResult = connectionResult;
            /*if (mResolveOnFail) {
                // This is a local helper function that starts
                // the resolution of the problem, which may be
                // showing the user an account chooser or similar.
                //startResolution();
            }*/
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
}