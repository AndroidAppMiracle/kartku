package kartku.com.dashboard_module.listener;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public interface HomeBannersListener {

    void onBannersSuccess(String response);
    void onBannersError(String error);
}
