package kartku.com.profile.listener;

/**
 * Created by satoti.garg on 9/14/2016.
 */
public interface ProfileListener {

    void onSuccess(String response);
    void onError(String error);
}
