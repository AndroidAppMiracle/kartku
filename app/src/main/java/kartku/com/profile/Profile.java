package kartku.com.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import kartku.com.R;
import kartku.com.activity.Splash;
import kartku.com.contact.Contact;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.profile.change_password.ChangePassword;
import kartku.com.profile.listener.ProfileListener;
import kartku.com.profile.manager.ProfileManager;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

public class Profile extends AppCompatActivity implements View.OnClickListener, ProfileListener {

    EditText first_name, last_name, address, contact_number;
    TextView email;
    Button update_profile;
    String user_first_name, user_lName, user_email, user_address, user_number;
    String profile_url;
    SharedPreferences pref;
    String user_id, loginType;
    ImageView profile_pic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_profile);
            try {
               /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
                pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
                user_id = Session.get_userId(pref);
                loginType = Session.getLogin_type(pref);



              /*  toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Profile.this, Home_dashboard.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });*/
            } catch (Exception e) {
                e.printStackTrace();
            }

            // find id's
            try {
                first_name = (EditText) findViewById(R.id.first_name);
                last_name = (EditText) findViewById(R.id.last_name);
                email = (TextView) findViewById(R.id.email);
                address = (EditText) findViewById(R.id.address);
                contact_number = (EditText) findViewById(R.id.contact_no);
                update_profile = (Button) findViewById(R.id.btn_update_profile);
                profile_pic = (ImageView) findViewById(R.id.profile_pic);
                update_profile.setOnClickListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // get values from session saved in login response to set as profile details.

            try {

                try {
                    String response = Session.getlogin_user_details(pref);
                    JSONObject user_details = new JSONObject(response);
                    if (loginType.equals("email")) {

                        user_first_name = user_details.getString(Constant.FIRST_NAME);
                        user_lName = user_details.getString(Constant.LAST_NAME);
                        user_email = user_details.getString(Constant.EMAIL);
                        user_address = user_details.getString(Constant.ADDRESS);
                        user_number = user_details.getString(Constant.MOBILE_NUMBER);
                        profile_url = user_details.getString(Constant.PROFILE_IMAGE_PATH);
                        first_name.setText(user_first_name);
                        last_name.setText(user_lName);
                        email.setText(user_email);
                        contact_number.setText(user_number);
                        update_profile.setVisibility(View.VISIBLE);
                        if (user_address.equalsIgnoreCase("null") || user_address.equalsIgnoreCase(null)) {
                            user_address = "";
                        }
                        address.setText(user_address);
                    } else {
                        update_profile.setVisibility(View.GONE);

                        user_first_name = user_details.getString(Constant.USERNAME);
                        user_email = user_details.getString(Constant.EMAIL);
                        first_name.setHint("Username");
                        first_name.setText(user_first_name);
                        last_name.setVisibility(View.GONE);
                        contact_number.setVisibility(View.GONE);
                        email.setText(user_email);
                        address.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                /*first_name.setText(user_first_name);
                last_name.setText(user_lName);
                email.setText(user_email);*/


            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onSuccess(String response) {
        try {
            ToastMsg.showShortToast(Profile.this, getString(R.string.profile_success_message));
            finish();
            /*Intent intent = new Intent(Profile.this, Home_dashboard.class);
            startActivity(intent);
            finish();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String error) {
        try {
            ToastMsg.showShortToast(Profile.this, error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update_profile:
                try {
                    new ProfileManager(Profile.this, this).sendRequest(first_name.getText().toString(), last_name.getText().toString(), user_id, address.getText().toString(), contact_number.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (loginType.equals("email")) {
            getMenuInflater().inflate(R.menu.profile_menu, menu);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                    Intent intent = new Intent(Profile.this, Home_dashboard.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.change_pwd:
                try {
                    Intent intent = new Intent(Profile.this, ChangePassword.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Profile.this, Home_dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
