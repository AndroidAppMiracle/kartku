package kartku.com.profile.manager;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.contact.api.ContactApi;
import kartku.com.contact.listener.Contactlistener;
import kartku.com.profile.api.ProfileApi;
import kartku.com.profile.change_password.api.ChangePasswordApi;
import kartku.com.profile.change_password.listener.ChangePasswordlistener;
import kartku.com.profile.listener.ProfileListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/14/2016.
 */
public class ProfileManager {

    private Context context;
    private ProfileListener listener;
    private SharedPreferences pref;

    public ProfileManager(Context context, ProfileListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context,context.getSharedPreferences(Session.PREFERENCES,Context.MODE_PRIVATE));
    }

    public void sendRequest(String firstname, String lastname, String user_id, String address,String contact_number) {
        if (validateFields(firstname, lastname, address,contact_number)) {
            try {
                Map<String, String> params = new HashMap<>();
                try {
                    try {
                        params.put(RequestConstants.PROFILE_FIRST_NAME, "" + firstname);
                        params.put(RequestConstants.PROFILE_LAST_NAME, "" + lastname);
                        params.put(RequestConstants.PROFILE_Address, "" + address);
                        params.put(RequestConstants.PROFILE_MOBILE_NUMBER, "" + contact_number);
                        params.put(RequestConstants.PROFILE_USER_ID, "" + user_id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new ProfileApi(ProfileManager.this, context).sendRequest(API.UPDATE_USER_REQUEST_ID, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validateFields(String firstname, String lastname, String address, String contactNumber) {
        if (firstname.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.first_name));
            return false;
        }else if (lastname.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.last_name));
            return false;
        } else if (address.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.address));
            return false;
        }else if(contactNumber.length()<=0)
        {
            listener.onError(context.getResources().getString(R.string.mobile_no));
            return false;
        }
        else {
            return true;
        }

    }


    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                String message = res_object.getString(Constant.MESSAGE);

                JSONObject user_data = res_object.getJSONObject(Constant.USER_DATA);
                Session.setlogin_user_details(pref,""+user_data);

                listener.onSuccess(message);

            }else{
                String message = res_object.getString(Constant.MESSAGE);
                listener.onError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void onError(String message) {

        listener.onError(message);
    }
}
