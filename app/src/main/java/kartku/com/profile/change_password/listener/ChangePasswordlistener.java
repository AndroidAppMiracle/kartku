package kartku.com.profile.change_password.listener;

/**
 * Created by satoti.garg on 9/14/2016.
 */
public interface ChangePasswordlistener {

    void onSuccess(String response);
    void onError(String error);
}
