package kartku.com.profile.change_password.manager;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.contact.api.ContactApi;
import kartku.com.contact.listener.Contactlistener;
import kartku.com.profile.change_password.api.ChangePasswordApi;
import kartku.com.profile.change_password.listener.ChangePasswordlistener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;

/**
 * Created by satoti.garg on 9/14/2016.
 */
public class ChangePasswordManager {
    private Context context;
    private ChangePasswordlistener listener;

    public ChangePasswordManager(Context context, ChangePasswordlistener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void sendRequest(String user_id, String current_pwd, String new_pwd , String confirm_password) {
        if (validateFields(current_pwd, new_pwd, confirm_password)) {
            try {
                Map<String, String> params = new HashMap<>();
                try {
                    try {
                        Log.i("info params","user_id " + user_id + "  current_pwd  " +current_pwd + "  new_pwd  " +new_pwd);
                        params.put(RequestConstants.USER_ID, "" + user_id);
                        params.put(RequestConstants.CURRENT_PASSWORD, "" + current_pwd);
                        params.put(RequestConstants.NEW_PASSWORD, "" + new_pwd);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new ChangePasswordApi(ChangePasswordManager.this, context).sendRequest(API.CHANGE_PASSWORD_REQUEST_ID, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validateFields(String current_pwd, String new_pwd, String confirm_password) {
        if (current_pwd.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.old_password));
            return false;
        } else if (new_pwd.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.new_password));
            return false;
        }else if (confirm_password.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.confirm_password));
            return false;
        }else if (!new_pwd.equalsIgnoreCase(confirm_password)) {
            listener.onError(context.getResources().getString(R.string.password_validation));
            return false;
        }
        else {
            return true;
        }

    }


    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                String message = res_object.getString(Constant.MESSAGE);
                listener.onSuccess(message);

            }else{
                String message = res_object.getString(Constant.MESSAGE);
                listener.onError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void onError(String message) {

        listener.onError(message);
    }
}