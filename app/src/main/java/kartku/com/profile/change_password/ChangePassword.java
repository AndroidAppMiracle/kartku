package kartku.com.profile.change_password;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import kartku.com.R;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.profile.Profile;
import kartku.com.profile.change_password.listener.ChangePasswordlistener;
import kartku.com.profile.change_password.manager.ChangePasswordManager;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.ChangePasswordModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity implements /*ChangePasswordlistener,*/ View.OnClickListener, ApiServerResponse {

    EditText old_pwd, new_pwd, confirm_pwd;
    Button update_profile;
    String user_id = "";
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_change_password);

        // find ids
        old_pwd = (EditText) findViewById(R.id.old_password);
        new_pwd = (EditText) findViewById(R.id.new_password);
        confirm_pwd = (EditText) findViewById(R.id.confirm_password);
        update_profile = (Button) findViewById(R.id.update_profile_btn);

        update_profile.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                    /*Intent intent = new Intent(ChangePassword.this, Profile.class);
                    startActivity(intent);
                    finish();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    /*@Override
    public void onSuccess(String response) {
        try {
            ToastMsg.showShortToast(ChangePassword.this, response);
            finish();
            *//*Intent intent = new Intent(ChangePassword.this, Profile.class);
            startActivity(intent);
            finish();*//*
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/
   /* @Override
    public void onError(String error) {
        ToastMsg.showShortToast(ChangePassword.this, error);
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_profile_btn:
                try {

                    if (validateFields(old_pwd.getText().toString().trim(), new_pwd.getText().toString().trim(), confirm_pwd.getText().toString().trim())) {
                        ServerAPI.getInstance().changePassword(ApiServerResponse.CHANGE_PASSWORD, Session.get_userId(pref), old_pwd.getText().toString().trim(), new_pwd.getText().toString().trim(), this);
                    }
                    //new ChangePasswordManager(ChangePassword.this, this).sendRequest(user_id, old_pwd.getText().toString(), new_pwd.getText().toString(), confirm_pwd.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    private boolean validateFields(String current_pwd, String new_pwd, String confirm_password) {
        if (current_pwd.length() <= 0) {
            ToastMsg.showShortToast(ChangePassword.this, getResources().getString(R.string.old_password));
            return false;
        } else if (new_pwd.length() <= 0) {
            ToastMsg.showShortToast(ChangePassword.this, getResources().getString(R.string.new_password));

            return false;
        } else if (confirm_password.length() <= 0) {
            ToastMsg.showShortToast(ChangePassword.this, getResources().getString(R.string.confirm_password));
            return false;
        } else if (!new_pwd.equalsIgnoreCase(confirm_password)) {
            ToastMsg.showShortToast(ChangePassword.this, getResources().getString(R.string.confirm_password));
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            ChangePasswordModal changePasswordModal;

            switch (tag) {

                case ApiServerResponse.CHANGE_PASSWORD:
                    changePasswordModal = (ChangePasswordModal) response.body();
                    if (changePasswordModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                        ToastMsg.showShortToast(ChangePassword.this, changePasswordModal.getMessage());
                        finish();
                    }


                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
