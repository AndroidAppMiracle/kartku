package kartku.com.sort.manager;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import kartku.com.MainCategories.api.MainCategoriesApi;
import kartku.com.MainCategories.listener.MainCategoriesListener;
import kartku.com.sort.api.MaterialApi;
import kartku.com.sort.listener.MaterialListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;

/**
 * Created by satoti.garg on 9/23/2016.
 */
public class MaterialManager {

    private Context context;
    private MaterialListener listener;

    public MaterialManager(Context context, MaterialListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void sendRequest() {
        try {
            new MaterialApi(MaterialManager.this, context).sendRequest(API.MATERIAL_LISTING_REQUEST_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onSuccess(String response) {
        try{
            JSONObject response_object = new JSONObject(response);
            String status = response_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                JSONArray brand_info = response_object.getJSONArray(Constant.MATERIAL_INFO);
                listener.onMaterialSuccess(""+brand_info);

            }else{
                try {
                    String brand_info = response_object.getString(Constant.MATERIAL_INFO);
                    listener.onMaterialError(brand_info);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onError(String message) {

        listener.onMaterialError(message);
    }
}
