package kartku.com.sort.manager;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import kartku.com.MainCategories.api.MainCategoriesApi;
import kartku.com.MainCategories.listener.MainCategoriesListener;
import kartku.com.sort.api.BrandApi;
import kartku.com.sort.listener.BrandListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;

/**
 * Created by satoti.garg on 9/23/2016.
 */
public class BrandManager {

    private Context context;
    private BrandListener listener;

    public BrandManager(Context context, BrandListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void sendRequest() {
        try {
            new BrandApi(BrandManager.this, context).sendRequest(API.BRAND_LISTING_REQUEST_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onSuccess(String response) {
        try{
            JSONObject response_object = new JSONObject(response);
            String status = response_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                JSONArray brand_info = response_object.getJSONArray(Constant.BRAND_INFO);
                listener.onBrandSuccess(""+brand_info);

            }else{
                try {
                    String brand_info = response_object.getString(Constant.BRAND_INFO);
                    listener.onBrandError(brand_info);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void onError(String message) {

        listener.onBrandError(message);
    }
}
