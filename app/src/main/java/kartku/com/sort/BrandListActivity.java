package kartku.com.sort;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.adapter.CustomDialogAdapter;
import kartku.com.product_category.DemoProductList;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.BrandListModal;
import kartku.com.search.SearchActivity;
import kartku.com.sort.listener.BrandListener;
import kartku.com.sort.manager.BrandManager;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

public class BrandListActivity extends Utility implements /*BrandListener,*/ ApiServerResponse {

    ListView brandList;
    ArrayList<String> brandItems;
    ArrayList<String> brandsItemsIds;
    String search_key = "";
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_brand_list);
        try {
            pref = new ObscuredSharedPreferences(BrandListActivity.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
            brandItems = new ArrayList<String>();
            brandsItemsIds = new ArrayList<String>();
            brandList = (ListView) findViewById(R.id.brand_items_list);
            fetchBrands();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Intent intent = getIntent();
            search_key = intent.getStringExtra("search_key");

            brandList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        String name = brandItems.get(position);
                        String brandID = brandsItemsIds.get(position);
                        int search_id = position + 1;

                        //String query = "?query=".concat(search_key).concat("&brnd").concat("" + search_id).concat("=").concat(name);
                       /* Intent intent = new Intent(BrandListActivity.this, DemoProductList.class);
                        *//*Intent intent = new Intent(BrandListActivity.this, SearchActivity.class);*//*
                        intent.putExtra("search_query", query);
                        //intent.putExtra("search_id",position);
                        //intent.putExtra("search_cat","brnd");
                        startActivity(intent);*/

                        Intent data = new Intent();
                        data.setData(Uri.parse(brandID.trim()));
                        /*data.setData(Uri.parse(query));*/
                        setResult(RESULT_OK, data);
                        finish();
                        /*finish();*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void fetchBrands() {


        try {
            showLoading();
            ServerAPI.getInstance().getBrandList(ApiServerResponse.BRAND_LIST, Session.get_userId(pref), this);

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*new BrandManager(getApplicationContext(), this).sendRequest();*/
    }


/*    @Override
    public void onBrandSuccess(String response) {
        Log.d("", "on brand success response ******  " + response);
        try {
            JSONArray brand_info = new JSONArray(response);
            for (int j = 0; j < brand_info.length(); j++) {
                JSONObject item_object = brand_info.getJSONObject(j);
                String name = item_object.getString("name");
                String id = item_object.getString("id");
                brandItems.add(name);
                brandsItemsIds.add(id);
            }
            System.out.println("brand items *******  " + brandItems);
            CustomDialogAdapter adapter = new CustomDialogAdapter(getApplicationContext(), brandItems);
            brandList.setAdapter(adapter);

            brandList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    try {
                        String name = brandItems.get(position);
                        String brandID = brandsItemsIds.get(position);
                        int search_id = position + 1;

                        String query = "?query=".concat(search_key).concat("&brnd").concat("" + search_id).concat("=").concat(name);
                       *//* Intent intent = new Intent(BrandListActivity.this, DemoProductList.class);
                        *//**//*Intent intent = new Intent(BrandListActivity.this, SearchActivity.class);*//**//*
                        intent.putExtra("search_query", query);
                        //intent.putExtra("search_id",position);
                        //intent.putExtra("search_cat","brnd");
                        startActivity(intent);*//*

                        Intent data = new Intent();
                        data.setData(Uri.parse(brandID.trim()));
                        *//*data.setData(Uri.parse(query));*//*
                        setResult(RESULT_OK, data);
                        finish();
                        *//*finish();*//*
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBrandError(String error) {
        Log.d("", "on brand error response ******  " + error);
        ToastMsg.showShortToast(BrandListActivity.this, error);
    }*/


    @Override
    public void onSuccess(int tag, Response response) {

        try {

            BrandListModal brandListModal;

            switch (tag) {
                case ApiServerResponse.BRAND_LIST:

                    brandListModal = (BrandListModal) response.body();
                    if (brandListModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                        for (int i = 0; i < brandListModal.getBrand_info().size(); i++) {
                            brandItems.add(brandListModal.getBrand_info().get(i).getName());
                            brandsItemsIds.add(String.valueOf(brandListModal.getBrand_info().get(i).getId()));
                        }

                        CustomDialogAdapter adapter = new CustomDialogAdapter(getApplicationContext(), brandItems);
                        brandList.setAdapter(adapter);
                    }

                    hideLoading();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }
}
