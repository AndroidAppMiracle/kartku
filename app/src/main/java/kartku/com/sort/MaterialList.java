package kartku.com.sort;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.activity.SubProductCategoryList;
import kartku.com.adapter.CustomDialogAdapter;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.MaterialListingModal;
import kartku.com.sort.listener.MaterialListener;
import kartku.com.sort.manager.MaterialManager;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

public class MaterialList extends Utility implements /*MaterialListener,*/ ApiServerResponse {

    ListView brandList;
    ArrayList<String> brandItems;
    ArrayList<String> brandIds;
    String search_key = "";
    SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.content_brand_list);

            pref = new ObscuredSharedPreferences(MaterialList.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
            brandItems = new ArrayList<String>();
            brandIds = new ArrayList<String>();
            brandList = (ListView) findViewById(R.id.brand_items_list);
            getMaterials();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Intent intent = getIntent();
            search_key = intent.getStringExtra("search_key");

            brandList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String name = brandItems.get(position);
                    String materialID = brandIds.get(position);
                    int search_id = position;

                   // String query = "?query=".concat(search_key).concat("&mat").concat("" + search_id).concat("=").concat(name);

                    Intent data = new Intent();
                    data.setData(Uri.parse(materialID.trim()));
                        /*data.setData(Uri.parse(query));*/
                    setResult(RESULT_OK, data);
                    finish();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getMaterials() {


        //new MaterialManager(getApplicationContext(), this).sendRequest();
        try {

            showLoading();
            ServerAPI.getInstance().getMaterialList(ApiServerResponse.MATERIAL_LIST, Session.get_userId(pref), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


/*    @Override
    public void onMaterialSuccess(String response) {
        Log.d("", "on brand success response ******  " + response);
        try {
            JSONArray brand_info = new JSONArray(response);
            for (int j = 0; j < brand_info.length(); j++) {
                JSONObject item_object = brand_info.getJSONObject(j);
                String name = item_object.getString("name");
                String id = item_object.getString("id");
                brandItems.add(name);
                brandIds.add(id);
            }
            System.out.println("brand items *******  " + brandItems);
            CustomDialogAdapter adapter = new CustomDialogAdapter(getApplicationContext(), brandItems);
            brandList.setAdapter(adapter);


            brandList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    try {
                        String name = brandItems.get(position);
                        String materialID = brandIds.get(position);
                        int search_id = position;

                        String query = "?query=".concat(search_key).concat("&mat").concat("" + search_id).concat("=").concat(name);

                        Intent data = new Intent();
                        data.setData(Uri.parse(materialID.trim()));
                        *//*data.setData(Uri.parse(query));*//*
                        setResult(RESULT_OK, data);
                        finish();

                        //Intent intent = new Intent(MaterialList.this, DemoProductList.class);
                        *//*Intent intent = new Intent(MaterialList.this, SearchActivity.class);*//*
                        //intent.putExtra("search_query", query);

                        *//*Utility utility = new Utility();
                        utility.setQuery(query);*//*
                        *//*Utility.setQuery(query);*//*

                        // startActivity(intent);
                        //finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMaterialError(String error) {
        ToastMsg.showShortToast(MaterialList.this, error);
    }*/

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            MaterialListingModal materialListingModal;

            switch (tag) {


                case ApiServerResponse.MATERIAL_LIST:

                    materialListingModal = (MaterialListingModal) response.body();

                    if (materialListingModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                        for (int i = 0; i < materialListingModal.getMaterial_info().size(); i++) {
                            brandItems.add(materialListingModal.getMaterial_info().get(i).getName());
                            brandIds.add(String.valueOf(materialListingModal.getMaterial_info().get(i).getId()));

                        }


                        CustomDialogAdapter adapter = new CustomDialogAdapter(getApplicationContext(), brandItems);
                        brandList.setAdapter(adapter);

                    }

                    hideLoading();
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }
}
