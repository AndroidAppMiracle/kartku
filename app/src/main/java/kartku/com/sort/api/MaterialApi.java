package kartku.com.sort.api;

import android.content.Context;

import kartku.com.MainCategories.manager.MainCategoriesManager;
import kartku.com.sort.manager.MaterialManager;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/23/2016.
 */
public class MaterialApi implements CustomResponse {

    MaterialManager manager ;
    Context context;
    RequestServer request_server;

    public MaterialApi(MaterialManager manager , Context context)
    {
        this.manager = manager;
        this.context = context;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId){
        try {
            request_server = new RequestServer(context, requestId,this,"");
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
