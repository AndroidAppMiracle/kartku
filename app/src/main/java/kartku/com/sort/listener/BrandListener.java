package kartku.com.sort.listener;

/**
 * Created by satoti.garg on 9/23/2016.
 */
public interface BrandListener {
    void onBrandSuccess(String response);
    void onBrandError(String error);
}
