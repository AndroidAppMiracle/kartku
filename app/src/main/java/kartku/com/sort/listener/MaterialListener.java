package kartku.com.sort.listener;

/**
 * Created by satoti.garg on 9/23/2016.
 */
public interface  MaterialListener {

    void onMaterialSuccess(String response);
    void onMaterialError(String error);

}
