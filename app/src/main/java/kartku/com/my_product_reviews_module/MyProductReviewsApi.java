package kartku.com.my_product_reviews_module;

import android.content.Context;

import java.util.Map;

import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by Kshitiz Bali on 1/19/2017.
 */

public class MyProductReviewsApi implements CustomResponse {

    private RequestServer request_server;
    private Context context;
    private MyProductReviewsManager manager;

    public MyProductReviewsApi(Context context, MyProductReviewsManager manager) {
        this.context = context;
        this.manager = manager;
    }


    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }



    public void sendRequest(int requestId, Map<String, String> params) {
        try {
            request_server = new RequestServer(context, requestId, params, this, RequestConstants.GET_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
