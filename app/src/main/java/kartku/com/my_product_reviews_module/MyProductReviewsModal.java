package kartku.com.my_product_reviews_module;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/19/2017.
 */

public class MyProductReviewsModal {

    /**
     * status : OK
     * results : [{"id":"56","user_id":"181","product_id":"17","rating":"2.5","review":"","status":"","created_at":"2017-01-03 00:26:02","updated_at":null,"product_title":"My new product","product_description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","product_image":"http://kartku.s3.amazonaws.com/product/medium/QQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg"},{"id":"57","user_id":"181","product_id":"9","rating":"3.0","review":"","status":"","created_at":"2017-01-04 02:33:14","updated_at":null,"product_title":"Benches","product_description":"Benches","product_image":"http://kartku.s3.amazonaws.com/product/medium/cCQnlVVlz3cyd5CntsT8bZR2pMyzHXNk.jpg"},{"id":"58","user_id":"181","product_id":"17","rating":"2.0","review":"","status":"","created_at":"2017-01-04 03:20:08","updated_at":null,"product_title":"My new product","product_description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","product_image":"http://kartku.s3.amazonaws.com/product/medium/QQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg"},{"id":"59","user_id":"181","product_id":"17","rating":"3.5","review":"","status":"","created_at":"2017-01-12 07:20:27","updated_at":null,"product_title":"My new product","product_description":"this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg","product_image":"http://kartku.s3.amazonaws.com/product/medium/QQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg"},{"id":"60","user_id":"181","product_id":"9","rating":"0","review":"","status":"","created_at":"2017-01-16 00:44:23","updated_at":null,"product_title":"Benches","product_description":"Benches","product_image":"http://kartku.s3.amazonaws.com/product/medium/cCQnlVVlz3cyd5CntsT8bZR2pMyzHXNk.jpg"},{"id":"61","user_id":"181","product_id":"9","rating":"2","review":"good","status":"","created_at":"2017-01-16 00:45:01","updated_at":null,"product_title":"Benches","product_description":"Benches","product_image":"http://kartku.s3.amazonaws.com/product/medium/cCQnlVVlz3cyd5CntsT8bZR2pMyzHXNk.jpg"},{"id":"62","user_id":"181","product_id":"9","rating":"2","review":"KB GOOD MIRACLE","status":"","created_at":"2017-01-16 00:45:23","updated_at":null,"product_title":"Benches","product_description":"Benches","product_image":"http://kartku.s3.amazonaws.com/product/medium/cCQnlVVlz3cyd5CntsT8bZR2pMyzHXNk.jpg"},{"id":"63","user_id":"181","product_id":"9","rating":"3","review":"Good","status":"","created_at":"2017-01-18 06:19:41","updated_at":null,"product_title":"Benches","product_description":"Benches","product_image":"http://kartku.s3.amazonaws.com/product/medium/cCQnlVVlz3cyd5CntsT8bZR2pMyzHXNk.jpg"}]
     */

    private String status;
    private List<ResultsBean> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class ResultsBean {
        /**
         * id : 56
         * user_id : 181
         * product_id : 17
         * rating : 2.5
         * review :
         * status :
         * created_at : 2017-01-03 00:26:02
         * updated_at : null
         * product_title : My new product
         * product_description : this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test product!  this is test hg
         * product_image : http://kartku.s3.amazonaws.com/product/medium/QQfzk9YNRyZ7SsOuShrdVwFEV_w5GH3n.jpg
         */

        private String id;
        private String user_id;
        private String product_id;
        private String rating;
        private String review;
        private String status;
        private String created_at;
        private Object updated_at;
        private String product_title;
        private String product_description;
        private String product_image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }

        public String getProduct_title() {
            return product_title;
        }

        public void setProduct_title(String product_title) {
            this.product_title = product_title;
        }

        public String getProduct_description() {
            return product_description;
        }

        public void setProduct_description(String product_description) {
            this.product_description = product_description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }
    }
}
