package kartku.com.my_product_reviews_module;

/**
 * Created by Kshitiz Bali on 1/19/2017.
 */

public interface MyProductReviewsListener {

    void onMyProductReviewSuccess(String response);

    void onMyProductReviewError(String error);
}
