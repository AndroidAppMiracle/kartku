package kartku.com.my_product_reviews_module;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 1/19/2017.
 */

public class MyProductReviewsManager {

    private Context context;
    private MyProductReviewsListener listener;
    SharedPreferences pref;


    public MyProductReviewsManager(Context context, MyProductReviewsListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }


    public void sendRequest(String user_id) {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.ORDER_ID, "" + user_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new MyProductReviewsApi(context, MyProductReviewsManager.this).sendRequest(API.MY_PRODUCT_REVIEWS_REQUEST_ID, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                JSONArray my_reviews_list = res_object.getJSONArray(Constant.RESULTS);
                listener.onMyProductReviewSuccess("" + my_reviews_list);

            } else {
                //String message = res_object.getString(Constant.STATUS);
                listener.onMyProductReviewError(status);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onError(String message) {
        listener.onMyProductReviewError(message);
    }
}
