package kartku.com.firebase;

/**
 * Created by tanuja.gupta on 10/13/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import kartku.com.activity.ProductCategoryList;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    // private Context context;
    public static String refreshedToken;
    SharedPreferences pref;


    /*public static FirebaseInstanceIdService getInstance() {
        return new FirebaseInstanceIdService();
    }*/

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        //context = this.context;


       /* String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("TOKEN", "Refreshed token: " + refreshedToken);*/

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.

        //pref = new ObscuredSharedPreferences(context, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        pref = new ObscuredSharedPreferences(this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        Session.setSessionToken(pref, token);


        //mPrefs.edit().putString(Constants.FCM_TOKEN, token).commit()


        //Session.setSessionToken(pref, token);
        /*Log.e("Token", "---" + token);*/
    }
}
