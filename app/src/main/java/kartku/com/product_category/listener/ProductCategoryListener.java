package kartku.com.product_category.listener;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public interface ProductCategoryListener {

    void onSuccess(String response);

    void onError(String error);
}
