package kartku.com.product_category;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import kartku.com.MainCategories.CategoriesActivity;
import kartku.com.R;
import kartku.com.adapter.ProductListAdapter;
import kartku.com.adapter.SearchListAdapter;
import kartku.com.cart.Cart;
import kartku.com.cart.CartDetailsNew;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.listener.RecyclerItemClickListener;
import kartku.com.product_category.model.ProductListModel;
import kartku.com.product_category.listener.ProductCategoryListener;
import kartku.com.product_category.manager.ProductCategoryManager;
import kartku.com.product_detail.ProductDetail;
import kartku.com.product_review.ProductReview;
import kartku.com.search.SearchActivity;
import kartku.com.search.listener.SearchListener;
import kartku.com.search.manager.SearchManager;
import kartku.com.search.model.SearchBean;
import kartku.com.sort.BrandListActivity;
import kartku.com.sort.MaterialList;
import kartku.com.sort.listener.BrandListener;
import kartku.com.sort.listener.MaterialListener;
import kartku.com.sort.manager.BrandManager;
import kartku.com.sort.manager.MaterialManager;
import kartku.com.utils.BadgeDrawable;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.listener.WishlistListener;
import kartku.com.wishlist.manager.WishlistManager;
import kartku.com.wishlist.model.WishlistBean;

public class ProductList extends AppCompatActivity implements ProductCategoryListener, View.OnClickListener, WishlistListener {

    ArrayList<ProductListModel> arrayList = new ArrayList<ProductListModel>();
    ArrayList<SearchBean> searchList = new ArrayList<SearchBean>();
    RecyclerView recyclerView;
    ImageView search_icon;
    EditText search_text;
    Button button_sort, button_filter;
    ProductListModel productListModel;
    SharedPreferences pref;
    String id;
    TextView cancel, tv_productPath, tv_productListPath, tv_SortBy, tv_FilterBy;
    LinearLayout search_layout, filter_layout;
    ArrayList<WishlistBean> WishlistarrayList = new ArrayList<>();

    JSONArray products_list;

    int spanCount = 2; // 3 columns
    int spacing = 10; // 50px
    boolean includeEdge = false;

    boolean pendingIntroAnimation;

    //private static final int ANIM_DURATION_TOOLBAR = 300;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        //setupWindowAnimations();

        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        recyclerView.setItemAnimator(new SlideInUpAnimator());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);

        search_icon = (ImageView) findViewById(R.id.search_icon);
        search_text = (EditText) findViewById(R.id.search_text);
        search_layout = (LinearLayout) findViewById(R.id.top_wrapper);
        cancel = (TextView) findViewById(R.id.cancel_text);
        filter_layout = (LinearLayout) findViewById(R.id.filterWrapper);
        search_icon.setOnClickListener(this);
        button_sort = (Button) findViewById(R.id.button_sort);
        button_filter = (Button) findViewById(R.id.button_filter);
        tv_productPath = (TextView) findViewById(R.id.tv_productPath);
        tv_productListPath = (TextView) findViewById(R.id.tv_productListPath);
        tv_SortBy = (TextView) findViewById(R.id.tv_SortBy);
        tv_FilterBy = (TextView) findViewById(R.id.tv_FilterBy);

        cancel.setOnClickListener(this);
        tv_SortBy.setOnClickListener(this);


        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        setSupportActionBar(toolbar);


        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Resources res = getResources();
            String text = String.format(res.getString(R.string.product_path), Session.getselectedCategory(pref));
            String textPathList = String.format(res.getString(R.string.product_path_list_name), Session.getselectedSubCategory(pref));
            CharSequence styledText = Html.fromHtml(textPathList);
            //setMultipleColourText(Session.getselectedCategory(pref), Session.getselectedSubCategory(pref), tv_productPath);
            tv_productPath.setText(text);
            tv_productListPath.setText(styledText);

        } catch (Exception e) {
            e.printStackTrace();
        }

        /***
         * get product categories on basis of category id
         */
        try {
            id = Session.get_selected_category_id(pref);
            Log.d("", "selected cat id " + id);
            new ProductCategoryManager(ProductList.this, this).sendRequest(id);

        } catch (Exception e) {
            e.printStackTrace();
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_SortBy:
                try {
                    //ToastMsg.showLongToast(ProductList.this, "SORT BY");
                    /*SearchActivity searchActivity = new SearchActivity();
                    searchActivity.filterDialog();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_list_menu, menu);
        return true;
    }

    @Override
    public void onSuccess(String response) {
        try {
            products_list = new JSONArray(response);
            Log.d("", "product categories list " + products_list);
            try {
                String user_id = Session.get_userId(pref);
                new WishlistManager(ProductList.this, this).sendRequest(user_id, "");
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String error) {
        ToastMsg.showShortToast(ProductList.this, error);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case android.R.id.home:
                try {
                  /*  Intent intent = new Intent(ProductList.this, Home_dashboard.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);*/
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.search:
                try {
                    Intent intent = new Intent(ProductList.this, SearchActivity.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.miCompose:
                Intent in = new Intent(this, CartDetailsNew.class);
                /*Intent in = new Intent(this, Cart.class);*/
                startActivity(in);
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }


    @Override
    public void onwishlistError(String error) {
        ToastMsg.showLongToast(ProductList.this, error);
    }

    @Override
    public void onwishlistSuccess(String response) {
        try {
            try {
                JSONArray products_wishlist = new JSONArray(response);
                System.out.println("product wishlist *** " + products_wishlist);
                Log.i("ProdWishApi ", response);
                ArrayList<String> wishlistIds = new ArrayList<String>();
                for (int i = 0; i < products_wishlist.length(); i++) {
                    JSONObject jsonObject = products_wishlist.getJSONObject(i);
                    WishlistBean list_model = new WishlistBean();
                    Log.d("", "wishlist id " + jsonObject.getString(Constant.ID));
                    String wishlist_id = jsonObject.getString(Constant.ID);
                    wishlistIds.add(wishlist_id);
                    list_model.setId(jsonObject.getString(Constant.ID));
                    list_model.setName(jsonObject.getString(Constant.NAME));
                    list_model.setPrice(jsonObject.getString(Constant.PRICE_KEY));
                    list_model.setDescription(jsonObject.getString(Constant.DESCRIPTION));
                    list_model.setImage(jsonObject.getString(Constant.IMAGE));
                    WishlistarrayList.add(list_model);
                }

                for (int j = 0; j < products_list.length(); j++) {
                    JSONObject jsonObject1 = products_list.getJSONObject(j);
                    productListModel = new ProductListModel();
                    String product_id = jsonObject1.getString(Constant.ID);

                    productListModel = new ProductListModel();
                    productListModel.setName(jsonObject1.getString(Constant.TITTLE));
                    productListModel.setPrice(jsonObject1.getString(Constant.PRICE));
                    productListModel.setImage_product(jsonObject1.getString(Constant.IMAGE_URL));
                    productListModel.setId(jsonObject1.getString(Constant.ID));
                    productListModel.setBrand(jsonObject1.getString(Constant.BRAND));
                    productListModel.setDescription(jsonObject1.getString(Constant.DESCRIPTION));
                    productListModel.setMaterial(jsonObject1.getString(Constant.MATERIAl));

                    //if(product_id!=null && wishlist_id!=null) {
                    if (wishlistIds.contains(product_id)) {
                        productListModel.setWishlist("add");

                    } else {
                        productListModel.setWishlist("remove");
                    }
                    //}
                    arrayList.add(productListModel);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                ProductListAdapter adapter = new ProductListAdapter(getApplicationContext(), arrayList, WishlistarrayList);
                recyclerView.setAdapter(adapter);
              /*  int spanCount = 2; // 3 columns
                int spacing = 25; // 50px
                boolean includeEdge = false;
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                recyclerView.setItemAnimator(new SlideInUpAnimator());
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
                recyclerView.setLayoutManager(layoutManager);*/
                if (pendingIntroAnimation) {
                    pendingIntroAnimation = false;
                    startIntroAnimation();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // getFragmentManager().popBackStack();
    }

    private void startIntroAnimation() {
        recyclerView.setTranslationY(recyclerView.getHeight());
        recyclerView.setAlpha(0f);
        recyclerView.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();


    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            //products_list = new JSONArray(response);
            Log.d("", "product categories list " + products_list);
            try {
                if (!arrayList.isEmpty()) {
                    arrayList.clear();
                }
                if (!WishlistarrayList.isEmpty()) {
                    WishlistarrayList.clear();
                }
                String user_id = Session.get_userId(pref);
                new WishlistManager(ProductList.this, this).sendRequest(user_id, "");
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    /*private void setupWindowAnimations() {

        *//*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = (Slide) TransitionInflater.from(this).inflateTransition(R.transition.activity_slide);
            getWindow().setExitTransition(slide);
        }*//*
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = new Slide();
            slide.setDuration(1000);
            getWindow().setExitTransition(slide);

        }


    }*/


    /*public void setMultipleColourText(String one, String two, TextView textView) {
        try {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            *//*String white = "this is white";*//*
            SpannableString whiteSpannable = new SpannableString(one);
            whiteSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, one.length(), 0);

            builder.append(whiteSpannable);
            *//*String red = "this is red";*//*
            SpannableString redSpannable = new SpannableString(two);
            redSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, two.length(), 0);
            builder.append(redSpannable);



            *//*String blue = "this is blue";
            SpannableString blueSpannable = new SpannableString(blue);
            blueSpannable.setSpan(new ForegroundColorSpan(Color.BLUE), 0, blue.length(), 0);
            builder.append(blueSpannable);*//*

            //String text = String.format(res.getString(R.string.product_path), Session.getselectedCategory(pref), Session.getselectedSubCategory(pref));
            ///CharSequence styledText = Html.fromHtml(text);
            textView.setText(builder, TextView.BufferType.SPANNABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

    /*private void startIntroToolbarAnimation() {

        //btnCreate.setTranslationY(2 * getResources().getDimensionPixelOffset(R.dimen.btn_fab_size));

        int actionbarSize = Utility.dpToPx(56);
        toolbar.setTranslationY(-actionbarSize);
        //ivLogo.setTranslationY(-actionbarSize);
        //inboxMenuItem.getActionView().setTranslationY(-actionbarSize);

        toolbar.animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(300);
        ivLogo.animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(400);
        inboxMenuItem.getActionView().animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        startContentAnimation();
                    }
                })
                .start();
    }*/
}
