package kartku.com.product_category.manager;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.dashboard_module.listener.HomeBannersListener;
import kartku.com.login_module.api.LoginAPI;
import kartku.com.product_category.api.ProductCategoryApi;
import kartku.com.product_category.listener.ProductCategoryListener;
import kartku.com.product_detail.api.ProductDetailApi;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class ProductCategoryManager {

    private Context context;
    private ProductCategoryListener listener;

    public ProductCategoryManager(Context context, ProductCategoryListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void sendRequest(String categoryId) {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.CATEGORY_ID, "" + categoryId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                new ProductCategoryApi(context, ProductCategoryManager.this).sendRequest(API.CATEGORY_PRODUCTS_REQUEST_ID, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                try {
                    JSONArray products_data = new JSONArray(res_object.getString(Constant.PRODUCTS_DATA));
                    listener.onSuccess("" + products_data);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    String message = res_object.getString(Constant.PRODUCTS_DATA);
                    listener.onError(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onError(String message) {

        listener.onError(message);
    }
}
