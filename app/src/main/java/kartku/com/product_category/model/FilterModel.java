package kartku.com.product_category.model;

/**
 * Created by tanuja.gupta on 9/20/2016.
 */
public class FilterModel {

    String categoryName,categoryId;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
