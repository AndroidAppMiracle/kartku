package kartku.com.product_category.api;

import android.content.Context;

import java.util.Map;

import kartku.com.contact.Contact;
import kartku.com.contact.manager.ContactManager;
import kartku.com.product_category.manager.ProductCategoryManager;
import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class ProductCategoryApi implements CustomResponse {

    ProductCategoryManager manager;
    Context context;
    RequestServer request_server;

    public ProductCategoryApi(Context context, ProductCategoryManager manager) {
        this.context = context;
        this.manager = manager;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId, Map<String, String> params) {
        try {
            request_server = new RequestServer(context, requestId, params, this, RequestConstants.POST_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
