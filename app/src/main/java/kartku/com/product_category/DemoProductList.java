package kartku.com.product_category;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import kartku.com.R;
import kartku.com.adapter.ProductListAdapter;
import kartku.com.adapter.SearchListAdapter;
import kartku.com.cart.CartDetailsNew;
import kartku.com.product_category.listener.ProductCategoryListener;
import kartku.com.product_category.manager.ProductCategoryManager;
import kartku.com.product_category.model.ProductListModel;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.EventBusProductDetailUpdateModal;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.search.SearchActivity;
import kartku.com.search.listener.HighLowListener;
import kartku.com.search.listener.LowHighListener;
import kartku.com.search.listener.NewArrivalsListener;
import kartku.com.search.listener.SearchListener;
import kartku.com.search.manager.HighLowManager;
import kartku.com.search.manager.LowHighManager;
import kartku.com.search.manager.NewArrivalsManager;
import kartku.com.search.manager.SearchManager;
import kartku.com.search.model.SearchBean;
import kartku.com.sort.BrandListActivity;
import kartku.com.sort.MaterialList;
import kartku.com.utils.Constant;
import kartku.com.utils.ResponseKeys;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.listener.WishlistListener;
import kartku.com.wishlist.manager.WishlistManager;
import kartku.com.wishlist.model.WishlistBean;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 1/4/2017.
 */

public class DemoProductList extends Utility implements ProductCategoryListener, View.OnClickListener, WishlistListener, SearchListener, ApiServerResponse {

    ArrayList<ProductListModel> arrayList = new ArrayList<ProductListModel>();
    ArrayList<SearchBean> searchList = new ArrayList<SearchBean>();
    RecyclerView recyclerView;
    ImageView search_icon;
    EditText search_text;
    Button button_sort, button_filter;
    ProductListModel productListModel;
    SharedPreferences pref;
    String id;
    TextView cancel, tv_productPath, tv_productListPath, tv_SortBy, tv_FilterBy;
    LinearLayout search_layout, filter_layout;
    ArrayList<WishlistBean> WishlistarrayList = new ArrayList<>();
    String textPathList;

    JSONArray products_list;

    int spanCount = 2; // 3 columns
    int spacing = 10; // 50px
    boolean includeEdge = false;

    boolean pendingIntroAnimation;
    boolean filterOptionEnable, searchEnable = false;

    String filterQuery;

    //private static final int ANIM_DURATION_TOOLBAR = 300;

    static final int PICK_BRAND_LIST = 1;
    static final int PICK_MATERIAL_LIST = 2;
    TextView tv_count;

    Button buttonSort, buttonFilter;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        //setupWindowAnimations();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        recyclerView.setItemAnimator(new SlideInUpAnimator());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        /*SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);*/

        search_icon = (ImageView) findViewById(R.id.search_icon);
        search_text = (EditText) findViewById(R.id.search_text);
        search_layout = (LinearLayout) findViewById(R.id.top_wrapper);
        cancel = (TextView) findViewById(R.id.cancel_text);
        filter_layout = (LinearLayout) findViewById(R.id.filterWrapper);
        search_icon.setOnClickListener(this);
        button_sort = (Button) findViewById(R.id.button_sort);
        button_filter = (Button) findViewById(R.id.button_filter);
        tv_productPath = (TextView) findViewById(R.id.tv_productPath);
        tv_productListPath = (TextView) findViewById(R.id.tv_productListPath);
        tv_SortBy = (TextView) findViewById(R.id.tv_SortBy);
        tv_FilterBy = (TextView) findViewById(R.id.tv_FilterBy);

        buttonSort = (Button) findViewById(R.id.buttonSort);
        buttonFilter = (Button) findViewById(R.id.buttonFilter);

        cancel.setOnClickListener(this);
        tv_SortBy.setOnClickListener(this);
        tv_FilterBy.setOnClickListener(this);
        buttonFilter.setOnClickListener(this);
        buttonSort.setOnClickListener(this);


        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        setSupportActionBar(toolbar);


       /* try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Resources res = getResources();
            String text = String.format(res.getString(R.string.product_path), Session.getselectedCategory(pref));
            String textPathList = String.format(res.getString(R.string.product_path_list_name), Session.getselectedSubCategory(pref));
            CharSequence styledText = Html.fromHtml(textPathList);
            //setMultipleColourText(Session.getselectedCategory(pref), Session.getselectedSubCategory(pref), tv_productPath);
            tv_productPath.setText(text);
            tv_productListPath.setText(styledText);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /***
         * get product categories on basis of category id
         */
        try {
            id = Session.get_selected_category_id(pref);
            Log.d("", "selected cat id " + id);
            //ShowingProgressDialog

            //Utility.includeProgressDialog(DemoProductList.this, true);
            showLoading();
            new ProductCategoryManager(DemoProductList.this, this).sendRequest(id);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Intent intent = getIntent();
            String search_query = intent.getStringExtra("search_query");
            /*String sub_cat_heading = intent.getStringExtra("sub_cat_heading");*/
            String cat_name = intent.getStringExtra(ResponseKeys.SUB_SUB_CAT_NAME);
            // String search_cat = intent.getStringExtra("search_cat");
            //int search_id = intent.getIntExtra("search_id",0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Resources res = getResources();

            String text = String.format(res.getString(R.string.product_path), Session.getselectedCategory(pref));
            textPathList = String.format(res.getString(R.string.product_path_list_name), cat_name);
            //ToastMsg.showLongToast(DemoProductList.this, "Cat " + text + " SubCat " + textPathList);
            CharSequence styledText = Html.fromHtml(textPathList);
            //setMultipleColourText(Session.getselectedCategory(pref), Session.getselectedSubCategory(pref), tv_productPath);
            tv_productPath.setText(text);
            if (tv_productListPath.getVisibility() == View.GONE) {
                tv_productListPath.setVisibility(View.VISIBLE);
            }
            tv_productListPath.setText(styledText);

            //new SearchManager(getApplicationContext(), this).sendRequest(search_query);

        } catch (Exception e) {
            e.printStackTrace();
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_SortBy:
                try {
                    /*//ToastMsg.showLongToast(DemoProductList.this, "SORT BY");
                    SearchActivity searchActivity = new SearchActivity();
                    searchActivity.filterDialog();*/
                    openDialogforMasterCat();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.tv_FilterBy:

                try {
                    filterDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.buttonFilter:


                try {
                    filterDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.buttonSort:
                try {
                    /*//ToastMsg.showLongToast(DemoProductList.this, "SORT BY");
                    SearchActivity searchActivity = new SearchActivity();
                    searchActivity.filterDialog();*/
                    openDialogforMasterCat();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

       /* MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.product_list_menu, menu);*/
        getMenuInflater().inflate(R.menu.menu_cart_count_product_list_login, menu);

        /*try {
            MenuItem searchItem = menu.findItem(R.id.search);
          *//*  MenuItem searchItem = menu.findItem(R.id.action_search);*//*
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    if (!query.equals("") || !query.isEmpty()) {
                        ToastMsg.showLongToast(DemoProductList.this, query);
                        searchEnable = true;
                        searchProduct(query);
                        *//*searchView.clearFocus();*//*
                    } else {
                        //ToastMsg.showLongToast(DemoProductList.this, "Please enter keyword to search.");
                        searchEnable = false;
                    }

                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_count);
        RelativeLayout rl_menu_layout = (RelativeLayout) item.getActionView();
        tv_count = (TextView) rl_menu_layout.findViewById(R.id.tv_count);

        rl_menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DemoProductList.this, CartDetailsNew.class);
                /*Intent in = new Intent(getActivity(), Cart.class);*/
                startActivity(in);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    public void searchProduct(String query) {
        try {
            new SearchManager(DemoProductList.this, this).sendRequest("?query=" + query);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccess(String response) {
        try {
            products_list = new JSONArray(response);
            Log.d("", "product categories list " + products_list);
            try {
                String user_id = Session.get_userId(pref);
                new WishlistManager(DemoProductList.this, this).sendRequest(user_id, "");
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSearchSuccess(String response) {
        try {
            products_list = new JSONArray(response);
            System.out.println("search product_categories_list *****  " + products_list);
            searchList.clear();
            try {
                String user_id = Session.get_userId(pref);
                new WishlistManager(DemoProductList.this, this).sendRequest(user_id, "");
            } catch (Exception e) {
                e.printStackTrace();
            }

           /* for(int i=0;i<products_list.length();i++){
                JSONObject jsonObject = products_list.getJSONObject(i);
                SearchBean obj_bean = new SearchBean();
                obj_bean.setName(jsonObject.getString(Constant.TITTLE).toString());
                obj_bean.setPrice(jsonObject.getString(Constant.PRICE));
                obj_bean.setImage(jsonObject.getString(Constant.IMAGE_URL));
                obj_bean.setId(jsonObject.getString(Constant.ID));
                searchList.add(obj_bean);
            }
            SearchListAdapter adapter=new SearchListAdapter(getApplicationContext(),searchList);
            recyclerView.setAdapter(adapter);
            recyclerView.setVisibility(View.VISIBLE);
            filter_layout.setVisibility(View.VISIBLE);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(String error) {
        ToastMsg.showShortToast(DemoProductList.this, error);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case android.R.id.home:
                try {
                  /*  Intent intent = new Intent(DemoProductList.this, Home_dashboard.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);*/
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.search:
                try {
                    Intent intent = new Intent(DemoProductList.this, SearchActivity.class);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.miCompose:
                Intent in = new Intent(this, CartDetailsNew.class);
                /*Intent in = new Intent(this, Cart.class);*/
                startActivity(in);
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }


    @Override
    public void onwishlistError(String error) {
        hideLoading();
        ToastMsg.showLongToast(DemoProductList.this, error);
    }

    @Override
    public void onwishlistSuccess(String response) {
        try {
            try {
                hideLoading();
                JSONArray products_wishlist = new JSONArray(response);
                System.out.println("product wishlist *** " + products_wishlist);
                ArrayList<String> wishlistIds = new ArrayList<String>();
                if (!WishlistarrayList.isEmpty()) {
                    WishlistarrayList.clear();
                }

                for (int i = 0; i < products_wishlist.length(); i++) {
                    JSONObject jsonObject = products_wishlist.getJSONObject(i);
                    WishlistBean list_model = new WishlistBean();
                    Log.d("", "wishlist id " + jsonObject.getString(Constant.ID));
                    String wishlist_id = jsonObject.getString(Constant.ID);
                    wishlistIds.add(wishlist_id);
                    list_model.setId(jsonObject.getString(Constant.ID));
                    list_model.setName(jsonObject.getString(Constant.NAME));
                    list_model.setPrice(jsonObject.getString(Constant.PRICE_KEY));
                    list_model.setDescription(jsonObject.getString(Constant.DESCRIPTION));
                    list_model.setImage(jsonObject.getString(Constant.IMAGE));
                    WishlistarrayList.add(list_model);
                }

                if (!arrayList.isEmpty()) {
                    arrayList.clear();
                }

                if (searchEnable) {

                    try {
                        if (!searchList.isEmpty()) {
                            searchList.clear();
                        }

                        System.out.println("search product_categories_list *****  " + products_list);
                        for (int i = 0; i < products_list.length(); i++) {
                            JSONObject jsonObject = products_list.getJSONObject(i);
                            SearchBean obj_bean = new SearchBean();
                            String product_id = jsonObject.getString(Constant.ID);

                            obj_bean.setName(jsonObject.getString(Constant.TITTLE));
                            obj_bean.setPrice(jsonObject.getString(Constant.PRICE));
                            obj_bean.setImage(jsonObject.getString(Constant.IMAGE_URL));
                            obj_bean.setId(jsonObject.getString(Constant.ID));

                            if (wishlistIds.contains(product_id)) {
                                obj_bean.setWishlist("add");

                            } else {
                                obj_bean.setWishlist("remove");
                            }
                            searchList.add(obj_bean);
                        }
                        SearchListAdapter adapter = new SearchListAdapter(getApplicationContext(), searchList);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setItemAnimator(new SlideInUpAnimator());
                        recyclerView.setVisibility(View.VISIBLE);
                        //filter_layout.setVisibility(View.VISIBLE);
                        if (pendingIntroAnimation) {
                            pendingIntroAnimation = false;
                            startIntroAnimation();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                for (int j = 0; j < products_list.length(); j++) {
                    JSONObject jsonObject1 = products_list.getJSONObject(j);
                    productListModel = new ProductListModel();
                    String product_id = jsonObject1.getString(Constant.ID);

                    productListModel = new ProductListModel();
                    productListModel.setName(jsonObject1.getString(Constant.TITTLE));
                    productListModel.setPrice(jsonObject1.getString(Constant.PRICE));
                    productListModel.setImage_product(jsonObject1.getString(Constant.IMAGE_URL));
                    productListModel.setId(jsonObject1.getString(Constant.ID));
                    productListModel.setBrand(jsonObject1.getString(Constant.BRAND));
                    productListModel.setDescription(jsonObject1.getString(Constant.DESCRIPTION));
                    productListModel.setMaterial(jsonObject1.getString(Constant.MATERIAl));

                    //if(product_id!=null && wishlist_id!=null) {
                    if (wishlistIds.contains(product_id)) {
                        productListModel.setWishlist("add");

                    } else {
                        productListModel.setWishlist("remove");
                    }
                    //}
                    arrayList.add(productListModel);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                ProductListAdapter adapter = new ProductListAdapter(getApplicationContext(), arrayList, WishlistarrayList);
                recyclerView.setAdapter(adapter);

                //Utility.includeProgressDialog(DemoProductList.this, false);

              /*  int spanCount = 2; // 3 columns
                int spacing = 25; // 50px
                boolean includeEdge = false;
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                recyclerView.setItemAnimator(new SlideInUpAnimator());
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
                recyclerView.setLayoutManager(layoutManager);*/
                if (pendingIntroAnimation) {
                    pendingIntroAnimation = false;
                    startIntroAnimation();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // getFragmentManager().popBackStack();
    }

    private void startIntroAnimation() {
        recyclerView.setTranslationY(recyclerView.getHeight());
        recyclerView.setAlpha(0f);
        recyclerView.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1f)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();


    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            //products_list = new JSONArray(response);
            Log.d("", "product categories list " + products_list);
            try {

                //Utility utility = new Utility();

                //filterQuery = utility.getQuery();
                //ToastMsg.showLongToast(DemoProductList.this, filterQuery);


                if (!arrayList.isEmpty()) {
                    arrayList.clear();
                }
                if (!WishlistarrayList.isEmpty()) {
                    WishlistarrayList.clear();
                }
                String user_id = Session.get_userId(pref);
                ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS,Session.get_userId(pref), Session.getcart_id(pref), this);
                new WishlistManager(DemoProductList.this, this).sendRequest(user_id, "");


            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

/*
    private void setupWindowAnimations() {

        *//*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = (Slide) TransitionInflater.from(this).inflateTransition(R.transition.activity_slide);
            getWindow().setExitTransition(slide);
        }*//*
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = new Slide();
            slide.setDuration(1000);
            getWindow().setExitTransition(slide);

        }


    }*/

    public void filterDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);

        TextView new_arrivals = (TextView) dialog.findViewById(R.id.new_arrivals);
        new_arrivals.setText("Brands");

        TextView LowHigh = (TextView) dialog.findViewById(R.id.lowHigh);
        LowHigh.setText("Materials");

        TextView HighLow = (TextView) dialog.findViewById(R.id.Highlow);
        HighLow.setVisibility(View.GONE);

        View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        new_arrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new NewArrivalsManager(getApplicationContext(), new NewArrivalsListener() {
                        @Override
                        public void onnewArrivalsListenerSuccess(String response) {

                            try {
                                dialog.dismiss();
                                Intent intent = new Intent(DemoProductList.this, BrandListActivity.class);
                                intent.putExtra("search_key", textPathList.trim());
                                filterOptionEnable = true;
                                /*intent.putExtra("search_key", search_text.getText().toString());*/
                                startActivityForResult(intent, PICK_BRAND_LIST);
                                //startActivity(intent);
                                //finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(DemoProductList.this, error);
                        }
                    }).sendRequest("?query=" + textPathList.trim());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LowHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new LowHighManager(getApplicationContext(), new LowHighListener() {
                        @Override
                        public void onlowhighListenerSuccess(String response) {
                            try {
                                dialog.dismiss();
                                Intent intent = new Intent(DemoProductList.this, MaterialList.class);
                                intent.putExtra("search_key", textPathList.trim());
                                filterOptionEnable = true;
                                startActivityForResult(intent, PICK_MATERIAL_LIST);
                                //startActivity(intent);
                                //finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(DemoProductList.this, error);
                        }
                    }).sendRequest("?query=" + textPathList.trim() + "&less=less");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        /*SeekBar seek_bar = (SeekBar)dialog.findViewById(R.id.price_seekbar);
        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });*/
        dialog.show();
    }

    public void openDialogforMasterCat() {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("Support");

        TextView new_arrivals = (TextView) dialog.findViewById(R.id.new_arrivals);

        TextView LowHigh = (TextView) dialog.findViewById(R.id.lowHigh);

        TextView HighLow = (TextView) dialog.findViewById(R.id.Highlow);

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);

        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        new_arrivals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String link = "?query=" + textPathList.trim() + "&less=less";
                    Log.i("query ", link);
                    ToastMsg.showLongToast(DemoProductList.this, link);
                    new NewArrivalsManager(getApplicationContext(), new NewArrivalsListener() {
                        @Override
                        public void onnewArrivalsListenerSuccess(String response) {

                            try {

                                bindData(response);
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(DemoProductList.this, error);
                        }
                    }).sendRequest("?query=" + textPathList.trim());
                    /*.sendRequest("?query=" + search_text.getText().toString());*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LowHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String link = "?query=" + textPathList.trim() + "&less=less";
                    Log.i("query ", link);
                    ToastMsg.showLongToast(DemoProductList.this, link);
                    new LowHighManager(getApplicationContext(), new LowHighListener() {
                        @Override
                        public void onlowhighListenerSuccess(String response) {
                            try {

                                bindData(response);
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(DemoProductList.this, error);
                        }
                    }).sendRequest("?query=" + textPathList.trim() + "&less=less");
                    /*.sendRequest("?query=" + search_text.getText().toString() + "&less=less");*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        HighLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new HighLowManager(getApplicationContext(), new HighLowListener() {
                        @Override
                        public void onhighlowListenerSuccess(String response) {
                            try {
                                bindData(response);
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {
                            ToastMsg.showShortToast(DemoProductList.this, error);
                        }
                    }).sendRequest("?query=" + textPathList.trim() + "&high=high");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }


    private void bindData(String response) {
        try {
            try {

                products_list = new JSONArray(response);
                System.out.println("search product_categories_list *****  " + products_list);
                searchList.clear();
                try {
                    String user_id = Session.get_userId(pref);
                    new WishlistManager(DemoProductList.this, this).sendRequest(user_id, "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
               /* for(int i=0;i<products_list.length();i++){
                    JSONObject jsonObject = products_list.getJSONObject(i);
                    SearchBean obj_bean = new SearchBean();
                    obj_bean.setName(jsonObject.getString(Constant.TITTLE).toString());
                    obj_bean.setPrice(jsonObject.getString(Constant.PRICE));
                    obj_bean.setImage(jsonObject.getString(Constant.IMAGE_URL));
                    obj_bean.setId(jsonObject.getString(Constant.ID));
                    searchList.add(obj_bean);
                }
                SearchListAdapter adapter=new SearchListAdapter(getApplicationContext(),searchList);
                recyclerView.setAdapter(adapter);
                recyclerView.setVisibility(View.VISIBLE);
                filter_layout.setVisibility(View.VISIBLE);*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_BRAND_LIST || requestCode == PICK_MATERIAL_LIST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                //ToastMsg.showLongToast(DemoProductList.this, data.getData().toString());
                filterQuery = data.getData().toString();

                if (filterOptionEnable) {
                    /*ToastMsg.showLongToast(DemoProductList.this, "" + filterOptionEnable);*/
                    //Intent intent = getIntent();
                    //String search_query = intent.getStringExtra("search_query");
                    new SearchManager(getApplicationContext(), this).sendRequest(filterQuery);
                }
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                // Do something with the contact here (bigger example below)
            }
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                MyCartDetailModal myCartDetailModal;
                switch (tag) {
                    case ApiServerResponse.MY_CART_DETAILS:
                        myCartDetailModal = (MyCartDetailModal) response.body();
                        if (myCartDetailModal.getItems().size() == 0) {
                            tv_count.setText("0");
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            Session.setCartItemsQuantity(pref, "0");
                        } else {
                            Session.setCartItemsQuantity(pref, String.valueOf(myCartDetailModal.getItems().size()));
                            //ToastMsg.showLongToast(getActivity(), String.valueOf(myCartDetailModal.getItems().size()));
                            tv_count.setText(String.valueOf(myCartDetailModal.getItems().size()));
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            hideLoading();
            switch (tag) {
                case ApiServerResponse.MY_CART_DETAILS:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }


    @Subscribe
    public void onCartUpdate(EventBusProductDetailUpdateModal eventBusProductDetailUpdateModal) {

        try {
            tv_count.setText(eventBusProductDetailUpdateModal.getMessage());
            /*if (eventBusProductDetailUpdateModal.getMessage() == 1) {
                String cart_id = Session.getcart_id(pref);
                if (!cart_id.equalsIgnoreCase("")) {
                    ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, cart_id, this);
                }
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }


    }




    /* @Override
    public void onFilterSuccess(String response) {

        try {
            filterQuery = response;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFilterError(String error) {

    }*/
}
