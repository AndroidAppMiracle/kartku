package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.activity.ProductCategoryList;
import kartku.com.retrofit.modal.HomeScreenModal;
import kartku.com.utils.Constant;

/**
 * Created by Kshitizb on 14-03-2017.
 */

public class AllCategoriesAdapter extends RecyclerView.Adapter<AllCategoriesAdapter.ViewHolder> {


    private Context mContext;
    private ArrayList<HomeScreenModal.CategoryListBean> allCategoryList = new ArrayList<HomeScreenModal.CategoryListBean>();

    public AllCategoriesAdapter(Context context, ArrayList<HomeScreenModal.CategoryListBean> listToShow) {
        this.mContext = context;
        this.allCategoryList = listToShow;
      /*  this.mItemsList = itemsList;
        this.mProductsList = productList;
        this.ll_bottomLayout = bottomLayout;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        this.textViewAmount = textView;*/
       /* user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);*/
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_all_categories, parent, false);
        return new AllCategoriesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.tv_category_title.setText(allCategoryList.get(position).getTitle());
        Glide.with(mContext)
                .load(allCategoryList.get(position).getImage())
                .thumbnail(0.1f)
                .into(holder.iv_category_images);

        holder.rl_category_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, ProductCategoryList.class);
                   /* Bundle b = new Bundle();
                    b.putString(Constants.BOOK_ID, String.valueOf(list.get(position).getId()));*/
                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                i.putExtra(Constant.CATEGORY_ID, String.valueOf(allCategoryList.get(holder.getAdapterPosition()).getId()));
                mContext.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (allCategoryList.size() != 0) {
            return allCategoryList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_category_title;
        public ImageView iv_category_images;
        RelativeLayout rl_category_item;


        public ViewHolder(final View container) {
            super(container);
            tv_category_title = (TextView) container.findViewById(R.id.tv_category_title);
            iv_category_images = (ImageView) container.findViewById(R.id.iv_category_images);
            rl_category_item = (RelativeLayout) container.findViewById(R.id.rl_category_item);
        }
    }
}
