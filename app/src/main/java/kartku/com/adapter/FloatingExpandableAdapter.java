package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import kartku.com.R;
import kartku.com.product_category.DemoProductList;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti on 9/19/2016.
 */
public class FloatingExpandableAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, JSONArray> listChildData;
    private JSONObject sub_cat_item;
    private SharedPreferences pref;
    private String id;
    JSONArray sub_cat_info;

    public FloatingExpandableAdapter(Context context, List<String> listDataHeader,
                                     HashMap<String, JSONArray> listChildData, JSONArray sub_cat_info) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listChildData = listChildData;
        this.sub_cat_info = sub_cat_info;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        try {
            return this.listChildData.get(this.listDataHeader.get(groupPosition))
                    .get(childPosititon);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        // final String childText = (String) getChild(groupPosition, childPosition);
        try {
            sub_cat_item = new JSONObject("" + getChild(groupPosition, childPosition));
            System.out.println("sub_cat_item ****  " + sub_cat_item);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.categories_floatinglist, null);
        }
        TextView notifyMessage = (TextView) convertView
                .findViewById(R.id.notifyMessage);

        //  Log.d("","childtexttttt     " +getChild(groupPosition, childPosition) +  "  sub_cat_item  " + sub_cat_item);
        // if(sub_cat_item.length()!=0)
        {
            try {
                // String sub_sub_cat_id = "" , sub_sub_cat_name = "";
                final String cat_name = sub_cat_item.getString("sub_subcat_name");
                final String cat_id = sub_cat_item.getString("sub_subcat_id");

                //ToastMsg.showLongToast(context, "SubCate "+cat_name);
                //ToastMsg.showLongToast(context, "SubCatID "+cat_id);
                /*JSONArray sub_subcat_info = sub_cat_item.getJSONArray("sub_subcat_info");
                for (int j = 0 ; j<sub_subcat_info.length();j++)
                {
                    JSONObject item_object = sub_subcat_info.getJSONObject(j);
                    sub_sub_cat_id = item_object.getString("sub_subcat_id");
                    sub_sub_cat_name = item_object.getString("sub_subcat_name");

                }*/
                notifyMessage.setText(cat_name);
                notifyMessage.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent(v.getContext(), DemoProductList.class);
                            /*Intent intent = new Intent(v.getContext(), ProductList.class);*/
                            // intent.putExtra(Constant.SELECTED_CATEGORY_INDEX,cat_id);
                            if (!cat_name.isEmpty() && !cat_name.equals("")) {
                                intent.putExtra("cat_name", cat_name);
                                //Session.setselectedSubCategory(pref, cat_name);
                            } else {
                                intent.putExtra("cat_name", "");
                                //Session.setselectedSubCategory(pref, "");
                            }
                            Session.set_selected_category_id(pref, cat_id);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            v.getContext().startActivity(intent);


                            //ToastMsg.showLongToast(context, "SubCate " + cat_name);
                            /*ToastMsg.showLongToast(context, "SubCatID "+cat_id);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return this.listChildData.get(this.listDataHeader.get(groupPosition))
                    .length();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.categories_floatinglistgroup, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        TextView nxt_icon = (TextView) convertView.findViewById(R.id.nexticon);

        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        int counts = getChildrenCount(groupPosition);
        if (counts == 0) {
            nxt_icon.setVisibility(View.VISIBLE);
        } else {
            nxt_icon.setVisibility(View.GONE);
        }

        nxt_icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    // sub_cat_item = new JSONObject(""+getChild(groupPosition, groupPosition));
                    System.out.println("next ixon view clicked sub_cat_item listDataHeader *****  " + listDataHeader);
                    System.out.println("next ixon view clicked sub_cat_item listChildData *****  " + listChildData);
                    String sub_cat_name = null;
                    System.out.println("next ixon view clicked sub_cat_item sub_cat_info *****  " + sub_cat_info);
                    for (int j = 0; j < sub_cat_info.length(); j++) {
                        JSONObject item_object = sub_cat_info.getJSONObject(j);
                        sub_cat_name = item_object.getString("subcat_name");
                        String sub_cat_id = item_object.getString("subcat_id");
                        if (sub_cat_name.equalsIgnoreCase(headerTitle)) {
                            id = sub_cat_id;
                        }
                        //System.out.println("next ixon view clicked sub_cat_item listDataHeader 111*****  " + listDataHeader);
                        //System.out.println("next ixon view clicked sub_cat_item listChildData *1111 ****  " + listDataChild);
                    }
                    System.out.println("next ixon view clicked sub_cat_item sub_cat_id id *****  " + id);
                    try {
                        Intent intent = new Intent(view.getContext(), DemoProductList.class);
                        /*Intent intent = new Intent(view.getContext(), ProductList.class);*/
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        //Session.setselectedCategory(pref, sub_cat_name);
                        intent.putExtra("cat_name", sub_cat_name);
                        Session.set_selected_category_id(pref, id);
                        view.getContext().startActivity(intent);
                        //ToastMsg.showLongToast(context, "GROUP " + sub_cat_name);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        /*System.out.println("group viewchild 1111 *******  " + headerTitle + " counts  *****  " + getChildrenCount(groupPosition));*/
        /*Log.d("", "group viewchild 1111 *******  " + headerTitle + " counts  *****  " + getChildrenCount(groupPosition));*/

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
