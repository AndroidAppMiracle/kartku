package kartku.com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.AddToWishListModal;
import kartku.com.retrofit.modal.RemoveFromWishlistModal;
import kartku.com.search.model.SearchBean;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by satoti.garg on 9/23/2016.
 */
public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {

    ArrayList<SearchBean> arrayList = new ArrayList<>();
    Context mcontext;
    private AQuery aquery;
    SharedPreferences pref;
    String product_id, selected_productId = "";
    ;
    String user_id;
    int pos;
    SearchBean bean_obj;
    private Context alertContext;

    public SearchListAdapter(Context context, ArrayList<SearchBean> items) {
        this.mcontext = context;
        this.arrayList = items;
        aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        bean_obj = new SearchBean();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false);
        alertContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Log.d("", "banners list size onBindViewHolder *********" + arrayList);
        holder.text_name.setText(arrayList.get(position).getName());
        holder.text_price.setText(arrayList.get(position).getPrice());
        product_id = "" + arrayList.get(position).getId();
        aquery.id(holder.image_product).image(arrayList.get(position).getImage(), true, true, 0, 0, null, AQuery.FADE_IN);
        //  Picasso.with(context).load(arrayList.get(position).getImage_product()).into(holder.image_product);
//        Log.d("", "banners list size ViewHolder image_product *********" + arrayList.get(position).getImage());
       /* holder.mark_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected_productId = "" + arrayList.get(position).getId();
                pos = position;
                Log.d("", "mark_wishlist clicked ******   ");
                if (holder.mark_wishlist.getTag().equals(R.mipmap.like)) {
                    if (Session.get_login_statuc(pref)) {
                        holder.mark_wishlist.setImageResource(R.mipmap.fvlike);
                        holder.mark_wishlist.setTag(R.mipmap.fvlike);
                        addWishlist();
                    } else {
                        ToastMsg.showShortToast(context, context.getResources().getString(R.string.login_text));
                    }
                } else {
                    if (Session.get_login_statuc(pref)) {
                        holder.mark_wishlist.setImageResource(R.mipmap.like);
                        holder.mark_wishlist.setTag(R.mipmap.like);
                        removeWishlist();
                    } else {
                        ToastMsg.showShortToast(context, context.getResources().getString(R.string.login_text));
                    }
                }

            }
        });*/

        try {

            if (Session.get_login_statuc(pref)) {
                if (arrayList.get(position).getWishlist().equalsIgnoreCase("1")) {
                    //holder.mark_wishlist.setImageResource(R.mipmap.fvlike);
                    Glide.with(mcontext)
                            .load(R.drawable.ic_wishlist)
                            .override(30, 30)
                            .into(holder.mark_wishlist);
                    //holder.mark_wishlist.setTag(R.mipmap.fvlike);
                } else {
                    //holder.mark_wishlist.setImageResource(R.mipmap.like);
                    //holder.mark_wishlist.setTag(R.mipmap.like);
                    Glide.with(mcontext)
                            .load(R.drawable.ic_wishlist_not)
                            .override(30, 30)
                            .into(holder.mark_wishlist);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

     /*   holder.image_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(" add d product id ***** " + product_id);
                System.out.println(" arry add prod id ** " + arrayList.get(position).getId());
                Intent intent = new Intent(context, ProductDetail.class);
                intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, "" + arrayList.get(position).getId());
                *//*intent.putExtra(Constant.SELECTED_CATEGORY_INDEX,""+product_id);*//*
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });*/

        holder.ll_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mcontext, ProductDetailNew.class);
                i.putExtra(Constant.SELECTED_CATEGORY_INDEX, String.valueOf(arrayList.get(holder.getAdapterPosition()).getId()));
                i.putExtra(Constant.WISHLIST, arrayList.get(holder.getAdapterPosition()).getWishlist());
                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                mcontext.startActivity(i);
            }
        });


        holder.iv_extra_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //  ToastMsg.showLongToast(context, "Clicked");
                    String description = arrayList.get(holder.getAdapterPosition()).getDescription();
                    String brand = arrayList.get(holder.getAdapterPosition()).getBrand();
                    String material = arrayList.get(holder.getAdapterPosition()).getMaterial();
                    extraDetailsDialog(description, brand, material);
                    //ToastMsg.showLongToast(context, "Clicked2");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        if (arrayList.size() != 0) {
            return arrayList.size();
        } else {
            return 0;
        }


    }

    public class ViewHolder extends RecyclerView.ViewHolder implements ApiServerResponse {
        public TextView text_name, text_price;
        public ImageView mark_wishlist, image_product;
        LinearLayout ll_product;
        ImageView iv_extra_options;

        public ViewHolder(final View container) {
            super(container);
            text_name = (TextView) container.findViewById(R.id.text_name);
            text_price = (TextView) container.findViewById(R.id.text_price);
            mark_wishlist = (ImageView) container.findViewById(R.id.mark_wishlist);
            //mark_wishlist.setTag(R.mipmap.like);
            image_product = (ImageView) container.findViewById(R.id.image_product);
            ll_product = (LinearLayout) container.findViewById(R.id.ll_product);
            iv_extra_options = (ImageView) container.findViewById(R.id.iv_extra_options);

            mark_wishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String flag = arrayList.get(getAdapterPosition()).getWishlist();

                    if (Session.get_login_statuc(pref)) {
                        if (arrayList.get(getAdapterPosition()).getWishlist().equalsIgnoreCase("1")) {
                    /*ServerAPI.getInstance().removeFromWishlist(Api);*/
                            ((Utility) mcontext).showLoading();
                            ServerAPI.getInstance().removeFromWishlist(ApiServerResponse.REMOVE_FROM_WISHLIST, Session.get_userId(pref), String.valueOf(arrayList.get(getAdapterPosition()).getId()), ViewHolder.this);
                        } else {

                            ((Utility) mcontext).showLoading();
                            ServerAPI.getInstance().addToWishlist(ApiServerResponse.ADD_TO_WISHLIST, Session.get_userId(pref), String.valueOf(arrayList.get(getAdapterPosition()).getId()), ViewHolder.this);
                        }
                    } else {
                        ToastMsg.showShortToast(mcontext, mcontext.getString(R.string.login_to_continuew));
                    }


                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    AddToWishListModal addToWishListModal;
                    RemoveFromWishlistModal removeFromWishlistModal;

                    switch (tag) {
                        case ApiServerResponse.ADD_TO_WISHLIST:
                            addToWishListModal = (AddToWishListModal) response.body();
                            if (addToWishListModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                                arrayList.get(getAdapterPosition()).setWishlist("1");
                                Glide.with(mcontext)
                                        .load(R.drawable.ic_wishlist)
                                        .override(30, 30)
                                        .into(mark_wishlist);
                                ToastMsg.showShortToast(mcontext, addToWishListModal.getMessage());
                            } else {
                                ToastMsg.showShortToast(mcontext, addToWishListModal.getMessage());
                            }

                            ((Utility) mcontext).hideLoading();
                            break;
                        case ApiServerResponse.REMOVE_FROM_WISHLIST:
                            removeFromWishlistModal = (RemoveFromWishlistModal) response.body();

                            if (removeFromWishlistModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                                Glide.with(mcontext)
                                        .load(R.drawable.ic_wishlist_not)
                                        .override(30, 30)
                                        .into(mark_wishlist);
                                ToastMsg.showShortToast(mcontext, mcontext.getString(R.string.removed_from_wishlist));
                                arrayList.get(getAdapterPosition()).setWishlist("0");
                            } else {
                                ToastMsg.showShortToast(mcontext, removeFromWishlistModal.getMessage());
                            }
                            ((Utility) mcontext).hideLoading();
                            break;
                    }

                } else {
                    ((Utility) mcontext).hideLoading();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((Utility) mcontext).hideLoading();
        }
    }


    private void extraDetailsDialog(String description, String brand, String material) {
        final Dialog dialog = new Dialog(alertContext);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.title_activity_product_detail);
        dialog.setContentView(R.layout.product_list_more_options);

        EditText edittextDescription = (EditText) dialog.findViewById(R.id.et_description);
        edittextDescription.setText(description);

        TextView textViewBrand = (TextView) dialog.findViewById(R.id.text_brand);
        textViewBrand.setText(brand);

        TextView textViewMaterial = (TextView) dialog.findViewById(R.id.text_material);
        textViewMaterial.setText(material);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }

   /* private void addWishlist() {
        Log.d("", "addWishlist product_id *****  " + selected_productId);
        System.out.println("add to wishlist product id ****  " + selected_productId);

        new AddToWishlistManager(context, this).sendRequest(user_id, selected_productId);
    }

    private void removeWishlist() {
        Log.d("", "addWishlist product_id *****  " + selected_productId);
        System.out.println("add to wishlist product id ****  " + selected_productId);

        new RemoveFromWishlistManager(context, this).sendRequest(user_id, selected_productId);
    }

    @Override
    public void onAddwishlistSuccess(String response) {
        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(context, response);
        arrayList.set(pos, bean_obj).setWishlist("add");
    }

    @Override
    public void onAddwishlistError(String error) {
        ToastMsg.showShortToast(context, error);
    }

    @Override
    public void removewishlistOnSuccess(String response) {
        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(context, response);
        arrayList.set(pos, bean_obj).setWishlist("remove");
    }

    @Override
    public void removewishlistOnError(String error) {
        ToastMsg.showShortToast(context, error);
    }*/
}
