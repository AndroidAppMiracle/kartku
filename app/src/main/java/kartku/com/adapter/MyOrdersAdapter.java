package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import kartku.com.MyOrders.modal.MyOrderModal;
import kartku.com.R;
import kartku.com.activity.OrderDetail;
import kartku.com.utils.Constant;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 1/19/2017.
 */

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {


    private Context mContext;
    private List<MyOrderModal.ResultsBean> mItems;
    private SharedPreferences pref;
    private String user_id;
    private Resources resources;

    public MyOrdersAdapter(Context context, List<MyOrderModal.ResultsBean> items) {
        this.mContext = context;
        this.mItems = items;
        //aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        resources = mContext.getResources();
        //cart_id = Session.getcart_id(pref);
    }

    @Override
    public MyOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_my_orders, parent, false);
        return new MyOrdersAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.title_order_id.setText(String.format(resources.getString(R.string.two_parameters), "Order ID", mItems.get(position).getId()));
        holder.title_created_at.setText(String.format(resources.getString(R.string.two_parameters), mContext.getString(R.string.created_at), mItems.get(position).getCreated_at()));
        holder.title_grand_total.setText(String.format(resources.getString(R.string.two_parameters), mContext.getString(R.string.amount), mItems.get(position).getGrand_total()));
        holder.title_payment_status.setText(String.format(resources.getString(R.string.two_parameters), mContext.getString(R.string.payment_status), mItems.get(position).getStatus()));

        holder.ll_item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext, OrderDetail.class);
                    intent.putExtra(Constant.ORDER_ID, mItems.get(position).getId());
                    intent.putExtra(Constant.ORDER_STATUS, mItems.get(position).getStatus());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mItems.isEmpty()) {
            return 0;
        } else {
            return mItems.size();
        }

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title_order_id, title_created_at, title_grand_total, title_payment_status;
        public LinearLayout ll_item_layout;
        //public ImageView product_image, image_product, delete_product;
        // public EditText qtyText;
        //public Button add_btn, sub_btn;

        public ViewHolder(final View container) {
            super(container);
            title_order_id = (TextView) container.findViewById(R.id.title_order_id);
            title_created_at = (TextView) container.findViewById(R.id.title_created_at);
            title_grand_total = (TextView) container.findViewById(R.id.title_grand_total);
            title_payment_status = (TextView) container.findViewById(R.id.title_payment_status);
            ll_item_layout = (LinearLayout) container.findViewById(R.id.ll_item_layout);

        }
    }

}
