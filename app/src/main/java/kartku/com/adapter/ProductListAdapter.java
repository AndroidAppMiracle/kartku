package kartku.com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.product_category.model.ProductListModel;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import kartku.com.wishlist.listener.AddToWishlistListener;
import kartku.com.wishlist.listener.RemoveFromWishlistListener;
import kartku.com.wishlist.manager.AddToWishlistManager;
import kartku.com.wishlist.manager.RemoveFromWishlistManager;
import kartku.com.wishlist.model.WishlistBean;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> implements AddToWishlistListener, RemoveFromWishlistListener {

    ArrayList<ProductListModel> arrayList = new ArrayList<>();
    Context context;
    private AQuery aquery;
    SharedPreferences pref;
    String product_id = "", selected_productId = "";
    String user_id;
    ArrayList<WishlistBean> wishlist;
    int pos;
    ProductListModel product_model;
    boolean isInWishlist;
    Context alertContext;

    public ProductListAdapter(Context context, ArrayList<ProductListModel> items, ArrayList<WishlistBean> wishlist) {
        this.context = context;
        this.arrayList = items;
        this.wishlist = wishlist;
        aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        product_model = new ProductListModel();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false);
        alertContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Log.d("", "banners list size onBindViewHolder *********" + arrayList);
        holder.text_name.setText("" + arrayList.get(position).getName());
        holder.text_price.setText("Rs " + arrayList.get(position).getPrice());
        product_id = "" + arrayList.get(position).getId();

        //aquery.id(holder.image_product).image(arrayList.get(position).getImage_product(), true, true, 0, 0, null, AQuery.FADE_IN);

        //if (!arrayList.get(position).getImage_product().isEmpty() && arrayList.get(position).getImage_product() != null) {
        // Picasso.with(context).load(arrayList.get(position).getImage_product()).resize(150, 150).into(holder.image_product);
        if (arrayList.get(position).getImage_product() != null && !arrayList.get(position).getImage_product().isEmpty()) {
            Picasso.with(context)
                    .load(arrayList.get(position).getImage_product())
                    .placeholder(R.drawable.placeholder)   // optional
                    .error(R.drawable.placeholder)
                    .resize(100, 100)
                    .into((holder.image_product));
        } else {
            Picasso.with(context)
                    .load(R.drawable.placeholder)
                    //.placeholder(R.drawable.placeholder)   // optional
                    //.error(R.drawable.placeholder)
                    .resize(100, 100)
                    .into((holder.image_product));
        }


        /*Picasso.with(context)
                .load(arrayList.get(position).getImage_product())
                .placeholder(R.drawable.placeholder)   // optional
                .error(R.drawable.placeholder)
                .fit().centerCrop()
                .into((holder.image_product));*/
        //}

        /*else {
            Picasso.with(context)
                    .load(arrayList.get(position).getImage_product())
                    .placeholder(R.drawable.placeholder)   // optional
                    .error(R.drawable.placeholder)
                    .fit().centerCrop()
                    .into((holder.image_product));
        }*/

        /*Picasso.with(context).load((arrayList.get(position).getImage_product()).resize(100,100).into(imageView);*/

//        Log.d("", "banners list size ViewHolder image_product *********" + arrayList.get(position).getImage_product());


        /*for (int j =0 ; j<wishlist.size();j++)
        {
            String wishlist_id = wishlist.get(j).getId();

            Log.d("", "product list adapter wishist id *********" + wishlist_id  +  " product_id  " + product_id);

            if(product_id.equalsIgnoreCase(wishlist_id))
            {
                holder.mark_wishlist.setImageResource(R.mipmap.fvlike);
                holder.mark_wishlist.setTag(R.mipmap.fvlike);
            }else{
                holder.mark_wishlist.setImageResource(R.mipmap.like);
                holder.mark_wishlist.setTag(R.mipmap.like);
            }
        }*/

        holder.mark_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected_productId = "" + arrayList.get(holder.getAdapterPosition()).getId();

                pos = holder.getAdapterPosition();
//                Log.d("", "mark_wishlist clicked ******   " + selected_productId);
                if (holder.mark_wishlist.getTag().equals(R.drawable.ic_wishlist_not)) {
                    if (Session.get_login_statuc(pref)) {
                        holder.mark_wishlist.setImageResource(R.drawable.ic_wishlist);
                        holder.mark_wishlist.setTag(R.drawable.ic_wishlist);

                        if (Session.get_login_statuc(pref)) {
                            addWishlist();

                        } else {
                            ToastMsg.showShortToast(context, "Please login to continue.");
                        }


                    } else {
                        ToastMsg.showShortToast(context, context.getResources().getString(R.string.login_text));
                    }
                } else {
                    if (Session.get_login_statuc(pref)) {
                        holder.mark_wishlist.setImageResource(R.drawable.ic_wishlist_not);
                        holder.mark_wishlist.setTag(R.drawable.ic_wishlist_not);

                        if (Session.get_login_statuc(pref)) {
                            removeWishlist();
                        } else {
                            ToastMsg.showShortToast(context, "Please login to continue.");
                        }


                    } else {
                        ToastMsg.showShortToast(context, context.getResources().getString(R.string.login_text));
                    }
                }

            }
        });


        try {

            if (Session.get_login_statuc(pref)) {
                if (arrayList.get(position).getWishlist().equalsIgnoreCase("add")) {
                    holder.mark_wishlist.setImageResource(R.drawable.ic_wishlist);
                    holder.mark_wishlist.setTag(R.drawable.ic_wishlist);
                } else {
                    holder.mark_wishlist.setImageResource(R.drawable.ic_wishlist_not);
                    holder.mark_wishlist.setTag(R.drawable.ic_wishlist_not);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.image_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String productid = arrayList.get(position).getId();
                String productName = arrayList.get(position).getName();
                String productPrice = arrayList.get(position).getPrice();
                System.out.println(" add d product id ***** " + productid);

                Intent intent = new Intent(context, ProductDetailNew.class);
                /*Intent intent = new Intent(context, ProductDetail.class);*/

                intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, "" + productid);
                intent.putExtra(Constant.PRODUCT_NAME, "" + productName);
                intent.putExtra(Constant.PRODUCT_PRICE, "" + productPrice);
                if (holder.mark_wishlist.getTag().equals(R.drawable.ic_wishlist)) {
                    isInWishlist = true;
                } else {
                    isInWishlist = false;
                }
                intent.putExtra(Constant.WISHLIST, isInWishlist);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.iv_extra_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //  ToastMsg.showLongToast(context, "Clicked");
                    String description = arrayList.get(position).getDescription();
                    String brand = arrayList.get(position).getBrand();
                    String material = arrayList.get(position).getMaterial();
                    extraDetailsDialog(description, brand, material);
                    //ToastMsg.showLongToast(context, "Clicked2");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text_name, text_price;
        public ImageView mark_wishlist, image_product, iv_extra_options;

        public ViewHolder(final View container) {
            super(container);
            text_name = (TextView) container.findViewById(R.id.text_name);
            text_price = (TextView) container.findViewById(R.id.text_price);
            mark_wishlist = (ImageView) container.findViewById(R.id.mark_wishlist);
            mark_wishlist.setTag(R.drawable.ic_wishlist_not);
            image_product = (ImageView) container.findViewById(R.id.image_product);
            iv_extra_options = (ImageView) container.findViewById(R.id.iv_extra_options);
        }
    }


    private void addWishlist() {
//        Log.d("", "addWishlist product_id *****  " + selected_productId);
        System.out.println("add to wishlist product id ****  " + selected_productId);

        new AddToWishlistManager(context, this).sendRequest(user_id, selected_productId);
    }

    private void removeWishlist() {
//        Log.d("", "addWishlist product_id *****  " + selected_productId);
        System.out.println("add to wishlist product id ****  " + selected_productId);

        new RemoveFromWishlistManager(context, this).sendRequest(user_id, selected_productId);
    }

    @Override
    public void onAddwishlistSuccess(String response) {
//        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(context, response);
        arrayList.set(pos, product_model).setWishlist("add");
    }

    @Override
    public void onAddwishlistError(String error) {
        ToastMsg.showShortToast(context, error);
    }

    @Override
    public void removewishlistOnSuccess(String response) {
//        Log.d("", " onAddwishlistSuccess ****  " + response);
        ToastMsg.showShortToast(context, response);
        arrayList.set(pos, product_model).setWishlist("remove");
    }

    @Override
    public void removewishlistOnError(String error) {
        ToastMsg.showShortToast(context, error);
    }


    private void extraDetailsDialog(String description, String brand, String material) {
        final Dialog dialog = new Dialog(alertContext);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.title_activity_product_detail);
        dialog.setContentView(R.layout.product_list_more_options);

        EditText edittextDescription = (EditText) dialog.findViewById(R.id.et_description);
        edittextDescription.setText(description);

        TextView textViewBrand = (TextView) dialog.findViewById(R.id.text_brand);
        textViewBrand.setText(brand);

        TextView textViewMaterial = (TextView) dialog.findViewById(R.id.text_material);
        textViewMaterial.setText(material);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }


   /* public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(alertContext, v);
        popup.inflate(R.menu.actions);
        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.archive:
                        archive(item);
                        return true;
                    case R.id.delete:
                        delete(item);
                        return true;
                    default:
                        return false;
                }

            }
        });

        popup.show();
    }
*/




/*    private void extraDetailsDialogTwo(String description, String brand, String material) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.product_list_more_options, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view);

        TextView textViewDescription = (TextView) view.findViewById(R.id.text_description);
        textViewDescription.setText(description);

        TextView textViewBrand = (TextView) view.findViewById(R.id.text_brand);
        textViewBrand.setText(brand);

        TextView textViewMaterial = (TextView) view.findViewById(R.id.text_material);
        textViewMaterial.setText(material);
        // Add action buttons
                *//*.setPositiveButton(R.string.signin, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LoginDialogFragment.this.getDialog().cancel();
                    }
                });*//*
        *//*return builder.create();*//*
        AlertDialog alertDialogAndroid = builder.create();
        alertDialogAndroid.show();
    }*/

/*    private void method(String description, String brand, String material) {

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
        View mView = layoutInflaterAndroid.inflate(R.layout.product_list_more_options, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
        alertDialogBuilderUserInput.setView(mView);

        // final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        TextView textViewDescription = (TextView) mView.findViewById(R.id.text_description);
        textViewDescription.setText(description);

        TextView textViewBrand = (TextView) mView.findViewById(R.id.text_brand);
        textViewBrand.setText(brand);

        TextView textViewMaterial = (TextView) mView.findViewById(R.id.text_material);
        textViewMaterial.setText(material);

        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        // ToDo get user input here
                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();

    }*/
}

