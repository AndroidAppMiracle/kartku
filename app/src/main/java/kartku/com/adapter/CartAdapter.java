package kartku.com.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.cart.Cart;
import kartku.com.cart.Model.CartModel;
import kartku.com.cart.listener.DeleteItemListener;
import kartku.com.cart.manager.DeleteItemManager;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by tanuja.gupta on 9/27/2016.
 */
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> implements DeleteItemListener {

    ArrayList<CartModel> arrayList = new ArrayList<>();
    private Context context;
    private AQuery aquery;
    SharedPreferences pref;
    String product_id;
    private String user_id;
    private String cart_id;
    private int pos;
    TextView textViewMessage;

    public CartAdapter(Context context, ArrayList<CartModel> items, TextView textView) {
        this.context = context;
        this.arrayList = items;
        this.textViewMessage = textView;
        aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Log.d("", "banners list size onBindViewHolder *********" + arrayList);
        holder.text_name.setText("" + arrayList.get(position).getProductName());
        holder.text_desc.setText("" + arrayList.get(position).getProductDesc());
        holder.qtyText.setText("" + arrayList.get(position).getProductQuantity());

        // Log.i("quantTXT", "" + Double.parseDouble(holder.qtyText.getText().toString()));
        //Log.i("arralisPos", "" + Double.parseDouble(arrayList.get(position).getProductPrice()));

        //String newPrice = "" + String.valueOf(Double.parseDouble(arrayList.get(position).getProductQuantity()) * Double.parseDouble(arrayList.get(position).getProductPrice().trim()));
        try {
            double newPrice = (Double.parseDouble(arrayList.get(position).getProductQuantity().trim()) * Double.parseDouble(arrayList.get(position).getProductPrice().trim()));
            holder.text_price.setText("Price " + newPrice);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        /*holder.text_price.setText("" + arrayList.get(position).getProductPrice());*/

//        Log.i("Adapter log Image", "" + arrayList.get(position).getImagePath());
        //aquery.id(holder.product_image).image(arrayList.get(position).getImagePath(), true, true, 0, 0, null, AQuery.FADE_IN);
        Picasso.with(context).load(arrayList.get(position).getImagePath()).resize(100, 100).into(holder.product_image);

        //  Picasso.with(context).load(arrayList.get(position).getImage_product()).into(holder.image_product);
        // Log.d("", "banners list size ViewHolder image_product *********" + arrayList.get(position).getImagePath());

        holder.add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                    Log.d("", "qtyvalueee " + holder.qtyText.getText().toString().trim());
                    int count = Integer.parseInt("" + holder.qtyText.getText().toString().trim());

                    if (count != 1 && count > 1) {
                        String newVersion = "" + (Integer.parseInt(holder.qtyText.getText().toString()) - 1);
                        //count = count--;
                        holder.qtyText.setText(newVersion);
                        String newPrice = "" + (Double.parseDouble(newVersion.trim()) * Double.parseDouble(arrayList.get(position).getProductPrice()));
                        holder.text_price.setText("Price " + newPrice);

                        double price = Double.parseDouble(arrayList.get(position).getProductPrice());
                        double amount = price * Double.parseDouble(newVersion);
                        Cart.total_value = Cart.total_value - amount;
                        Cart.amount_text.setText("" + Cart.total_value);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.sub_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    String newVersion = "" + (Integer.parseInt(holder.qtyText.getText().toString()) + 1);
                    //count = count++;
                    holder.qtyText.setText(newVersion);
                    String newPrice = "" + (Double.parseDouble(newVersion.trim()) * Double.parseDouble(arrayList.get(position).getProductPrice()));
                    holder.text_price.setText("Price " + newPrice);
                    //int count = Integer.parseInt(holder.qtyText.getText().toString().trim());

                    double price = Double.parseDouble(arrayList.get(position).getProductPrice());
                    double amount = price * (Double.parseDouble(newVersion) - 1.0);
                    Cart.total_value = Cart.total_value + amount;
                    Cart.amount_text.setText("" + Cart.total_value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        holder.delete_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                deleteCart();

            }
        });
    }

    private void deleteCart() {
        String item_id = arrayList.get(pos).getItem_id();
        new DeleteItemManager(context, this).sendRequest(item_id);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text_name, text_price, text_desc;
        public ImageView product_image, image_product, delete_product;
        public EditText qtyText;
        public Button add_btn, sub_btn;

        public ViewHolder(final View container) {
            super(container);
            text_name = (TextView) container.findViewById(R.id.title_name);
            text_price = (TextView) container.findViewById(R.id.text_price);
            text_desc = (TextView) container.findViewById(R.id.text_desc);
            product_image = (ImageView) container.findViewById(R.id.product_image);
            qtyText = (EditText) container.findViewById(R.id.qty_text);
            add_btn = (Button) container.findViewById(R.id.add_qty);
            sub_btn = (Button) container.findViewById(R.id.sub_qty);
            delete_product = (ImageView) container.findViewById(R.id.deleteCart);
        }
    }


    @Override
    public void onDeleteItemError(String error) {


    }

    @Override
    public void onDeleteItemSuccess(String message) {
        try {
            JSONObject item = new JSONObject(message);
            String status = item.getString("status");
            if (status.equalsIgnoreCase("ok")) {
                String item_info = item.getString("item_info");
                ToastMsg.showShortToast(context, "" + item_info);
                double price = Double.parseDouble(arrayList.get(pos).getProductPrice());
                Cart.total_value = Cart.total_value - price;
                if (Cart.total_value == 0.0 || arrayList.size() == 0) {
                    Cart.amount_text.setVisibility(View.GONE);
                    Cart.shipping_text.setVisibility(View.GONE);
                    Cart.message_text.setVisibility(View.VISIBLE);
                    textViewMessage.setText("No items in cart");
                }
                Cart.amount_text.setText("Total Amount - Rs " + Cart.total_value);
                arrayList.remove(pos);
                notifyDataSetChanged();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
