package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kartku.com.R;
import kartku.com.orders_module.modal.OrderDetailsModal;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.utils.Constant;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 1/20/2017.
 */

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.ViewHolder> {


    private Context mContext;
    private List<OrderDetailsModal.OrderDetailBean.OrderItemsBean> mItems;
    private SharedPreferences pref;
    private String user_id;
    private Resources resources;

    public OrderDetailsAdapter(Context context, List<OrderDetailsModal.OrderDetailBean.OrderItemsBean> items) {
        this.mContext = context;
        this.mItems = items;
        //aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        resources = mContext.getResources();
        //cart_id = Session.getcart_id(pref);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_detail_product_item, parent, false);
        return new OrderDetailsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        try {

            if (mItems.get(position).getProduct_detail().getProduct_image() != null && !mItems.get(position).getProduct_detail().getProduct_image().isEmpty()) {
               /* Picasso.with(mContext)
                        .load(mItems.get(position).getProduct_detail().getProduct_image())
                        .placeholder(R.drawable.placeholder)   // optional
                        .error(R.drawable.placeholder)
                        .resize(64, 64)
                        .into((holder.iv_product_image));*/

                Glide.with(mContext)
                        .load(mItems.get(position).getProduct_detail().getProduct_image()).placeholder(R.drawable.placeholder).override(50, 50).thumbnail(0.1f)
                        .into(holder.iv_product_image);
            } else {
               /* Picasso.with(mContext)
                        .load(R.drawable.placeholder)
                        //.placeholder(R.drawable.placeholder)   // optional
                        //.error(R.drawable.placeholder)
                        .resize(64, 64)
                        .into((holder.iv_product_image));*/
                Glide.with(mContext)
                        .load(R.drawable.placeholder).placeholder(R.drawable.placeholder).override(50, 50).thumbnail(0.1f)
                        .into(holder.iv_product_image);
            }


            holder.tv_item_name.setText(mItems.get(position).getProduct_detail().getTitle());
            holder.tv_item_quantity.setText(String.format(resources.getString(R.string.two_parameters), mContext.getString(R.string.quantity), mItems.get(position).getQuantity()));
            holder.tv_item_price.setText(String.format(resources.getString(R.string.two_parameters), mContext.getString(R.string.price), mItems.get(position).getTotal_price()));
            holder.tv_item_shipping_status.setText(String.format(resources.getString(R.string.shipping_status), mItems.get(position).getShipping_status()));
            //holder.tv_item_price.setText(mItems.get(position).get);

            holder.ll_ordered_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        Intent intent = new Intent(mContext, ProductDetailNew.class);
                /*Intent intent = new Intent(context, ProductDetail.class);*/

                        String productID = String.valueOf(mItems.get(holder.getAdapterPosition()).getProduct_detail().getId());
                        intent.putExtra(Constant.SELECTED_CATEGORY_INDEX, productID);
                        //intent.putExtra(Constant.PRODUCT_NAME, "" + productName);
                        //intent.putExtra(Constant.PRODUCT_PRICE, "" + productPrice);
                        //intent.putExtra(Constant.WISHLIST, isInWishlist);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public int getItemCount() {
        if (mItems.isEmpty()) {
            return 0;
        } else {
            return mItems.size();
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_item_name, tv_item_quantity, tv_item_price, tv_item_shipping_status;
        public LinearLayout ll_ordered_product;
        public ImageView iv_product_image;

        //public ImageView product_image, image_product, delete_product;
        // public EditText qtyText;
        //public Button add_btn, sub_btn;

        public ViewHolder(final View container) {
            super(container);
            tv_item_name = (TextView) container.findViewById(R.id.tv_item_name);
            tv_item_quantity = (TextView) container.findViewById(R.id.tv_item_quantity);
            tv_item_price = (TextView) container.findViewById(R.id.tv_item_price);
            ll_ordered_product = (LinearLayout) container.findViewById(R.id.ll_ordered_product);
            iv_product_image = (ImageView) container.findViewById(R.id.iv_product_image);
            tv_item_shipping_status = (TextView) container.findViewById(R.id.tv_item_shipping_status);

        }
    }

}
