package kartku.com.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import kartku.com.R;
import kartku.com.my_product_reviews_module.MyProductReviewsModal;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitizb on 20-01-2017.
 */

public class MyProductReviewsAdapter extends RecyclerView.Adapter<MyProductReviewsAdapter.ViewHolder> {
    private Context mContext;
    private List<MyProductReviewsModal.ResultsBean> mItems;
    private SharedPreferences pref;
    //private String user_id;
    private Resources resources;

    public MyProductReviewsAdapter(Context context, List<MyProductReviewsModal.ResultsBean> items) {
        this.mContext = context;
        this.mItems = items;
        //aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        //user_id = Session.get_userId(pref);
        resources = mContext.getResources();
        //cart_id = Session.getcart_id(pref);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_my_product_reviews, parent, false);
        return new MyProductReviewsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            holder.title_my_reviews_title.setText(mItems.get(position).getProduct_title());
            holder.title_my_reviews_created_at.setText(mItems.get(position).getCreated_at());
            holder.et_my_product_review_given.setText(mItems.get(position).getReview());
            holder.rb_my_product_review_rating.setRating(Float.parseFloat(mItems.get(position).getRating()));

            if (mItems.get(position).getProduct_image() != null && !mItems.get(position).getProduct_image().isEmpty()) {
                Picasso.with(mContext)
                        .load((mItems.get(position).getProduct_image()))
                        .placeholder(R.drawable.placeholder)   // optional
                        .error(R.drawable.placeholder)
                        .resize(64, 64)
                        .into((holder.iv_product_image));
            } else {
                Picasso.with(mContext)
                        .load(R.drawable.placeholder)
                        //.placeholder(R.drawable.placeholder)   // optional
                        //.error(R.drawable.placeholder)
                        .resize(64, 64)
                        .into((holder.iv_product_image));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (mItems.isEmpty()) {
            return 0;
        } else {
            return mItems.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title_my_reviews_title, title_my_reviews_created_at;
        public TextView et_my_product_review_given;
        public ImageView iv_product_image;
        public RatingBar rb_my_product_review_rating;
        //public ImageView product_image, image_product, delete_product;
        // public EditText qtyText;
        //public Button add_btn, sub_btn;

        public ViewHolder(final View container) {
            super(container);
            et_my_product_review_given = (TextView) container.findViewById(R.id.et_my_product_review_given);
            title_my_reviews_title = (TextView) container.findViewById(R.id.title_my_reviews_title);
            title_my_reviews_created_at = (TextView) container.findViewById(R.id.title_my_reviews_created_at);
            rb_my_product_review_rating = (RatingBar) container.findViewById(R.id.rb_my_product_review_rating);
            iv_product_image = (ImageView) container.findViewById(R.id.iv_product_image);

        }
    }
}
