package kartku.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import kartku.com.R;

/**
 * Created by satoti.garg on 9/8/2016.
 */
public class HomeBannerAdapter extends RecyclerView.Adapter<HomeBannerAdapter.ViewHolder> {

    Context context;
    List<String> bannersList;

    public HomeBannerAdapter(Context context, List<String> bannersList) {
        this.context = context;
        this.bannersList = bannersList;
        /*Log.d("", "banners list size HomeBannerAdapter *********" + bannersList);*/
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        /*Log.d("", "banners list size onBindViewHolder *********" + bannersList);*/
        holder.text.setText("" + bannersList.get(position));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.banner_recycleview_rowview, parent);
        /*Log.d("", "banners list size onCreateViewHolder *********" + bannersList);*/
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {

        /*Log.d("","banners list size getItemCount *********"+bannersList.size());*/

        return bannersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public ImageView banner_image;

        public ViewHolder(final View view) {
            super(view);
            // text = (TextView)view.findViewById(R.id.textview);
            banner_image = (ImageView) view.findViewById(R.id.banner_img_view);
            /*Log.d("", "banners list size ViewHolder *********" + bannersList.size());*/

        }
    }
}
