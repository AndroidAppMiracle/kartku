package kartku.com.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import kartku.com.R;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.DeleteCartItemModal;
import kartku.com.retrofit.modal.EventBusCartUpdate;
import kartku.com.retrofit.modal.EventBusProductDetailUpdateModal;
import kartku.com.retrofit.modal.EventBusPromoCodeApplied;
import kartku.com.retrofit.modal.MyCartDetailModal;
import kartku.com.retrofit.modal.UpdateCartItemModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 1/23/2017.
 */

public class MyCartDetailsAdapter extends RecyclerView.Adapter<MyCartDetailsAdapter.ViewHolder> {

    private List<MyCartDetailModal.ItemsBean> mItemsList = new ArrayList<>();
    private List<MyCartDetailModal.ProductsBean> mProductsList = new ArrayList<>();
    private Context context;
    SharedPreferences pref;
    //private int pos;
    TextView textViewAmount;
    LinearLayout ll_bottomLayout;
    private Resources res;
    private Context alertContext;

    public MyCartDetailsAdapter(Context context, List<MyCartDetailModal.ItemsBean> itemsList, List<MyCartDetailModal.ProductsBean> productList, TextView textView, LinearLayout bottomLayout) {
        this.context = context;
        this.mItemsList = itemsList;
        this.mProductsList = productList;
        this.ll_bottomLayout = bottomLayout;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        this.textViewAmount = textView;
        res = context.getResources();

       /* user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);*/
    }


    @Override
    public MyCartDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_row, parent, false);
        alertContext = parent.getContext();
        return new MyCartDetailsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.title_name.setText(mProductsList.get(position).getProduct_name());


            holder.qtyText.setText(mItemsList.get(position).getQuantity());
            if (!mItemsList.get(holder.getAdapterPosition()).getColour().equalsIgnoreCase("")) {
                holder.tv_shipping_status.setText(mItemsList.get(holder.getAdapterPosition()).getColour());
            } else {
                holder.tv_shipping_status.setText(R.string.no_colour);
            }

            holder.tv_category.setText(mProductsList.get(holder.getAdapterPosition()).getCategory());
            holder.qtyText.setText(mItemsList.get(holder.getAdapterPosition()).getQuantity());
            if (!mProductsList.get(holder.getAdapterPosition()).getMax_quantity().equalsIgnoreCase("")) {
                String textStock = String.format(res.getString(R.string.left_in_stock_with_parameter), mProductsList.get(holder.getAdapterPosition()).getMax_quantity());
                holder.title_max_items_stock.setText(textStock);

                if (mItemsList.get(position).getQuantity().equalsIgnoreCase(mProductsList.get(holder.getAdapterPosition()).getMax_quantity()) &&
                        mItemsList.get(position).getQuantity().equalsIgnoreCase("1")) {
                    holder.add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                    holder.add_btn.setEnabled(false);
                    holder.sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                    holder.sub_btn.setEnabled(false);
                } else if (mItemsList.get(position).getQuantity().equalsIgnoreCase(mProductsList.get(holder.getAdapterPosition()).getMax_quantity()) &&
                        !mItemsList.get(position).getQuantity().equalsIgnoreCase("1")) {
                    holder.add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                    holder.add_btn.setEnabled(false);
                    holder.sub_btn.getBackground().setColorFilter(null);
                    holder.sub_btn.setEnabled(true);
                } else if (mItemsList.get(position).getQuantity().equalsIgnoreCase(mProductsList.get(holder.getAdapterPosition()).getMax_quantity()) || mItemsList.get(position).getQuantity().equalsIgnoreCase("1")) {

                    holder.sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                    holder.sub_btn.setEnabled(false);
                } else {
                    holder.add_btn.getBackground().setColorFilter(null);
                    holder.add_btn.setEnabled(true);
                    holder.sub_btn.getBackground().setColorFilter(null);
                    holder.sub_btn.setEnabled(true);

                }

            } else {

                holder.title_max_items_stock.setVisibility(View.GONE);
            }


            String textPrice = String.format(res.getString(R.string.two_parameters_options), "RP", String.format(Locale.getDefault(), "%.2f", Float.parseFloat(mItemsList.get(holder.getAdapterPosition()).getTotal_price())));

            holder.text_price.setText(textPrice);

            if (mProductsList.get(position).getProduct_image() != null && !mProductsList.get(position).getProduct_image().isEmpty()) {
                Picasso.with(context)
                        .load(mProductsList.get(position).getProduct_image())
                        .placeholder(R.drawable.placeholder)   // optional
                        .error(R.drawable.placeholder)
                        .resize(100, 100)
                        .into((holder.product_image));

               /* Glide.with(context)
                        .load(mProductsList.get(holder.getAdapterPosition()).getProduct_image()).placeholder(R.drawable.placeholder).thumbnail(0.1f).override(100, 100)
                        .into(holder.product_image);*/
            } else {
                Picasso.with(context)
                        .load(R.drawable.placeholder)
                        //.placeholder(R.drawable.placeholder)   // optional
                        //.error(R.drawable.placeholder)
                        .resize(100, 100)
                        .into((holder.product_image));

               /* Glide.with(context)
                        .load(R.drawable.placeholder).thumbnail(0.1f).override(100, 100)
                        .into(holder.product_image);*/

            }

          /*  holder.add_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   *//* try {

                        Log.d("", "qtyvalueee " + holder.qtyText.getText().toString().trim());
                        int count = Integer.parseInt("" + holder.qtyText.getText().toString().trim());

                        if (count != 1 && count > 1) {
                            String newVersion = "" + (Integer.parseInt(holder.qtyText.getText().toString()) - 1);
                            //count = count--;
                            holder.qtyText.setText(newVersion);

                            ((Utility) context).showLoading();
                            // ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, String.valueOf(mProductsList.get(position).getPid()), newVersion, Session.get_userId(pref), Session.getcart_id(pref), MyCartDetailsAdapter.this);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }*//*
                }
            });*/

         /*   holder.sub_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    *//*try {
                        String newVersion = "" + (Integer.parseInt(holder.qtyText.getText().toString()) + 1);
                        //count = count++;
                        holder.qtyText.setText(newVersion);
                        int productID = mProductsList.get(holder.getAdapterPosition()).getPid();
                        ((Utility) context).showLoading();
                        //ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, String.valueOf(productID), newVersion, Session.get_userId(pref), Session.getcart_id(pref), MyCartDetailsAdapter.this);
                        holder.text_price.setText(mProductsList.get(holder.getAdapterPosition()).getPrice());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*//*
                }
            });
*/
          /*  holder.delteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   *//* ((Utility) context).showLoading();
                    int pos = position;
                    deleteCart(pos);*//*


                }
            });*/


            holder.product_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, ProductDetailNew.class);
                        i.putExtra(Constant.SELECTED_CATEGORY_INDEX, String.valueOf(mProductsList.get(holder.getAdapterPosition()).getPid()));
                        //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (mItemsList.isEmpty()) {
            return 0;
        } else {
            return mItemsList.size();
        }
    }

    public int getProductItemCount() {
        if (mProductsList.isEmpty()) {
            return 0;
        } else {
            return mProductsList.size();
        }
    }

/*    @Override
    public void onSuccess(int tag, Response response) {
        try {
            ((Utility) context).hideLoading();
            if (response.isSuccessful()) {
                AddToCartNewModal addToCartNewModal;

                switch (tag) {
                    case ApiServerResponse.ADD_TO_CART_NEW:
                        addToCartNewModal = (AddToCartNewModal) response.body();
                        if (addToCartNewModal.getStatus().equals("OK")) {
                            ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);
                            ToastMsg.showLongToast(context, addToCartNewModal.getResult());

                        } else {
                            ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);
                            ToastMsg.showLongToast(context, addToCartNewModal.getResult());
                        }
                        break;
                    case ApiServerResponse.MY_CART_DETAILS:
                        MyCartDetailModal myCartDetailModal = (MyCartDetailModal) response.body();
                        Resources res = context.getResources();

                        if (myCartDetailModal.getStatus().equals("OK")) {

                            mItemsList = myCartDetailModal.getItems();
                            mProductsList = myCartDetailModal.getProducts();

                            Log.i("itemSize", "" + myCartDetailModal.getItems().size());
                            Log.i("productlistSize", "" + myCartDetailModal.getProducts().size());
                            MyCartDetailsAdapter as = new MyCartDetailsAdapter(null, mItemsList, mProductsList, null, null);
                            notifyDataSetChanged();
                            String text = String.format(res.getString(R.string.two_parameters), "Total Amount", myCartDetailModal.getCart_info().getTotal_amount());
                            textViewAmount.setText(text);
                        }
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/
/*
    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            ((Utility) context).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case ApiServerResponse.ADD_TO_CART_NEW:
                    System.out.println("Error");
                    break;
                case ApiServerResponse.MY_CART_DETAILS:
                    System.out.println("Error");
                    break;

                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

/*    @Override
    public void onDeleteItemSuccess(String message) {
        try {
            JSONObject item = new JSONObject(message);
            String status = item.getString("status");
            if (status.equalsIgnoreCase("ok")) {
                String item_info = item.getString("item_info");
                ToastMsg.showShortToast(context, "" + item_info);
                mProductsList.remove(pos);
                mItemsList.remove(pos);
                notifyDataSetChanged();
                if (mProductsList.isEmpty() && mItemsList.isEmpty()) {
                    textViewAmount.setVisibility(View.VISIBLE);
                    ll_bottomLayout.setVisibility(View.GONE);
                    textViewAmount.setText("No items in the cart.");
                } else {
                    ll_bottomLayout.setVisibility(View.VISIBLE);
                    textViewAmount.setVisibility(View.GONE);
                }

                ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteItemError(String error) {
        Log.i("ERROR", error);
    }*/


    public class ViewHolder extends RecyclerView.ViewHolder implements ApiServerResponse {
        public TextView title_name, text_price, tv_shipping_status, tv_category, title_max_items_stock;
        public ImageView product_image, delteItem, updateItem;
        public EditText qtyText;
        public Button add_btn, sub_btn;
        LinearLayout ll_cart_item;

        public ViewHolder(final View container) {
            super(container);
            title_name = (TextView) container.findViewById(R.id.title_name);
            text_price = (TextView) container.findViewById(R.id.text_price);
            product_image = (ImageView) container.findViewById(R.id.product_image);
            qtyText = (EditText) container.findViewById(R.id.qty_text);
            add_btn = (Button) container.findViewById(R.id.add_qty);
            sub_btn = (Button) container.findViewById(R.id.sub_qty);
            delteItem = (ImageView) container.findViewById(R.id.delteItem);

            updateItem = (ImageView) container.findViewById(R.id.updateItem);
            tv_shipping_status = (TextView) container.findViewById(R.id.tv_shipping_status);
            tv_category = (TextView) container.findViewById(R.id.tv_category);
            title_max_items_stock = (TextView) container.findViewById(R.id.title_max_items_stock);
            ll_cart_item = (LinearLayout) container.findViewById(R.id.ll_cart_item);


            delteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ((Utility) context).showLoading();
                        //EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));
                        ServerAPI.getInstance().deleteCartItem(ApiServerResponse.DELETE_CART_ITEM, Session.get_userId(pref), String.valueOf(mItemsList.get(getAdapterPosition()).getId()), ViewHolder.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            add_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {


                        String count = qtyText.getText().toString();
                        count = String.valueOf(Integer.parseInt(count) + 1);
                        if (count.equalsIgnoreCase(mProductsList.get(getAdapterPosition()).getMax_quantity())) {
                            add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                            add_btn.setEnabled(false);
                            qtyText.setText(count);
                            sub_btn.getBackground().setColorFilter(null);
                            sub_btn.setEnabled(true);
                            //count = String.valueOf(Integer.parseInt(count) - 1);
                            ((Utility) context).showLoading();
                            //EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));
                            add_btn.setEnabled(false);
                            sub_btn.setEnabled(false);
                            ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, Session.get_userId(pref), String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), qtyText.getText().toString(), String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), ViewHolder.this);
                        } else {
                            add_btn.getBackground().setColorFilter(null);
                            add_btn.setEnabled(true);
                            sub_btn.getBackground().setColorFilter(null);
                            sub_btn.setEnabled(true);
                            qtyText.setText(count);
                            ((Utility) context).showLoading();
                            add_btn.setEnabled(false);
                            sub_btn.setEnabled(false);
                            // EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));
                            ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, Session.get_userId(pref), String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), qtyText.getText().toString(), String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), ViewHolder.this);
                        }


                        /*if (count.equalsIgnoreCase(mProductsList.get(getAdapterPosition()).getMax_quantity())) {

                            add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                            add_btn.setEnabled(false);
                        } else {
                            add_btn.getBackground().setColorFilter(null);
                            count = String.valueOf(Integer.parseInt(count) + 1);
                            qtyText.setText(count);
                            add_btn.setEnabled(true);

                            if (qtyText.getText().toString().equalsIgnoreCase(mProductsList.get(getAdapterPosition()).getMax_quantity())) {
                                add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                add_btn.setEnabled(false);
                            } else {
                                add_btn.getBackground().setColorFilter(null);
                                add_btn.setEnabled(true);
                            }
                            //((Utility) context).showLoading();
                            //EventBus.getDefault().post(new EventBusProductDetailUpdateModal(mProductsList.get(getAdapterPosition()).getPid()));
                            //ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), count, ViewHolder.this);

                        }*/


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            sub_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        String count = qtyText.getText().toString();
                        count = String.valueOf(Integer.parseInt(count) - 1);
                        if (count.equalsIgnoreCase("1")) {
                            sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                            sub_btn.setEnabled(false);
                            qtyText.setText(count);
                            add_btn.getBackground().setColorFilter(null);
                            add_btn.setEnabled(true);
                            add_btn.getBackground().setColorFilter(null);
                            add_btn.setEnabled(true);
                            ((Utility) context).showLoading();
                            //EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));

                            add_btn.setEnabled(false);
                            sub_btn.setEnabled(false);
                            ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, Session.get_userId(pref), String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), qtyText.getText().toString(), String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), ViewHolder.this);

                        } else if (count.equalsIgnoreCase("0")) {
                            qtyText.setText("1");
                            sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                            sub_btn.setEnabled(false);
                            add_btn.getBackground().setColorFilter(null);
                            add_btn.setEnabled(true);
                            add_btn.getBackground().setColorFilter(null);
                            add_btn.setEnabled(true);
                        } else {
                            add_btn.getBackground().setColorFilter(null);
                            add_btn.setEnabled(true);
                            sub_btn.getBackground().setColorFilter(null);
                            //count = String.valueOf(Integer.parseInt(count) - 1);
                            qtyText.setText(count);
                            sub_btn.setEnabled(true);

                            //((Utility) context).showLoading();
                            ((Utility) context).showLoading();
                            //EventBus.getDefault().post(new EventBusProductDetailUpdateModal(1));
                            add_btn.setEnabled(false);
                            sub_btn.setEnabled(false);
                            ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, Session.get_userId(pref), String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), qtyText.getText().toString(), String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), ViewHolder.this);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });


            qtyText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {





                       /* CharSequence[] testArray = new CharSequence[50];
                        for (int i = 0; i < testArray.length; i++) {
                            testArray[i] = String.valueOf(i);
                        }*/

                        // final ArrayAdapter<String> quantity = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);

                        ArrayList<String> list = new ArrayList<String>();
                        for (int i = 1; i <= Integer.valueOf(mProductsList.get(getAdapterPosition()).getMax_quantity()); i++) {
                            //quantity.add(String.valueOf(i));
                            list.add(String.valueOf(i));
                        }


                        final CharSequence[] quantity = list.toArray(new String[list.size()]);

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(alertContext);
                        dialogBuilder.setTitle(R.string.quantity);
                        dialogBuilder.setCancelable(true);
                        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                String selectedText = quantity[item].toString();  //Selected item in listview
                                //ToastMsg.showShortToast(context, selectedText);

                                if (!selectedText.equalsIgnoreCase(qtyText.getText().toString())) {
                                    ((Utility) context).showLoading();
                                    add_btn.setEnabled(false);
                                    sub_btn.setEnabled(false);
                                    ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, Session.get_userId(pref), String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), selectedText, String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), ViewHolder.this);
                                }

                                qtyText.setText(selectedText);


                                if (mItemsList.get(getAdapterPosition()).getQuantity().equalsIgnoreCase(mProductsList.get(getAdapterPosition()).getMax_quantity()) &&
                                        mItemsList.get(getAdapterPosition()).getQuantity().equalsIgnoreCase("1")) {
                                    add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    add_btn.setEnabled(false);
                                    sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    sub_btn.setEnabled(false);
                                } else if (selectedText.equalsIgnoreCase(mProductsList.get(getAdapterPosition()).getMax_quantity())) {
                                    add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    add_btn.setEnabled(false);
                                    sub_btn.getBackground().setColorFilter(null);
                                    sub_btn.setEnabled(true);
                                } else if (selectedText.equalsIgnoreCase("1")) {
                                    sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    sub_btn.setEnabled(false);
                                    add_btn.getBackground().setColorFilter(null);
                                    add_btn.setEnabled(true);
                                } else {
                                    add_btn.getBackground().setColorFilter(null);
                                    add_btn.setEnabled(true);
                                    sub_btn.getBackground().setColorFilter(null);
                                    sub_btn.setEnabled(true);
                                }
                            }
                        });
                        //Create alert dialog object via builder
                        AlertDialog alertDialogObject = dialogBuilder.create();
                        //Show the dialog
                        alertDialogObject.show();

                        //showDialog(list);

/*

                        final Dialog dialog = new Dialog(alertContext);

                        //View view =  context.inflate(R.layout.custom_dialog_layout_quantity, null);
                        dialog.setContentView(R.layout.custom_dialog_layout_quantity);
                        dialog.setTitle("Quantity");
                        dialog.setCancelable(true);

                        ListView lv = (ListView) dialog.findViewById(R.id.custom_list);
                        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);

                        // Change MyActivity.this and myListOfItems to your own values
                        CustomAlertDialogAdapter clad = new CustomAlertDialogAdapter(context, list);

                        lv.setAdapter(clad);

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                try {
                                    String strName = adapterView.getAdapter().getItem(i).toString();

                                    if (!strName.equalsIgnoreCase(qtyText.getText().toString())) {
                                        ((Utility) context).showLoading();
                                        ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), strName, String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), ViewHolder.this);
                                    }

                                    qtyText.setText(strName);


                                    if (strName.equalsIgnoreCase(mProductsList.get(getAdapterPosition()).getMax_quantity())) {
                                        add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                        add_btn.setEnabled(false);
                                        sub_btn.getBackground().setColorFilter(null);
                                        sub_btn.setEnabled(true);
                                    } else if (strName.equalsIgnoreCase("1")) {
                                        sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                        sub_btn.setEnabled(false);
                                        add_btn.getBackground().setColorFilter(null);
                                        add_btn.setEnabled(true);
                                    } else {
                                        add_btn.getBackground().setColorFilter(null);
                                        add_btn.setEnabled(true);
                                        sub_btn.getBackground().setColorFilter(null);
                                        sub_btn.setEnabled(true);
                                    }


                                    /*/
/*EventBus.getDefault().post(new EventBusProductDetailUpdateModal(mProductsList.get(getAdapterPosition()).getPid()));*//*
*/
/*


                                    dialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }
                        });

                        text_Cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });


                        dialog.show();
*/

               /*         AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                        //builderSingle.setIcon(R.drawable.ic_launcher);
                        //builderSingle.setTitle("Select One Name:-");


                        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        builderSingle.setAdapter(quantity, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String strName = quantity.getItem(which);
                                qtyText.setText(strName);


                                if (strName.equalsIgnoreCase(mProductsList.get(getAdapterPosition()).getMax_quantity())) {
                                    add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    add_btn.setEnabled(false);
                                    sub_btn.getBackground().setColorFilter(null);
                                    sub_btn.setEnabled(true);
                                } else if (strName.equalsIgnoreCase("1")) {
                                    sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    sub_btn.setEnabled(false);
                                    add_btn.getBackground().setColorFilter(null);
                                    add_btn.setEnabled(true);
                                } else {
                                    add_btn.getBackground().setColorFilter(null);
                                    add_btn.setEnabled(true);
                                    sub_btn.getBackground().setColorFilter(null);
                                    sub_btn.setEnabled(true);
                                }


                                ((Utility) context).showLoading();
                                *//*EventBus.getDefault().post(new EventBusProductDetailUpdateModal(mProductsList.get(getAdapterPosition()).getPid()));*//*
                                ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), strName, String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), ViewHolder.this);

                   *//* AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                    builderInner.setMessage(strName);
                    builderInner.setTitle("Your Selected Item is");
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();*//*
                            }
                        });
                        builderSingle.show();*/


                        // quantityList(quantity, qtyText, getAdapterPosition(),ViewHolder.this );


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                if (response.isSuccessful()) {

                    DeleteCartItemModal deleteCartItemModal;
                    UpdateCartItemModal updateCartItemModal;
                    MyCartDetailModal myCartDetailModal;
                    switch (tag) {

                        case ApiServerResponse.DELETE_CART_ITEM:

                            deleteCartItemModal = (DeleteCartItemModal) response.body();

                            if (deleteCartItemModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                                EventBus.getDefault().post(new EventBusPromoCodeApplied(Constant.OK));
                                //ToastMsg.showShortToast(context, deleteCartItemModal.getItem_info());
                                refresh(getAdapterPosition());
                                EventBus.getDefault().post(new EventBusProductDetailUpdateModal(String.valueOf(mItemsList.size())));
                                EventBus.getDefault().post(new EventBusCartUpdate(String.valueOf(mItemsList.size())));
                                if (mItemsList.size() == 0 && mProductsList.size() == 0) {
                                    ll_bottomLayout.setVisibility(View.GONE);
                                    textViewAmount.setVisibility(View.VISIBLE);
                                    textViewAmount.setText(R.string.no_items_in_cart);

                                } else {


                                    ll_bottomLayout.setVisibility(View.VISIBLE);
                                    textViewAmount.setVisibility(View.VISIBLE);
                                    String text = String.format(res.getString(R.string.total_price_in_RP), deleteCartItemModal.getCart_info().getTotal_amount());
                                    textViewAmount.setText(text);
                                    //textViewAmount.setText(deleteCartItemModal.getCart_info().getTotal_amount());


                                    //textViewAmount.setText("No items in the cart.");
                                }
                            } else {
                                ToastMsg.showShortToast(context, deleteCartItemModal.getItem_info());
                            }

                            ((Utility) context).hideLoading();
                            //ServerAPI.getInstance().getMyCartDetails(ApiServerResponse.MY_CART_DETAILS, Session.getcart_id(pref), this);
                            break;
                        case ApiServerResponse.UPDATE_CART_ITEM:
                            updateCartItemModal = (UpdateCartItemModal) response.body();
                            if (updateCartItemModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                                EventBus.getDefault().post(new EventBusPromoCodeApplied(Constant.OK));

                                String text = String.format(res.getString(R.string.total_price_in_RP), updateCartItemModal.getCart_info().getTotal_amount());
                                textViewAmount.setText(text);

                                String textPrice = String.format(res.getString(R.string.price_in_RP), String.format(Locale.getDefault(), "%.2f", Float.parseFloat(updateCartItemModal.getItems().get(getAdapterPosition()).getTotal_price())));
                                text_price.setText(textPrice);
                                qtyText.setText(updateCartItemModal.getItems().get(getAdapterPosition()).getQuantity());

                                if (updateCartItemModal.getItems().get(getAdapterPosition()).getQuantity().equalsIgnoreCase(updateCartItemModal.getProducts().get(getAdapterPosition()).getMax_quantity())) {

                                    add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    add_btn.setEnabled(false);
                                    sub_btn.getBackground().setColorFilter(null);
                                    sub_btn.setEnabled(true);

                                } else if (updateCartItemModal.getItems().get(getAdapterPosition()).getQuantity().equalsIgnoreCase("1")) {
                                    sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    sub_btn.setEnabled(false);
                                    add_btn.getBackground().setColorFilter(null);
                                    add_btn.setEnabled(true);
                                } else {
                                    add_btn.getBackground().setColorFilter(null);
                                    add_btn.setEnabled(true);
                                    sub_btn.getBackground().setColorFilter(null);
                                    sub_btn.setEnabled(true);
                                }

                                /*add_btn.setEnabled(true);
                                sub_btn.setEnabled(false);*/
                            }

                            ((Utility) context).hideLoading();
                            break;
                        case ApiServerResponse.MY_CART_DETAILS:
                            myCartDetailModal = (MyCartDetailModal) response.body();

                            EventBus.getDefault().post(new EventBusProductDetailUpdateModal(String.valueOf(myCartDetailModal.getItems().size())));
                            EventBus.getDefault().post(new EventBusCartUpdate(String.valueOf(myCartDetailModal.getItems().size())));

                            break;

                    }

                } else {
                    ((Utility) context).hideLoading();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((Utility) context).hideLoading();
        }
    }


    public void refresh(int position) {
        /*Log.e("Refreshed Position", "Pos" + position);*/
        mItemsList.remove(position);
        mProductsList.remove(position);
        notifyItemRemoved(position);
    }
   /* private void deleteCart(int position) {
        pos = position;
        String item_id = String.valueOf(mItemsList.get(position).getId());
        new DeleteItemManager(context, this).sendRequest(item_id);
    }*/


   /* private void quantityList(final ArrayAdapter<String> arrayAdapter, final EditText editText, final int adapterPosition, final ViewHolder viewHolder) {

        try {

            AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
            //builderSingle.setIcon(R.drawable.ic_launcher);
            //builderSingle.setTitle("Select One Name:-");


            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                    editText.setText(strName);


                    ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, String.valueOf(mItemsList.get(adapterPosition).getCart_id()), strName, String.valueOf(mProductsList.get(adapterPosition).getPid()), viewHolder);

                   *//* AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                    builderInner.setMessage(strName);
                    builderInner.setTitle("Your Selected Item is");
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();*//*
                }
            });
            builderSingle.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/


    /*private void showDialog(ArrayList<String> arrayList) {

        final Dialog dialog = new Dialog(alertContext);

        //View view =  context.inflate(R.layout.custom_dialog_layout_quantity, null);
        dialog.setContentView(R.layout.custom_dialog_layout_quantity);
        dialog.setTitle("Quantity");
        dialog.setCancelable(true);

        ListView lv = (ListView) dialog.findViewById(R.id.custom_list);
        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);

        // Change MyActivity.this and myListOfItems to your own values
        CustomAlertDialogAdapter clad = new CustomAlertDialogAdapter(context, arrayList);

        lv.setAdapter(clad);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ToastMsg.showShortToast(context, adapterView.getAdapter().getItem(i).toString());
                try {
                    String strName = adapterView.getAdapter().getItem(i).toString();
                    qtyText.setText(strName);


                    if (strName.equalsIgnoreCase(mProductsList.get(getAdapterPosition()).getMax_quantity())) {
                        add_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                        add_btn.setEnabled(false);
                        sub_btn.getBackground().setColorFilter(null);
                        sub_btn.setEnabled(true);
                    } else if (strName.equalsIgnoreCase("1")) {
                        sub_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                        sub_btn.setEnabled(false);
                        add_btn.getBackground().setColorFilter(null);
                        add_btn.setEnabled(true);
                    } else {
                        add_btn.getBackground().setColorFilter(null);
                        add_btn.setEnabled(true);
                        sub_btn.getBackground().setColorFilter(null);
                        sub_btn.setEnabled(true);
                    }


                    ((Utility) context).showLoading();
                                *//**//*EventBus.getDefault().post(new EventBusProductDetailUpdateModal(mProductsList.get(getAdapterPosition()).getPid()));*//**//*
                    ServerAPI.getInstance().updateCartItem(ApiServerResponse.UPDATE_CART_ITEM, String.valueOf(mItemsList.get(getAdapterPosition()).getCart_id()), strName, String.valueOf(mProductsList.get(getAdapterPosition()).getPid()), ViewHolder.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }*/
}
