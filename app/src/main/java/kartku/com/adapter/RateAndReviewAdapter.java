package kartku.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.rate_review.model.RateAndReviewModel;

/**
 * Created by Kshitiz Bali on 12/30/2016.
 */

public class RateAndReviewAdapter extends RecyclerView.Adapter<RateAndReviewAdapter.ViewHolder> {

    private List<RateAndReviewModel.ReviewsBean> arrayListReviewsBean = new ArrayList<>();
    private Context context;
    //private AQuery aquery;
    //SharedPreferences pref;

    //int pos;
    // RateAndReviewModel review_model;
    /*boolean isInWishlist;*/

    public RateAndReviewAdapter(Context context, List<RateAndReviewModel.ReviewsBean> itemsReviews) {
        this.context = context;
        this.arrayListReviewsBean = itemsReviews;
        /*this.arrayListUserInfo = itemUserInfo;*/
        //this.wishlist = wishlist;
        //aquery = new AQuery(context);
        //pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        //user_id = Session.get_userId(pref);
        //review_model = new RateAndReviewModel();
    }

    @Override
    public RateAndReviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviews_adapter_layout_ll, parent, false);
       /*ViewHolder rootView = new ViewHolder(v);
        return rootView;*/
        return new RateAndReviewAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Log.d("", "Review List*********" + arrayListReviewsBean);
        /*holder.text_name.setText("" + arrayList.get(position).getName());
        holder.text_price.setText("Rs " + arrayList.get(position).getPrice());
        product_id = "" + arrayList.get(position).getId();
        aquery.id(holder.image_product).image(arrayList.get(position).getImage_product(), true, true, 0, 0, null, AQuery.FADE_IN);*/
        try {

            if (arrayListReviewsBean.get(position).getUser_info().getFirst_name().isEmpty() || arrayListReviewsBean.get(position).getUser_info().getFirst_name().equals("")) {
                holder.tv_firstName.setText(R.string.anonymous);
                holder.tv_last_name.setText(R.string.user);
            } else {
                holder.tv_firstName.setText(arrayListReviewsBean.get(position).getUser_info().getFirst_name());
                holder.tv_last_name.setText(arrayListReviewsBean.get(position).getUser_info().getLast_name());
            }

            if (!arrayListReviewsBean.get(position).getRating().equalsIgnoreCase("")) {

                holder.ratingReviews.setRating(Float.parseFloat(arrayListReviewsBean.get(position).getRating()));
            }


            holder.tv_review_body.setText(arrayListReviewsBean.get(position).getReview());
            holder.tv_timestamp.setText(arrayListReviewsBean.get(position).getCreated_at());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        if (arrayListReviewsBean.size() != 0) {
            return arrayListReviewsBean.size();
        } else {
            return 0;
        }

    }

    /*public int getItemCountUserInfo() {
        return arrayListUserInfo.size();
    }
*/

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_firstName, tv_last_name, tv_timestamp, tv_review_body;
        public RatingBar ratingReviews;

        public ViewHolder(final View container) {
            super(container);
            tv_firstName = (TextView) container.findViewById(R.id.tv_firstName);
            tv_last_name = (TextView) container.findViewById(R.id.tv_last_name);
            tv_timestamp = (TextView) container.findViewById(R.id.tv_timestamp);
            tv_review_body = (TextView) container.findViewById(R.id.tv_review_body);
            ratingReviews = (RatingBar) container.findViewById(R.id.ratingReviews);


        }
    }
}
