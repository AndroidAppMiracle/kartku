package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kartku.com.R;
import kartku.com.activity.CategoryProducts;
import kartku.com.activity.SubSubProductCategoryDetail;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.SubCategoryModel;
import kartku.com.retrofit.modal.SubProductCategoryListModal;
import kartku.com.utils.Constant;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 1/12/2017.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SubCategoryModel> subCategoryList = new ArrayList<SubCategoryModel>();
    //private LinkedHashMap<String, String> listSubSubCategory = new LinkedHashMap<String, String>();
    private SharedPreferences pref;

    public SubCategoryAdapter(Context mContext, ArrayList<SubCategoryModel> mSubCategoryList) {
        this.context = mContext;
        this.subCategoryList = mSubCategoryList;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        // user_id = Session.get_userId(pref);
        //product_model = new ProductListModel();
    }


    @Override
    public SubCategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product_category, parent, false);
        return new SubCategoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SubCategoryAdapter.ViewHolder holder, final int position) {

        try {
//            Log.d("", "name List*********" + subCategoryList.get(position).getSub_subcat_name());
//            Log.d("", "id Info***" + subCategoryList.get(position).getSub_subcat_id());
            holder.lblListItem.setText(subCategoryList.get(position).getSub_subcat_name());
           /* for (int i = 0; i <subCategoryList.size(); i++){

            }*/

            /*holder.rl_category_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ToastMsg.showLongToast(context, subCategoryList.get(position).getSub_subcat_name());
                    //ToastMsg.showLongToast(context, subCategoryList.get(position).getSub_subcat_id());

                    Session.set_selected_category_id(pref, subCategoryList.get(position).getSub_subcat_id());
                    Intent intent = new Intent(context, DemoProductList.class);
                *//*Intent intent = new Intent(context, ProductDetail.class);*//*
                    intent.putExtra(ResponseKeys.SUB_SUB_CAT_NAME, subCategoryList.get(position).getSub_subcat_name());
                    intent.putExtra(ResponseKeys.SUB_SUB_CAT_ID, subCategoryList.get(position).getSub_subcat_id());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(intent);
                }
            });
*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return subCategoryList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements ApiServerResponse {
        public TextView lblListItem;
        public RelativeLayout rl_category_item;

        public ViewHolder(final View container) {
            super(container);
            lblListItem = (TextView) container.findViewById(R.id.lblListItem);
            rl_category_item = (RelativeLayout) container.findViewById(R.id.rl_category_item);


            rl_category_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ServerAPI.getInstance().getSubProductCategoryDetails(ApiServerResponse.SUB_PRODUCT_CATEGORY, Session.get_userId(pref), subCategoryList.get(getAdapterPosition()).getSub_subcat_id(), ViewHolder.this);
                    //ServerAPI.getInstance().getProductCategoryDetails(ApiServerResponse.PRODUCT_CATEGORY, , ViewHolder.this);
                }
            });

        }

        @Override
        public void onSuccess(int tag, Response response) {

            if (response.isSuccessful()) {
                SubProductCategoryListModal subProductCategoryListModal;

                switch (tag) {

                    case ApiServerResponse.SUB_PRODUCT_CATEGORY:

                        subProductCategoryListModal = (SubProductCategoryListModal) response.body();

                        if (subProductCategoryListModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            if (subProductCategoryListModal.getSubcategory_detail().getIs_subcategory().equalsIgnoreCase("1")) {
                                //has SubCategory
                                Intent i = new Intent(context, SubSubProductCategoryDetail.class);
                                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                                i.putExtra(Constant.CATEGORY_ID, String.valueOf(subProductCategoryListModal.getSubcategory_detail().getId()));
                                context.startActivity(i);
                            } else {
                                Intent i = new Intent(context, CategoryProducts.class);
                                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                                i.putExtra(Constant.CATEGORY_ID, String.valueOf(subProductCategoryListModal.getSubcategory_detail().getId()));
                                i.putExtra(Constant.CATEGORY_NAME, subProductCategoryListModal.getSubcategory_detail().getTitle());
                                context.startActivity(i);

                            }
                        }

                        break;
                }

            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {

        }
    }
}
