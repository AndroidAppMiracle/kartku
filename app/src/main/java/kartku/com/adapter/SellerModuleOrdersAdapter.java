package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import kartku.com.R;
import kartku.com.activity.OrderDetail;
import kartku.com.retrofit.modal.SellerOrdersModal;
import kartku.com.sellermodule.SellerOrderDetails;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 1/24/2017.
 */

public class SellerModuleOrdersAdapter extends RecyclerView.Adapter<SellerModuleOrdersAdapter.ViewHolder> {

    private Context mContext;
    private List<SellerOrdersModal.ResultsBean> mItems;
    //private List<SellerOrdersModal.ResultsBean.OrderDetailBean> mOrderDetailsList;
    //private List<SellerOrdersModal.ResultsBean.ProductDetailBean> mProductDetailsList;
    private SharedPreferences pref;
    private String user_id;
    private Resources resources;

    public SellerModuleOrdersAdapter(Context context, List<SellerOrdersModal.ResultsBean> items) {
        this.mContext = context;
        this.mItems = items;
        //this.mOrderDetailsList = orderDetailsList;
        //this.mProductDetailsList = productDetailsList;
        //aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        resources = mContext.getResources();
        //cart_id = Session.getcart_id(pref);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_seller_sold_orders, parent, false);
        return new SellerModuleOrdersAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        try {

            holder.tv_orderID.setText(String.format(resources.getString(R.string.two_parameters_with_int), resources.getString(R.string.order_id), mItems.get(position).getOrder_detail().getId()));
            holder.tv_createdAt.setText(String.format(resources.getString(R.string.two_parameters), resources.getString(R.string.date), mItems.get(position).getOrder_detail().getCreated_at()));


            if (mItems.get(position).getOrder_detail().getStatus().matches(".*\\d.*")) {
                // contains a number
                holder.tv_payment_status.setText(String.format(resources.getString(R.string.two_parameters), resources.getString(R.string.payment_status), mContext.getString(R.string.products_in_cart)));
            } else {
                // does not contain a number
                holder.tv_payment_status.setText(String.format(resources.getString(R.string.two_parameters), resources.getString(R.string.payment_status), mItems.get(position).getOrder_detail().getStatus()));
            }

            holder.tv_grand_total.setText(String.format(resources.getString(R.string.two_parameters_with_int), resources.getString(R.string.grand_total), mItems.get(position).getOrder_detail().getGrand_total()));

            holder.ll_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //SellerOrderDetails sellerDetailsData = new SellerOrderDetails(mItems.get(position));

                        if (mItems.get(holder.getAdapterPosition()).getOrder_detail().getStatus().matches(".*\\d.*")) {

                            ToastMsg.showShortToast(mContext, mContext.getString(R.string.products_in_cart));

                        } else {
                            Intent intent = new Intent(mContext, OrderDetail.class);
                            int str = mItems.get(holder.getAdapterPosition()).getOrder_detail().getId();
                            Bundle b = new Bundle();
                            b.putString(Constant.ORDER_ID, String.valueOf(str));
                            intent.putExtras(b);

                          /*  intent.putExtra(Constant.TOTAL_AMOUNT, mItems.get(holder.getAdapterPosition()).getOrder_detail().getTotal_amount());
                            intent.putExtra(Constant.SHIPPING_COST, mItems.get(holder.getAdapterPosition()).getOrder_detail().getShipping_cost());
                            intent.putExtra(Constant.TAX, mItems.get(holder.getAdapterPosition()).getOrder_detail().getTax());
                            intent.putExtra(Constant.GRAND_TOTAL, mItems.get(holder.getAdapterPosition()).getOrder_detail().getGrand_total());
                            intent.putExtra(Constant.PICKUP_CITY, mItems.get(holder.getAdapterPosition()).getPickup_city());
                            intent.putExtra(Constant.PICKUP_CITY_CODE, mItems.get(holder.getAdapterPosition()).getPickup_city_code());
                            intent.putExtra(Constant.DEST_CITY, mItems.get(holder.getAdapterPosition()).getDest_city());
                            intent.putExtra(Constant.DEST_CITY_CODE, mItems.get(holder.getAdapterPosition()).getDest_city_code());
                            intent.putExtra(Constant.CREATED_AT, mItems.get(holder.getAdapterPosition()).getCreated_at());*/
                            mContext.startActivity(intent);

                          /*  Intent intent = new Intent(mContext, SellerOrderDetails.class);
                            intent.putExtra(Constant.ORDER_ID, mItems.get(holder.getAdapterPosition()).getOrder_detail().getId());
                            intent.putExtra(Constant.TOTAL_AMOUNT, mItems.get(holder.getAdapterPosition()).getOrder_detail().getTotal_amount());
                            intent.putExtra(Constant.SHIPPING_COST, mItems.get(holder.getAdapterPosition()).getOrder_detail().getShipping_cost());
                            intent.putExtra(Constant.TAX, mItems.get(holder.getAdapterPosition()).getOrder_detail().getTax());
                            intent.putExtra(Constant.GRAND_TOTAL, mItems.get(holder.getAdapterPosition()).getOrder_detail().getGrand_total());
                            intent.putExtra(Constant.PICKUP_CITY, mItems.get(holder.getAdapterPosition()).getPickup_city());
                            intent.putExtra(Constant.PICKUP_CITY_CODE, mItems.get(holder.getAdapterPosition()).getPickup_city_code());
                            intent.putExtra(Constant.DEST_CITY, mItems.get(holder.getAdapterPosition()).getDest_city());
                            intent.putExtra(Constant.DEST_CITY_CODE, mItems.get(holder.getAdapterPosition()).getDest_city_code());
                            intent.putExtra(Constant.CREATED_AT, mItems.get(holder.getAdapterPosition()).getCreated_at());
                            mContext.startActivity(intent);*/
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (mItems.isEmpty()) {
            return 0;
        } else {
            return mItems.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_orderID, tv_createdAt, tv_payment_status, tv_grand_total;
        public LinearLayout ll_order;
        //public ImageView product_image, image_product, delete_product;
        // public EditText qtyText;
        //public Button add_btn, sub_btn;

        public ViewHolder(final View container) {
            super(container);
            tv_orderID = (TextView) container.findViewById(R.id.tv_orderID);
            tv_createdAt = (TextView) container.findViewById(R.id.tv_createdAt);
            tv_payment_status = (TextView) container.findViewById(R.id.tv_payment_status);
            tv_grand_total = (TextView) container.findViewById(R.id.tv_grand_total);
            ll_order = (LinearLayout) container.findViewById(R.id.ll_order);

        }
    }

}
