package kartku.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;

/**
 * Created by satoti.garg on 9/13/2016.
 */
public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.ViewHolder> {

    List<String> items = new ArrayList<String>();
    Context context;
    // private AQuery aquery;
    // Allows to remember the last item shown on screen
    //private int lastPosition = -1;

    public BannerAdapter(Context context, List<String> items) {
        this.context = context;
        this.items = items;
        // aquery = new AQuery(context);
//        Log.d("", "banners list size onBindViewHolder items size *********" + items.size());
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.banner_recycleview_rowview, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        Log.d("", "banners list size onBindViewHolder *********" + items);
        // holder.text.setText("" + items.get(position));
        /*aquery.id(holder.banner_image).image(items.get(position), true, true, 0, 0, null, AQuery.FADE_IN);*/
        Picasso.with(context)
                .load(items.get(position))
                .placeholder(R.drawable.placeholder)   // optional
                .error(R.drawable.placeholder)
                .fit().centerCrop()
                .into((holder.banner_image));
        // Here you apply the animation when the view is bound
      /*  setAnimation(holder.ll_home_banner_images, position);*/

        //setAnimation(holder.ll_home_banner_images, position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView banner_image;
        //LinearLayout ll_home_banner_images;


        public ViewHolder(final View container) {
            super(container);
            banner_image = (ImageView) container.findViewById(R.id.banner_img_view);
            //ll_home_banner_images = (LinearLayout) container.findViewById(R.id.ll_home_banner_images);
//            Log.d("", "banners list size ViewHolder *********" + items.size());
        }

     /*   public void clearAnimation() {
            ll_home_banner_images.clearAnimation();
        }*/
    }


    /*private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_PARENT, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }*/

/*    *//**
     * key method to apply the animation
     *//*
    private void setAnimation(View viewToAnimate, int position) {

        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }

    }


    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        ((ViewHolder) holder).clearAnimation();
    }*/
}
