package kartku.com.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.LinkedHashMap;
import java.util.List;

import kartku.com.R;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private LinkedHashMap<String, List<String>> _listDataChild;
    private List<String> _listDataHeaderImages;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 LinkedHashMap<String, List<String>> listChildData, List<String> listDataHeaderImages) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this._listDataHeaderImages = listDataHeaderImages;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    /*@Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }*/
    @Override
    public int getChildrenCount(int groupPosition) {
        try {
//            Log.d("", "group position " + groupPosition);
            //Intent intent = null;

            switch (groupPosition) {

                case 0:
                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();

                case 1:

                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();


                case 2:

                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                case 3:

                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();

                case 4:

                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                case 5:
                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                case 6:

                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                case 7:
                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                case 8:
                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                default:

                    break;


            }
            /*_context.startActivity(intent);
            ((Activity) _context).finish();*/


        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }


    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        ImageView iv_category_images = (ImageView) convertView.findViewById(R.id.iv_category_images);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        /*Glide.with(_context)
                .load(_listDataHeaderImages.get(groupPosition)).placeholder(R.drawable.placeholder).override(50, 50).thumbnail(0.1f)
                .into(iv_category_images);*/
        Picasso.with(_context)
                .load(_listDataHeaderImages.get(groupPosition))
                .placeholder(R.drawable.placeholder)   // optional
                .error(R.drawable.placeholder)
                .resize(64, 64)
                .into(iv_category_images);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

