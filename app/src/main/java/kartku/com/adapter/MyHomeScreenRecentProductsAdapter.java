package kartku.com.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kartku.com.R;
import kartku.com.activity.CategoryProducts;
import kartku.com.retrofit.modal.HomeScreenModal;
import kartku.com.utils.Constant;

/**
 * Created by Kshitiz Bali on 3/14/2017.
 */

public class MyHomeScreenRecentProductsAdapter extends RecyclerView.Adapter<MyHomeScreenRecentProductsAdapter.ViewHolder> {


    private Context mContext, alertContext;
    private List<HomeScreenModal.SubCategoriesBean> productsList;
    private Resources resources;


    public MyHomeScreenRecentProductsAdapter(Context context, List<HomeScreenModal.SubCategoriesBean> list) {
        this.mContext = context;
        this.productsList = list;
        resources = mContext.getResources();
      /*  this.mItemsList = itemsList;
        this.mProductsList = productList;
        this.ll_bottomLayout = bottomLayout;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        this.textViewAmount = textView;*/
       /* user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);*/
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_homescreen_recent_items, parent, false);
        alertContext = parent.getContext();
        return new MyHomeScreenRecentProductsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        try {
            holder.tv_recent_products_title.setText(productsList.get(position).getTitle());
            holder.text_price_starts_from.setText(String.format(resources.getString(R.string.product_onwards), String.valueOf(productsList.get(position).getStart_range())));


            Glide.with(mContext)
                    .load(productsList.get(position).getImage())
                    .thumbnail(0.1f).override(150, 150).centerCrop()
                    .into(holder.iv_recent_products_image);

            holder.ll_recent_categories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (productsList.get(holder.getAdapterPosition()).getIs_active().equalsIgnoreCase("1")) {

                        Intent i = new Intent(mContext, CategoryProducts.class);
                        i.putExtra(Constant.CATEGORY_ID, String.valueOf(productsList.get(holder.getAdapterPosition()).getId()));
                        i.putExtra(Constant.CATEGORY_NAME, productsList.get(holder.getAdapterPosition()).getTitle());
                        //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                        mContext.startActivity(i);
                    } else {
                        showAlertDialog(alertContext);
                    }

                }
            });

        } catch (Exception e) {

        }


    }

    @Override
    public int getItemCount() {
        if (productsList.size() != 0) {
            return productsList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_recent_products_title, text_price_starts_from;
        public ImageView iv_recent_products_image;
        public LinearLayout ll_recent_categories;


        public ViewHolder(final View container) {
            super(container);
            tv_recent_products_title = (TextView) container.findViewById(R.id.tv_recent_products_title);
            iv_recent_products_image = (ImageView) container.findViewById(R.id.iv_recent_products_image);
            ll_recent_categories = (LinearLayout) container.findViewById(R.id.ll_recent_categories);
            text_price_starts_from = (TextView) container.findViewById(R.id.text_price_starts_from);
        }
    }

    public void showAlertDialog(Context context) {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
            //dialogBuilder.setTitle("Connection Problem");
            dialogBuilder.setMessage(R.string.products_not_available);
            dialogBuilder.setCancelable(true);
           /* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*/
            dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                }
            });
           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
