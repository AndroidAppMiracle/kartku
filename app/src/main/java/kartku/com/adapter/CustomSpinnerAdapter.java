package kartku.com.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.retrofit.modal.ProductDetailModal;

/**
 * Created by Kshitiz Bali on 4/18/2017.
 */

public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private List<ProductDetailModal.ProductInfoBean.ColorBean> colourList = new ArrayList<>();

    public CustomSpinnerAdapter(Context context, List<ProductDetailModal.ProductInfoBean.ColorBean> colours) {
        this.colourList = colours;
        activity = context;
    }


    public int getCount() {
        if (colourList.size() != 0) {
            return colourList.size();
        } else {
            return 0;
        }

    }

    public Object getItem(int i) {
        if (colourList.size() != 0) {
            return colourList.get(i);
        } else {
            return 0;
        }
    }

    public long getItemId(int i) {
        if (colourList.size() != 0) {
            return (long) colourList.get(i).getId();
        } else {
            return 0;
        }
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(18);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(colourList.get(position).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(activity);
        txt.setGravity(Gravity.CENTER);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
        txt.setText(colourList.get(i).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return txt;
    }

}