package kartku.com.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kartku.com.R;

/**
 * Created by tanuja.gupta on 9/20/2016.
 */
public class CustomDialogAdapter extends BaseAdapter {

    LayoutInflater inflater;
    Context ctx;
    String status;
    ProgressDialog pDialog;
    ArrayList<String> arrayList;

    public CustomDialogAdapter(Context context, ArrayList<String> _arrayList) {
        this.ctx = context;
        inflater = LayoutInflater.from(this.ctx);
        this.arrayList = _arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final ViewHolder holder;
        int loader = R.mipmap.ic_launcher;
        try {
            if (v == null) {
                holder = new ViewHolder();
                v = inflater.inflate(R.layout.custom_dialog_item, parent, false);
                holder.text_item = (TextView) v.findViewById(R.id.text_item);
                v.setTag(holder);
                holder.text_item.setText(arrayList.get(position));


            } else {
                holder = (ViewHolder) v.getTag();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return v;

    }

    class ViewHolder {
        View view1;
        TextView text_item;


    }

}



