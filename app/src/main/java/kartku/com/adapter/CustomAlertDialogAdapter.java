package kartku.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kartku.com.R;

/**
 * Created by Kshitizb on 26-03-2017.
 */


public class CustomAlertDialogAdapter extends BaseAdapter {

    private ArrayList<String> listData;

    private LayoutInflater layoutInflater;

    public CustomAlertDialogAdapter(Context context, ArrayList<String> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_single_list_item, null);
            holder = new ViewHolder();
            holder.tv_quantity = (TextView) convertView.findViewById(R.id.tv_quantity);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_quantity.setText(listData.get(position));

        return convertView;
    }

    static class ViewHolder {
        TextView tv_quantity;
    }

}
