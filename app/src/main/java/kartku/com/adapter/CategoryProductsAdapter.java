package kartku.com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import kartku.com.R;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.AddToWishListModal;
import kartku.com.retrofit.modal.CategoryProductsModal;
import kartku.com.retrofit.modal.RemoveFromWishlistModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/15/2017.
 */

public class CategoryProductsAdapter extends RecyclerView.Adapter<CategoryProductsAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryProductsModal.ProductsDataBean> productsList;
    private SharedPreferences pref;
    private Context alertContext;
    private Resources res;

    public CategoryProductsAdapter(Context context, List<CategoryProductsModal.ProductsDataBean> list) {
        this.mContext = context;
        this.productsList = list;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        res = mContext.getResources();
      /*  this.mItemsList = itemsList;
        this.mProductsList = productList;
        this.ll_bottomLayout = bottomLayout;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        this.textViewAmount = textView;*/
       /* user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);*/
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.demodemodemo, parent, false);*/
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false);
        alertContext = parent.getContext();
        return new CategoryProductsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        /*Glide.with(mContext)
                .load(productsList.get(position).getProduct_image()).override(200, 200)
                .thumbnail(0.1f).dontTransform()*//*.centerCrop()*//*.crossFade()
                .into(holder.image_product);*/

        Picasso.with(mContext).load(productsList.get(position).getProduct_image()).placeholder(R.drawable.placeholder).resize(200, 200)/*.centerInside()*/.into(holder.image_product);

        holder.text_name.setText(productsList.get(position).getTitle());
        holder.text_price.setText(String.format(res.getString(R.string.price_in_RP), productsList.get(position).getProducts_price()));


        if (Session.get_login_statuc(pref)) {
            if (productsList.get(holder.getAdapterPosition()).getWishlist() == 1) {
                Glide.with(mContext)
                        .load(R.drawable.ic_wishlist).override(30, 30)
                    /*.thumbnail(0.1f)*/
                        .into(holder.mark_wishlist);
            } else {
                Glide.with(mContext)
                        .load(R.drawable.ic_wishlist_not).override(30, 30)
                    /*.thumbnail(0.1f)*/
                        .into(holder.mark_wishlist);
            }

        }


        holder.ll_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, ProductDetailNew.class);
                i.putExtra(Constant.SELECTED_CATEGORY_INDEX, String.valueOf(productsList.get(holder.getAdapterPosition()).getId()));
                i.putExtra(Constant.WISHLIST, productsList.get(holder.getAdapterPosition()).getWishlist());
                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                mContext.startActivity(i);
            }
        });

        holder.ll_product_extra_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //  ToastMsg.showLongToast(context, "Clicked");
                    String description = productsList.get(holder.getAdapterPosition()).getDescription();
                    String brand = productsList.get(holder.getAdapterPosition()).getProducts_brand();
                    String material = productsList.get(holder.getAdapterPosition()).getProduct_material();
                    extraDetailsDialog(description, brand, material);
                    //ToastMsg.showLongToast(context, "Clicked2");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (productsList.size() != 0) {
            return productsList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements ApiServerResponse {
        public TextView text_name, text_price;
        public ImageView image_product, iv_extra_options, mark_wishlist;
        public LinearLayout ll_product, ll_product_extra_options;


        public ViewHolder(final View container) {
            super(container);
            text_name = (TextView) container.findViewById(R.id.text_name);
            text_price = (TextView) container.findViewById(R.id.text_price);
            image_product = (ImageView) container.findViewById(R.id.image_product);
            iv_extra_options = (ImageView) container.findViewById(R.id.iv_extra_options);
            mark_wishlist = (ImageView) container.findViewById(R.id.mark_wishlist);
            ll_product = (LinearLayout) container.findViewById(R.id.ll_product);
            ll_product_extra_options = (LinearLayout) container.findViewById(R.id.ll_product_extra_options);

            mark_wishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Utility.checkInternetConnection(mContext)) {
                        if (Session.get_login_statuc(pref)) {
                            if (productsList.get(getAdapterPosition()).getWishlist() == 1) {
                    /*ServerAPI.getInstance().removeFromWishlist(Api);*/
                                ((Utility) mContext).showLoading();
                                ServerAPI.getInstance().removeFromWishlist(ApiServerResponse.REMOVE_FROM_WISHLIST, Session.get_userId(pref), String.valueOf(productsList.get(getAdapterPosition()).getId()), ViewHolder.this);
                            } else {
                                ((Utility) mContext).showLoading();
                                ServerAPI.getInstance().addToWishlist(ApiServerResponse.ADD_TO_WISHLIST, Session.get_userId(pref), String.valueOf(productsList.get(getAdapterPosition()).getId()), ViewHolder.this);
                            }
                        } else {
                            ToastMsg.showShortToast(mContext, mContext.getString(R.string.login_to_continuew));
                        }
                    } else {
                        ((Utility) mContext).showAlertDialog(alertContext);
                    }


                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                if (response.isSuccessful()) {
                    AddToWishListModal addToWishListModal;
                    RemoveFromWishlistModal removeFromWishlistModal;

                    switch (tag) {
                        case ApiServerResponse.ADD_TO_WISHLIST:
                            addToWishListModal = (AddToWishListModal) response.body();
                            if (addToWishListModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                                productsList.get(getAdapterPosition()).setWishlist(1);
                                Glide.with(mContext)
                                        .load(R.drawable.ic_wishlist)
                                        .override(30, 30)
                                        .into(mark_wishlist);
                                ToastMsg.showShortToast(mContext, addToWishListModal.getMessage());
                            } else {
                                ToastMsg.showShortToast(mContext, addToWishListModal.getMessage());
                            }

                            ((Utility) mContext).hideLoading();
                            break;
                        case ApiServerResponse.REMOVE_FROM_WISHLIST:
                            removeFromWishlistModal = (RemoveFromWishlistModal) response.body();
                            productsList.get(getAdapterPosition()).setWishlist(0);
                            if (removeFromWishlistModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                                Glide.with(mContext)
                                        .load(R.drawable.ic_wishlist_not)
                                        .override(30, 30)
                                        .into(mark_wishlist);
                                ToastMsg.showShortToast(mContext, mContext.getString(R.string.removed_from_wishlist));

                            } else {
                                ToastMsg.showShortToast(mContext, removeFromWishlistModal.getMessage());
                            }
                            ((Utility) mContext).hideLoading();
                            break;
                    }

                } else {
                    ((Utility) mContext).hideLoading();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((Utility) mContext).hideLoading();
        }
    }

    private void extraDetailsDialog(String description, String brand, String material) {
        final Dialog dialog = new Dialog(alertContext);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.title_activity_product_detail);
        dialog.setContentView(R.layout.product_list_more_options);

        EditText edittextDescription = (EditText) dialog.findViewById(R.id.et_description);
        edittextDescription.setText(description);

        TextView textViewBrand = (TextView) dialog.findViewById(R.id.text_brand);
        textViewBrand.setText(brand);

        TextView textViewMaterial = (TextView) dialog.findViewById(R.id.text_material);
        textViewMaterial.setText(material);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }

}
