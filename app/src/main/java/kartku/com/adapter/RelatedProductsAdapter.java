package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kartku.com.R;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.retrofit.modal.ProductDetailModal;
import kartku.com.utils.Constant;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 4/13/2017.
 */

public class RelatedProductsAdapter extends RecyclerView.Adapter<RelatedProductsAdapter.ViewHolder> {

    private Context mContext;
    private List<ProductDetailModal.RelatedProductsBean> productsList;
    private SharedPreferences pref;
    private Context alertContext;
    private Resources res;


    public RelatedProductsAdapter(Context context, List<ProductDetailModal.RelatedProductsBean> list) {
        this.mContext = context;
        this.productsList = list;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        res = mContext.getResources();
      /*  this.mItemsList = itemsList;
        this.mProductsList = productList;
        this.ll_bottomLayout = bottomLayout;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        this.textViewAmount = textView;*/
       /* user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);*/
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_layout_related_product, parent, false);
        alertContext = parent.getContext();
        return new RelatedProductsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        try {
            Glide.with(mContext)
                    .load(productsList.get(position).getProduct_image())
                    .thumbnail(0.1f).override(150, 150)
                    .into(holder.image_product);


            holder.text_name.setText(productsList.get(position).getTitle());
            holder.text_price.setText(String.format(res.getString(R.string.price_in_RP), productsList.get(position).getProducts_price()));

            holder.ll_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, ProductDetailNew.class);
                    i.putExtra(Constant.SELECTED_CATEGORY_INDEX, String.valueOf(productsList.get(holder.getAdapterPosition()).getId()));
                    //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                    mContext.startActivity(i);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (productsList.size() != 0) {
            return productsList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //public TextView title_order_id, title_created_at, title_grand_total, title_payment_status;
        public LinearLayout ll_product;
        public ImageView image_product;
        public TextView text_price, text_name;
        //public ImageView product_image, image_product, delete_product;
        // public EditText qtyText;
        //public Button add_btn, sub_btn;

        public ViewHolder(final View container) {
            super(container);
            //title_order_id = (TextView) container.findViewById(R.id.title_order_id);
            //title_created_at = (TextView) container.findViewById(R.id.title_created_at);
            //title_grand_total = (TextView) container.findViewById(R.id.title_grand_total);
            //title_payment_status = (TextView) container.findViewById(R.id.title_payment_status);
            ll_product = (LinearLayout) container.findViewById(R.id.ll_product);
            image_product = (ImageView) container.findViewById(R.id.image_product);
            text_price = (TextView) container.findViewById(R.id.text_price);
            text_name = (TextView) container.findViewById(R.id.text_name);

        }
    }
}
