package kartku.com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import kartku.com.R;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.AddToWishListModal;
import kartku.com.retrofit.modal.EventBusCategoryProductsUpdateWishlist;
import kartku.com.retrofit.modal.ProductSearchBrandModal;
import kartku.com.retrofit.modal.RemoveFromWishlistModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/22/2017.
 */

public class SearchResultListAdapter extends RecyclerView.Adapter<SearchResultListAdapter.ViewHolder> {

    private Context mContext;
    private List<ProductSearchBrandModal.ProductsDataBean> searchProductList;
    private SharedPreferences pref;
    private Context alertContext;


    public SearchResultListAdapter(Context context, List<ProductSearchBrandModal.ProductsDataBean> list) {
        this.mContext = context;
        this.searchProductList = list;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
      /*  this.mItemsList = itemsList;
        this.mProductsList = productList;
        this.ll_bottomLayout = bottomLayout;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        this.textViewAmount = textView;*/
       /* user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);*/
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false);
        alertContext = parent.getContext();
        return new SearchResultListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.text_name.setText(searchProductList.get(position).getTitle());
        holder.text_price.setText(searchProductList.get(position).getProducts_price());
        Glide.with(mContext)
                .load(searchProductList.get(position).getProduct_image())
                .thumbnail(0.1f).override(100, 100)
                .into(holder.image_product);


        if (searchProductList.get(holder.getAdapterPosition()).getWishlist().equalsIgnoreCase("1")) {
            Glide.with(mContext)
                    .load(R.drawable.ic_wishlist).override(30, 30)
                    /*.thumbnail(0.1f)*/
                    .into(holder.mark_wishlist);
        } else {
            Glide.with(mContext)
                    .load(R.drawable.ic_wishlist_not).override(30, 30)
                    /*.thumbnail(0.1f)*/
                    .into(holder.mark_wishlist);
        }

        holder.ll_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, ProductDetailNew.class);
                i.putExtra(Constant.SELECTED_CATEGORY_INDEX, String.valueOf(searchProductList.get(holder.getAdapterPosition()).getId()));
                i.putExtra(Constant.WISHLIST, searchProductList.get(holder.getAdapterPosition()).getWishlist());
                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                mContext.startActivity(i);
            }
        });

        holder.ll_product_extra_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //  ToastMsg.showLongToast(context, "Clicked");
                    String description = searchProductList.get(holder.getAdapterPosition()).getDescription();
                    String brand = searchProductList.get(holder.getAdapterPosition()).getProducts_brand();
                    String material = searchProductList.get(holder.getAdapterPosition()).getProduct_material();
                    extraDetailsDialog(description, brand, material);
                    //ToastMsg.showLongToast(context, "Clicked2");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (searchProductList.size() != 0) {
            return searchProductList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements ApiServerResponse {
        public TextView text_name, text_price;
        public ImageView image_product, iv_extra_options, mark_wishlist;
        public LinearLayout ll_product, ll_product_extra_options;


        public ViewHolder(final View container) {
            super(container);
            text_name = (TextView) container.findViewById(R.id.text_name);
            text_price = (TextView) container.findViewById(R.id.text_price);
            image_product = (ImageView) container.findViewById(R.id.image_product);
            iv_extra_options = (ImageView) container.findViewById(R.id.iv_extra_options);
            mark_wishlist = (ImageView) container.findViewById(R.id.mark_wishlist);
            ll_product = (LinearLayout) container.findViewById(R.id.ll_product);
            ll_product_extra_options = (LinearLayout) container.findViewById(R.id.ll_product_extra_options);

            mark_wishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (searchProductList.get(getAdapterPosition()).getWishlist().equalsIgnoreCase("1")) {
                    /*ServerAPI.getInstance().removeFromWishlist(Api);*/
                        ((Utility) mContext).showLoading();
                        ServerAPI.getInstance().removeFromWishlist(ApiServerResponse.REMOVE_FROM_WISHLIST, Session.get_userId(pref), String.valueOf(searchProductList.get(getAdapterPosition()).getId()), ViewHolder.this);
                    } else {
                        ((Utility) mContext).showLoading();
                        ServerAPI.getInstance().addToWishlist(ApiServerResponse.ADD_TO_WISHLIST, Session.get_userId(pref), String.valueOf(searchProductList.get(getAdapterPosition()).getId()), ViewHolder.this);
                    }
                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                if (response.isSuccessful()) {
                    AddToWishListModal addToWishListModal;
                    RemoveFromWishlistModal removeFromWishlistModal;

                    switch (tag) {
                        case ApiServerResponse.ADD_TO_WISHLIST:
                            addToWishListModal = (AddToWishListModal) response.body();
                            if (addToWishListModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                                EventBus.getDefault().post(new EventBusCategoryProductsUpdateWishlist(addToWishListModal.getStatus()));
                                Glide.with(mContext)
                                        .load(R.drawable.ic_wishlist)
                                        .override(30, 30)
                                        .into(mark_wishlist);
                                ToastMsg.showShortToast(mContext, addToWishListModal.getMessage());
                            } else {
                                ToastMsg.showShortToast(mContext, addToWishListModal.getMessage());
                            }

                            ((Utility) mContext).hideLoading();
                            break;
                        case ApiServerResponse.REMOVE_FROM_WISHLIST:
                            removeFromWishlistModal = (RemoveFromWishlistModal) response.body();

                            if (removeFromWishlistModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                                EventBus.getDefault().post(new EventBusCategoryProductsUpdateWishlist(removeFromWishlistModal.getStatus()));

                                Glide.with(mContext)
                                        .load(R.drawable.ic_wishlist_not)
                                        .override(30, 30)
                                        .into(mark_wishlist);
                                ToastMsg.showShortToast(mContext, mContext.getString(R.string.removed_from_wishlist));

                            } else {
                                ToastMsg.showShortToast(mContext, removeFromWishlistModal.getMessage());
                            }
                            ((Utility) mContext).hideLoading();
                            break;
                    }

                } else {
                    ((Utility) mContext).hideLoading();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((Utility) mContext).hideLoading();
        }
    }


    private void extraDetailsDialog(String description, String brand, String material) {
        final Dialog dialog = new Dialog(alertContext);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.title_activity_product_detail);
        dialog.setContentView(R.layout.product_list_more_options);

        EditText edittextDescription = (EditText) dialog.findViewById(R.id.et_description);
        edittextDescription.setText(description);

        TextView textViewBrand = (TextView) dialog.findViewById(R.id.text_brand);
        textViewBrand.setText(brand);

        TextView textViewMaterial = (TextView) dialog.findViewById(R.id.text_material);
        textViewMaterial.setText(material);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/

        TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        text_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.show();
    }
}
