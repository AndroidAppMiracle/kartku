package kartku.com.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import kartku.com.R;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;


/**
 * Created by satoti.garg on 9/12/2016.
 */
public class DemoExpandableAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title

    private HashMap<String, List<String>> _listDataChild;

    /*private HashMap<String, List<String>> _listDataChild;*/
    FragmentManager fragmentManager;
    int id;
    boolean login_status;
    SharedPreferences pref;
    List<String> categoryImages;

    public DemoExpandableAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData, FragmentManager fragmentManager, int id, boolean login_status, List<String> categoryImages) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.fragmentManager = fragmentManager;
        this.id = id;
        this.categoryImages = categoryImages;
        this.login_status = login_status;
        pref = new ObscuredSharedPreferences(_context, _context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        //return _listDataHeader.get(childPosititon);
       /* ToastMsg.showLongToast(_context, "" + _listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon));*/
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
//            Log.d("", "group position " + groupPosition);
            //Intent intent = null;

            switch (groupPosition) {

                case 0:
                    ToastMsg.showShortToast(_context, "select" + this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size());
                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();

                case 1:

                   /* if (NetworkConnection.checkInternetConnection(_context)) {
                        intent = new Intent(_context, Home_dashboard.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {

                        ToastMsg.showShortToast(_context, "Not connected to internet.");

                    }*/

                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();


                //_context.startActivity(intent);


                case 2:

                   /* if (NetworkConnection.checkInternetConnection(_context)) {
                        if (login_status) {
                            intent = new Intent(_context, Orders.class);
                            //_context.startActivity(intent);
                        } else {
                            intent = new Intent(_context, Login.class);
                            // _context.startActivity(intent);
                        }

                    } else {

                        ToastMsg.showShortToast(_context, "Not connected to internet.");

                    }*/
                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();


                case 3:

                  /*  if (NetworkConnection.checkInternetConnection(_context)) {
                        if (login_status) {
                            intent = new Intent(_context, ProductReview.class);
                            // _context.startActivity(intent);

                        } else {
                    *//*FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(id, new FAQFragment());
                    fragmentTransaction.commit();*//*
                            intent = new Intent(_context, FAQ.class);
                            //_context.startActivity(intent);
                        }
                    } else {

                        ToastMsg.showShortToast(_context, "Not connected to internet.");

                    }*/


                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();

                case 4:

                   /* if (NetworkConnection.checkInternetConnection(_context)) {
                        if (login_status) {
                            intent = new Intent(_context, Wishlist.class);
                            //_context.startActivity(intent);
                        } else {
                            *//*intent = new Intent(_context, Contact.class);*//*
                            //_context.startActivity(intent);
                        }
                    } else {

                        ToastMsg.showShortToast(_context, "Not connected to internet.");

                    }*/


                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                case 5:
                   /* if (NetworkConnection.checkInternetConnection(_context)) {
                        intent = new Intent(_context, Profile.class);
                    } else {

                        ToastMsg.showShortToast(_context, "Not connected to internet.");

                    }*/

                    // _context.startActivity(intent);
                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                case 6:

                   /* if (NetworkConnection.checkInternetConnection(_context)) {
                        intent = new Intent(_context, Contact.class);
                    } else {

                        ToastMsg.showShortToast(_context, "Not connected to internet.");

                    }*/

                    //_context.startActivity(intent);
                    return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                            .size();
                case 7:
                  /*  intent = new Intent(_context, Login.class);
                    // _context.startActivity(intent);
                    Session.set_login_status(pref, false);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    pref.edit().clear().apply();
                    break;*/
                default:

                    break;


            }
            /*_context.startActivity(intent);
            ((Activity) _context).finish();*/



/*            if (groupPosition == 0) {
                return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                        .size();
            } else if (groupPosition == 1) {
                Intent intent = new Intent(_context, Home_dashboard.class);
                _context.startActivity(intent);
                return 0;
            } else if (groupPosition == 2) {
                if (login_status) {
                    Intent intent = new Intent(_context, Orders.class);
                    _context.startActivity(intent);
                } else {
                    Intent intent = new Intent(_context, Login.class);
                    _context.startActivity(intent);
                }
                return 0;
            } else if (groupPosition == 3) {
                if (login_status) {
                    Intent intent = new Intent(_context, ProductReview.class);
                    _context.startActivity(intent);

                } else {
                    *//*FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(id, new FAQFragment());
                    fragmentTransaction.commit();*//*
                    Intent intent = new Intent(_context, FAQ.class);
                    _context.startActivity(intent);
                }
                return 0;
            } else if (groupPosition == 4) {
                try {
                    if (login_status) {
                        Intent intent = new Intent(_context, Wishlist.class);
                        _context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(_context, Contact.class);
                        _context.startActivity(intent);
                    }

                    *//*FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(id, new ContactUsFragment());
                    fragmentTransaction.commit();*//*
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            } else if (groupPosition == 5) {
                Intent intent = new Intent(_context, Profile.class);
                _context.startActivity(intent);
                return 0;
            } else if (groupPosition == 6) {
                Intent intent = new Intent(_context, Contact.class);
                _context.startActivity(intent);
                return 0;
            } else if (groupPosition == 7) {
                Intent intent = new Intent(_context, Login.class);
                _context.startActivity(intent);
                Session.set_login_status(pref, false);
                return 0;
            } else {
                return 0;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        try {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }

            TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
            /*ImageView iv_category_images = (ImageView) convertView.findViewById(R.id.iv_category_images);
            iv_category_images.setImageResource(Integer.parseInt(categoryImages.get(groupPosition)));*/
            lblListHeader.setText(headerTitle);
            ImageView iv_category_images = (ImageView) convertView.findViewById(R.id.iv_category_images);
            //iv_category_images.setImageResource(Integer.intValue(categoryImages.get(0)));
            Picasso.with(_context)
                    .load(categoryImages.get(groupPosition))
                    .placeholder(R.drawable.placeholder)   // optional
                    .error(R.drawable.placeholder)
                    .into(iv_category_images);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        //System.out.println("child selected isChildSelectable  "+groupPosition+ " child pod  " +_listDataChild.get(childPosition));
        return true;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        //System.out.println("child selected getChildType  "+groupPosition+ " child pod  " +_listDataChild.get(childPosition));
        return super.getChildType(groupPosition, childPosition);
    }

}

