package kartku.com.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kartku.com.R;
import kartku.com.retrofit.modal.SellerOrdersModal;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 1/24/2017.
 */

public class SellerOrderDetailsAdapter extends RecyclerView.Adapter<SellerOrderDetailsAdapter.ViewHolder> {

    private Context mContext;
    private List<SellerOrdersModal.ResultsBean> mItems;
    //private List<SellerOrdersModal.ResultsBean.OrderDetailBean> mOrderDetailsList;
    //private List<SellerOrdersModal.ResultsBean.ProductDetailBean> mProductDetailsList;
    private SharedPreferences pref;
    private Resources resources;

    public SellerOrderDetailsAdapter(Context context, List<SellerOrdersModal.ResultsBean> items) {
        this.mContext = context;
        this.mItems = items;
        //this.mOrderDetailsList = orderDetailsList;
        //this.mProductDetailsList = productDetailsList;
        //aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        resources = mContext.getResources();
        //cart_id = Session.getcart_id(pref);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_seller_order_detail, parent, false);
        return new SellerOrderDetailsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            Glide.with(mContext)
                    .load(mItems.get(holder.getAdapterPosition()).getProduct_detail().getProduct_image())
                    .thumbnail(0.1f).override(50, 50)
                    .into(holder.iv_product_image);

            holder.tv_item_id.setText(String.format(resources.getString(R.string.two_parameters_with_int), mContext.getString(R.string.product_id), mItems.get(holder.getAdapterPosition()).getProduct_id()));
            holder.tv_item_name.setText(String.format(resources.getString(R.string.two_parameters), mContext.getString(R.string.name), mItems.get(holder.getAdapterPosition()).getProduct_detail().getTitle()));
            holder.tv_item_quantity.setText(String.format(resources.getString(R.string.two_parameters), mContext.getString(R.string.desc), mItems.get(holder.getAdapterPosition()).getProduct_detail().getDescription()));
            holder.tv_item_price.setText(String.format(resources.getString(R.string.two_parameters), mContext.getString(R.string.stock), mItems.get(holder.getAdapterPosition()).getProduct_detail().getStock_availability()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (mItems.isEmpty()) {
            return 0;
        } else {
            return mItems.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_item_name, tv_item_quantity, tv_item_price, tv_item_id;
        public LinearLayout ll_ordered_product;
        public ImageView iv_product_image;

        //public ImageView product_image, image_product, delete_product;
        // public EditText qtyText;
        //public Button add_btn, sub_btn;

        public ViewHolder(final View container) {
            super(container);
            tv_item_name = (TextView) container.findViewById(R.id.tv_item_name);
            tv_item_id = (TextView) container.findViewById(R.id.tv_item_id);
            tv_item_quantity = (TextView) container.findViewById(R.id.tv_item_quantity);
            tv_item_price = (TextView) container.findViewById(R.id.tv_item_price);
            ll_ordered_product = (LinearLayout) container.findViewById(R.id.ll_ordered_product);
            iv_product_image = (ImageView) container.findViewById(R.id.iv_product_image);

        }
    }

}