package kartku.com.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.retrofit.modal.MyAddressModal;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by Kshitiz Bali on 1/21/2017.
 */

public class MyAddressAdapter extends RecyclerView.Adapter<MyAddressAdapter.ViewHolder> {

    private List<MyAddressModal> mItems = new ArrayList<>();


    private SharedPreferences pref;
    private Context mContext;
    String user_id;

    public MyAddressAdapter(Context context, List<MyAddressModal> items) {
        this.mContext = context;
        this.mItems = items;
        //aquery = new AQuery(context);
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        //cart_id = Session.getcart_id(pref);
    }

    @Override
    public MyAddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(MyAddressAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        if (mItems.isEmpty()) {
            return 0;
        } else {
            return mItems.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title_order_id, title_created_at, title_grand_total, title_payment_status;
        public LinearLayout ll_item_layout;
        //public ImageView product_image, image_product, delete_product;
        // public EditText qtyText;
        //public Button add_btn, sub_btn;

        public ViewHolder(final View container) {
            super(container);
            title_order_id = (TextView) container.findViewById(R.id.title_order_id);
            title_created_at = (TextView) container.findViewById(R.id.title_created_at);
            title_grand_total = (TextView) container.findViewById(R.id.title_grand_total);
            title_payment_status = (TextView) container.findViewById(R.id.title_payment_status);
            ll_item_layout = (LinearLayout) container.findViewById(R.id.ll_item_layout);

        }
    }
}
