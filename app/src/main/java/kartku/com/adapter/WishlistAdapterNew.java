package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import kartku.com.R;
import kartku.com.cart.CartDetailsNew;
import kartku.com.product_detail.ProductDetailNew;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.AddToCartNewModal;
import kartku.com.retrofit.modal.CartItemCountModal;
import kartku.com.retrofit.modal.EventBusCartUpdate;
import kartku.com.retrofit.modal.EventBusCategoryProductsUpdateWishlist;
import kartku.com.retrofit.modal.EventBusProductDetailUpdateModal;
import kartku.com.retrofit.modal.RemoveFromWishlistModal;
import kartku.com.retrofit.modal.WishlistModal;
import kartku.com.utils.Constant;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/28/2017.
 */

public class WishlistAdapterNew extends RecyclerView.Adapter<WishlistAdapterNew.ViewHolder> {


    private List<WishlistModal.ProductInfoBean> wishList = new ArrayList<>();
    private Context context;
    SharedPreferences pref;

    String user_id, cart_id;
    Resources res;


    public WishlistAdapterNew(Context context, List<WishlistModal.ProductInfoBean> items) {
        this.context = context;
        this.wishList = items;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);
        res = context.getResources();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wishlist_row, parent, false);
        return new WishlistAdapterNew.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {


        holder.text_name.setText(wishList.get(holder.getAdapterPosition()).getName());
        holder.text_price.setText(String.format(res.getString(R.string.price_in_RP), wishList.get(holder.getAdapterPosition()).getPrice()));
        Glide.with(context)
                .load(wishList.get(position).getImage())
                .thumbnail(0.1f)/*.override(40, 40)*/
                .into(holder.image_product);
        //if (wishList.get(holder.getAdapterPosition()).getWishlist().equalsIgnoreCase("1")){
        Glide.with(context)
                .load(R.drawable.ic_wishlist)
                .thumbnail(0.1f)/*.override(40, 40)*/
                .into(holder.fvt_like);

        holder.ll_wishlist_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProductDetailNew.class);
                i.putExtra(Constant.SELECTED_CATEGORY_INDEX, String.valueOf(wishList.get(holder.getAdapterPosition()).getId()));
                i.putExtra(Constant.WISHLIST, wishList.get(holder.getAdapterPosition()).getWishlist());
                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                context.startActivity(i);
            }
        });

        if (wishList.get(holder.getAdapterPosition()).getIs_cart().equalsIgnoreCase("1")) {

            holder.btn_add_tocart.setText(context.getString(R.string.open_cart));

        } else {
            if (!wishList.get(holder.getAdapterPosition()).getMax_quantity().equalsIgnoreCase("") && !wishList.get(holder.getAdapterPosition()).getMax_quantity().equalsIgnoreCase("0")) {

                holder.btn_add_tocart.setEnabled(true);
                holder.btn_add_tocart.setText(context.getString(R.string.add_to_cart));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.btn_add_tocart.setBackground(context.getResources().getDrawable(R.drawable.roundedbutton, null));
                } else {
                    holder.btn_add_tocart.setBackground(context.getResources().getDrawable(R.drawable.roundedbutton));
                }

            } else {
                holder.btn_add_tocart.setEnabled(false);
                holder.btn_add_tocart.setText(context.getString(R.string.out_of_stock_title));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.btn_add_tocart.setBackground(context.getResources().getDrawable(R.drawable.roundedbutton_disabled, null));
                } else {
                    holder.btn_add_tocart.setBackground(context.getResources().getDrawable(R.drawable.roundedbutton_disabled));
                }
            }
        }


       /* }else {

        }
*/
    }

    @Override
    public int getItemCount() {

        if (wishList.size() != 0) {
            return wishList.size();
        } else {
            return 0;
        }

    }


    public class ViewHolder extends RecyclerView.ViewHolder implements ApiServerResponse {
        public TextView text_name, text_price;
        public ImageView image_product;
        public Button btn_add_tocart;
        public ImageView fvt_like;
        LinearLayout ll_wishlist_item;

        public ViewHolder(final View container) {
            super(container);
            text_name = (TextView) container.findViewById(R.id.title_name);
            text_price = (TextView) container.findViewById(R.id.text_desc);
            image_product = (ImageView) container.findViewById(R.id.product_image);
            btn_add_tocart = (Button) container.findViewById(R.id.btn_add_tocart);
            fvt_like = (ImageView) container.findViewById(R.id.fvt_like);
            ll_wishlist_item = (LinearLayout) container.findViewById(R.id.ll_wishlist_item);
            //fvt_like.setTag(R.mipmap.fvlike);


            btn_add_tocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        if (wishList.get(getAdapterPosition()).getIs_cart().equalsIgnoreCase("1")) {
                            Intent i = new Intent(context, CartDetailsNew.class);
                            context.startActivity(i);
                        } else {
                            if (!cart_id.equalsIgnoreCase("0")) {
                                ((Utility) context).showLoading();

                                ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, wishList.get(getAdapterPosition()).getPrice(), String.valueOf(wishList.get(getAdapterPosition()).getId()), "1", user_id, cart_id, "0", ViewHolder.this);
                            } else {
                                ((Utility) context).showLoading();
                                ServerAPI.getInstance().addToCartNew(ApiServerResponse.ADD_TO_CART_NEW, wishList.get(getAdapterPosition()).getPrice(), String.valueOf(wishList.get(getAdapterPosition()).getId()), "1", user_id, "0", "0", ViewHolder.this);
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });


            fvt_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        ((Utility) context).showLoading();
                        ServerAPI.getInstance().removeFromWishlist(ApiServerResponse.REMOVE_FROM_WISHLIST, user_id, String.valueOf(wishList.get(getAdapterPosition()).getId()), ViewHolder.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {


                if (response.isSuccessful()) {
                    AddToCartNewModal addToCartNewModal;
                    RemoveFromWishlistModal removeFromWishlistModal;
                    CartItemCountModal cartItemCountModal;


                    switch (tag) {

                        case ApiServerResponse.ADD_TO_CART_NEW:

                            addToCartNewModal = (AddToCartNewModal) response.body();


                            if (addToCartNewModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                                ToastMsg.showShortToast(context, "Product added to cart.");
                                ServerAPI.getInstance().removeFromWishlist(ApiServerResponse.REMOVE_FROM_WISHLIST, user_id, String.valueOf(wishList.get(getAdapterPosition()).getId()), ViewHolder.this);
                                //refresh(getAdapterPosition());

                                if (Session.getcart_id(pref).equalsIgnoreCase("0")) {
                                    Session.setcart_id(pref, String.valueOf(addToCartNewModal.getCart_info().getId()));
                                }
                                ServerAPI.getInstance().getCartItemCount(ApiServerResponse.CART_ITEM_COUNT, Session.get_userId(pref), Session.getcart_id(pref), ViewHolder.this);
                            }


                            /*else {
                                ToastMsg.showShortToast(context, "Product added to cart.");
                                ServerAPI.getInstance().removeFromWishlist(ApiServerResponse.REMOVE_FROM_WISHLIST, user_id, String.valueOf(wishList.get(getAdapterPosition()).getId()), ViewHolder.this);

                                //refresh(getAdapterPosition());
                            }*/
                            //((Utility) context).hideLoading();
                            break;

                        case ApiServerResponse.CART_ITEM_COUNT:
                            cartItemCountModal = (CartItemCountModal) response.body();

                            if (cartItemCountModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                                EventBus.getDefault().post(new EventBusProductDetailUpdateModal(String.valueOf(cartItemCountModal.getItem_count())));
                                EventBus.getDefault().post(new EventBusCartUpdate(String.valueOf(cartItemCountModal.getItem_count())));
                            }


                            break;

                        case ApiServerResponse.REMOVE_FROM_WISHLIST:

                            removeFromWishlistModal = (RemoveFromWishlistModal) response.body();

                            if (removeFromWishlistModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                                EventBus.getDefault().post(new EventBusCategoryProductsUpdateWishlist(removeFromWishlistModal.getStatus()));
                                //ToastMsg.showShortToast(context, "Product removed from wishlist.");
                                Glide.with(context)
                                        .load(R.drawable.ic_wishlist_not)/*.override(40, 40)*/
                                        .into(fvt_like);
                                refresh(getAdapterPosition());
                            }
                            ((Utility) context).hideLoading();
                            break;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((Utility) context).hideLoading();
        }
    }


    public void refresh(int position) {
//        Log.e("Refreshed Position", "Pos" + position);
        wishList.remove(position);
        notifyItemRemoved(position);
    }

}
