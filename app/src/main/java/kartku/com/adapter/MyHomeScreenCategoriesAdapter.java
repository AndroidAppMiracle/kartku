package kartku.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import kartku.com.R;
import kartku.com.activity.ProductCategoryList;
import kartku.com.retrofit.modal.HomeScreenModal;
import kartku.com.utils.Constant;

/**
 * Created by Kshitiz Bali on 3/14/2017.
 */

public class MyHomeScreenCategoriesAdapter extends RecyclerView.Adapter<MyHomeScreenCategoriesAdapter.ViewHolder> {


    private Context mContext;
    private List<HomeScreenModal.CategoryListBean> categoriesList;
    private List<HomeScreenModal.CategoryListBean> categoryListToShow;

    public MyHomeScreenCategoriesAdapter(Context context, List<HomeScreenModal.CategoryListBean> list, List<HomeScreenModal.CategoryListBean> listToShow) {
        this.mContext = context;
        this.categoriesList = list;
        this.categoryListToShow = listToShow;
      /*  this.mItemsList = itemsList;
        this.mProductsList = productList;
        this.ll_bottomLayout = bottomLayout;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        this.textViewAmount = textView;*/
       /* user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);*/
    }


    @Override
    public MyHomeScreenCategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_categories_home, parent, false);
        return new MyHomeScreenCategoriesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyHomeScreenCategoriesAdapter.ViewHolder holder, int position) {

        try {
           /* if (position < (categoriesList.size() - 1)) {*/
            holder.tv_categories_home_title.setText(categoryListToShow.get(position).getTitle());
            Glide.with(mContext)
                    .load(categoryListToShow.get(position).getImage())
                    .thumbnail(0.1f)
                    .into(holder.iv_categories_home_image);
        /*    }*/


            holder.ll_category_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, ProductCategoryList.class);
                   /* Bundle b = new Bundle();
                    b.putString(Constants.BOOK_ID, String.valueOf(list.get(position).getId()));*/
                    //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                    i.putExtra(Constant.CATEGORY_ID, String.valueOf(categoryListToShow.get(holder.getAdapterPosition()).getId()));
                    mContext.startActivity(i);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (categoryListToShow.size() != 0) {
            return categoryListToShow.size();
        } else {
            return 0;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_categories_home_title;
        public ImageView iv_categories_home_image;
        public LinearLayout ll_category_item;


        public ViewHolder(final View container) {
            super(container);
            tv_categories_home_title = (TextView) container.findViewById(R.id.tv_categories_home_title);
            iv_categories_home_image = (ImageView) container.findViewById(R.id.iv_categories_home_image);
            ll_category_item = (LinearLayout) container.findViewById(R.id.ll_category_item);
        }
    }

}
