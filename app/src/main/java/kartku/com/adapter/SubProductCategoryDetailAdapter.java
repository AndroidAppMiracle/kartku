package kartku.com.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import kartku.com.R;
import kartku.com.activity.CategoryProducts;
import kartku.com.activity.SubSubProductCategoryDetail;
import kartku.com.retrofit.modal.SubProductCategoryListModal;
import kartku.com.utils.Constant;

/**
 * Created by Kshitizb on 15-03-2017.
 */

public class SubProductCategoryDetailAdapter extends RecyclerView.Adapter<SubProductCategoryDetailAdapter.ViewHolder> {


    private Context mContext;
    private List<SubProductCategoryListModal.SubcategoryDetailBean.SubcategoriesBean> subCategoriesList;
    private Context alertContext;

    public SubProductCategoryDetailAdapter(Context context, List<SubProductCategoryListModal.SubcategoryDetailBean.SubcategoriesBean> list) {
        this.mContext = context;
        this.subCategoriesList = list;
        /*this.categoryListToShow = listToShow;*/
      /*  this.mItemsList = itemsList;
        this.mProductsList = productList;
        this.ll_bottomLayout = bottomLayout;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        this.textViewAmount = textView;*/
       /* user_id = Session.get_userId(pref);
        cart_id = Session.getcart_id(pref);*/
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product_category, parent, false);
        alertContext = parent.getContext();
        return new SubProductCategoryDetailAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.lblListItem.setText(subCategoriesList.get(position).getTitle());
        holder.ll_item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (subCategoriesList.get(holder.getAdapterPosition()).getIs_active().equalsIgnoreCase("1")) {
                    if (subCategoriesList.get(position).getIs_subcategory().equalsIgnoreCase("1")) {

                        //has SubCategory
                        Intent i = new Intent(mContext, SubSubProductCategoryDetail.class);
                        //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                        i.putExtra(Constant.CATEGORY_ID, String.valueOf(subCategoriesList.get(holder.getAdapterPosition()).getId()));
                        mContext.startActivity(i);

                    } else {
                        //has SubCategory
                        Intent i = new Intent(mContext, CategoryProducts.class);
                        //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                        i.putExtra(Constant.CATEGORY_ID, String.valueOf(subCategoriesList.get(holder.getAdapterPosition()).getId()));
                        i.putExtra(Constant.CATEGORY_NAME, subCategoriesList.get(holder.getAdapterPosition()).getTitle());
                        mContext.startActivity(i);


                    }
                } else {
                    showAlertDialog(alertContext);
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        if (subCategoriesList.size() != 0) {
            return subCategoriesList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView lblListItem;
        RelativeLayout ll_item_layout;


        public ViewHolder(final View container) {
            super(container);
            lblListItem = (TextView) container.findViewById(R.id.lblListItem);
            ll_item_layout = (RelativeLayout) container.findViewById(R.id.ll_item_layout);
        }
    }


    public void showAlertDialog(Context context) {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
            //dialogBuilder.setTitle("Connection Problem");
            dialogBuilder.setMessage(mContext.getString(R.string.products_not_available));
            dialogBuilder.setCancelable(true);
           /* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*/
            dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                }
            });
           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
