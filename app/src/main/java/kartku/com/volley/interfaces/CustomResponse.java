package kartku.com.volley.interfaces;

/**
 * Created by CGPL-17 [satuti] on 5/20/2016.
 */
public interface CustomResponse {

    void onCustomResponse(String response);
    void onCustomError(String error);
}
