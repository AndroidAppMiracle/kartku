package kartku.com.volley;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.Map;

import kartku.com.utils.API;
import kartku.com.utils.RequestConstants;
import kartku.com.volley.interfaces.CustomResponse;


/**
 * Created by CGPL-17 [satuti] on 5/20/2016.
 */
public class RequestServer implements Response.ErrorListener, Response.Listener {


    private RequestQueue request_queue;
    private Context context;
    private int requestId;
    Map<String, String> params;
    private String TAG = "RequestServer";
    CustomResponse listener;

    public RequestServer(Context context, int requestId, Map<String, String> params, CustomResponse listener, String url_type) {
        this.context = context;
        this.requestId = requestId;
        this.params = params;
        this.listener = listener;
        postvolleyRequest();


    }

    public RequestServer(Context context, int requestId, CustomResponse listener, String params) {
        this.context = context;
        this.requestId = requestId;
        this.listener = listener;
        getVolleyRequest(params);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("" + getClass(), TAG.concat("onErrorResponse error *****  ") + error);
        listener.onCustomError("" + error);
    }

    @Override
    public void onResponse(Object response) {
        Log.d("" + getClass(), TAG.concat("onResponse response *****  ") + response);
        listener.onCustomResponse("" + response);
    }


    private void postvolleyRequest() {
        request_queue = CustomVolleyRequestQueue.getInstance(context.getApplicationContext()).getRequestQueue();
        String url = requestURL(requestId);
        Log.d("" + getClass(), TAG.concat("volleyRequest post url *****  ") + url);
        CustomStringRequest string_request = new CustomStringRequest(Request.Method.POST, url, params, this, this);
        /**
         * volley time out error handling
         */
        string_request.setRetryPolicy(new DefaultRetryPolicy(120000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        string_request.setShouldCache(false);
        request_queue.add(string_request);
    }

    private void getVolleyRequest(String params) {
        request_queue = CustomVolleyRequestQueue.getInstance(context.getApplicationContext()).getRequestQueue();
        String url = getRequestURL(requestId, params);
        Log.d("" + getClass(), TAG.concat("volleyRequest get url *****  ") + url);
        CustomStringRequest string_request = new CustomStringRequest(Request.Method.GET, url, this, this);

        /**
         * volley time out error handling
         */
        string_request.setRetryPolicy(new DefaultRetryPolicy(120000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        string_request.setShouldCache(false);
        request_queue.add(string_request);
    }

    // method for both get and post urls without params
    private String requestURL(int requestId) {
        String URL = "";
        switch (requestId) {
            case 1:
                URL = API.HOST_URL.concat(API.USER_LOGIN);
                return URL;
            case 2:
                URL = API.HOST_URL.concat(API.USER_REGISTER);
                return URL;
            case 5:
                URL = API.HOST_URL.concat(API.CONTACT_US);
            case 6:
                URL = API.HOST_URL.concat(API.UPDATE_USER);
                return URL;
            case 7:
                URL = API.HOST_URL.concat(API.CHANGE_PASSWORD);
                return URL;
            case 8:
                URL = API.HOST_URL.concat(API.FORGOT_PASSWORD);
                return URL;
            case 9:
                URL = API.HOST_URL.concat(API.CATEGORY_PRODUCTS);
                return URL;
            case 11:
                URL = API.HOST_URL.concat(API.SOCIAL_MEDIA_LOGIN);
                return URL;
            case 12:
                URL = API.HOST_URL.concat(API.ORDER_DETAIL);
                return URL;
            case 13:
                URL = API.HOST_URL.concat(API.ORDER_HISTORY);
                return URL;
            case 14:
                URL = API.HOST_URL.concat(API.WISHLIST);
                return URL;
            case 15:
                URL = API.HOST_URL.concat(API.ADD_TO_WISHLIST);
                return URL;
            case 16:
                URL = API.HOST_URL.concat(API.ADD_TO_CART);
                return URL;
            case 17:
                URL = API.HOST_URL.concat(API.UPDATE_CART);
                return URL;
            case 18:
                URL = API.HOST_URL.concat(API.CART_DELETE);
                return URL;
            case 20:
                URL = API.HOST_URL.concat(API.DELETE_ITEM);
                return URL;
            case 21:
                URL = API.HOST_URL.concat(API.PRODUCT_REVIEW_RATING);
                return URL;
            case 24:
                URL = API.HOST_URL.concat(API.REMOVE_FROM_WISHLIST);
                return URL;
            default:
                return URL;
        }
    }

    // method for get request url with params
    private String getRequestURL(int requestId, String params) {
        String URL = "";
        switch (requestId) {
            case 3:
                URL = API.HOST_URL.concat(API.HOME_BANNERS);
                return URL;
            case 4:
                URL = API.HOST_URL.concat(API.MAIN_CATEGORIES);
                return URL;
            case 10:
                URL = API.HOST_URL.concat(API.PRODUCT_DETAIL).concat(params);
                return URL;
            case 19:
                URL = API.HOST_URL.concat(API.CART_DETAIL).concat(params);
                return URL;
            case 22:
                URL = API.HOST_URL.concat(API.PRODUCT_REVIEW).concat(params);
                return URL;
            case 23:
                URL = API.HOST_URL.concat(API.PRODUCT_SEARCH).concat(params);
                return URL;
            case 25:
                URL = API.HOST_URL.concat(API.BRAND_LISTING);
                return URL;
            case 26:
                URL = API.HOST_URL.concat(API.MATERIAL_LISTING);
                return URL;
            case 27:
                URL = API.HOST_URL.concat(API.HIGH_LOW_PRICE).concat(params);
                return URL;
            case 28:
                URL = API.HOST_URL.concat(API.LOW_HIGH_PRICE).concat(params);
                return URL;
            case 29:
                URL = API.HOST_URL.concat(API.NEW_ARRIVALS).concat(params);
                return URL;
            case 30:
                URL = API.HOST_URL.concat(API.CART_ITEMS).concat(params);
                return URL;
            case 31:
                URL = API.HOST_URL.concat(API.MY_ORDERS).concat(params);
            default:
                return URL;
        }
    }
}
