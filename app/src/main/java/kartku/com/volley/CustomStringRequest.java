package kartku.com.volley;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.utils.RequestConstants;

/**
 * Created by CGPL-17 [satuti] on 5/19/2016.
 */
public class CustomStringRequest extends StringRequest {

    private Map<String, String> params;

    public CustomStringRequest(int method, String url, Map<String, String> params, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url,listener, errorListener);
        this.params = params;
    }

    public CustomStringRequest(int method, String url,  Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url,listener, errorListener);
    }

    @Override
    protected Map<String, String> getParams() {
            /**
             * send data to server.
             */
        return params;
    }

}
