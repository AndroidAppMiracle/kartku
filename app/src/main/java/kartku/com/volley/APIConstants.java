package kartku.com.volley;

/**
 * Created by CGPL-17 [satuti] on 5/19/2016.
 * class contains all the constants required to get and send data to server
 * and used for volley lib.
 */
public class APIConstants {

    /**
     * Local host address
     */
    //public static String HOST="http://192.168.21.138:3000/fpAttendance/";

    /**
     * server address
      */
    public static String HOST="https://mobile.feetport.com/fpAttendance/";

    /**
     * API s used in Biometric app
     */

    /**
     * to register new user.
     */
    public static String UserRegistraion="userRegistration";
    public static Integer REQUEST_ID_REGISTERATION = 1 ;
    /**
     * get list of all users to download
     */
    public static String GetAllUsers = "getUsersListToDownload";
    public static int REQUEST_ID_ALL_USERS = 2 ;
    /**
     * get downloaded and selected users list and data from server.
     */
    public static String GetPublicUserList = "downloadUsers";
    public static int REQUEST_ID_DOWNLOAD_USERS = 3 ;
    /**
     * to sync attendance to server.
     */
    public static String PostAttendaceToServer="syncAttendance";
    public static int REQUEST_ID_SYNC_ATTENDANCE = 4 ;
    /**
     * get detailed attendance list of slected user's.
     */
    public static String EmployeeAttendanceDetail="getUserAttendance";
    public static int REQUEST_ID_GET_ATTENDANCE = 5 ;

}
