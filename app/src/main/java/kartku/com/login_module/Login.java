package kartku.com.login_module;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import kartku.com.R;
import kartku.com.dashboard_module.DemoHomeDashBoard;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.login_module.forgot_password.ForgotPassword;
import kartku.com.login_module.listener.LoginListener;
import kartku.com.login_module.social_login.listener.SocialLoginListener;
import kartku.com.registeration_module.Register;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.LoginModal;
import kartku.com.retrofit.modal.SocialUserLoginModal;
import kartku.com.sellermodule.SellerHomeDashBoard;
import kartku.com.utils.Constant;
import kartku.com.utils.NetworkConnection;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

public class Login extends Utility implements View.OnClickListener, LoginListener, SocialLoginListener, GoogleApiClient.OnConnectionFailedListener, ApiServerResponse {

    private EditText user_email, password;
    //private Button login;
    // private LoginButton facebook;
    //private SignInButton google;
    private TextView forget_password;
    private LinearLayout signup_here;
    private ProgressBar progressBar;
    private SharedPreferences pref;
    boolean mResolveOnFail;


    private int RC_SIGN_IN = 2;
    private GoogleApiClient mGoogleApiClient;
    private RelativeLayout rl_submit, rl_fb_login, rl_google_login;
    LoginButton loginButton;

    // for facebook
    CallbackManager callbackManager;
    String email;
    String firstName = "", lastName = "", fullGoogleName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FacebookSdk.sdkInitialize(Login.this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.secondary_content_login);
        pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");


        try {
            // find id's
            user_email = (EditText) findViewById(R.id.username);
            password = (EditText) findViewById(R.id.password);
            //login = (Button) findViewById(R.id.btn_login);
            //facebook = (LoginButton) findViewById(R.id.btn_facebook);
            //google = (SignInButton) findViewById(R.id.btn_google);
            //google.setSize(SignInButton.SIZE_STANDARD);
            forget_password = (TextView) findViewById(R.id.forget_password);
            signup_here = (LinearLayout) findViewById(R.id.signup_wrapper);
            rl_fb_login = (RelativeLayout) findViewById(R.id.rl_fb_login);
            progressBar = (ProgressBar) findViewById(R.id.progress_bar);
            rl_submit = (RelativeLayout) findViewById(R.id.rl_submit);
            rl_google_login = (RelativeLayout) findViewById(R.id.rl_google_login);

            // set listeners
            rl_submit.setOnClickListener(this);
            // facebook.setOnClickListener(this);
            //google.setOnClickListener(this);
            forget_password.setOnClickListener(this);
            signup_here.setOnClickListener(this);
            rl_fb_login.setOnClickListener(this);
            rl_google_login.setOnClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {

                case R.id.rl_submit:
                    try {

                        if (NetworkConnection.checkInternetConnection(Login.this)) {

                            //ToastMsg.showLongToast(Login.this, Session.getSessionToken(pref));

                            if (validateFields(user_email.getText().toString(), password.getText().toString())) {
                                progressBar.setVisibility(View.VISIBLE);
                                Session.setLoginTypeButton(pref, "email");
                                Utility.hideSoftKeyboard(Login.this);
                                if (Session.getSessionToken(pref).equalsIgnoreCase("")) {
                                    Session.setSessionToken(pref, FirebaseInstanceId.getInstance().getToken());

                                }
                                ServerAPI.getInstance().userLogin(ApiServerResponse.USER_LOGIN, user_email.getText().toString(), password.getText().toString(), Session.getSessionToken(pref), this);
                            }

                            //new kartku.com.login_module.manager.LoginManager(Login.this, this).sendRequest(user_email.getText().toString(), password.getText().toString());
                        } else {

                            showAlertDialog(Login.this);

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.rl_fb_login:

                    try {

                        if (NetworkConnection.checkInternetConnection(Login.this)) {
                            Session.setLoginTypeButton(pref, "facebook");
                            loginFacebook();
                        } else {
                            showAlertDialog(Login.this);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.rl_google_login:
                    if (NetworkConnection.checkInternetConnection(Login.this)) {
                        Session.setLoginTypeButton(pref, "google");

                        signIn();
                    } else {
                        showAlertDialog(Login.this);
                    }

                    break;
                case R.id.btn_facebook:
                    try {

                        if (NetworkConnection.checkInternetConnection(Login.this)) {
                            Session.setLoginTypeButton(pref, "facebook");
                            loginFacebook();
                        } else {
                            showAlertDialog(Login.this);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.btn_google:

                    if (NetworkConnection.checkInternetConnection(Login.this)) {
                        Session.setLoginTypeButton(pref, "google");

                        signIn();
                    } else {
                        showAlertDialog(Login.this);
                    }

                    break;
                case R.id.forget_password:
                    try {
                        Intent intent = new Intent(Login.this, ForgotPassword.class);
                        startActivity(intent);
                        //finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.signup_wrapper:
                    try {
                        Intent intent = new Intent(Login.this, Register.class);
                        startActivity(intent);
                        //finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.login_button:

                    loginFacebook();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validateFields(String username, String password) {
        if (username.length() <= 0) {
            hideSoftKeyboard(Login.this);
            ToastMsg.showShortToast(Login.this, getResources().getString(R.string.user_email));
            return false;
        } else if (username.length() > 0 && !Validation.validateEmail(username)) {
            hideSoftKeyboard(Login.this);
            ToastMsg.showShortToast(Login.this, getResources().getString(R.string.email_validation));
            return false;
        } else if (password.length() <= 0) {
            hideSoftKeyboard(Login.this);
            ToastMsg.showShortToast(Login.this, getResources().getString(R.string.password));
            return false;
        } else {
            return true;
        }

    }


    private void loginFacebook1() {
        FacebookSdk.sdkInitialize(Login.this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("Success", "Login");
            }

            @Override
            public void onCancel() {
                Log.d("onCancel", "Login");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("onError", "Login");
            }
        });
    }


    private void loginFacebook() {

       /* FacebookSdk.sdkInitialize(Login.this);
        callbackManager = CallbackManager.Factory.create();*/
        Session.setLoginTypeButton(pref, "facebook");
        //LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(Login.this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code

                                if (response != null) {
                                    try {
                                        String mFbid = object.getString("id");
                                        String name = object.getString("name");
                                        String firstName = object.getString("first_name");
                                        String lastName = object.getString("last_name");
                                        if (!object.has("email")) {
                                            email = showEditAlert(Login.this, firstName, lastName, name);
                                            // registerSocialLogin(email,firstName,lastName,name);

                                        } else {
                                            email = object.getString("email");
                                            registerSocialLogin(email, firstName, lastName, name);
                                        }
                                        // Log.i("F and L Name ", fName + " " + lName);

                                        //Toast.makeText(Login.this, name + "---" + email + " " + mFbid, Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }


                                // Toast.makeText(Login.this,""+response,Toast.LENGTH_LONG).show();


                                Log.d("----fb response----", "" + response);

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }


            @Override
            public void onCancel() {
                Log.d("onCancel", "Login");
                // Toast.makeText(Login.this,"on cancel",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("onError", "Login");
                // Toast.makeText(Login.this,"on Error",Toast.LENGTH_LONG).show();
            }
        });
    }


    // override methods of login listener

    @Override
    public void onError(String error) {
        try {
            progressBar.setVisibility(View.GONE);
            ToastMsg.showShortToast(Login.this, error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(String message) {
        try {
            progressBar.setVisibility(View.GONE);
            ToastMsg.showShortToast(Login.this, message);
            Session.set_login_status(pref, true);
            /*Intent intent = new Intent(Login.this, Home_dashboard.class);
            startActivity(intent);
            finish();*/

            if (Session.getUserRoleType(pref).equals(Constant.ROLE_USER)) {
                Intent intent = new Intent(Login.this, DemoHomeDashBoard.class);
            /*Intent intent = new Intent(getActivity(), Home_dashboard.class);*/
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(Login.this, SellerHomeDashBoard.class);
            /*Intent intent = new Intent(getActivity(), Home_dashboard.class);*/
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
            /*Intent intent = new Intent(Login.this, Home_dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSocialSuccess(String response) {
        progressBar.setVisibility(View.GONE);
        ToastMsg.showShortToast(this, response);
        Session.set_login_status(pref, true);
        Intent intent = new Intent(Login.this, DemoHomeDashBoard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    //From  Fragment

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK) {
            mResolveOnFail = true;
            mGoogleApiClient.connect();
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } *//*else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }*//*
    }*/

    public void loginWithGoogle() {
        try {
            // Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                /*.enableAutoManage(Login.this *//* FragmentActivity *//*, Login.this *//* OnConnectionFailedListener *//*)*/
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
            //mGoogleApiClient.connect();
        /*revokeAccess();*/
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("GoogleSignIn", "ConnectionFailed");
        // Most of the time, the connection will fail with a
        // user resolvable result. We can store that in our
        // mConnectionResult property ready for to be used
        // when the user clicks the sign-in button.
        if (connectionResult.hasResolution()) {
            ConnectionResult mConnectionResult = connectionResult;
            if (mResolveOnFail) {
                // This is a local helper function that starts
                // the resolution of the problem, which may be
                // showing the user an account chooser or similar.
                //startResolution();
            }
        }
    }

    private void signIn() {

        try {
            loginWithGoogle();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if (!acct.getDisplayName().isEmpty()) {
                String name = acct.getDisplayName();
                fullGoogleName = acct.getDisplayName();
                String arr[] = name.split(" ");

                if (arr.length != 1) {
                    firstName = arr[0];
                } else {
                    firstName = arr[0];
                    lastName = arr[1];
                }


                Session.setGooglName(pref, name);
            }

            String email = acct.getEmail();
            if (acct.getPhotoUrl() != null) {
                String photoUrl = acct.getPhotoUrl().toString();
                Log.i("PhotoUri", "" + photoUrl);
                Session.set_google_signin_user_profile_pic(pref, photoUrl);
            }
            /*GoogleSignInAccount acct = result.getSignInAccount();
            String name = acct.getDisplayName();
            String email = acct.getEmail();

            if (acct.getPhotoUrl() != null) {
                String photoUrl = acct.getPhotoUrl().toString();
                Log.i("PhotoUri", "" + photoUrl);
                Session.set_google_signin_user_profile_pic(pref, photoUrl);
            }
*/

            registerSocialLogin(email, firstName, lastName, fullGoogleName);
        }
    }

    /*public void callSocialApi(String email) {
        progressBar.setVisibility(View.VISIBLE);
        new kartku.com.login_module.social_login.manager.SocialLoginManager(this, Login.this).sendRequest(email);
    }*/

    public void registerSocialLogin(String email, String firstName, String lastName, String name) {
        try {
            progressBar.setVisibility(View.VISIBLE);

            if (Session.getSessionToken(pref).equalsIgnoreCase("")) {
                Session.setSessionToken(pref, FirebaseInstanceId.getInstance().getToken());

            }
            ServerAPI.getInstance().userSocialLogin(ApiServerResponse.USER_SOCIAL_LOGIN, email, firstName, lastName, name, Session.getSessionToken(pref), this);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Login.this, DemoHomeDashBoard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

   /* public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }*/


   /* public void callSocialApiGoogle(String email, String displayName) {
        progressBar.setVisibility(View.VISIBLE);
        new kartku.com.login_module.social_login.manager.SocialLoginManager(this, Login.this).sendRequest(email, displayName);
    }*/

    /*@Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();

    }*/

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            //mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            LoginModal loginModal;
            SocialUserLoginModal socialUserLoginModal;

            switch (tag) {
                case ApiServerResponse.USER_LOGIN:

                    loginModal = (LoginModal) response.body();
                    if (loginModal.getStatus().equalsIgnoreCase(Constant.OK)) {
                        progressBar.setVisibility(View.GONE);
                        ToastMsg.showShortToast(Login.this, loginModal.getMessage());
                        Session.set_login_status(pref, true);
                        Session.setLogin_type(pref, "email");

                        //ToastMsg.showLongToast(Login.this, String.valueOf(loginModal.getUser_data().getUser_id()));

                        Session.set_userId(pref, String.valueOf(loginModal.getUser_data().getUser_id()));
                        Session.setUserRoleType(pref, loginModal.getUser_data().getRole());
                        Session.setFirstName(pref, loginModal.getUser_data().getFirst_name());
                        Session.setLastName(pref, loginModal.getUser_data().getLast_name());
                        Session.setUserEmail(pref, loginModal.getUser_data().getEmail());
                        Session.setUserMobile(pref, loginModal.getUser_data().getMobile_number());
                        Session.setAddress(pref, loginModal.getUser_data().getAddress());
                        Session.setAppLanguage(pref, loginModal.getUser_data().getLanguage());


            /*Intent intent = new Intent(Login.this, Home_dashboard.class);
            startActivity(intent);
            finish();*/

                        if (loginModal.getUser_data().getRole().equalsIgnoreCase(Constant.ROLE_USER)) {
                            Intent intent = new Intent(Login.this, DemoHomeDashBoard.class);
            /*Intent intent = new Intent(getActivity(), Home_dashboard.class);*/
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(Login.this, SellerHomeDashBoard.class);
            /*Intent intent = new Intent(getActivity(), Home_dashboard.class);*/
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        ToastMsg.showShortToast(Login.this, loginModal.getMessage());
                    }


                    break;
                case ApiServerResponse.USER_SOCIAL_LOGIN:

                    socialUserLoginModal = (SocialUserLoginModal) response.body();
                    if (socialUserLoginModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                        Session.setAppLanguage(pref, socialUserLoginModal.getUser_profile_data().getLanguage());

                        if (Session.getLoginTypeButton(pref).equalsIgnoreCase("google")) {
                            Session.set_userId(pref, String.valueOf(socialUserLoginModal.getUser_profile_data().getUser_id()));
                            Session.setLogin_type(pref, "google");
                            Session.setGooglEmailID(pref, socialUserLoginModal.getUser_profile_data().getEmail());

                        } else if (Session.getLoginTypeButton(pref).equalsIgnoreCase("facebook")) {
                            Session.set_userId(pref, String.valueOf(socialUserLoginModal.getUser_profile_data().getUser_id()));
                            Session.setLogin_type(pref, "facebook");
                            Session.set_facebookName(pref, socialUserLoginModal.getUser_profile_data().getUsername());
                            Session.set_facebookEmail(pref, socialUserLoginModal.getUser_profile_data().getEmail());
                        }

                        progressBar.setVisibility(View.GONE);
                        ToastMsg.showShortToast(Login.this, socialUserLoginModal.getMessage());
                        Session.set_login_status(pref, true);
                        Intent intent = new Intent(Login.this, DemoHomeDashBoard.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        ToastMsg.showShortToast(Login.this, socialUserLoginModal.getMessage());
                    }

                    break;
            }

            /*Intent intent = new Intent(Login.this, Home_dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        progressBar.setVisibility(View.GONE);
    }


   /* private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Toast.makeText(Login.this, "StatusRevoke " + status.toString(), Toast.LENGTH_LONG).show();
                        // ...
                    }
                });
    }*/
}
