package kartku.com.login_module.listener;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public interface LoginListener {

    void onError(String error);
    void onSuccess(String message);
}
