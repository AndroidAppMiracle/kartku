package kartku.com.login_module.forgot_password.manager;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.login_module.forgot_password.api.ForgotPasswordApi;
import kartku.com.login_module.forgot_password.listener.ForgotPasswordListener;
import kartku.com.profile.api.ProfileApi;
import kartku.com.profile.listener.ProfileListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;

/**
 * Created by satoti.garg on 9/14/2016.
 */
public class ForgotPasswordManager {

    private Context context;
    private ForgotPasswordListener listener;

    public ForgotPasswordManager(Context context, ForgotPasswordListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void sendRequest(String email) {
        if (validateFields(email)) {
            try {
                Map<String, String> params = new HashMap<>();
                try {
                    try {
                        params.put(RequestConstants.EMAIL, "" + email);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new ForgotPasswordApi(ForgotPasswordManager.this, context).sendRequest(API.FORGOT_PASSWORD_PASSWORD_REQUEST_ID, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validateFields(String email){
         if (email.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.email));
            return false;
        } else if (email.length() > 0 && !Validation.validateEmail(email)) {
             listener.onError(context.getResources().getString(R.string.email_validation));
             return false;
         }
        else {
            return true;
        }

    }


    public void onSuccess(String response) {
        try {
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if(status.equalsIgnoreCase(Constant.OK))
            {
                String message = res_object.getString(Constant.MESSAGE);
                listener.onSuccess(message);

            }else{
                String message = res_object.getString(Constant.MESSAGE);
                listener.onError(message);
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void onError(String message) {

        listener.onError(message);
    }
}
