package kartku.com.login_module.forgot_password.listener;

/**
 * Created by satoti.garg on 9/14/2016.
 */
public interface ForgotPasswordListener {

    void onSuccess(String response);
    void onError(String error);
}
