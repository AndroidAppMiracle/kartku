package kartku.com.login_module.forgot_password;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kartku.com.R;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.login_module.Login;
import kartku.com.login_module.forgot_password.listener.ForgotPasswordListener;
import kartku.com.login_module.forgot_password.manager.ForgotPasswordManager;
import kartku.com.retrofit.ApiServerResponse;
import kartku.com.retrofit.ServerAPI;
import kartku.com.retrofit.modal.ForgotPasswordModal;
import kartku.com.utils.Constant;
import kartku.com.utils.NetworkConnection;
import kartku.com.utils.ToastMsg;
import kartku.com.utils.Utility;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;
import retrofit2.Response;

public class ForgotPassword extends Utility implements View.OnClickListener, /*ForgotPasswordListener*/ ApiServerResponse {

    EditText email_id;
    Button submit;

    private SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        try {

            pref = new ObscuredSharedPreferences(getApplicationContext(), getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.title_activity_forgot_password);


            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ForgotPassword.this.finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        // find id's

        email_id = (EditText) findViewById(R.id.forgot_email);
        submit = (Button) findViewById(R.id.btn_forgot_pwd);
        submit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_forgot_pwd:
                try {
                    if (NetworkConnection.checkInternetConnection(ForgotPassword.this)) {

                        if (email_id.getText().toString().length() > 0 && !Validation.validateEmail(email_id.getText().toString())) {
                            ToastMsg.showShortToast(ForgotPassword.this, getResources().getString(R.string.email_validation));
                        } else {
                            ServerAPI.getInstance().forgotPassword(ApiServerResponse.FORGOR_PASSWORD, Session.get_userId(pref), email_id.getText().toString(), this);
                        }
                        /*new ForgotPasswordManager(ForgotPassword.this, this).sendRequest(email_id.getText().toString());*/
                    } else {
                        ToastMsg.showShortToast(ForgotPassword.this, getString(R.string.check_internet_connection));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

/*    @Override
    public void onError(String error) {
        ToastMsg.showShortToast(ForgotPassword.this, error);
    }

    @Override
    public void onSuccess(String response) {
        try {
            ToastMsg.showLongToast(ForgotPassword.this, response);
            finish();
            *//*Intent intent = new Intent(ForgotPassword.this, Login.class);
            startActivity(intent);
            finish();*//*
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
       /* switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                    Intent intent = new Intent(ForgotPassword.this, Login.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }*/
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {

                ForgotPasswordModal forgotPasswordModal;

                switch (tag) {

                    case ApiServerResponse.FORGOR_PASSWORD:
                        forgotPasswordModal = (ForgotPasswordModal) response.body();

                        if (forgotPasswordModal.getStatus().equalsIgnoreCase(Constant.OK)) {

                            ToastMsg.showLongToast(ForgotPassword.this, forgotPasswordModal.getMessage());
                        } else {
                            ToastMsg.showLongToast(ForgotPassword.this, forgotPasswordModal.getMessage());
                        }


                        break;
                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

        throwable.printStackTrace();
    }
}
