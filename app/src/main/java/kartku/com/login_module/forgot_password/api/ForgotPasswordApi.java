package kartku.com.login_module.forgot_password.api;

import android.content.Context;

import java.util.Map;

import kartku.com.login_module.forgot_password.manager.ForgotPasswordManager;
import kartku.com.profile.manager.ProfileManager;
import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/14/2016.
 */
public class ForgotPasswordApi implements CustomResponse {

    ForgotPasswordManager manager ;
    Context context;
    RequestServer request_server;

    public ForgotPasswordApi(ForgotPasswordManager manager , Context context)
    {
        this.manager = manager;
        this.context = context;
    }

    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId ,  Map<String, String> params){
        try {
            request_server = new RequestServer(context, requestId, params,this , RequestConstants.POST_URL);
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }


}
