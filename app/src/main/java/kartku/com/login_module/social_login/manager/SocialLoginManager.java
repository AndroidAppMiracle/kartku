package kartku.com.login_module.social_login.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.login_module.api.LoginAPI;
import kartku.com.login_module.listener.LoginListener;
import kartku.com.login_module.social_login.api.SocialLoginApi;
import kartku.com.login_module.social_login.listener.SocialLoginListener;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public class SocialLoginManager {
    private Context context;
    private SocialLoginListener listener;
    SharedPreferences pref;

    public SocialLoginManager(Context context, SocialLoginListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    public void sendRequest(String emailid) {
        try {
            Map<String, String> params = new HashMap<>();
            try {
                try {
                    params.put(RequestConstants.EMAIL, "" + emailid);
                    params.put(RequestConstants.DEVICE_ID, Session.getSessionToken(pref));
                    params.put(RequestConstants.DEVICE_TYPE, "ANDROID");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new SocialLoginApi(context, SocialLoginManager.this).sendRequest(API.SOCIAL_LOGIN_REQUEST_ID, params);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onSuccess(String response) {


        try {

            String loginTypeButton = Session.getLoginTypeButton(pref);
            if (loginTypeButton.equals("facebook")) {
                Log.e("Response Imp", response);
                JSONObject res_object = new JSONObject(response);
                String status = res_object.getString(Constant.STATUS1);
                if (status.equalsIgnoreCase(Constant.OK)) {
                    String message = res_object.getString(Constant.MESSAGE);
                    Log.e("Response Msg", message);
                    JSONObject user_profile_data = res_object.getJSONObject(Constant.USER_PROFILE_DATA);
                    Session.setlogin_user_details(pref, "" + user_profile_data);

                    String user_id = user_profile_data.getString(Constant.USER_ID);
                    Log.e("Response User_Id", user_id);
                    Session.set_userId(pref, user_id);
                    Session.setLogin_type(pref, "facebook");
                    Session.set_facebookName(pref, user_profile_data.getString(Constant.USERNAME));
                    Session.set_facebookEmail(pref, user_profile_data.getString(Constant.EMAIL));

                    listener.onSocialSuccess(response);

                } else {
                    String message = res_object.getString(Constant.MESSAGE);
                    listener.onError(message);
                }
            } else if (loginTypeButton.equals("google")) {
                Log.e("Response Imp", response);
                JSONObject res_object = new JSONObject(response);
                String status = res_object.getString(Constant.STATUS1);

                if (status.equalsIgnoreCase(Constant.OK)) {
                    String message = res_object.getString(Constant.MESSAGE);
                    Log.e("Response Msg", message);
                    JSONObject user_profile_data = res_object.getJSONObject(Constant.USER_PROFILE_DATA);
                    Session.setlogin_user_details(pref, "" + user_profile_data);

                    String user_id = user_profile_data.getString(Constant.USER_ID);
                    Log.e("Response User_Id", user_id);
                    Session.set_userId(pref, user_id);
                    Session.setLogin_type(pref, "google");

                    listener.onSocialSuccess(response);

                } else {
                    String message = res_object.getString(Constant.MESSAGE);
                    listener.onError(message);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onError(String message) {
        listener.onError(message);
    }

}
