package kartku.com.login_module.social_login.listener;

/**
 * Created by satoti.garg on 9/15/2016.
 */
public interface SocialLoginListener {

    void onSocialSuccess(String response);
    void onError(String error);
}
