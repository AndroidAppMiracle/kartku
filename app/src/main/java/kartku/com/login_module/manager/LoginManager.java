package kartku.com.login_module.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kartku.com.R;
import kartku.com.login_module.api.LoginAPI;
import kartku.com.login_module.listener.LoginListener;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.utils.Validation;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public class LoginManager {

    private Context context;
    private LoginListener listener;
    SharedPreferences pref;

    public LoginManager(Context context, LoginListener listener) {
        this.context = context;
        this.listener = listener;
        pref = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }

    public void sendRequest(String username, String password) {
        if (validateFields(username, password)) {
            try {
                Map<String, String> params = new HashMap<>();
                try {
                    try {
                        //String deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                        params.put(RequestConstants.EMAIL, "" + username);
                        params.put(RequestConstants.PASSWORD, "" + password);
                        params.put(RequestConstants.DEVICE_ID, Session.getSessionToken(pref));
                        params.put(RequestConstants.DEVICE_TYPE, "1");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new LoginAPI(LoginManager.this, context).sendRequest(1, params);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validateFields(String username, String password) {
        if (username.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.user_email));
            return false;
        } else if (username.length() > 0 && !Validation.validateEmail(username)) {
            listener.onError(context.getResources().getString(R.string.email_validation));
            return false;
        } else if (password.length() <= 0) {
            listener.onError(context.getResources().getString(R.string.password));
            return false;
        } else {
            return true;
        }

    }

    public void onSuccess(String response) {
        try {
            Log.e("Response Imp", response);
            JSONObject res_object = new JSONObject(response);
            String status = res_object.getString(Constant.STATUS);
            if (status.equalsIgnoreCase(Constant.OK)) {
                String message = res_object.getString(Constant.MESSAGE);

                JSONObject user_data = res_object.getJSONObject(Constant.USER_DATA);
                Session.setlogin_user_details(pref, "" + user_data);

                String user_id = user_data.getString(Constant.USER_ID);
                String userRole = user_data.getString(Constant.ROLE);
                String userFirstName = user_data.getString(Constant.FIRST_NAME);
                String userLastName = user_data.getString(Constant.LAST_NAME);
                String userEmail = user_data.getString(Constant.EMAIL);
                String userAddress = user_data.getString(Constant.ADDRESS);
                String userMobile = user_data.getString(Constant.MOBILE_NUMBER);
                Session.set_userId(pref, user_id);
                Session.setLogin_type(pref, "email");
                Session.setUserRoleType(pref, userRole);
                Session.setFirstName(pref, userFirstName);
                Session.setLastName(pref, userLastName);
                Session.setUserEmail(pref, userEmail);
                Session.setUserMobile(pref, userMobile);
                Session.setAddress(pref, userAddress);

                listener.onSuccess(message);

            } else {
                String message = res_object.getString(Constant.MESSAGE);
                listener.onError(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onError(String message) {
        listener.onError(message);
    }
}
