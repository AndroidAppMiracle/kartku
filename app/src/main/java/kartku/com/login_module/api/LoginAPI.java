package kartku.com.login_module.api;

import android.content.Context;


import org.json.JSONObject;

import java.util.Map;

import kartku.com.login_module.manager.LoginManager;
import kartku.com.utils.Constant;
import kartku.com.utils.RequestConstants;
import kartku.com.volley.RequestServer;
import kartku.com.volley.interfaces.CustomResponse;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public class LoginAPI implements CustomResponse {
    LoginManager manager;
    Context context;
    RequestServer request_server;

    public LoginAPI(LoginManager manager , Context context)
    {
        this.manager = manager;
        this.context = context;
    }


    @Override
    public void onCustomResponse(String response) {
        manager.onSuccess(response);
    }

    @Override
    public void onCustomError(String error) {
        manager.onError(error);
    }

    /**
     * request server
     */

    public void sendRequest(int requestId ,  Map<String, String> params){
        try {
            request_server = new RequestServer(context, requestId, params,this , RequestConstants.POST_URL);
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
