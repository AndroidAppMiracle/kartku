package kartku.com.faq_module;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kartku.com.R;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.product_review.ProductReview;
import kartku.com.utils.API;
import kartku.com.utils.Constant;
import kartku.com.utils.Utility;

public class FAQ extends Utility {
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_faq);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(FAQ.this, Home_dashboard.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                //startActivity(intent);
                finish();
            }
        });


        webview = (WebView) findViewById(R.id.web_view);
        try {
            loadUrlWithWebView(Constant.FAQ_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadUrlWithWebView(String url) {
        try {
            webview.setWebViewClient(webViewClient);
            webview.loadUrl(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // Loading started for URL
            showLoading();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // Redirecting to URL
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // Loading finished for URL
            hideLoading();
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                    Intent intent = new Intent(FAQ.this, Home_dashboard.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        /*Intent intent = new Intent(FAQ.this, Home_dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);*/
    }

}
