package kartku.com.MainCategories.listener;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public interface MainCategoriesListener {

    void onCategoriesSuccess(String response);
    void onCategoriesError(String error);
}
