package kartku.com.MainCategories;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kartku.com.R;
import kartku.com.adapter.FloatingExpandableAdapter;
import kartku.com.dashboard_module.Home_dashboard;
import kartku.com.floatingexpandable.FloatingGroupExpandableListView;
import kartku.com.floatingexpandable.WrapperExpandableListAdapter;
import kartku.com.utils.Constant;
import kartku.com.utils.prefrences.ObscuredSharedPreferences;
import kartku.com.utils.prefrences.Session;

public class CategoriesActivity extends AppCompatActivity {

    FloatingExpandableAdapter adapter;
    List<String> listDataHeader;
    HashMap<String, JSONArray> listDataChild;
    FloatingGroupExpandableListView expListView;
    TextView textview;
    SharedPreferences pref;
    JSONArray sub_cat_info;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch (Exception e)
        {
            e.printStackTrace();
        }


        pref = new ObscuredSharedPreferences(getApplicationContext(),getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        expListView = (FloatingGroupExpandableListView)findViewById(R.id.lvExp);
        textview = (TextView)findViewById(R.id.txt_no_record);

        prepareListData();
        adapter = new FloatingExpandableAdapter(getApplicationContext(),listDataHeader, listDataChild , sub_cat_info);
        WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(adapter);
        expListView.setAdapter(wrapperAdapter);
        /*** code to open all childs by default ***/
        for(int i=0; i < adapter.getGroupCount(); i++){
            expListView.expandGroup(i);
        }

    }

    /*** code to open first child by default ***/
    //expListView.expandGroup(0);


    private void prepareListData() {
        try {
            listDataHeader = new ArrayList<String>();
            listDataChild = new HashMap<String, JSONArray>();

            Intent intent = getIntent();
            int index = intent.getIntExtra(Constant.SELECTED_CATEGORY_INDEX,0);

            JSONObject res = new JSONObject(Session.get_main_categories_res(pref));
            JSONArray categories_array = res.getJSONArray("categories_info");

            JSONObject selected_array = categories_array.getJSONObject(index);
            Log.d("selected " ," array  " + selected_array);
            System.out.println("categories selected_array *****  " + selected_array);

            String title  = selected_array.getString("title");
            try{
                getSupportActionBar().setTitle(title);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
             sub_cat_info = selected_array.getJSONArray("subcat_info");

            for (int j=0;j<sub_cat_info.length();j++) {
                JSONObject item_object = sub_cat_info.getJSONObject(j);
                String sub_cat_name = item_object.getString("subcat_name");
                String sub_cat_id = item_object.getString("subcat_id");
                JSONArray sub_subcat_info = item_object.getJSONArray("sub_subcat_info");

                listDataHeader.add(sub_cat_name);
                listDataChild.put(sub_cat_name, sub_subcat_info);

                //System.out.println("next ixon view clicked sub_cat_item listDataHeader 111*****  " + listDataHeader);
                //System.out.println("next ixon view clicked sub_cat_item listChildData *1111 ****  " + listDataChild);
            }

        } catch (NullPointerException e) {

            e.printStackTrace();
        }catch (IndexOutOfBoundsException e) {

            e.printStackTrace();
        }catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                try {
                    Intent intent = new Intent(CategoriesActivity.this, Home_dashboard.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
        }
        return (super.onOptionsItemSelected(menuItem));
    }

}


