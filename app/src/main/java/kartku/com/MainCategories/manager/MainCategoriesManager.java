package kartku.com.MainCategories.manager;

import android.content.Context;

import kartku.com.MainCategories.api.MainCategoriesApi;
import kartku.com.MainCategories.listener.MainCategoriesListener;
import kartku.com.dashboard_module.api.HomeBannersApi;
import kartku.com.dashboard_module.listener.HomeBannersListener;
import kartku.com.utils.API;

/**
 * Created by satoti.garg on 9/12/2016.
 */
public class MainCategoriesManager {
    private Context context;
    private MainCategoriesListener listener;

    public MainCategoriesManager(Context context, MainCategoriesListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void sendRequest() {
        try {
            new MainCategoriesApi(MainCategoriesManager.this, context).sendRequest(API.MAIN_CATEGORIES_REQUEST_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onSuccess(String response) {
        listener.onCategoriesSuccess(response);
    }

    public void onError(String message) {

        listener.onCategoriesError(message);
    }
}
